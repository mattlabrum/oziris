<?php

namespace app\models;

use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "brand_translation".
 */
class BrandTranslation extends \app\models\base\BrandTranslation
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'language_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['brand_name'], 'string', 'max' => 255]
        ];
    }


}
