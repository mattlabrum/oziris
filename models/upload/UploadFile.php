<?php

namespace app\models\upload;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadFile extends Model
{
    /**
     * @var UploadedFile
     */
    public $upload_file;

    public function rules()
    {
        return [
            [['upload_file'], 'file', 'skipOnEmpty' => false, 'extensions' => 'jpg , jpeg , JPEG , PNG , png'],];
    }

    public function upload($category,$name)
    {
        if ($this->validate()) {
            return $this->upload_file->saveAs('uploads/'.$category.'/'. $name . '.' . $this->upload_file->extension, true);

        } else {
            return false;
        }
    }
}