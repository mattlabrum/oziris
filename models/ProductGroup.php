<?php

namespace app\models;

use app\models\base\ProductGroupTranslation;
use bedezign\yii2\audit\AuditTrailBehavior;
use bedezign\yii2\audit\models\AuditTrail;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_group".
 */
class ProductGroup extends \app\models\base\ProductGroup
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(\app\models\ProductGroup::className(), ['id' => 'id']);
    }

    /**
     * Gets Group Translation Value to be displayed on product update
     * @return ProductGroupTranslation
     */

    public function getGroupTranslation($language_code)
    {

        if($this->deleted_at == null) {
            $productGroupTranslation = ProductGroupTranslation::find()->where(['product_group_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
            if (isset($productGroupTranslation)) {
                return $productGroupTranslation;
            }
            return $this;
        }
    }

    public function getAuditTrails()
    {
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ]);
    }

    /**
     * @Set Product Type Status
     */
    public function changeStatus()
    {
        if($this->deleted_at == null || $this->deleted_at == ''){
            $this->deleted_at = time();
        }else{
            $this->deleted_at = null;
        }

        return $this->save();
    }


}
