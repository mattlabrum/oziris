<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "collection_item".
 */
class CollectionItem extends \app\models\base\CollectionItem
{

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatchID($order_id)
    {
        $items = BatchItem::find()->joinWith('batch')->where(['batch.product_id' => $this->product_id, 'order_id' => $order_id])->all();
        if(isset($items)){
            $batches = '';
            foreach($items as $item){
                $batches .= $item->batch_id.' , ';
            }
            return $batches;
        }else{
            return '';
        }
    }


}
