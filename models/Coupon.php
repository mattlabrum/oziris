<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "coupons".
 */
class Coupon extends \app\models\base\Coupon
{
    public function behaviors()
    {

        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'date_created',
                'updatedAtAttribute' => 'date_modified',
            ],
            AuditTrailBehavior::className(),
        ];
    }
}
