<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "reported_review".
 */
class ReportedReview extends \app\models\base\ReportedReview
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getapp_user()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Reporting User'),
            'rating_review_id' => Yii::t('app', 'Reported Item'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingReview()
    {
        return $this->hasOne(\app\models\ReviewRating::className(), ['id' => 'rating_review_id']);
    }


}
