<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "scanned_products".
 */
class ScannedProduct extends \app\models\base\ScannedProduct
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['batch_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['user_id'], 'required'],
            [['scanned_longitude', 'scanned_latitude'], 'number'],
            [['batch_number', 'qr_code', 'product_code', 'city'], 'string', 'max' => 255],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'batch_id' => Yii::t('app', 'OZIRIS Batch ID'),
            'user_id' => Yii::t('app', 'User Name'),
            'scanned_longitude' => Yii::t('app', 'Scanned Longitude'),
            'scanned_latitude' => Yii::t('app', 'Scanned Latitude'),
            'city' => Yii::t('app', 'City'),
            'created_at' => Yii::t('app', 'Scan Date/Time'),

        ];
    }
}
