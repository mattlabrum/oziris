<?php

namespace app\models;

use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product".
 */
class TranslationSummary extends \app\models\base\Product
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
        ];
    }

    /**
     * Returns the attribute labels.
     *
     * See Model class for more details
     *
     * @return array attribute labels (name => label).
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_number' => Yii::t('app', 'Product Number'),
            'product_group_id' => Yii::t('app', 'Product Group'),
            'product_name_id' => Yii::t('app', 'Product Name'),
            'product_description_id' => Yii::t('app', 'Product Description'),
            'product_type_id' => Yii::t('app', 'Product Type'),
            'brand_id' => Yii::t('app', 'Brand'),
            'unit_size' => Yii::t('app', 'Unit Size'),
            'unit_size_kg' => Yii::t('app', 'Unit Size Kg'),
            'shelf_life' => Yii::t('app', 'Shelf Life'),
            'product_image' => Yii::t('app', 'Product Image'),
            'nutrition_image' => Yii::t('app', 'Nutrition Image'),
            'nutrition_text' => Yii::t('app', 'Nutrition Text'),
            'hash_tag_id' => Yii::t('app', 'Hash Tag'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer'),
        ];
    }
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_number', 'product_group_id', 'product_name_id', 'product_description_id', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id'],
            'create' => ['id', 'product_number', 'product_group_id', 'product_name_id', 'product_description_id', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id'],
            'update' => ['id', 'product_number', 'product_group_id', 'product_name_id', 'product_description_id', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id'],
            'duplicate' => ['id', 'product_number', 'product_group_id', 'product_name_id', 'product_description_id', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_number', 'product_group_id', 'product_name_id', 'product_description_id', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'nutrition_text', 'manufacturer_id'], 'required'],
            [['product_group_id', 'product_name_id', 'product_description_id', 'product_type_id', 'brand_id', 'manufacturer_id'], 'safe'],
            [['product_number', 'unit_size', 'unit_size_kg', 'shelf_life', 'product_image', 'nutrition_text'], 'string', 'max' => 255],
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'id']);
    }
}
