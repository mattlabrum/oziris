<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Response extends Model
{
    private $_message;
    private $_return_response;


    public function setMessage($message)
    {
        $this->_message = $message;
    }

    public function setResponse($return_response)
    {
        $this->_return_response = $return_response;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function getReturnResponse()
    {
        return $this->_return_response;
    }

    public function getFullResponse()
    {
        return [
            'message' => $this->getMessage(),
            'response' => $this->getReturnResponse(),
        ];
    }

}