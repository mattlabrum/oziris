<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use bedezign\yii2\audit\models\AuditTrail;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "manufacturer".
 */
class Manufacturer extends \app\models\base\Manufacturer
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'location'],
            'create' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'location'],
            'update' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'location'],
            'duplicate' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'location'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_description'], 'required'],
            [['manufacturer_location_latitude', 'manufacturer_location_longitude'], 'double'],
            [['manufacturer_name', 'manufacturer_logo', 'manufacturer_video', 'location'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'OZIRIS ID'),
            'manufacturer_name' => Yii::t('app', 'Manufacturer Name'),
            'manufacturer_location_latitude' => Yii::t('app', 'Manufacturer Location Latitude'),
            'manufacturer_location_longitude' => Yii::t('app', 'Manufacturer Location Longitude'),
            'manufacturer_logo' => Yii::t('app', 'Manufacturer Logo'),
            'manufacturer_video' => Yii::t('app', 'Manufacturer Video'),
            'manufacturer_description' => Yii::t('app', 'Manufacturer Description'),
            'location' => Yii::t('app', 'Location'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(\app\models\Manufacturer::className(), ['id' => 'id']);
    }

    public function getManufacturerLogo()
    {
        if(strrpos( $this->manufacturer_logo, "/" , 0 )){
            $this->manufacturer_logo = substr($this->manufacturer_logo, (strrpos( $this->manufacturer_logo, "/" , 0 )+1));
        }
        return $this->manufacturer_logo = 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/manufacturer'). '/' .$this->manufacturer_logo;
    }

    /**
     * Gets Manufacturer Translation Value to be displayed on product update
     * @return Manufacturer
     */

    public function getManufacturerTranslation($language_code)
    {



        if($this->deleted_at != null){
            return false;
        }else{
            $manufacturerTranslation = ManufacturerTranslation::find()->where(['manufacturer_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
            if(isset($manufacturerTranslation)){
                return $manufacturerTranslation;
            }
            $this->getManufacturerLogo();
            return $this;
        }
    }

    public function getAllManufacturerTranslation($language_code)
    {
        $manufacturerTranslation = ManufacturerTranslation::find()->where(['manufacturer_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
        if(isset($manufacturerTranslation)){
            $this->manufacturer_name = $manufacturerTranslation->manufacturer_name;
            $this->manufacturer_description = $manufacturerTranslation->manufacturer_description;
            $this->getManufacturerLogo();
        }
        return $this;
    }

    public function getAuditTrails()
    {
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ]);
    }

    /**
     * @Set Manufacturer Status
     */
    public function changeStatus()
    {
        if($this->deleted_at == null || $this->deleted_at == ''){
            $this->deleted_at = time();
        }else{
            $this->deleted_at = null;
        }

        return $this->save();
    }

}
