<?php

namespace app\models\form;

use app\models\Manufacturer;
use app\models\ManufacturerTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class ManufacturerForm extends Model
{
    private $_manufacturer;
    private $_manufacturertranslation;


    public function getManufacturer()
    {
        if(!isset($this->_manufacturer)){
            $this->_manufacturer = new Manufacturer();
        }
        return $this->_manufacturer;
    }

    public function setManufacturer($manufacturer)
    {
        if ($manufacturer instanceof ActiveRecord) {
            $this->_manufacturer = $manufacturer;
        }
    }

    public function getManufacturerTranslation()
    {
        if(!isset($this->_manufacturer->id)){
            $this->_manufacturertranslation = new ManufacturerTranslation();
        }else{
            $this->_manufacturertranslation = ManufacturerTranslation::find()->andWhere(['manufacturer_id' => $this->_manufacturer->id])->one();
        }
        if (!isset($this->_manufacturertranslation) ) {
            $this->_manufacturertranslation = new ManufacturerTranslation();
        }
        return $this->_manufacturertranslation;
    }

    public function setManufacturerTranslation($manufacturertranslation)
    {
        if ($this->_manufacturertranslation instanceof ActiveRecord) {
            $this->_manufacturertranslation = $manufacturertranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getManufacturer(),
            $this->getManufacturerTranslation(),
        ];
    }

}