<?php

namespace app\models\form;

use app\models\ProductType;
use app\models\ProductTypeTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class ProductTypeForm extends Model
{
    private $_producttype;
    private $_producttypetranslation;


    public function getProductType()
    {
        if(!isset($this->_producttype)){
            $this->_producttype = new ProductType();
        }
        return $this->_producttype;
    }

    public function setProductType($producttype)
    {
        if ($producttype instanceof ActiveRecord) {
            $this->_producttype = $producttype;
        }
    }

    public function getProductTypeTranslation()
    {
        if(!isset($this->_producttype->id)){
            $this->_producttypetranslation = new ProductTypeTranslation();
        }else{
            $this->_producttypetranslation = ProductTypeTranslation::find()->andWhere(['product_type_id' => $this->_producttype->id])->one();
        }
        if (!isset($this->_producttypetranslation) ) {
            $this->_producttypetranslation = new ProductTypeTranslation();
        }
        return $this->_producttypetranslation;
    }

    public function setProductTypeTranslation($producttypetranslation)
    {
        if ($this->_producttypetranslation instanceof ActiveRecord) {
            $this->_producttypetranslation = $producttypetranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getProductType(),
            $this->getProductTypeTranslation(),
        ];
    }

}