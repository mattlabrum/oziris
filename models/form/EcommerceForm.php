<?php

namespace app\models\form;

use app\models\Supplier;
use app\models\SupplierTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class EcommerceForm extends Model
{
    private $_sales;
    private $_orders;
    private $_customers;
    private $_taxes;
    private $_productSearchModel;
    private $_productDataProvider;

    public function getSales()
    {
        return $this->_sales;
    }
    public function setSales($sales)
    {
        $this->_sales = $sales;
    }
    public function getOrders()
    {
        return $this->_sales;
    }
    public function setOrders($orders)
    {
        $this->_orders = $orders;
    }
    public function getCustomers()
    {
        return $this->_customers;
    }
    public function setCustomers($customers)
    {
        $this->_customers = $customers;
    }
    public function getTaxes()
    {
        return $this->_taxes;
    }
    public function setTaxes($taxes)
    {
        $this->_taxes = $taxes;
    }
    public function getProductSearchModel()
    {
        return $this->_productSearchModel;
    }
    public function setProductSearchModel($productSearchModel)
    {
        $this->_productSearchModel = $productSearchModel;
    }
    public function getProductDataProvider()
    {
        return $this->_productDataProvider;
    }
    public function setProductDataProvider($productDataProvider)
    {
        $this->_productDataProvider = $productDataProvider;
    }
}