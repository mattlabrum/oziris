<?php

namespace app\models\form;

use app\models\NutritionTag;
use app\models\NutritionTagTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class NutritionTagForm extends Model
{
    private $_nutritiontag;
    private $_nutritiontagtranslation;


    public function getNutritionTag()
    {
        if(!isset($this->_nutritiontag)){
            $this->_nutritiontag = new NutritionTag();
        }
        return $this->_nutritiontag;
    }

    public function setNutritionTag($nutritiontag)
    {
        if ($nutritiontag instanceof ActiveRecord) {
            $this->_nutritiontag = $nutritiontag;
        }
    }

    public function getNutritionTagTranslation()
    {
        if(!isset($this->_nutritiontag)){
            $this->_nutritiontagtranslation = new NutritionTagTranslation();
        }else{
            $this->_nutritiontagtranslation = NutritionTagTranslation::find()->andWhere(['nutrition_tag_id' => $this->_nutritiontag->id])->one();
        }
        if (!isset($this->_nutritiontagtranslation) ) {
            $this->_nutritiontagtranslation = new NutritionTagTranslation();
        }
        return $this->_nutritiontagtranslation;
    }

    public function setNutritionTagTranslation($nutritiontagtranslation)
    {
        if ($this->_nutritiontagtranslation instanceof ActiveRecord) {
            $this->_nutritiontagtranslation = $nutritiontagtranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getNutritionTag(),
            $this->getNutritionTagTranslation(),
        ];
    }

}