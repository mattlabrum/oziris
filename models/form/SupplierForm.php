<?php

namespace app\models\form;

use app\models\Supplier;
use app\models\SupplierTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class SupplierForm extends Model
{
    private $_supplier;
    private $_suppliertranslation;


    public function getSupplier()
    {
        if(!isset($this->_supplier)){
            $this->_supplier = new Supplier();
        }
        return $this->_supplier;
    }

    public function setSupplier($supplier)
    {
        if ($supplier instanceof ActiveRecord) {
            $this->_supplier = $supplier;
        }
    }

    public function getSupplierTranslation()
    {
        if(!isset($this->_supplier)){
            $this->_suppliertranslation = new SupplierTranslation();
        }else{
            $this->_suppliertranslation = SupplierTranslation::find()->andWhere(['supplier_id' => $this->_supplier->id])->one();
        }
        if (!isset($this->_suppliertranslation) ) {
            $this->_suppliertranslation = new SupplierTranslation();
        }
        return $this->_suppliertranslation;
    }

    public function setSupplierTranslation($suppliertranslation)
    {
        if ($this->_suppliertranslation instanceof ActiveRecord) {
            $this->_suppliertranslation = $suppliertranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getSupplier(),
            $this->getSupplierTranslation(),
        ];
    }

}