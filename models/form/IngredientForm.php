<?php

namespace app\models\form;

use app\models\Ingredient;
use app\models\IngredientTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class IngredientForm extends Model
{
    private $_ingredient;
    private $_ingredienttranslation;


    public function getIngredient()
    {
        if(!isset($this->_ingredient)){
            $this->_ingredient = new Ingredient();
        }
        return $this->_ingredient;
    }

    public function setIngredient($ingredient)
    {
        if ($ingredient instanceof ActiveRecord) {
            $this->_ingredient = $ingredient;
        }
    }

    public function getIngredientTranslation()
    {
        if(!isset($this->_ingredient)){
            $this->_ingredienttranslation = new IngredientTranslation();
        }else{
            $this->_ingredienttranslation = IngredientTranslation::find()->andWhere(['ingredient_id' => $this->_ingredient->id])->one();
        }
        if (!isset($this->_ingredienttranslation) ) {
            $this->_ingredienttranslation = new IngredientTranslation();
        }
        return $this->_ingredienttranslation;
    }

    public function setIngredientTranslation($ingredienttranslation)
    {
        if ($this->_ingredienttranslation instanceof ActiveRecord) {
            $this->_ingredienttranslation = $ingredienttranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getIngredient(),
            $this->getIngredientTranslation(),
        ];
    }

}