<?php

namespace app\models\form;

use app\models\ProductGroup;
use app\models\ProductGroupTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class ProductGroupForm extends Model
{
    private $_productgroup;
    private $_productgrouptranslation;


    public function getProductGroup()
    {
        if(!isset($this->_productgroup)){
            $this->_productgroup = new ProductGroup();
        }
        return $this->_productgroup;
    }

    public function setProductGroup($productgroup)
    {
        if ($productgroup instanceof ActiveRecord) {
            $this->_productgroup = $productgroup;
        }
    }

    public function getProductGroupTranslation()
    {
        if(!isset($this->_productgroup->id)){
            $this->_productgrouptranslation = new ProductGroupTranslation();
        }else{
            $this->_productgrouptranslation = ProductGroupTranslation::find()->andWhere(['product_group_id' => $this->_productgroup->id])->one();
        }
        if (!isset($this->_productgrouptranslation) ) {
            $this->_productgrouptranslation = new ProductGroupTranslation();
        }
        return $this->_productgrouptranslation;
    }

    public function setProductGroupTranslation($productgrouptranslation)
    {
        if ($this->_productgrouptranslation instanceof ActiveRecord) {
            $this->_productgrouptranslation = $productgrouptranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getProductGroup(),
            $this->getProductGroupTranslation(),
        ];
    }

}