<?php

namespace app\models\form;

use app\models\Nutrition;
use app\models\NutritionTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class NutritionForm extends Model
{
    private $_nutrition;
    private $_nutritiontranslation;


    public function getNutrition()
    {
        if(!isset($this->_nutrition)){
            $this->_nutrition = new Nutrition();
        }
        return $this->_nutrition;
    }

    public function setNutrition($nutrition)
    {
        if ($nutrition instanceof ActiveRecord) {
            $this->_nutrition = $nutrition;
        }
    }

    public function getNutritionTranslation()
    {
        if(!isset($this->_nutrition->id)){
            $this->_nutritiontranslation = new NutritionTranslation();
        }else{
            $this->_nutritiontranslation = NutritionTranslation::find()->andWhere(['nutrition_id' => $this->nutrition->id])->one();
        }
        if (!isset($this->_nutritiontranslation) ) {
            $this->_nutritiontranslation = new NutritionTranslation();
        }
        return $this->_nutritiontranslation;
    }

    public function setNutritionTranslation($nutritiontranslation)
    {
        if ($this->_nutritiontranslation instanceof ActiveRecord) {
            $this->_nutritiontranslation = $nutritiontranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getNutrition(),
            $this->getNutritionTranslation(),
        ];
    }

}