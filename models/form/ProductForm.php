<?php

namespace app\models\form;

use app\models\Collection;
use app\models\CollectionItem;
use app\models\Product;
use app\models\ProductLocationAttribute;
use app\models\ProductPrice;
use app\models\ProductTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class ProductForm extends Model
{
    private $_product;
    private $_producttranslation;
    private $_collection;
    private $_product_location_attributes;


    public function getProduct()
    {
        if(!isset($this->_product)){
            $this->_product = new Product();
        }
        return $this->_product;
    }
    public function getCollection()
    {
            if(!isset($this->_collection)){
                $this->_collection = new CollectionItem();
            }
            return $this->_collection;
    }

    public function getProductTranslation()
    {
        if(!isset($this->_product->id)){
            $this->_producttranslation = new ProductTranslation();
        }else{
            $this->_producttranslation = ProductTranslation::find()->andWhere(['product_id' => $this->_product->id])->one();
        }
        if (!isset($this->_producttranslation) ) {
            $this->_producttranslation = new ProductTranslation();
        }
        return $this->_producttranslation;
    }

    public function getProductLocationAttributes()
    {
        if(!isset($this->_product_location_attributes)){
            $this->_product_location_attributes = new ProductLocationAttribute();
        }
        return $this->_product_location_attributes;
    }

    public function getAllModels()
    {
        return [
            $this->getProduct(),
            $this->getProductTranslation(),
            $this->getProductLocationAttributes(),
        ];
    }


    public function setCollection($collection)
    {
        if ($this->_collection instanceof ActiveRecord) {
            $this->_collection = $collection;
        }

    }

    public function setProduct($product)
    {
        if ($product instanceof ActiveRecord) {
            $this->_product = $product;
        }
    }

    public function setProductLocationAttributes($productLocationAttributes)
    {
        if ($productLocationAttributes instanceof ActiveRecord) {
            $this->_product_location_attributes = $productLocationAttributes;
        }
    }

    public function setProductTranslation($producttranslation)
    {
        if ($this->_producttranslation instanceof ActiveRecord) {
            $this->_producttranslation = $producttranslation;
        }

    }

}