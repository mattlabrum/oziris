<?php

namespace app\models\form;

use app\models\Brand;
use app\models\BrandTranslation;
use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;

class BrandForm extends Model
{
    private $_brand;
    private $_brandtranslation;


    public function getBrand()
    {
        if(!isset($this->_brand)){
            $this->_brand = new Brand();
        }
        return $this->_brand;
    }

    public function setBrand($brand)
    {
        if ($brand instanceof ActiveRecord) {
            $this->_brand = $brand;
        }
    }

    public function getBrandTranslation()
    {
        if(!isset($this->_brand)){
            $this->_brandtranslation = new BrandTranslation();
        }else{
            $this->_brandtranslation = BrandTranslation::find()->andWhere(['brand_id' => $this->_brand->id])->one();
        }
        if (!isset($this->_brandtranslation) ) {
            $this->_brandtranslation = new BrandTranslation();
        }
        return $this->_brandtranslation;
    }

    public function setBrandTranslation($brandtranslation)
    {
        if ($this->_brandtranslation instanceof ActiveRecord) {
            $this->_brandtranslation = $brandtranslation;
        }

    }

    public function getAllModels()
    {
        return [
            $this->getBrand(),
            $this->getBrandTranslation(),
        ];
    }

}