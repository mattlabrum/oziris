<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_translation".
 */
class ProductTranslation extends \app\models\base\ProductTranslation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'language_id', 'product_name', 'product_description'], 'required'],
            [['nutrition_text', 'nutrition_image', 'product_description'], 'string'],
            [['product_id', 'language_id'], 'integer'],
            [['product_name'], 'string', 'max' => 255]
        ];
    }
}
