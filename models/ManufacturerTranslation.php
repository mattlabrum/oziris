<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "manufacturer_translation".
 */
class ManufacturerTranslation extends \app\models\base\ManufacturerTranslation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'manufacturer_name', 'manufacturer_description'], 'required'],
            [['language_id', 'manufacturer_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['manufacturer_name'], 'string', 'max' => 255]
        ];
    }
}
