<?php

namespace app\models;

use app\widgets\xj\QRcode;
use app\widgets\xj\Text;
use bedezign\yii2\audit\AuditTrailBehavior;
use bedezign\yii2\audit\models\AuditTrail;
use cornernote\softdelete\SoftDeleteBehavior;
use Imagick;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "batch".
 */
class Batch extends \app\models\base\Batch
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }

    public function scenarios()
    {
        return [
            'default' => ['id', 'manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'batchItems',  'product_id', 'batch_number', 'ingredients' , 'quantity' , 'physicalQuantity', 'created_at', 'updated_at', 'deleted_at', 'overall_coo_percentage' , 'label_type_text' , 'cool_image'],
            'create' => ['id', 'manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'batchItems', 'product_id', 'batch_number', 'ingredients', 'quantity' , 'physicalQuantity', 'created_at', 'updated_at', 'deleted_at', 'overall_coo_percentage' , 'label_type_text' , 'cool_image'],
            'update' => ['id', 'manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'batchItems', 'product_id', 'batch_number', 'ingredients', 'quantity' , 'physicalQuantity', 'created_at', 'updated_at', 'deleted_at', 'overall_coo_percentage' , 'label_type_text' , 'cool_image'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacture_date', 'qr_code', 'product_id', 'batch_number'], 'required'],
            [['product_id', 'created_at', 'quantity' , 'physicalQuantity', 'updated_at', 'deleted_at', 'label_type_text'], 'integer'],
            [['manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'batch_number', 'cool_image' , 'expiry_date'], 'string', 'max' => 255],
            [['overall_coo_percentage'], 'double']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'manufacture_date' => Yii::t('app', 'Manufacture Date'),
            'recalled_date' => Yii::t('app', 'Recalled Date'),
            'qr_code' => Yii::t('app', 'Qr Code'),
            'qr_code_image' => Yii::t('app', 'Qr Code Image'),
            'product_id' => Yii::t('app', 'Product ID'),
            'batch_number' => Yii::t('app', 'Batch Number'),
            'quantity' => Yii::t('app', 'Batch Quantity'),
            'physicalQuantity' => Yii::t('app', 'Actual Units Available'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'label_type_text' => Yii::t('app', ''),
            'expiry_date' => Yii::t('app', 'Expiry Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatchItems()
    {
        $items = $this->hasMany(\app\models\BatchItem::className(), ['batch_id' => 'id'])->asArray()->all();
//        $items_to_return = [];
//        foreach($items as $item){
//            array_push($items_to_return,$item->id);
//        }
        return $items;
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhysicalQuantity()
    {

        return BatchItem::find()->where(['batch_id' => $this->id, 'item_status' => '' ])->count('*');
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(\app\models\Batch::className(), ['id' => 'id']);
    }

    public function generateQrCode(){

        $qr_text = 'www.oziris.com.au/home?batch_id=';
        $this->qr_code = $qr_text.$this->id;
        $filename = Text::widget([
            'outputDir' => '@webroot/upload/qrcode',
            'outputDirWeb' => '@web/upload/qrcode',
            'ecLevel' => QRcode::QR_ECLEVEL_H,
            'text' => $this->qr_code,
            'batch_id' => $this->id,
            'date' => date('d-m-Y'),//'07-01-2016',
            'size' => 14,
        ]);

        $final_filename = date('d-m-Y').'/'.$this->id.'.png';

      if($this->prettify('https://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/qrcode').'/'.$filename, 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/').'logo.png', 'upload/qrcode/'.$final_filename)){
          echo unlink('upload/qrcode/'.$filename);
          return $final_filename;
      }


    }


    function prettify($file, $logo_file, $output = '') {
        $qr = false;
        $geometry = false;

        /* Load image */
        $qr = new Imagick();
        $qr -> readImage($file);
        $geometry =   $qr -> getImageGeometry();

        if(!$logo_file) {
            /* No logo to add */
            return false;
        }
        $logo = new Imagick();
        $logo -> readImage($logo_file);
        $logo_size = $logo -> getImageGeometry();
        $x = (  $geometry['width'] - $logo_size['width']) / 2;
        $y = (  $geometry['height'] - $logo_size['height']) / 2;
        $qr -> compositeImage($logo, imagick::COMPOSITE_OVER, $x, $y);

        /* Output image */
        if($output != '') {
            $qr -> setImageFileName($output);
        }
        return $qr -> writeImage();
    }

    public function getTranslatedData($language_code)
    {
        if(isset($this->product)){
            $translated_product = $this->product->getTranslatedData($language_code);
            if($translated_product != false){
                return $batch = [
                    "id" => $this->id,
                    "manufacture_date" => $this->manufacture_date,
                    "recalled_date" => $this->recalled_date,
                    "qr_code_image" => ($this->qr_code_image == null ? $this->qr_code_image = '' : 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/qrcode').'/'.$this->qr_code_image),
                    "qr_code" => $this->qr_code,
                    "product" => $translated_product,
                    "batch_number" => $this->batch_number,
                    "cool" => $this->getCoolImageUrl(),
                    'received_at_fulfilment_center' => ($this->batch_number == $this->product_id ? '' : date('d/m/Y h:i A ',$this->created_at)  ),
                    "ingredients" => $this->getIngredients(),
                    "order_trace" => [],
                ];
            }else {
                return false;
            }
        }else{
            return false;
        }

    }

    /**
     * @return BatchIngredient[]
     */
    public function getIngredients()
    {
        $ingredients = BatchIngredient::find()->where(['batch_id'=>$this->id])->all();
        $batchIngredients = [];
        foreach($ingredients as $ingredient){
            $coo = [];
            if(isset($ingredient->coo)){
                $coo = [
                    'CooName' => $ingredient->coo->city.' ,'.$ingredient->coo->country,
                    'CooLatitude' => $ingredient->coo->coo_latitude,
                    'CooLongitude' => $ingredient->coo->coo_longitude,
                    'CooFlag' => ($ingredient->coo->flag == null ? $ingredient->coo->flag = '' : 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/coo').'/'.$ingredient->coo->flag),
                ];
            }
            $batchIngredients[] = [
                'Ingredient' => $ingredient->ingredient->name,
                'IngredientGroup' => $ingredient->ingredient->group->name,
                'Percentage' => $ingredient->percentage,
                'ArrivalAtManufacturer' => $ingredient->arrival_at_manufacturer_date,
                'Coo' => $coo,
                'Supplier' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_name : ''),
                'Latitude' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_location_latitude : ''),
                'Longitude' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_location_longitude : ''),
                'SupplierDetails' => [
                                'SupplierName' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_name : ''),
                                'SupplierLatitude' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_location_latitude : ''),
                                'SupplierLongitude' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_location_longitude : ''),
                                'SupplierLogo' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_logo : ''),
                                'SupplierDescription' => (isset($ingredient->supplier)  ? $ingredient->supplier->supplier_description : ''),
                            ],
                'ManufactureDate' => $ingredient->manufacture_date,
                'ShowSupplierOnOziris' => $ingredient->show_on_oziris,
            ];
        }
        if(count($ingredients)==0){
            $productIngredients = ProductIngredient::find()->where(['product_id'=>$this->product_id])->all();
            foreach($productIngredients as $productIngredient){
                $batchIngredients[] = [
                    'Ingredient' => $productIngredient->ingredient->name,
                    'IngredientGroup' => $productIngredient->ingredient->group->name,
                    'Percentage' => $productIngredient->percentage
                ];
            }
        }
        return $batchIngredients;
    }

    public function getAuditTrails()
    {
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ]);
    }


    /**
     * @return integer $quantity
     */
    public function getProductQuantity()
    {
        $quantity = 0;
        if($this->product->is_collection){
            $quantity = 0;
        }else{
                $quantity = BatchItem::find()->where(['batch_id' => $this->id, 'item_status' => '' ])->count('*');
        }
        return $quantity;
    }

    /**
     * @return integer $sold quantity
     */
    public function getProductSoldQuantity()
    {
        $quantity = BatchItem::find()->select('item_status')->where(['batch_id' => $this->id, 'item_status' => 'sold'])->count('*');

        return $quantity;
    }

    /**
     * @return integer $sold quantity
     */
    public function getProductDiscardQuantity()
    {

        $quantity = BatchItem::find()->select('item_status')->where(['batch_id' => $this->id, 'item_status' => 'discard'])->count('*');

        return $quantity;
    }

    /**
     * @return integer $sold quantity
     */
    public function getTheoraticalQuantity()
    {
        return  $this->getProductBatchLevelQuantity() - ($this->getProductDiscardQuantity() + $this->getProductSoldQuantity());
    }

    /**
     * @return integer $sold quantity
     */
    public function getProductVariance()
    {
        return $this->getProductQuantity() - $this->getTheoraticalQuantity();
    }

    /**
     * @return integer $theoQuantity
     */
    public function getProductBatchLevelQuantity()
    {
        return $this->quantity;
    }



}
