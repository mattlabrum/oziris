<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "app_user".
 */
class AppUser extends \app\models\base\AppUser
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email_address' => Yii::t('app', 'Email Address'),
            'mobile' => Yii::t('app', 'Mobile'),
            'country' => Yii::t('app', 'Country'),
            'is_active' => Yii::t('app', 'Is Active'),
            'gender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Date Of Birth'),
            'created_at' => Yii::t('app', 'Joined On'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['email_address'], 'required'],

            [['first_name', 'last_name', 'email_address'], 'string', 'max' => 255],
            [['dob', 'country'], 'string', 'max' => 25],
            [['gender'], 'string', 'max' => 12],
            [['mobile'], 'string', 'max' => 15]
        ];
    }
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }
    /**
     * @Get User Full Name
     */
    public function getUserName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * @Set User Status
     */
    public function changeUserStatus()
    {
        if($this->deleted_at == null || $this->deleted_at == ''){
            $this->deleted_at = time();
        }else{
            $this->deleted_at = null;
        }
        return $this->save();
    }

}
