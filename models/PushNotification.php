<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "push_notification".
 */
class PushNotification extends \app\models\base\PushNotification
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message'], 'required'],
            [['created_at', 'updated_at', 'for_android', 'for_ios'], 'integer'],
            [['message'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'message' => Yii::t('app', 'Message'),
            'created_at' => Yii::t('app', 'Sent At'),
            'for_ios' => Yii::t('app', 'For Apple'),
            'for_android' => Yii::t('app', 'For Android'),
        ];
    }

}
