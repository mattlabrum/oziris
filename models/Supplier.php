<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "supplier".
 */
class Supplier extends \app\models\base\Supplier
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }
}
