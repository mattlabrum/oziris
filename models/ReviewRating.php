<?php

namespace app\models;

use app\models\base\ReviewType;
use bedezign\yii2\audit\AuditTrailBehavior;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "review_rating".
 */
class ReviewRating extends \app\models\base\ReviewRating
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(\app\models\ReviewRating::className(), ['id' => 'id']);
    }

    public function getUserName()
    {
        return $this->user->first_name ." ".$this->user->last_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getapp_user()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }


}
