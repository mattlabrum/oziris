<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use bedezign\yii2\audit\models\AuditTrail;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "product_type".
 */
class ProductType extends \app\models\base\ProductType
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['icon', 'type_name'], 'string', 'max' => 255]
        ];
    }

    public function getId()
    {
        return $this->hasOne(\app\models\ProductType::className(), ['id' => 'id']);
    }

    public function getProductTypeTranslation($language_code)
    {
        $productTypeTranslation = ProductTypeTranslation::find()->where(['product_type_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
        if (isset($productTypeTranslation)) {
            return $productTypeTranslation;
        }else{
            return $this->getProductType();
        }

    }
    public function getProductType()
    {


        $productType = ProductType::find()->select('id , icon , type_name')->where(['id' => $this->id])->one();
//        return $productType;

        if($productType->deleted_at != null){
            return false;
        }else{
            if(strrpos( $productType->icon, "/" , 0 )){
                $productType->icon = substr($productType->icon, (strrpos( $productType->icon, "/" , 0 )+1));
            }
            $productType->icon = $this->getTypeIconUrl();
            return $productType;
        }

    }

    /**
     * @return string $imageUrl
     */
    public function getTypeIconUrl()
    {
        return 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/type') . '/' . $this->icon;
    }

    public function getAuditTrails()
    {
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ]);
    }

    /**
     * @Set Product Type Status
     */
    public function changeStatus()
    {
        if($this->deleted_at == null || $this->deleted_at == ''){
            $this->deleted_at = time();
        }else{
            $this->deleted_at = null;
        }

        return $this->save();
    }

}
