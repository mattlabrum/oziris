<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use Yii;

/**
 * This is the model class for table "product_location_attributes".
 */
class ProductLocationAttribute extends \app\models\base\ProductLocationAttribute
{
    public function behaviors()
    {
        return [
            AuditTrailBehavior::className(),
        ];
    }

}
