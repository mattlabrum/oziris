<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use bedezign\yii2\audit\models\AuditTrail;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "brand".
 */
class Brand extends \app\models\base\Brand
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            SoftDeleteBehavior::className(),
            AuditTrailBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_description','manufacturer_id'], 'required'],
            [['brand_location_latitude', 'brand_location_longitude'], 'double'],
            [['brand_name', 'brand_logo', 'brand_video', 'brand_description'], 'string'],
            [['brand_name', 'brand_logo', 'brand_video'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'OZIRIS ID'),
            'brand_name' => Yii::t('app', 'Brand Name'),
            'brand_location_latitude' => Yii::t('app', 'Brand Location Latitude'),
            'brand_location_longitude' => Yii::t('app', 'Brand Location Longitude'),
            'brand_logo' => Yii::t('app', 'Brand Logo'),
            'brand_video' => Yii::t('app', 'Brand Video'),
            'brand_description' => Yii::t('app', 'Brand Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer Name'),
        ];
    }

    public function getBrandLogo()
    {
        if(strrpos( $this->brand_logo, "/" , 0 )){
            $this->brand_logo = substr($this->brand_logo, (strrpos( $this->brand_logo, "/" , 0 )+1));
        }
        return $this->brand_logo = 'https://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/brand'). '/' .$this->brand_logo;
    }


    public function getId()
    {
        return $this->hasOne(\app\models\Brand::className(), ['id' => 'id']);
    }

    /**
     * Gets Brand Translation Value to be displayed on product update
     * @return BrandTranslation || Brand
     */

    public function getBrandTranslation($language_code)
    {
        if ($this->deleted_at != null) {
            return false;
        } else {
            $this->getBrandLogo();
            $brandTranslation = BrandTranslation::find()->where(['brand_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
            if (isset($brandTranslation)) {
                return $brandTranslation;
            }
            return $this;
        }
    }

    public function getAllBrandTranslation($language_code)
    {
        $brandTranslation = BrandTranslation::find()->where(['brand_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
        if (isset($brandTranslation)) {
            $this->brand_name = $brandTranslation->brand_name;
            $this->brand_description = $brandTranslation->brand_description;
        }
    }

    public function getAuditTrails()
    {
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ]);
    }

    /**
     * @Set Brand Status
     */
    public function changeStatus()
    {
        if($this->deleted_at == null || $this->deleted_at == ''){
            $this->deleted_at = time();
        }else{
            $this->deleted_at = null;
        }

        return $this->save();
    }


}
