<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "hash_tags".
 */
class HashTag extends \app\models\base\HashTag
{
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
