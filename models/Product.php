<?php

namespace app\models;

use bedezign\yii2\audit\AuditTrailBehavior;
use bedezign\yii2\audit\models\AuditTrail;
use cornernote\softdelete\SoftDeleteBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "product".
 *
 */

class Product extends \app\models\base\Product
{

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            AuditTrailBehavior::className(),
            SoftDeleteBehavior::className(),
        ];
    }

    /**
     * Returns the attribute labels.
     *
     * See Model class for more details
     *
     * @return array attribute labels (name => label).
     */
    public function attributeLabels()
    {
        return [

            'is_collection' => Yii::t('app', 'Is Collection'),
            'ecommerce_enabled' => Yii::t('app', 'E-Commerce Enabled'),

            'id' => Yii::t('app', 'Oziris ID'),
            'product_number' => Yii::t('app', 'Product Number'),
            'barcode' => Yii::t('app', 'Barcode'),
            'product_group_id' => Yii::t('app', 'Product Group'),
            'product_name' => Yii::t('app', 'Product Name'),
            'product_description' => Yii::t('app', 'Product Description'),
            'product_type_id' => Yii::t('app', 'Product Type'),

            'brand_id' => Yii::t('app', 'Brand Name'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer Name'),
            'service_type_id' => Yii::t('app', 'Service Type'),

            'unit_size' => Yii::t('app', 'Unit Size'),
            'unit_size_kg' => Yii::t('app', 'Unit Size Kg'),
            'unit_per_box' => Yii::t('app', 'Unit Per Box'),

            'shelf_life' => Yii::t('app', 'Shelf Life'),
            'shelf_life_unit' => Yii::t('app', 'Shelf Life Unit'),
            'product_nature_id' => Yii::t('app', 'Product Nature'),

            'sample_availability' => Yii::t('app', 'Sample Availability'),
            'annual_supply' => Yii::t('app', 'Annual Supply'),
            'minimum_order' => Yii::t('app', 'Minimum Order'),
            'available_since' => Yii::t('app', 'Available Since'),

            'product_image' => Yii::t('app', 'Product Image'),
            'nutrition_image' => Yii::t('app', 'Nutrition Image'),
            'nutrition_text' => Yii::t('app', 'Nutrition Text'),

            'is_beston_pure_product' => Yii::t('app', 'Is this a Beston Pure Product ?'),
            'is_top_seller' => Yii::t('app', 'Is this a Top Seller Product ?'),
            'is_featured' => Yii::t('app', 'Is this a Featured Product ?'),
            'preorder_enabled' => Yii::t('app', 'Is this Product Pre-Order Enabled ?'),

            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Last Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }



    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->logistics = serialize($this->logistics);
            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {

        parent::afterFind();

        $this->logistics = unserialize($this->logistics);

    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_number', 'barcode','is_top_seller', 'is_featured',  'preorder_enabled', 'product_group_id', 'product_name', 'productQuantity', 'product_description', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection',  'created_at', 'updated_at', 'deleted_at', 'is_beston_pure_product', 'reorder_notification', 'show_batch', 'logistics' ],
            'create' => ['id', 'product_number', 'barcode','is_top_seller', 'is_featured',  'preorder_enabled', 'product_group_id', 'product_name', 'productQuantity', 'product_description', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection', 'created_at', 'updated_at', 'deleted_at', 'is_beston_pure_product', 'reorder_notification', 'show_batch', 'logistics'  ],
            'update' => ['id', 'product_number', 'barcode','is_top_seller', 'is_featured',  'preorder_enabled',  'product_group_id', 'product_name', 'productQuantity', 'product_description', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection', 'created_at', 'updated_at', 'deleted_at', 'is_beston_pure_product', 'reorder_notification', 'show_batch', 'logistics'  ],
            'duplicate' => ['id', 'product_number', 'barcode','is_top_seller', 'is_featured',  'preorder_enabled', 'product_group_id', 'product_name', 'productQuantity', 'product_description', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection', 'created_at', 'updated_at', 'deleted_at', 'is_beston_pure_product', 'reorder_notification', 'show_batch', 'logistics'  ],
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_number', 'product_group_id', 'product_name', 'product_description', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'manufacturer_id'], 'required'],
            [['product_group_id', 'product_type_id', 'brand_id',  'preorder_enabled', 'shelf_life_unit', 'service_type_id','is_top_seller', 'is_featured', 'unit_per_box', 'product_nature_id', 'manufacturer_id', 'is_collection', 'created_at', 'updated_at', 'deleted_at', 'is_beston_pure_product', 'beston_quantity', 'reorder_threshold', 'reorder_notification','show_batch' ], 'integer'],
            [['product_description', 'nutrition_text' ], 'string'],
            [['location_id'], 'safe'],
            [['product_number', 'barcode', 'product_name', 'unit_size', 'unit_size_kg', 'shelf_life', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_image', 'nutrition_image'], 'string', 'max' => 255]
        ];
    }

    static function getProductsForFormDropDown()
    {
        $raw_products = Product::findAll(['deleted_at' => null]);
        $products = [];
        foreach($raw_products as $raw_product){
            $products []  =[
                'id' =>   $raw_product->id,
                'name' =>   $raw_product->id .' - '.$raw_product->product_name,
            ];
        }

        return $products;
    }


    /**
     * Gets Product Translation Value to be displayed on product update
     * @return \yii\db\ActiveQuery
     */

    public function getProductTranslation($language_code)
    {
        $productTranslation = ProductTranslation::find()->where(['product_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
        if(isset($productTranslation)){
            return $productTranslation;
        }
        return false;
    }


    /**
     * Gets Product Translation Value to be displayed on product update
     * @return \yii\db\ActiveQuery
     */

    public function getGroupTranslation($language_code)
    {
        $groupTranslation = ProductGroupTranslation::find()->where(['product_group_id' => $this->productGroup->id, 'language_id' => $language_code])->one();
        if(isset($groupTranslation)){
            return $groupTranslation->product_group_name;
        }
        return "(Not Set)";
    }

    /**
     * Gets Product Translation Value to be displayed on product update
     * @return \yii\db\ActiveQuery
     */

    public function getBrandTranslation($language_code)
    {
        $brandTranslation = BrandTranslation::find()->where(['brand_id' => $this->brand->id, 'language_id' => $language_code])->one();
        if(isset($brandTranslation)){
            return $brandTranslation->brand_name;
        }
        return "(Not Set)";
    }

    /**
     * Gets Product Translation Value to be displayed on product update
     * @return \yii\db\ActiveQuery
     */

    public function getManufacturerTranslation($language_code)
    {
        $manufacturerTranslation = ManufacturerTranslation::find()->where(['manufacturer_id' => $this->manufacturer->id, 'language_id' => $language_code])->one();
        if(isset($manufacturerTranslation)){
            return $manufacturerTranslation->manufacturer_name;
        }
        return "(Not Set)";
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getId()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'id']);
    }

    public function getLimitedTranslatedData($language_code)
    {
        if (isset($this->getProductTranslation($language_code)->nutrition_image) && $this->getProductTranslation($language_code)->nutrition_image != '') {
            $nutrition_image = 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->getProductTranslation($language_code)->nutrition_image;
        } else {
            $nutrition_image = 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->nutrition_image;
        }

        $brand = Brand::find()->select('id, brand_name , brand_location_latitude , brand_location_longitude , brand_description , brand_video, manufacturer_id')->where(['id' => $this->brand_id])->one();
        $brand->brand_name = $this->brand->getBrandTranslation($language_code)->brand_name;
        $brand->brand_description = $this->brand->getBrandTranslation($language_code)->brand_description;
        $brand->brand_logo = $this->brand->getBrandLogo();

        $manufacturer = Manufacturer::find()->select('id , manufacturer_name , manufacturer_location_latitude , manufacturer_location_longitude , manufacturer_logo ,  manufacturer_video ,location , manufacturer_description')->where(['id' => $this->manufacturer_id])->one();
        $manufacturer->manufacturer_name = $this->manufacturer->getManufacturerTranslation($language_code)->manufacturer_name;
        $manufacturer->manufacturer_description = $this->manufacturer->getManufacturerTranslation($language_code)->manufacturer_description;
        $manufacturer->manufacturer_logo = $this->manufacturer->getManufacturerLogo();

        $productType = $this->productType->getProductType();

        $productGroup = ProductGroup::find()->where(['id' => $this->product_group_id])->one();
        $productGroup->product_group_name = $this->productGroup->getGroupTranslation($language_code)->product_group_name;

        if ($this->deleted_at != null || $brand == false || $manufacturer == false || $productType == false || $productGroup == false) {
            return false;
        } else {
            return $product = [
                "id" => $this->id,
                "is_collection" => $this->is_collection,
                "collection_items" => $this->getLimitedCollectionItems($language_code),
                "product_number" => $this->product_number,
                "barcode" => $this->barcode,
                "product_group" => $productGroup->product_group_name,
                "product_name" => ($this->getProductTranslation($language_code) != false ? trim($this->getProductTranslation($language_code)->product_name) : trim($this->product_name)),

                "product_description" => strip_tags(html_entity_decode(html_entity_decode($this->getProductTranslation($language_code) != false ? trim($this->getProductTranslation($language_code)->product_description) : trim($this->product_description), ENT_QUOTES | ENT_XML1, 'UTF-8'))),
                "product_type" => $productType,
                "brand" => $brand,
                "manufacturer" => $manufacturer,
                "shelf_life" => $this->shelf_life,
                "shelf_life_unit" => $this->shelf_life_unit,

//                "UnitSize" => $this->unit_size,
//                "ServesPerPack" => $this->serves_per_pack,

                "ServeSize" => $this->serve_size,
//                "NutritionData" => $this->getNutrition(),

//                "Tags" => $this->getProductNutritionTagsText(),

                "product_image" => 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->product_image,
                "nutrition_image" => ($this->getProductTranslation($language_code) != false ? $nutrition_image : 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->nutrition_image),
                "nutrition_text" => ($this->getProductTranslation($language_code) != false ? $this->getProductTranslation($language_code)->nutrition_text : $this->nutrition_text),
            ];

        }
    }



    public function getTranslatedData($language_code)
    {
        if (isset($this->getProductTranslation($language_code)->nutrition_image) && $this->getProductTranslation($language_code)->nutrition_image != '') {
            $nutrition_image = 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->getProductTranslation($language_code)->nutrition_image;
        } else {
            $nutrition_image = 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->nutrition_image;
        }

        $brand = Brand::find()->where(['id' => $this->brand_id])->one();
        $brand->brand_name = $this->brand->getBrandTranslation($language_code)->brand_name;
        $brand->brand_description = $this->brand->getBrandTranslation($language_code)->brand_description;
        $brand->brand_logo = $this->brand->getBrandLogo();

        $manufacturer = Manufacturer::find()->where(['id' => $this->manufacturer_id])->one();
        $manufacturer->manufacturer_name = $this->manufacturer->getManufacturerTranslation($language_code)->manufacturer_name;
        $manufacturer->manufacturer_description = $this->manufacturer->getManufacturerTranslation($language_code)->manufacturer_description;
        $manufacturer->manufacturer_logo = $this->manufacturer->getManufacturerLogo();

        $productType = $this->productType->getProductType();

        $productGroup = ProductGroup::find()->where(['id' => $this->product_group_id])->one();
        $productGroup->product_group_name = $this->productGroup->getGroupTranslation($language_code)->product_group_name;

        if ($this->deleted_at != null || $brand == false || $manufacturer == false || $productType == false || $productGroup == false) {
            return false;
        } else {
            return $product = [
                "id" => $this->id,
                "is_collection" => $this->is_collection,
                "beston_quantity" => $this->getBestonQuantity(2),
                "ecommerce_enabled" => $this->getEcommerceEnabled(2),
                "collection_items" => $this->getCollectionItems($language_code),
                "product_number" => $this->product_number,
                "barcode" => $this->barcode,
                "product_group" => $productGroup->product_group_name,
                "product_name" => ($this->getProductTranslation($language_code) != false ? trim($this->getProductTranslation($language_code)->product_name) : trim($this->product_name)),
                "product_description" => strip_tags(html_entity_decode(html_entity_decode($this->getProductTranslation($language_code) != false ? trim($this->getProductTranslation($language_code)->product_description) : trim($this->product_description), ENT_QUOTES | ENT_XML1, 'UTF-8'))),
                "product_type" => $productType,
                "product_price" => $this->getProductPrice($language_code),
                "product_sale_price" => $this->getProductSalePrice($language_code),
                "is_taxable" => $this->getProductTaxable(2),
                "brand" => $brand,
                "manufacturer" => $manufacturer,
                "shelf_life" => $this->shelf_life,
                "shelf_life_unit" => $this->shelf_life_unit,
                "UnitSize" => $this->unit_size,
                "ServesPerPack" => $this->serves_per_pack,
                "ServeSize" => $this->serve_size,
                "NutritionData" => $this->getNutrition(),
                "Tags" => $this->getProductNutritionTagsText(),
                "product_image" => $this->getProductImageUrl(),
                "nutrition_image" => ($this->getProductTranslation($language_code) != false ? $nutrition_image : 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->nutrition_image),
                "nutrition_text" => ($this->getProductTranslation($language_code) != false ? $this->getProductTranslation($language_code)->nutrition_text : $this->nutrition_text),
                'reviews' => $this->getProductReviews(),
                "is_eatable" => $this->is_eatable,
            ];

        }
    }

    public function getLimitedCollectionItems($language_code){

        $raw_items = CollectionItem::find()->where(['collection_id' => $this->id])->all();
//        return $raw_items;
        $items = [];
        foreach($raw_items as $item){
            $product = $item->product->getLimitedTranslatedData($language_code);
            if($product != false){
                $items[] = [
                    'quantity' => $item->quantity,
                    'product' => $product,
                ];
            }
        }
        return $items;
    }


    public function getCollectionItems($language_code){

        $raw_items = CollectionItem::find()->where(['collection_id' => $this->id])->all();
//        return $raw_items;
        $items = [];
        foreach($raw_items as $item){
            if($item->product->getTranslatedData($language_code) != false){
                $items[] = [
                    'quantity' => $item->quantity,
                    'product' => $item->product->getTranslatedData($language_code),
                ];
            }
        }
        return $items;
    }

    public function getCollectionItemsForReport(){

        return  CollectionItem::find()->where(['collection_id' => $this->id])->all();

    }

    public function getAuditTrails()
    {
//        print_r(ArrayHelper::map($this->getProductTranslations()->all(), 'id', 'id')) ;
        return AuditTrail::find()
            ->orOnCondition([
                'audit_trail.model_id' => ArrayHelper::map($this->getProductTranslations()->all(), 'id', 'id'),
                'audit_trail.model' => 'app\models\ProductTranslation',
            ])
            ->orOnCondition([
                'audit_trail.model_id' => ArrayHelper::map($this->getproductLocationAttributes()->all(), 'id', 'id'),
                'audit_trail.model' => 'app\models\ProductLocationAttribute',
            ])
            ->orOnCondition([
                'audit_trail.model_id' => $this->id,
                'audit_trail.model' => get_class($this),
            ]);

    }

    /**
     * @Set Product Status
     */
    public function changeStatus()
    {
        if($this->deleted_at == null || $this->deleted_at == ''){
            $this->deleted_at = time();
        }else{
            $this->deleted_at = null;
        }

        return $this->save();
    }




    /**
     * @Set is_beston_pure_product
     */
    public function setBestonPureProduct()
    {
        if($this->is_beston_pure_product == 1 ){
            $this->is_beston_pure_product= 0;
        }else{
            $this->is_beston_pure_product= 1;
        }

        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getproduct_type()
    {
        return $this->hasOne(\app\models\ProductType::className(), ['id' => 'product_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getproductLocationAttributes()
    {
        return $this->hasMany(\app\models\ProductLocationAttribute::className(), ['product_id' => 'id']);
    }

}