<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BatchItem;

/**
 * BatchItemsSearch represents the model behind the search form about `app\models\BatchItem`.
 */
class BatchItemsSearch extends BatchItem
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'batch_id'], 'integer'],
            [['qr_code_text', 'item_status'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BatchItem::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'batch_id' => $this->batch_id,
        ]);

        $query->andFilterWhere(['like', 'qr_code_text', $this->qr_code_text])
            ->andFilterWhere(['like', 'item_status', $this->item_status]);

        return $dataProvider;
    }

}