<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReviewRating;

/**
 * ReviewRatingSearch represents the model behind the search form about `app\models\ReviewRating`.
 */
class ReviewRatingSearch extends ReviewRating
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'brand_id', 'product_id', 'manufacturer_id', 'rating', 'created_at', 'updated_at'], 'string'],
            [['review'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReviewRating::find();
        $query->joinWith(['product' , 'brand' ,'manufacturer','app_user' ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->where(['review_rating.deleted_at' => null])
              ->andFilterWhere(['like', 'app_user.first_name' , $this->user_id])
              ->andFilterWhere(['like', 'product.product_name' , $this->product_id])
              ->andFilterWhere(['like', 'brand.brand_name', $this->brand_id])
              ->andFilterWhere(['like', 'manufacturer.manufacturer_name' , $this->manufacturer_id])
              ->andFilterWhere(['review_rating.rating' => $this->rating])
              ->andFilterWhere(['like', 'review_rating.review', $this->review]);

        $query->addOrderBy('product_id ASC')->addOrderBy('brand_id ASC')->addOrderBy('manufacturer_id ASC')->addOrderBy('id ASC');

        return $dataProvider;
    }

}