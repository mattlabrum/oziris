<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{

    public $location_id;
    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_group_id', 'product_name', 'product_description', 'product_type_id', 'brand_id', 'manufacturer_id'], 'string'],
            [['product_number', 'unit_size', 'unit_size_kg', 'shelf_life', 'product_image', 'nutrition_image', 'nutrition_text' ,'shelf_life_unit','product_nature_id','barcode','service_type_id', 'location_id' ], 'safe'],
        ];
    }

    public function appKeyWordSearch($params)
    {
        $query = Product::find();
        $query->joinWith(['productGroup','productType' , 'brand' ,'manufacturer', 'productLocationAttributes' ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $query->where(['product_location_attributes.language_id' => 2]);
        $query->andWhere(['product_location_attributes.ecommerce_enabled' => 1]);

        $terms = explode(" ", $params);

        $condition = ['or'];
        foreach ($terms as $key) {
            $condition[] = ['like', 'product_group.product_group_name', $key];
            $condition[] = ['like', 'product_name', $key];
            $condition[] = ['like', 'product_description', $key];
            $condition[] = ['like', 'product_type.type_name', $key];
        }
        $query->andWhere(['product.deleted_at' => NULL]);
        $query->andWhere($condition);

        return $dataProvider;
    }



    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//
//        print_r($params);
//die;
        $query = Product::find();
        $query->joinWith(['productGroup','productType' , 'brand' ,'manufacturer', 'productLocationAttributes' ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['product.id' => $this->id]);

        $query->andFilterWhere(['product.is_collection' => $this->is_collection]);


        $query->andFilterWhere(['like', 'product_number', $this->product_number])
            ->andFilterWhere(['like', 'unit_size', $this->unit_size])
            ->andFilterWhere(['like', 'unit_size_kg', $this->unit_size_kg])

            ->andFilterWhere(['like', 'shelf_life_unit', $this->shelf_life_unit])
            ->andFilterWhere(['like', 'product_nature_id', $this->product_nature_id])
            ->andFilterWhere(['like', 'service_type_id', $this->service_type_id])
            ->andFilterWhere(['like', 'barcode', $this->barcode])

            ->andFilterWhere(['like', 'shelf_life', $this->shelf_life])
            ->andFilterWhere(['like', 'product_image', $this->product_image])
            ->andFilterWhere(['like', 'nutrition_image', $this->nutrition_image])
            ->andFilterWhere(['like', 'nutrition_text', $this->nutrition_text])
            ->andFilterWhere(['like', 'product_group.product_group_name', $this->product_group_id])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'product_description', $this->product_description])
            ->andFilterWhere(['like', 'product_type.type_name', $this->product_type_id])
            ->andFilterWhere(['like', 'brand.brand_name', $this->brand_id])
            ->andFilterWhere(['like', 'manufacturer.manufacturer_name', $this->manufacturer_id]);

//            ->andFilterWhere(['product_location_attributes.product_id' => $this->id])
//            ->andFilterWhere(['product_location_attributes.language_id' => $this->location_id]);



//        var_dump($query);


        return $dataProvider;
    }

        public function stockSearch($params)
       {
//
//        print_r($params);
//die;
           $query = Product::find();
           $query->joinWith(['productGroup','productType' , 'brand' ,'manufacturer', 'productLocationAttributes' ]);

           $dataProvider = new ActiveDataProvider([
               'query' => $query,
               //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
           ]);

           $this->load($params);

           if (!$this->validate()) {
               return $dataProvider;
           }

           $query->andFilterWhere(['product.id' => $this->id]);

           $query->andFilterWhere(['product.is_collection' => $this->is_collection]);


           $query->andFilterWhere(['like', 'product_number', $this->product_number])
               ->andFilterWhere(['like', 'unit_size', $this->unit_size])
               ->andFilterWhere(['like', 'unit_size_kg', $this->unit_size_kg])

               ->andFilterWhere(['like', 'shelf_life_unit', $this->shelf_life_unit])
               ->andFilterWhere(['like', 'product_nature_id', $this->product_nature_id])
               ->andFilterWhere(['like', 'service_type_id', $this->service_type_id])
               ->andFilterWhere(['like', 'barcode', $this->barcode])

               ->andFilterWhere(['like', 'shelf_life', $this->shelf_life])
               ->andFilterWhere(['like', 'product_image', $this->product_image])
               ->andFilterWhere(['like', 'nutrition_image', $this->nutrition_image])
               ->andFilterWhere(['like', 'nutrition_text', $this->nutrition_text])
               ->andFilterWhere(['like', 'product_group.product_group_name', $this->product_group_id])
               ->andFilterWhere(['like', 'product_name', $this->product_name])
               ->andFilterWhere(['like', 'product_description', $this->product_description])
               ->andFilterWhere(['like', 'product_type.type_name', $this->product_type_id])
               ->andFilterWhere(['like', 'brand.brand_name', $this->brand_id])
               ->andFilterWhere(['like', 'manufacturer.manufacturer_name', $this->manufacturer_id])

            ->andFilterWhere(['product_location_attributes.product_id' => $this->id])
            ->andFilterWhere(['product_location_attributes.language_id' => $this->location_id]);



//        var_dump($query);


           return $dataProvider;
       }





}