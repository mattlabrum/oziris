<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/e0080b9d6ffa35acb85312bf99a557f2
 *
 * @package default
 */


namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Coupon;

/**
 * CouponsSearch represents the model behind the search form about `app\models\Coupon`.
 */
class CouponsSearch extends Coupon
{

	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function rules() {
		return [
			[['id', 'date_created', 'date_modified', 'expiry_date', 'individual_use', 'usage_limit', 'usage_limit_per_user', 'limit_usage_to_x_items', 'free_shipping', 'exclude_sale_items'], 'integer'],
			[['code', 'discount_type', 'description', 'email_restrictions'], 'safe'],
			[['amount', 'minimum_amount', 'maximum_amount'], 'number'],
		];
	}


	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}


	/**
	 * Creates data provider instance with search query applied
	 *
	 *
	 * @param array   $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Coupon::find();

		$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
				'id' => $this->id,
				'date_created' => $this->date_created,
				'date_modified' => $this->date_modified,
				'amount' => $this->amount,
				'expiry_date' => $this->expiry_date,
				'individual_use' => $this->individual_use,
				'usage_limit' => $this->usage_limit,
				'usage_limit_per_user' => $this->usage_limit_per_user,
				'limit_usage_to_x_items' => $this->limit_usage_to_x_items,
				'free_shipping' => $this->free_shipping,
				'exclude_sale_items' => $this->exclude_sale_items,
				'minimum_amount' => $this->minimum_amount,
				'maximum_amount' => $this->maximum_amount,
			]);

		$query->andFilterWhere(['like', 'code', $this->code])
		->andFilterWhere(['like', 'discount_type', $this->discount_type])
		->andFilterWhere(['like', 'description', $this->description])
		->andFilterWhere(['like', 'email_restrictions', $this->email_restrictions]);

		return $dataProvider;
	}


}
