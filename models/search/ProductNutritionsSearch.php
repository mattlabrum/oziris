<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductNutrition;

/**
 * ProductNutritionsSearch represents the model behind the search form about `app\models\ProductNutrition`.
 */
class ProductNutritionsSearch extends ProductNutrition
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'nutrition_id'], 'integer'],
            [['avg_qty_per_serving', 'avg_qty_per_100g'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductNutrition::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_id' => $this->product_id,
            'nutrition_id' => $this->nutrition_id,
        ]);

        $query->andFilterWhere(['like', 'avg_qty_per_serving', $this->avg_qty_per_serving])
            ->andFilterWhere(['like', 'avg_qty_per_100g', $this->avg_qty_per_100g]);

        return $dataProvider;
    }

}