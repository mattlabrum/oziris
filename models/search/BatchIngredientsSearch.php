<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BatchIngredient;

/**
 * BatchIngredientsSearch represents the model behind the search form about `app\models\BatchIngredient`.
 */
class BatchIngredientsSearch extends BatchIngredient
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'batch_id', 'ingredient_id', 'supplier_id'], 'integer'],
            [['manufacture_date'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BatchIngredient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'batch_id' => $this->batch_id,
            'ingredient_id' => $this->ingredient_id,
            'supplier_id' => $this->supplier_id,
        ]);

        $query->andFilterWhere(['like', 'manufacture_date', $this->manufacture_date]);

        return $dataProvider;
    }

}