<?php
/**
 * /vagrant/digitalnoir/oziris-backend/src/../runtime/giiant/e0080b9d6ffa35acb85312bf99a557f2
 *
 * @package default
 */


namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Coo;

/**
 * CooSearch represents the model behind the search form about `app\models\Coo`.
 */
class CooSearch extends Coo
{

	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function rules() {
		return [
			[['id'], 'integer'],
			[['coo_latitude', 'coo_longitude'], 'number'],
			[['city', 'country'], 'safe'],
		];
	}


	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}


	/**
	 * Creates data provider instance with search query applied
	 *
	 *
	 * @param array   $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Coo::find();

		$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
				'id' => $this->id,
				'coo_latitude' => $this->coo_latitude,
				'coo_longitude' => $this->coo_longitude,
			]);

		$query->andFilterWhere(['like', 'city', $this->city])
		->andFilterWhere(['like', 'country', $this->country]);

		return $dataProvider;
	}


}
