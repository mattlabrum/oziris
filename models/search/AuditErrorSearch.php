<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AuditError;

/**
 * AuditErrorSearch represents the model behind the search form about `app\models\AuditError`.
 */
class AuditErrorSearch extends AuditError
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entry_id', 'code', 'line', 'emailed'], 'integer'],
            [['created', 'message', 'file', 'trace', 'hash'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AuditError::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'entry_id' => $this->entry_id,
            'created' => $this->created,
            'code' => $this->code,
            'line' => $this->line,
            'emailed' => $this->emailed,
        ]);

        $query->andFilterWhere(['like', 'message', $this->message])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'trace', $this->trace])
            ->andFilterWhere(['like', 'hash', $this->hash]);

        return $dataProvider;
    }

}