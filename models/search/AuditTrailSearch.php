<?php

namespace app\models\search;

use bedezign\yii2\audit\models\AuditTrail;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * AuditTrailSearch represents the model behind the search form about `app\models\AuditTrail`.
 */
class AuditTrailSearch extends AuditTrail
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'entry_id', 'user_id'], 'integer'],
            [['action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'created'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param array $filter
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $params['query'];
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params['search_string']);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'entry_id' => $this->entry_id,
            'user_id' => $this->user_id,
            'created' => $this->created,
        ]);

        $query->andFilterWhere(['like', 'action', $this->action])
            ->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'model_id', $this->model_id])
            ->andFilterWhere(['like', 'field', $this->field])
            ->andFilterWhere(['like', 'old_value', $this->old_value])
            ->andFilterWhere(['like', 'new_value', $this->new_value]);

        $query->andFilterWhere([
            'field' => $params['filter']
        ]);


        return $dataProvider;
    }

}