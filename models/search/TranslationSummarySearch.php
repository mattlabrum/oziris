<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Product;

/**
 * ProductSearch represents the model behind the search form about `app\models\Product`.
 */
class TranslationSummarySearch extends Product
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_number'], 'string'],

        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
//
//        print_r($params);
//die;
        $query = Product::find();
        $query->joinWith(['productGroup', 'productType' , 'brand' ,'manufacturer' ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id]);
        $query->andWhere(['product.deleted_at' => null]);
        $query->andWhere(['brand.deleted_at' => null]);
        $query->andWhere(['manufacturer.deleted_at' => null]);
        $query->andWhere(['product_group.deleted_at' => null]);
        $query->andWhere(['product_type.deleted_at' => null]);

        $query->andFilterWhere(['like', 'product_number', $this->product_number]);



//        var_dump($query);


        return $dataProvider;
    }

}