<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScannedProduct;

/**
 * ScannedProductsSearch represents the model behind the search form about `app\models\ScannedProduct`.
 */
class ScannedProductsSearch extends ScannedProduct
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'batch_id', 'user_id', 'scanned_longitude', 'scanned_latitude'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScannedProduct::find();
        $query->joinWith(['batch','user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'batch.batch_number', $this->batch_id])
              ->andFilterWhere(['like', 'user.username', $this->user_id])
              ->andFilterWhere(['like', 'scanned_longitude', $this->scanned_longitude])
              ->andFilterWhere(['like', 'scanned_latitude', $this->scanned_latitude]);

        return $dataProvider;
    }

}