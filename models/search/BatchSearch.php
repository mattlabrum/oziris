<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Batch;

/**
 * BatchSearch represents the model behind the search form about `app\models\Batch`.
 */
class BatchSearch extends Batch
{
    public $product_name;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'manufacture_date', 'recalled_date', 'product_id' , 'product_name'], 'safe'],
            [['qr_code', 'batch_number'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Batch::find();
        $query->joinWith(['product']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }



        $query->where(
            "batch.deleted_at IS NULL AND batch_number != product_id"
        );

        $query->andFilterWhere(['like', 'qr_code', $this->qr_code])
        ->andFilterWhere(['like', 'batch_number', $this->batch_number])
        ->andFilterWhere(['like', 'manufacture_date', $this->manufacture_date])
        ->andFilterWhere(['like', 'recalled_date', $this->recalled_date])
        ->andFilterWhere(['like', 'product.product_name', $this->product_name])
        ->andFilterWhere(['product.id' => $this->product_id])
        ->andFilterWhere(['batch.id' => $this->id]);

        return $dataProvider;
    }

    public function filterCSV($params)
    {
        $query = Batch::find();
        $query->joinWith(['product']);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['product_id' => SORT_ASC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->where(
            "batch.deleted_at IS NULL AND batch_number != product_id"
        );

        $query->andFilterWhere(['like', 'qr_code', $this->qr_code])
            ->andFilterWhere(['like', 'batch_number', $this->batch_number])
            ->andFilterWhere(['like', 'manufacture_date', $this->manufacture_date])
            ->andFilterWhere(['like', 'recalled_date', $this->recalled_date])
            ->andFilterWhere(['like', 'product.product_name', $this->product_name])
            ->andFilterWhere(['product.id' => $this->product_id])
            ->andFilterWhere(['batch.id' => $this->id]);

        return $dataProvider;
    }


}