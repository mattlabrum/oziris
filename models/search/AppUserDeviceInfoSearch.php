<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AppUserDeviceInfo;

/**
 * AppUserDeviceInfoSearch represents the model behind the search form about `app\models\AppUserDeviceInfo`.
 */
class AppUserDeviceInfoSearch extends AppUserDeviceInfo
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['device_token', 'device_type', 'timezone', 'badge', 'app_identifier', 'app_name'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AppUserDeviceInfo::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
        ]);

        $query->andFilterWhere(['like', 'device_token', $this->device_token])
            ->andFilterWhere(['like', 'device_type', $this->device_type])
            ->andFilterWhere(['like', 'timezone', $this->timezone])
            ->andFilterWhere(['like', 'badge', $this->badge])
            ->andFilterWhere(['like', 'app_identifier', $this->app_identifier])
            ->andFilterWhere(['like', 'app_name', $this->app_name]);

        return $dataProvider;
    }

}