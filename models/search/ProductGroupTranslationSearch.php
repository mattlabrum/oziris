<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductGroupTranslation;

/**
 * ProductGroupTranslationSearch represents the model behind the search form about `app\models\ProductGroupTranslation`.
 */
class ProductGroupTranslationSearch extends ProductGroupTranslation
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_group_id', 'language_id'], 'integer'],
            [['product_group_name'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductGroupTranslation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'product_group_id' => $this->product_group_id,
            'language_id' => $this->language_id,
        ]);

        $query->andFilterWhere(['like', 'product_group_name', $this->product_group_name]);

        return $dataProvider;
    }

}