<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ScannedProduct;

/**
 * ScannedProductSearch represents the model behind the search form about `app\models\ScannedProduct`.
 */
class ScannedProductSearch extends ScannedProduct
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'batch_id', 'created_at', 'updated_at'], 'integer'],
            [['batch_number', 'qr_code', 'product_code','user_id'], 'safe'],
            [['scanned_longitude', 'scanned_latitude'], 'number'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ScannedProduct::find();
        $query->joinWith(['app_user']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'batch_id' => $this->batch_id,
            'scanned_longitude' => $this->scanned_longitude,
            'scanned_latitude' => $this->scanned_latitude,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'batch_number', $this->batch_number])
            ->andFilterWhere(['like', 'qr_code', $this->qr_code])
            ->andFilterWhere(['like', 'product_code', $this->product_code])
            ->andFilterWhere(['like', 'app_user.first_name', $this->user_id])
            ->orFilterWhere(['user_id' => $this->user_id]);


        return $dataProvider;
    }

}