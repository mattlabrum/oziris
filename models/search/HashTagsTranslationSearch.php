<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HashTagsTranslation;

/**
 * HashTagsTranslationSearch represents the model behind the search form about `app\models\HashTagsTranslation`.
 */
class HashTagsTranslationSearch extends HashTagsTranslation
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'hash_tags_id', 'language_id'], 'integer'],
            [['hash_tag'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HashTagsTranslation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'hash_tags_id' => $this->hash_tags_id,
            'language_id' => $this->language_id,
        ]);

        $query->andFilterWhere(['like', 'hash_tag', $this->hash_tag]);

        return $dataProvider;
    }

}