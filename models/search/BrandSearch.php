<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Brand;

/**
 * BrandSearch represents the model behind the search form about `app\models\Brand`.
 */
class BrandSearch extends Brand
{

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'brand_location_latitude', 'brand_location_longitude'], 'safe'],
            [['brand_name', 'brand_logo', 'brand_video', 'manufacturer_id', 'brand_description'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Brand::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere(['id' => $this->id,]);
//        $query->andWhere(['deleted_at' => NULL,]);

        $query->andFilterWhere(['like', 'brand_name', $this->brand_name])
            ->andFilterWhere(['like', 'brand_logo', $this->brand_logo])
            ->andFilterWhere(['like', 'brand_video', $this->brand_video])
            ->andFilterWhere(['like', 'manufacturer_id', $this->manufacturer_id])
            ->andFilterWhere(['like', 'brand_location_latitude', $this->brand_location_latitude])
            ->andFilterWhere(['like', 'brand_location_longitude', $this->brand_location_longitude])
            ->andFilterWhere(['like', 'brand_description', $this->brand_description]);



        return $dataProvider;
    }

}