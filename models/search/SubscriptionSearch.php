<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/e0080b9d6ffa35acb85312bf99a557f2
 *
 * @package default
 */


namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Subscription;

/**
 * SubscriptionSearch represents the model behind the search form about `app\models\Subscription`.
 */
class SubscriptionSearch extends Subscription
{

	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function rules() {
		return [
			[['id', 'start_date', 'next_payment_date', 'next_delivery_date'], 'integer'],
			[['billing_period', 'billing_interval', 'status', 'subscription_name', 'delivery_day', 'customer_id', 'subscription_billing_address_id', 'subscription_shipping_address_id', 'payment_method', 'payment_title', 'stript_customer_id', 'stripe_card_id', 'subscription_id'], 'safe'],
		];
	}


	/**
	 *
	 * @inheritdoc
	 * @return unknown
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}


	/**
	 * Creates data provider instance with search query applied
	 *
	 *
	 * @param array   $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Subscription::find();

		$dataProvider = new ActiveDataProvider([
				'query' => $query,
			]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
				'id' => $this->id,
                'subscription_id' => $this->subscription_id,
			]);

		$query->andFilterWhere(['like', 'billing_period', $this->billing_period])
		->andFilterWhere(['like', 'billing_interval', $this->billing_interval])
        ->andFilterWhere(['like', 'start_date', $this->start_date])
        ->andFilterWhere(['like', 'next_payment_date', $this->next_payment_date])
        ->andFilterWhere(['like', 'next_delivery_date', $this->next_delivery_date])
		->andFilterWhere(['like', 'status', $this->status])
		->andFilterWhere(['like', 'subscription_name', $this->subscription_name])
		->andFilterWhere(['like', 'delivery_day', $this->delivery_day])
		->andFilterWhere(['like', 'customer_id', $this->customer_id])
		->andFilterWhere(['like', 'subscription_billing_address_id', $this->subscription_billing_address_id])
		->andFilterWhere(['like', 'subscription_shipping_address_id', $this->subscription_shipping_address_id])
		->andFilterWhere(['like', 'payment_method', $this->payment_method])
		->andFilterWhere(['like', 'payment_title', $this->payment_title])
		->andFilterWhere(['like', 'stript_customer_id', $this->stript_customer_id])
		->andFilterWhere(['like', 'stripe_card_id', $this->stripe_card_id]);

		return $dataProvider;
	}


}
