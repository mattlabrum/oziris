<?php

namespace app\models\search;

use app\models\Order;
use DateTime;
use NumberFormatter;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    public $contact_no;
    public $location_id;
    public $first_name;
    public $last_name;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass parent scenarios
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status_id', 'currency_id', 'shipping_id', 'payment_id', 'woo_order_id'], 'integer'],
            [['subtotal', 'tax_amount', 'total_amount'], 'number'],
            [['customer_id', 'order_note', 'dispatch_date' ,  'last_name', 'last_name', 'first_name', 'contact_no', 'location_id' ,   'created_at', 'updated_at', 'customer_user_agent', 'subscription_id','parent_subscription_delivery_day', 'subscription_delivery_date'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */




    public function search($params)
    {
        $query = Order::find();

        $query->joinWith(['billingAddress bAddress', 'shippingAddress sAddress', 'customer']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'status_id' => $this->status_id,
            'woo_order_id' => $this->woo_order_id,
            'subscription_id' => $this->subscription_id,
        ]);
        $query->andFilterWhere(['like', 'app_user.email_address', $this->customer_id]);
        $query->orFilterWhere(['like', 'bAddress.email', $this->customer_id]);
        $query->andFilterWhere(['like', 'bAddress.contact_no', $this->contact_no]);
        $query->andFilterWhere(['like', 'bAddress.first_name', $this->first_name]);
        $query->andFilterWhere(['like', 'bAddress.last_name', $this->last_name]);

        $query->orFilterWhere(['like', 'sAddress.first_name', $this->first_name]);
        $query->orFilterWhere(['like', 'sAddress.last_name', $this->last_name]);


        $query->andFilterWhere(['like', 'order.order_note', $this->order_note]);
        $query->andFilterWhere(['like', 'order.subscription_delivery_date', $this->subscription_delivery_date]);
//        echo $this->location_id;
//        die;
        if($this->location_id == 1){
            $query->andFilterWhere(['>', 'order.woo_order_id', 10000000]);
        }else if($this->location_id == 2) {
            $query->andFilterWhere(['<', 'order.woo_order_id', 10000000]);
        }
        $query->andFilterWhere(['like', 'order.total_amount', $this->total_amount]);

        if (!is_null($this->created_at) && strpos($this->created_at, ' - ') !== false ) {
            list($cstart_date, $cend_date) = explode(' - ', $this->created_at);
            $csdate = new DateTime($cstart_date);
            $cstimestamp = ($csdate->getTimestamp());

            $cedate = new DateTime($cend_date);
            $cetimestamp = ($cedate->getTimestamp()+86400);
            $query->andFilterWhere(['>=', 'order.created_at', $cstimestamp])
                ->andFilterWhere(['<=', 'order.created_at', $cetimestamp]);
        }

        if (!is_null($this->updated_at) && strpos($this->updated_at, ' - ') !== false ) {
            list($ustart_date, $uend_date) = explode(' - ', $this->updated_at);
            $usdate = new DateTime($ustart_date);
            $ustimestamp = ($usdate->getTimestamp());

            $uedate = new DateTime($uend_date);
            $uetimestamp = ($uedate->getTimestamp()+86400);
            $query->andFilterWhere(['>=', 'order.updated_at', $ustimestamp])
                ->andFilterWhere(['<=', 'order.updated_at', $uetimestamp]);
        }

        $query->andFilterWhere(['like', 'order.dispatch_date', $this->dispatch_date]);
        return $dataProvider;
    }
}