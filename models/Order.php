<?php

namespace app\models;

use app\models\search\OrderCouponsSearch;
use app\widgets\xj\QRcode;
use app\widgets\xj\Text;
use Imagick;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "order".
 */
class Order extends \app\models\base\Order
{

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status_id' => Yii::t('app', 'Order Status'),
            'currency_id' => Yii::t('app', 'Currency'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'shipping_id' => Yii::t('app', 'Shipping Method'),
            'customer_id' => Yii::t('app', 'Customer Email'),
            'payment_id' => Yii::t('app', 'Payment Method'),
            'order_note' => Yii::t('app', 'Order Note'),
            'customer_ip' => Yii::t('app', 'Customer IP'),
            'customer_user_agent' => Yii::t('app', 'Customer User Agent'),
            'tax_amount' => Yii::t('app', 'Tax Amount'),
            'total_amount' => Yii::t('app', 'Total Amount'),
            'woo_order_id' => Yii::t('app', 'Order #'),
        ];
    }

    public function getOrderCoupons(){

        $coupons = OrderCoupon::find()->select('code')->where(['order_id' => $this->woo_order_id])->all();

        $strCoupons = '';
        for($i=0 ; $i<count($coupons) ; $i++){
            if($i==0){
                $strCoupons = $coupons[$i]->code;
            }else{
                $strCoupons = $strCoupons.', '.$coupons[$i]->code;
            }

        }
return $strCoupons;
    }

    public function generateQrCode(){

        $this->qr_code = 'order-'.$this->woo_order_id;
        $this->qr_code_text = 'order-'.$this->woo_order_id;
        $filename = Text::widget([
            'outputDir' => '@webroot/order/qrcode',
            'outputDirWeb' => '@web/order/qrcode',
            'ecLevel' => QRcode::QR_ECLEVEL_H,
            'text' => $this->qr_code,
            'batch_id' => $this->qr_code,
            'fgColor' => 0x000000,
            'bgColor' => 0xFFFFFF,
            'date' => date('d-m-Y'),
            'size' => 14,
        ]);

        $final_filename = date('d-m-Y').'/'.$this->qr_code.'.png';
        if($this->prettify('https://'.$_SERVER['HTTP_HOST'].Url::to('@web/order/qrcode').'/'.$filename, 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/order/').'logo.png', 'order/qrcode/'.$final_filename)){
            unlink('order/qrcode/'.$filename);
            $this->qr_code = $final_filename;
        }
    }

    function prettify($file, $logo_file, $output = '') {
        $qr = false;
        $geometry = false;
        /* Load image */
        $qr = new Imagick();
////        echo $file;
        $qr -> readImage($file);
        $geometry =   $qr -> getImageGeometry();

        if(!$logo_file) {
            /* No logo to add */
            return false;
        }
        $logo = new Imagick();
        $logo -> readImage($logo_file);
        $logo_size = $logo -> getImageGeometry();
        $x = (  $geometry['width'] - $logo_size['width']) / 2;
        $y = (  $geometry['height'] - $logo_size['height']) / 2;
        $qr -> compositeImage($logo, imagick::COMPOSITE_OVER, $x, $y);
//
//        /* Output image */
        if($output != '') {
            $qr -> setImageFileName($output);
        }
        $qr -> writeImage();
        return true;
    }


}
