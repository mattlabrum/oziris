<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\AppUser]].
 *
 * @see \app\models\AppUser
 */
class AppUserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\AppUser[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AppUser|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}