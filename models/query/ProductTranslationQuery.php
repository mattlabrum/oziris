<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\ProductTranslation]].
 *
 * @see \app\models\ProductTranslation
 */
class ProductTranslationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\ProductTranslation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\ProductTranslation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}