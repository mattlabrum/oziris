<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\ShippingBox]].
 *
 * @see \app\models\ShippingBox
 */
class ShippingBoxQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\ShippingBox[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\ShippingBox|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}