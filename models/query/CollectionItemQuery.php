<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\CollectionItem]].
 *
 * @see \app\models\CollectionItem
 */
class CollectionItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\CollectionItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\CollectionItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}