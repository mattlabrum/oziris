<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\AuditDatum]].
 *
 * @see \app\models\AuditDatum
 */
class AuditDatumQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\AuditDatum[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\AuditDatum|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}