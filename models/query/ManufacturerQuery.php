<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Manufacturer]].
 *
 * @see \app\models\Manufacturer
 */
class ManufacturerQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        $this->andWhere(['deleted_at'=>null]);
        return $this;
    }

    /**
     * @inheritdoc
     * @return \app\models\Manufacturer[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\Manufacturer|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}