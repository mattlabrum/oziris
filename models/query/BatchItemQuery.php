<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\BatchItem]].
 *
 * @see \app\models\BatchItem
 */
class BatchItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \app\models\BatchItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\BatchItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}