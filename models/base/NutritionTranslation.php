<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "nutrition_translation".
 *
 * @property integer $id
 * @property integer $language_id
 * @property integer $deleted_at
 * @property integer $nutrition_id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 */
class NutritionTranslation extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nutrition_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'language_id', 'deleted_at', 'nutrition_id', 'name', 'created_at', 'updated_at'],
            'create' => ['id', 'language_id', 'deleted_at', 'nutrition_id', 'name', 'created_at', 'updated_at'],
            'update' => ['id', 'language_id', 'deleted_at', 'nutrition_id', 'name', 'created_at', 'updated_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'deleted_at', 'nutrition_id', 'name'], 'required'],
            [['language_id', 'deleted_at', 'nutrition_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']],
            [['nutrition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nutrition::className(), 'targetAttribute' => ['nutrition_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'nutrition_id' => Yii::t('app', 'Nutrition ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\NutritionTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\NutritionTranslationQuery(get_called_class());
    }

}
