<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "shipping_pallet".
 *
 * @property integer $id
 * @property string $pallet_qr
 * @property integer $deleted_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class ShippingPallet extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipping_pallet';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'pallet_qr', 'deleted_at', 'created_at', 'updated_at'],
            'create' => ['id', 'pallet_qr', 'deleted_at', 'created_at', 'updated_at'],
            'update' => ['id', 'pallet_qr', 'deleted_at', 'created_at', 'updated_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['deleted_at'], 'integer'],
            [['pallet_qr'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'pallet_qr' => Yii::t('models', 'Pallet Qr'),
            'created_at' => Yii::t('models', 'Created At'),
            'updated_at' => Yii::t('models', 'Updated At'),
            'deleted_at' => Yii::t('models', 'Deleted At'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ShippingPalletQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ShippingPalletQuery(get_called_class());
    }

}
