<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_nutritions".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $nutrition_id
 * @property string $avg_qty_per_serving
 * @property string $avg_qty_per_100g
 *
 * @property \app\models\Nutrition $nutrition
 * @property \app\models\Product $product
 */
class ProductNutrition extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_nutritions';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'nutrition_id', 'avg_qty_per_serving', 'avg_qty_per_100g'],
            'create' => ['id', 'product_id', 'nutrition_id', 'avg_qty_per_serving', 'avg_qty_per_100g'],
            'update' => ['id', 'product_id', 'nutrition_id', 'avg_qty_per_serving', 'avg_qty_per_100g'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'nutrition_id'], 'integer'],
            [['avg_qty_per_serving', 'avg_qty_per_100g'], 'string', 'max' => 255],
            [['nutrition_id'], 'exist', 'skipOnError' => true, 'targetClass' => Nutrition::className(), 'targetAttribute' => ['nutrition_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'nutrition_id' => Yii::t('app', 'Nutrition ID'),
            'avg_qty_per_serving' => Yii::t('app', 'Avg Qty Per Serving'),
            'avg_qty_per_100g' => Yii::t('app', 'Avg Qty Per 100g'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutrition()
    {
        return $this->hasOne(Nutrition::className(), ['id' => 'nutrition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductNutritionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductNutritionQuery(get_called_class());
    }

}
