<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "hash_tags".
 *
 * @property integer $id
 * @property string $hash_tag
 * @property integer $product_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \app\models\HashTagsTranslation[] $hashTagsTranslations
 */
class HashTag extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hash_tags';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'hash_tag', 'product_id', 'created_at', 'updated_at'],
            'create' => ['id', 'hash_tag', 'product_id', 'created_at', 'updated_at'],
            'update' => ['id', 'hash_tag', 'product_id', 'created_at', 'updated_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash_tag', 'product_id'], 'required'],
            [['product_id', 'created_at', 'updated_at'], 'integer'],
            [['hash_tag'], 'string', 'max' => 255],
            [['product_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hash_tag' => Yii::t('app', 'Hash Tag'),
            'product_id' => Yii::t('app', 'Product ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHashTagsTranslations()
    {
        return $this->hasMany(\app\models\HashTagsTranslation::className(), ['hash_tags_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\HashTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\HashTagQuery(get_called_class());
    }

}
