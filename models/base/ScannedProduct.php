<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "scanned_product".
 *
 * @property integer $id
 * @property integer $batch_id
 * @property integer $user_id
 * @property string $batch_number
 * @property string $qr_code
 * @property string $product_code
 * @property string $continent
 * @property double $scanned_longitude
 * @property double $scanned_latitude
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \app\models\Batch $batch
 * @property \app\models\AppUser $user
 */
class ScannedProduct extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scanned_product';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'batch_id', 'user_id', 'batch_number', 'qr_code', 'product_code', 'scanned_longitude', 'scanned_latitude', 'created_at', 'updated_at', 'continent'],
            'create' => ['id', 'batch_id', 'user_id', 'batch_number', 'qr_code', 'product_code', 'scanned_longitude', 'scanned_latitude', 'created_at', 'updated_at', 'continent'],
            'update' => ['id', 'batch_id', 'user_id', 'batch_number', 'qr_code', 'product_code', 'scanned_longitude', 'scanned_latitude', 'created_at', 'updated_at', 'continent'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['batch_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['user_id'], 'required'],
            [['scanned_longitude', 'scanned_latitude'], 'number'],
            [['batch_number', 'qr_code', 'product_code', 'continent'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'batch_id' => Yii::t('app', 'Batch ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'batch_number' => Yii::t('app', 'Batch Number'),
            'qr_code' => Yii::t('app', 'Qr Code'),
            'product_code' => Yii::t('app', 'Product Code'),
            'scanned_longitude' => Yii::t('app', 'Scanned Longitude'),
            'scanned_latitude' => Yii::t('app', 'Scanned Latitude'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'continent' => Yii::t('app', 'Continent'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatch()
    {
        return $this->hasOne(\app\models\Batch::className(), ['id' => 'batch_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getapp_user()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ScannedProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ScannedProductQuery(get_called_class());
    }

}
