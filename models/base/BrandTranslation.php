<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "brand_translation".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property integer $language_id
 * @property string $brand_name
 * @property string $brand_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property \app\models\Brand $brand
 * @property \app\models\Language $language
 */
class BrandTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'brand_id', 'language_id', 'brand_name', 'brand_description', 'created_at', 'updated_at', 'deleted_at'],
            'create' => ['id', 'brand_id', 'language_id', 'brand_name', 'brand_description', 'created_at', 'updated_at', 'deleted_at'],
            'update' => ['id', 'brand_id', 'language_id', 'brand_name', 'brand_description', 'created_at', 'updated_at', 'deleted_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_id', 'language_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['brand_name', 'brand_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'brand_name' => Yii::t('app', 'Brand Name'),
            'brand_description' => Yii::t('app', 'Brand Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(\app\models\Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\BrandTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\BrandTranslationQuery(get_called_class());
    }

}
