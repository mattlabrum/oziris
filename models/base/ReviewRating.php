<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "review_rating".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $brand_id
 * @property integer $product_id
 * @property integer $manufacturer_id
 * @property integer $comment_id
 * @property integer $rating
 * @property string $review
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \app\models\ReportedReview $reportedReview
 * @property \app\models\AppUser $user
 * @property \app\models\Brand $brand
 * @property \app\models\Product $product
 * @property \app\models\Manufacturer $manufacturer
 */
class ReviewRating extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'review_rating';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'user_id', 'brand_id', 'product_id', 'manufacturer_id', 'rating', 'review', 'created_at', 'updated_at','comment_id'],
            'create' => ['id', 'user_id', 'brand_id', 'product_id', 'manufacturer_id', 'rating', 'review', 'created_at', 'updated_at','comment_id'],
            'update' => ['id', 'user_id', 'brand_id', 'product_id', 'manufacturer_id', 'rating', 'review', 'created_at', 'updated_at','comment_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'rating'], 'required'],
            [['user_id', 'brand_id', 'product_id', 'manufacturer_id', 'rating', 'created_at', 'updated_at','comment_id'], 'integer'],
            [['review'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'brand_id' => Yii::t('app', 'Brand Name'),
            'product_id' => Yii::t('app', 'Product Name'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer Name'),
            'rating' => Yii::t('app', 'Rating'),
            'review' => Yii::t('app', 'Review'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportedReview()
    {
        return $this->hasOne(\app\models\ReportedReview::className(), ['rating_review_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getapp_user()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(\app\models\Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(\app\models\Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ReviewRatingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ReviewRatingQuery(get_called_class());
    }

}
