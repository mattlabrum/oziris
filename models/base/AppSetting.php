<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "app_settings".
 *
 * @property integer $id
 * @property string $promo_image
 * @property string $promo_url
 * @property integer $promo_product_id
 * @property string $promo_search_string
 */
class AppSetting extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_settings';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'promo_image', 'promo_url', 'promo_product_id', 'promo_search_string'],
            'create' => ['id', 'promo_image', 'promo_url', 'promo_product_id', 'promo_search_string'],
            'update' => ['id', 'promo_image', 'promo_url', 'promo_product_id', 'promo_search_string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promo_product_id'], 'integer'],
            [['promo_image', 'promo_url', 'promo_search_string'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'promo_image' => Yii::t('models', 'Promo Image'),
            'promo_url' => Yii::t('models', 'Promo Url'),
            'promo_product_id' => Yii::t('models', 'Promo Product ID'),
            'promo_search_string' => Yii::t('models', 'Promo Search String'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\AppSettingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AppSettingQuery(get_called_class());
    }

}
