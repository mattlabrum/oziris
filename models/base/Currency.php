<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "currency".
 *
 * @property integer $id
 * @property string $name
 * @property string $sign
 *
 * @property \app\models\Order[] $orders
 */
class Currency extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name', 'sign'],
            'create' => ['id', 'name', 'sign'],
            'update' => ['id', 'name', 'sign'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sign'], 'required'],
            [['name', 'sign'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'sign' => Yii::t('app', 'Sign'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\app\models\Order::className(), ['currency_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\CurrencyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CurrencyQuery(get_called_class());
    }

}
