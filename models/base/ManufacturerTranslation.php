<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "manufacturer_translation".
 *
 * @property integer $id
 * @property integer $language_id
 * @property string $manufacturer_name
 * @property string $manufacturer_description
 * @property integer $manufacturer_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property \app\models\Language $language
 * @property \app\models\Manufacturer $manufacturer
 */
class ManufacturerTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'language_id', 'manufacturer_name', 'manufacturer_description', 'manufacturer_id', 'created_at', 'updated_at', 'deleted_at'],
            'create' => ['id', 'language_id', 'manufacturer_name', 'manufacturer_description', 'manufacturer_id', 'created_at', 'updated_at', 'deleted_at'],
            'update' => ['id', 'language_id', 'manufacturer_name', 'manufacturer_description', 'manufacturer_id', 'created_at', 'updated_at', 'deleted_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language_id', 'manufacturer_name', 'manufacturer_description'], 'required'],
            [['language_id', 'manufacturer_id', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['manufacturer_name', 'manufacturer_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'manufacturer_name' => Yii::t('app', 'Manufacturer Name'),
            'manufacturer_description' => Yii::t('app', 'Manufacturer Description'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(\app\models\Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ManufacturerTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ManufacturerTranslationQuery(get_called_class());
    }

}
