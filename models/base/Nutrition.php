<?php

namespace app\models\base;

use app\models\NutritionTranslation;
use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "nutrition".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \app\models\ProductNutrition[] $productNutritions
 */
class Nutrition extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nutrition';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name'],
            'create' => ['id', 'name'],
            'update' => ['id', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductNutritions()
    {
        return $this->hasMany(\app\models\ProductNutrition::className(), ['nutrition_id' => 'id']);
    }

    public function getNutritionTranslation($language_code)
    {
        $nutritionTranslation = NutritionTranslation::find()->where(['nutrition_id' => $this->id])->andWhere(['language_id' => $language_code])->one();
        if (isset($nutritionTranslation)) {
            return $nutritionTranslation->name;
        }else{
            return $this->name;
        }

    }


    
    /**
     * @inheritdoc
     * @return \app\models\query\NutritionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\NutritionQuery(get_called_class());
    }

}
