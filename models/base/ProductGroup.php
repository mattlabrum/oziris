<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_group".
 *
 * @property integer $id
 * @property string $product_group_name
 * @property integer $show_on_menu
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property \app\models\Product[] $products
 * @property \app\models\ProductGroupTranslation[] $productGroupTranslations
 */
class ProductGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_group';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_group_name', 'show_on_menu' , 'created_at', 'updated_at', 'deleted_at'],
            'create' => ['id', 'product_group_name', 'show_on_menu' , 'created_at', 'updated_at', 'deleted_at'],
            'update' => ['id', 'product_group_name', 'show_on_menu' , 'created_at', 'updated_at', 'deleted_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_group_name'], 'required'],
            [['created_at', 'updated_at', 'deleted_at', 'show_on_menu'], 'integer'],
            [['product_group_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_group_name' => Yii::t('app', 'Product Group Name'),
            'show_on_menu' => Yii::t('app', 'Show On Menu'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(\app\models\Product::className(), ['product_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroupTranslations()
    {
        return $this->hasMany(\app\models\ProductGroupTranslation::className(), ['product_group_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductGroupQuery(get_called_class());
    }

}
