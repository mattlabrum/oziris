<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "ingredient_translation".
 *
 * @property integer $id
 * @property string $name
 * @property integer $language_id
 * @property integer $ingredient_id
 */
class IngredientTranslation extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredient_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name', 'language_id', 'ingredient_id'],
            'create' => ['id', 'name', 'language_id', 'ingredient_id'],
            'update' => ['id', 'name', 'language_id', 'ingredient_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['language_id', 'ingredient_id'], 'integer'],
            [['name'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'language_id' => Yii::t('app', 'Language ID'),
            'ingredient_id' => Yii::t('app', 'Ingredient ID'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\IngredientTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\IngredientTranslationQuery(get_called_class());
    }

}
