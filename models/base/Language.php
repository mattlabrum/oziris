<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $logo
 * @property string $code
 *
 * @property \app\models\BrandTranslation[] $brandTranslations
 * @property \app\models\HashTagsTranslation[] $hashTagsTranslations
 * @property \app\models\ManufacturerTranslation[] $manufacturerTranslations
 * @property \app\models\ProductGroupTranslation $productGroupTranslation
 * @property \app\models\ProductTranslation[] $productTranslations
 * @property \app\models\ProductTypeTranslation[] $productTypeTranslations
 */
class Language extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name', 'logo', 'code'],
            'create' => ['id', 'name', 'logo', 'code'],
            'update' => ['id', 'name', 'logo', 'code'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'logo', 'code'], 'required'],
            [['name'], 'string', 'max' => 30],
            [['logo'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'logo' => Yii::t('app', 'Logo'),
            'code' => Yii::t('app', 'Code'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandTranslations()
    {
        return $this->hasMany(\app\models\BrandTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHashTagsTranslations()
    {
        return $this->hasMany(\app\models\HashTagsTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturerTranslations()
    {
        return $this->hasMany(\app\models\ManufacturerTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroupTranslation()
    {
        return $this->hasOne(\app\models\ProductGroupTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations()
    {
        return $this->hasMany(\app\models\ProductTranslation::className(), ['language_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTypeTranslations()
    {
        return $this->hasMany(\app\models\ProductTypeTranslation::className(), ['language_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\LanguageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\LanguageQuery(get_called_class());
    }

}
