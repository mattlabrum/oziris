<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_type_translation".
 *
 * @property integer $id
 * @property integer $product_type_id
 * @property integer $language_id
 * @property string $type_name
 *
 * @property \app\models\ProductType $productType
 * @property \app\models\Language $language
 */
class ProductTypeTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_type_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_type_id', 'language_id', 'type_name'],
            'create' => ['id', 'product_type_id', 'language_id', 'type_name'],
            'update' => ['id', 'product_type_id', 'language_id', 'type_name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_type_id', 'language_id', 'type_name'], 'required'],
            [['product_type_id', 'language_id'], 'integer'],
            [['type_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_type_id' => Yii::t('app', 'Product Type ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'type_name' => Yii::t('app', 'Type Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(\app\models\ProductType::className(), ['id' => 'product_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductTypeTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductTypeTranslationQuery(get_called_class());
    }

}
