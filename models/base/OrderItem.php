<?php

namespace app\models\base;

use app\models\query\OrderItemQuery;
use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "order_items".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $order_id
 * @property integer $quantity
 * @property string $price
 * @property string $tax
 * @property string $actual_price
 * @property string $actual_tax
 *
 * @property Product $product
 */
class OrderItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_items';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'order_id', 'quantity'],
            'create' => ['id', 'product_id', 'order_id', 'quantity'],
            'update' => ['id', 'product_id', 'order_id', 'quantity'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'order_id', 'quantity'], 'required'],
            [['product_id', 'order_id', 'quantity'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatchID()
    {
        $items = BatchItem::find()->joinWith('batch')->where(['batch.product_id' => $this->product_id, 'order_id' => $this->order_id])->all();
        if(isset($items)){
            $batches = '';
            foreach($items as $item){
//                if(strpos($batches,(string)$item->batch_id)  === false){
                    $batches .= $item->batch_id.' , ';
//                }
            }
            return $batches;
        }else{
            return '';
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\OrderItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderItemQuery(get_called_class());
    }

}
