<?php

namespace app\models\base;

use app\models\NutritionTagTranslation;
use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "nutrition_tags".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \app\models\ProductNutritionTag[] $productNutritionTags
 */
class NutritionTag extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nutrition_tags';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name'],
            'create' => ['id', 'name'],
            'update' => ['id', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductNutritionTags()
    {
        return $this->hasMany(\app\models\ProductNutritionTag::className(), ['nutrition_tag_id' => 'id']);
    }

    /**
     * @return NutritionTagTranslation $nutritionTagTranslation
     */
    public function getNutritionTagTranslation($language_id)
    {
        $nutritionTagTranslation = NutritionTagTranslation::find()->where(['nutrition_tag_id' => $this->id, 'language_id' => $language_id ])->one();
        if(isset($nutritionTagTranslation)){
            return $nutritionTagTranslation->name;
        }else{
            return $this->name;
        }
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\NutritionTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\NutritionTagQuery(get_called_class());
    }

}
