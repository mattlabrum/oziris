<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_cross_sell".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $cross_sell_product_id
 */
class ProductCrossSell extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_cross_sell';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'cross_sell_product_id'],
            'create' => ['id', 'product_id', 'cross_sell_product_id'],
            'update' => ['id', 'product_id', 'cross_sell_product_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'cross_sell_product_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'product_id' => Yii::t('models', 'Product ID'),
            'cross_sell_product_id' => Yii::t('models', 'Cross Sell Product ID'),
        ];
    }

    public function getProduct(){
        return Product::find()->where(['id' => $this->product_id])->one();
    }

    public function getCrossSellProduct(){
        return Product::find()->where(['id' => $this->cross_sell_product_id])->one();
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductCrossSellQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductCrossSellQuery(get_called_class());
    }

}
