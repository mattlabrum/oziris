<?php

namespace app\models\base;

use app\models\Address;
use app\models\OrderItem;
use \Yii;
use yii\data\ActiveDataProvider;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "order".
 *
 * @property integer $id
 * @property integer $status_id
 * @property integer $currency_id
 * @property double $subtotal
 * @property integer $shipping_id
 * @property integer $shipping_address_id
 * @property integer $billing_address_id
 * @property integer $customer_id
 * @property integer $payment_id
 * @property string $order_note
 * @property string $customer_ip
 * @property string $customer_user_agent
 * @property double $tax_amount
 * @property double $tax_id
 * @property double $total_amount
 * @property integer $woo_order_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $completed_id
 * @property integer $barcode
 * @property string $leave_at_door
 * @property string $qr_code
 * @property string $boxes
 * @property string $coupon_code
 * @property string $discounted_amount
 * @property string $trading_channel
 * @property double $shipping_amount
 *
 * @property string $subscription_alias
 * @property string $parent_billing_period
 * @property string $parent_billing_interval
 * @property string $parent_schedule_next_payment
 * @property string $parent_subscription_delivery_day
 * @property string $subscription_delivery_date
 * @property string $subscription_id
 * @property string $delivery_date
 *
 * @property \app\models\Currency $currency
 * @property \app\models\AppUser $customer
 * @property \app\models\Payment $payment
 * @property \app\models\Address $shippingAddress
 * @property \app\models\Address $billingAddress
 * @property \app\models\Shipping $shipping
 * @property \app\models\Tax $tax
 * @property \app\models\OrderStatus $status
 * @property \app\models\OrderItem[] $orderItems
 */
class Order extends ActiveRecord
{
    public $shippingAddress = null;
    public $billingAddress = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'status_id', 'currency_id', 'subtotal', 'barcode', 'billing_address_id', 'shipping_address_id', 'trading_channel' , 'customer_id', 'payment_id', 'order_note', 'customer_ip', 'customer_user_agent', 'tax_amount', 'total_amount', 'woo_order_id', 'qr_code', 'leave_at_door', 'boxes', 'shipping_amount', 'subscription_delivery_date'],
            'create' => ['id', 'status_id', 'currency_id', 'subtotal', 'barcode', 'billing_address_id', 'shipping_address_id', 'trading_channel', 'customer_id', 'payment_id', 'order_note', 'customer_ip', 'customer_user_agent', 'tax_amount', 'total_amount', 'woo_order_id', 'qr_code', 'leave_at_door', 'boxes', 'shipping_amount', 'subscription_delivery_date' ],
            'update' => ['id', 'status_id', 'currency_id', 'subtotal', 'barcode', 'billing_address_id', 'shipping_address_id', 'trading_channel', 'customer_id', 'payment_id', 'order_note', 'customer_ip', 'customer_user_agent', 'tax_amount', 'total_amount', 'woo_order_id', 'qr_code', 'leave_at_door', 'boxes', 'shipping_amount', 'subscription_delivery_date' ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status_id', 'currency_id', 'subtotal' , 'customer_id', 'tax_amount', 'total_amount'], 'required'],
            [['status_id', 'currency_id', 'billing_address_id', 'shipping_address_id', 'customer_id', 'payment_id', 'woo_order_id', 'boxes'], 'integer'],
            [['subtotal', 'tax_amount', 'total_amount'], 'number'],
            [['order_note', 'customer_user_agent', 'barcode', 'coupon_code','discounted_amount', 'trading_channel'], 'string'],
            [['contact_no'], 'safe'],
            [['customer_ip'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status_id' => Yii::t('app', 'Status ID'),
            'currency_id' => Yii::t('app', 'Currency ID'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'payment_id' => Yii::t('app', 'Payment ID'),
            'order_note' => Yii::t('app', 'Order Note'),
            'customer_ip' => Yii::t('app', 'Customer Ip'),
            'customer_user_agent' => Yii::t('app', 'Customer User Agent'),
            'tax_amount' => Yii::t('app', 'Tax Amount'),
            'barcode' => Yii::t('app', 'Barcode'),
            'boxes' => Yii::t('app', 'Barcode'),
            'total_amount' => Yii::t('app', 'Total Amount'),
            'woo_order_id' => Yii::t('app', 'Order #'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(\app\models\Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShippingAddress()
    {
        return $this->hasOne(\app\models\Address::className(), ['id' => 'shipping_address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBillingAddress()
    {
        return $this->hasOne(\app\models\Address::className(), ['id' => 'billing_address_id']);
    }

    public function getContactNo() {
        if($this->getBillingAddress()->one()){
            return $this->getBillingAddress()->one()->contact_no;
        }else{
            return '';
        }


    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatusTimes()
    {
        $order_status_times = OrderStatusTime::find()->where(['order_id' => $this->woo_order_id])->andWhere(['>' , 'sort' , 0])->orderBy('sort')->all();

        foreach($order_status_times as $order_status_time){

        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderStatus()
    {
        return $this->hasMany(\app\models\OrderStatus::className(), ['id' => 'status_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(\app\models\Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShipping()
    {
        return $this->hasOne(\app\models\Shipping::className(), ['id' => 'shipping_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(\app\models\OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTax()
    {
        return $this->hasOne(\app\models\Tax::className(), ['id' => 'tax_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
       return OrderItem::find()->where(['order_id' => $this->woo_order_id])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItemsDataProvider()
    {
        $query = OrderItem::find()->where(['order_id' => $this->woo_order_id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;

    }

    /**
     * @inheritdoc
     * @return \app\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\OrderQuery(get_called_class());
    }

}
