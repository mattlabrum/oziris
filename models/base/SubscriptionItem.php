<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "subscription_items".
 *
 * @property integer $id
 * @property integer $subscription_id
 * @property integer $subscription_item_id
 * @property integer $quantity
 * @property double $price
 *
 * @property \app\models\Product $subscriptionItem
 * @property \app\models\Subscription $subscription
 */
class SubscriptionItem extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_items';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'subscription_id', 'subscription_item_id', 'quantity', 'price'],
            'create' => ['id', 'subscription_id', 'subscription_item_id', 'quantity', 'price'],
            'update' => ['id', 'subscription_id', 'subscription_item_id', 'quantity', 'price'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'subscription_item_id', 'quantity'], 'integer'],
            [['price'], 'number'],
            [['subscription_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Product::className(), 'targetAttribute' => ['subscription_item_id' => 'id']],
            [['subscription_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Subscription::className(), 'targetAttribute' => ['subscription_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'subscription_id' => Yii::t('models', 'Subscription ID'),
            'subscription_item_id' => Yii::t('models', 'Subscription Item ID'),
            'quantity' => Yii::t('models', 'Quantity'),
            'price' => Yii::t('models', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionItem()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'subscription_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(\app\models\Subscription::className(), ['id' => 'subscription_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\SubscriptionItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SubscriptionItemQuery(get_called_class());
    }

}
