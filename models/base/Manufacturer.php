<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "manufacturer".
 *
 * @property integer $id
 * @property string $manufacturer_name
 * @property double $manufacturer_location_latitude
 * @property double $manufacturer_location_longitude
 * @property string $manufacturer_logo
 * @property string $manufacturer_video
 * @property string $manufacturer_description
 * @property string $location
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property \app\models\Brand[] $brands
 * @property \app\models\ManufacturerTranslation[] $manufacturerTranslations
 * @property \app\models\Product[] $products
 * @property \app\models\ReviewRating[] $reviewRatings
 */
class Manufacturer extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'manufacturer';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'created_at', 'updated_at', 'deleted_at'],
            'create' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'created_at', 'updated_at', 'deleted_at'],
            'update' => ['id', 'manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'created_at', 'updated_at', 'deleted_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description'], 'required'],
            [['manufacturer_location_latitude', 'manufacturer_location_longitude'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['manufacturer_name', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'manufacturer_name' => Yii::t('app', 'Manufacturer Name'),
            'manufacturer_location_latitude' => Yii::t('app', 'Manufacturer Location Latitude'),
            'manufacturer_location_longitude' => Yii::t('app', 'Manufacturer Location Longitude'),
            'manufacturer_logo' => Yii::t('app', 'Manufacturer Logo'),
            'manufacturer_video' => Yii::t('app', 'Manufacturer Video'),
            'manufacturer_description' => Yii::t('app', 'Manufacturer Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrands()
    {
        return \app\models\Brand::findAll(['manufacturer_id' => $this->id,'deleted_at' => null]);

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturerTranslations()
    {
        return $this->hasMany(\app\models\ManufacturerTranslation::className(), ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(\app\models\Product::className(), ['manufacturer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewRatings()
    {
        return $this->hasMany(\app\models\ReviewRating::className(), ['manufacturer' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ManufacturerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ManufacturerQuery(get_called_class());
    }

}
