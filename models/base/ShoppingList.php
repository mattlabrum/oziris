<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "shopping_list".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 */
class ShoppingList extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopping_list';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'user_id', 'name'],
            'create' => ['id', 'user_id', 'name'],
            'update' => ['id', 'user_id', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name'], 'required'],
            [['user_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => AppUser::className(), 'targetAttribute' => ['user_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ShoppingListQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ShoppingListQuery(get_called_class());
    }

}
