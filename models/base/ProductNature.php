<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_nature".
 *
 * @property integer $id
 * @property string $name
 */
class ProductNature extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_nature';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name'],
            'create' => ['id', 'name'],
            'update' => ['id', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductNatureQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductNatureQuery(get_called_class());
    }

}
