<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "shipping".
 *
 * @property integer $id
 * @property string $name
 * @property double $amount
 *
 * @property \app\models\Order[] $orders
 */
class Shipping extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipping';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name', 'amount'],
            'create' => ['id', 'name', 'amount'],
            'update' => ['id', 'name', 'amount'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'amount'], 'required'],
            [['amount'], 'number'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'amount' => Yii::t('app', 'Amount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\app\models\Order::className(), ['shipping_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ShippingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ShippingQuery(get_called_class());
    }

}
