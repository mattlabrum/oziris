<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "subscription".
 *
 * @property integer $id
 * @property integer $subscription_id
 * @property integer $start_date
 * @property integer $next_payment_date
 * @property string $billing_period
 * @property string $billing_interval
 * @property string $status
 * @property string $subscription_name
 * @property string $delivery_day
 * @property integer $next_delivery_date
 * @property integer $customer_id
 * @property integer $subscription_billing_address_id
 * @property integer $subscription_shipping_address_id
 * @property string $payment_method
 * @property string $payment_title
 * @property string $stript_customer_id
 * @property string $stripe_card_id
 *
 * @property \app\models\AppUser $customer
 * @property \app\models\SubscriptionItem[] $subscriptionItems
 * @property \app\models\SubscriptionOrder[] $subscriptionOrders
 */
class Subscription extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'subscription_id', 'start_date', 'next_payment_date', 'billing_period', 'billing_interval', 'status', 'subscription_name', 'delivery_day', 'next_delivery_date', 'customer_id', 'subscription_billing_address_id', 'subscription_shipping_address_id', 'payment_method', 'payment_title', 'stript_customer_id', 'stripe_card_id'],
            'create' => ['id', 'subscription_id', 'start_date', 'next_payment_date', 'billing_period', 'billing_interval', 'status', 'subscription_name', 'delivery_day', 'next_delivery_date', 'customer_id', 'subscription_billing_address_id', 'subscription_shipping_address_id', 'payment_method', 'payment_title', 'stript_customer_id', 'stripe_card_id'],
            'update' => ['id', 'subscription_id', 'start_date', 'next_payment_date', 'billing_period', 'billing_interval', 'status', 'subscription_name', 'delivery_day', 'next_delivery_date', 'customer_id', 'subscription_billing_address_id', 'subscription_shipping_address_id', 'payment_method', 'payment_title', 'stript_customer_id', 'stripe_card_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'customer_id', 'subscription_billing_address_id', 'subscription_shipping_address_id'], 'integer'],
            [['start_date','next_payment_date', 'next_delivery_date' ], 'string', 'max' => 255],
            [['billing_period', 'billing_interval', 'status', 'subscription_name', 'delivery_day', 'payment_method', 'payment_title', 'stript_customer_id', 'stripe_card_id'], 'string', 'max' => 15],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\AppUser::className(), 'targetAttribute' => ['customer_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'start_date' => Yii::t('models', 'Start Date'),
            'next_payment_date' => Yii::t('models', 'Next Payment Date'),
            'billing_period' => Yii::t('models', 'Billing Period'),
            'billing_interval' => Yii::t('models', 'Billing Interval'),
            'status' => Yii::t('models', 'Status'),
            'subscription_name' => Yii::t('models', 'Subscription Name'),
            'delivery_day' => Yii::t('models', 'Delivery Day'),
            'next_delivery_date' => Yii::t('models', 'Next Delivery Date'),
            'customer_id' => Yii::t('models', 'Customer ID'),
            'subscription_billing_address_id' => Yii::t('models', 'Subscription Billing Address ID'),
            'subscription_shipping_address_id' => Yii::t('models', 'Subscription Shipping Address ID'),
            'payment_method' => Yii::t('models', 'Payment Method'),
            'payment_title' => Yii::t('models', 'Payment Title'),
            'stript_customer_id' => Yii::t('models', 'Stripe Customer ID'),
            'subscription_id' => Yii::t('models', 'Subscription ID'),
            'stripe_card_id' => Yii::t('models', 'Stripe Card ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionItems()
    {
        return $this->hasMany(\app\models\SubscriptionItem::className(), ['subscription_id' => 'subscription_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionOrders()
    {
        return $this->hasMany(\app\models\SubscriptionOrder::className(), ['subscription_id' => 'subscription_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\SubscriptionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SubscriptionQuery(get_called_class());
    }

}
