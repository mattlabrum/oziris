<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "address".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address_1
 * @property string $address_2
 * @property string $type
 * @property string $contact_no
 * @property string $email
 * @property string $suburb
 * @property string $city
 * @property string $state
 * @property string $postcode
 * @property string $country
 * @property string $continent
 *
 * @property \app\models\Order $order
 */
class Address extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'address_1', 'address_2', 'type', 'suburb', 'city', 'state', 'postcode', 'country', 'first_name', 'last_name', 'contact_no', 'email', 'continent'],
            'create' => ['id', 'address_1', 'address_2', 'type', 'suburb', 'city', 'state', 'postcode', 'country', 'first_name', 'last_name', 'contact_no', 'email', 'continent'],
            'update' => ['id', 'address_1', 'address_2', 'type', 'suburb', 'city', 'state', 'postcode', 'country', 'first_name', 'last_name', 'contact_no', 'email', 'continent'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_1', 'suburb', 'city', 'state', 'postcode', 'country'], 'required'],
            [['order_id'], 'integer'],
            [['first_name', 'last_name', 'address_1', 'address_2', 'type', 'email', 'continent'], 'string', 'max' => 255],
            [['contact_no','suburb', 'city', 'state', 'country'], 'string', 'max' => 50],
            [['postcode'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'address_1' => Yii::t('app', 'Address 1'),
            'address_2' => Yii::t('app', 'Address 2'),
            'order_id' => Yii::t('app', 'Order ID'),
            'type' => Yii::t('app', 'Type'),
            'suburb' => Yii::t('app', 'Suburb'),
            'city' => Yii::t('app', 'City'),
            'state' => Yii::t('app', 'State'),
            'postcode' => Yii::t('app', 'Postcode'),
            'contact_no' => Yii::t('app', 'Contact Number'),
            'country' => Yii::t('app', 'Country'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'order_id']);
    }

    /**
     * @return string contact_no
     */
    public function getAddress()
    {
        $address = $this->address_1 . PHP_EOL .($this->address_2!='' ? $this->address_2 . PHP_EOL : '' )  . $this->suburb .", ". $this->state . PHP_EOL . $this->postcode . PHP_EOL .$this->country;
        return $address;
    }

    /**
     * @return string contact_no
     */
    public function getContactNo()
    {
        $phone_no = preg_replace('/\s+/', '', $this->contact_no);;
        if(substr($phone_no,0,3) != '+61'){
            $phone_no = '+61'.substr($phone_no,1,strlen($phone_no));
        }
        return $phone_no;
    }
    
    /**
     * @inheritdoc
     * @return \app\models\query\AddressQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AddressQuery(get_called_class());
    }

}
