<?php

namespace app\models\base;


use app\models\ProductNutrition;
use \Yii;
use \yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the base-model class for table "product".
 *
 * @property integer $id
 * @property string $product_number
 * @property string $barcode
 * @property integer $product_group_id
 * @property string $product_name
 * @property string $product_description
 * @property integer $product_type_id
 * @property integer $brand_id
 * @property string $unit_size
 * @property string $unit_size_kg
 * @property string $shelf_life
 * @property integer $shelf_life_unit
 * @property integer $service_type_id
 * @property integer $unit_per_box
 * @property string $sample_availability
 * @property string $annual_supply
 * @property string $minimum_order
 * @property string $available_since
 * @property integer $product_nature_id
 * @property string $product_image
 * @property string $nutrition_image
 * @property string $nutrition_text
 * @property integer $manufacturer_id
 * @property integer $is_collection
 * @property string  $serves_per_pack
 * @property string  $serve_size
 * @property string  $width
 * @property string  $height
 * @property string  $length
 * @property integer $preorder_enabled
 * @property string $logistics
 * @property integer $is_beston_pure_product
 * @property integer $is_top_seller
 * @property integer $is_featured
 * @property integer $is_eatable
 * @property integer $show_batch
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property \app\models\ProductRelatedImage[] $productImages
 * @property \app\models\ProductNutritionTag[] $product_nutrition_tags
 * @property \app\models\ProductNutrition[] $productNutrition
 * @property \app\models\Batch[] $defaultBatch
 * @property \app\models\ProductIngredient[] $productIngredients
 * @property \app\models\Batch[] $batches
 * @property \app\models\CollectionItem[] $collectionItems
 * @property \app\models\ProductNature $productNature
 * @property \app\models\Brand $brand
 * @property \app\models\Manufacturer $manufacturer
 * @property \app\models\ProductGroup $productGroup
 * @property \app\models\ServiceType $serviceType
 * @property \app\models\ShelfLifeUnit $shelfLifeUnit
 * @property \app\models\ProductType $productType
 * @property \app\models\ProductLocationAttribute $productLocationAttribute
 * @property \app\models\ProductTranslation[] $productTranslations
 * @property \app\models\ReviewRating[] $reviewRatings
 */
class Product extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_number', 'barcode', 'product_group_id', 'product_name','is_top_seller', 'is_featured', 'is_eatable' ,  'product_description', 'productQuantity', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection', 'created_at', 'updated_at', 'deleted_at','serves_per_pack','serve_size','width','height','length','product_nutrition_tags'],
            'create' => ['id', 'product_number', 'barcode', 'product_group_id', 'product_name','is_top_seller', 'is_featured', 'is_eatable' , 'product_description', 'productQuantity', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection' , 'created_at', 'updated_at', 'deleted_at','serves_per_pack','serve_size','width','height','length','product_nutrition_tags'],
            'update' => ['id', 'product_number', 'barcode', 'product_group_id', 'product_name','is_top_seller', 'is_featured', 'is_eatable' , 'product_description', 'productQuantity', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacturer_id', 'is_collection' , 'created_at', 'updated_at', 'deleted_at','serves_per_pack','serve_size','width','height','length','product_nutrition_tags'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_number', 'product_group_id', 'product_name', 'product_description', 'product_type_id', 'brand_id', 'unit_size', 'unit_size_kg', 'shelf_life', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_nature_id', 'nutrition_text', 'manufacturer_id'], 'required'],
            [['product_group_id', 'product_type_id', 'brand_id', 'shelf_life_unit', 'service_type_id', 'unit_per_box', 'product_nature_id', 'manufacturer_id', 'is_eatable', 'is_collection', 'created_at', 'updated_at', 'reorder_notification' , 'deleted_at'], 'integer'],
            [['product_description', 'nutrition_text','serves_per_pack','serve_size','width','height','length'], 'string'],
            [['product_nutrition_tags'], 'array'],
            [['product_number', 'barcode', 'product_name', 'unit_size', 'unit_size_kg', 'shelf_life', 'sample_availability', 'annual_supply', 'minimum_order', 'available_since', 'product_image', 'nutrition_image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_number' => Yii::t('app', 'Product Number'),
            'barcode' => Yii::t('app', 'Barcode'),
            'product_group_id' => Yii::t('app', 'Product Group ID'),
            'product_name' => Yii::t('app', 'Product Name'),
            'product_description' => Yii::t('app', 'Product Description'),
            'product_type_id' => Yii::t('app', 'Product Type ID'),
            'brand_id' => Yii::t('app', 'Brand ID'),
            'unit_size' => Yii::t('app', 'Unit Size'),
            'unit_size_kg' => Yii::t('app', 'Unit Size Kg'),
            'shelf_life' => Yii::t('app', 'Shelf Life'),
            'shelf_life_unit' => Yii::t('app', 'Shelf Life Unit'),
            'service_type_id' => Yii::t('app', 'Service Type ID'),
            'unit_per_box' => Yii::t('app', 'Unit Per Box'),
            'sample_availability' => Yii::t('app', 'Sample Availability'),
            'annual_supply' => Yii::t('app', 'Annual Supply'),
            'minimum_order' => Yii::t('app', 'Minimum Order'),
            'available_since' => Yii::t('app', 'Available Since'),
            'product_nature_id' => Yii::t('app', 'Product Nature ID'),
            'product_image' => Yii::t('app', 'Product Image'),
            'nutrition_image' => Yii::t('app', 'Nutrition Image'),
            'nutrition_text' => Yii::t('app', 'Nutrition Text'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer ID'),
            'is_collection' => Yii::t('app', 'Is Collection'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'is_eatable' => Yii::t('app', 'Is Edible'),

        ];
    }

    /**
     * @return integer $quantity
     */
    public function getProductQuantity()
    {
        $quantity = 0;
        if($this->is_collection){

            $collectionItems = CollectionItem::find()->where(['collection_id' => $this->id])->all();

            foreach ($collectionItems as $item){
                $item_quantity = 0;
                foreach($item->getProduct()->one()->getBatches()->all() as $batch){
                    $item_quantity += BatchItem::find()->where(['batch_id' => $batch->id, 'item_status' => '' ])->count('*');
                }
                if($quantity!= 0){
                    if($item_quantity == 0 ){
                        $quantity = 0;
                        return;
                    }
                    if($item_quantity< $quantity){
                        $quantity = $item_quantity;
                    }
                }else{
                    $quantity = $item_quantity;
                }

            }

//            $quantity = 1000;
        }else{
            foreach($this->getBatches()->all() as $batch){
                $quantity += BatchItem::find()->where(['batch_id' => $batch->id, 'item_status' => '' ])->count('*');
            }
            if($quantity == 0 && $this->preorder_enabled == 1){
                $quantity = -1;
            }
        }
        return $quantity;
    }

    /**
     * @return integer $sold quantity
     */
    public function getProductSoldQuantity()
    {
        $quantity = 0;
        foreach ($this->getBatches()->all() as $batch) {
            $quantity += BatchItem::find()->select('item_status')->where(['batch_id' => $batch->id, 'item_status' => 'sold'])->count('*');
        }
        return $quantity;
    }

    /**
     * @return integer $sold quantity
     */
    public function getProductDiscardQuantity()
    {
        $quantity = 0;
        foreach ($this->getBatches()->all() as $batch) {
            $quantity += BatchItem::find()->select('item_status')->where(['batch_id' => $batch->id, 'item_status' => 'discard'])->count('*');
        }
        return $quantity;
    }

    /**
     * @return integer $sold quantity
     */
    public function getTheoraticalQuantity()
    {
        return  $this->getProductBatchLevelQuantity() - ($this->getProductDiscardQuantity() + $this->getProductSoldQuantity());
    }

    /**
     * @return integer $sold quantity
     */
    public function getProductVariance()
    {
        return $this->getProductQuantity() - $this->getTheoraticalQuantity();
    }

    /**
     * @return integer $theoQuantity
     */
    public function getProductBatchLevelQuantity()
    {
        $quantity = 0;

        foreach($this->getBatches()->all() as $batch){
            $quantity += $batch->quantity;
        }
        return $quantity;
    }

    /**
     * @return Batch[]
     */
    public function getBatches()
    {
        return Batch::find()->where(['product_id' => $this->id , 'deleted_at' => NULL]);
    }

    /**
     * @return Batch
     */
    public function getDefaultBatch()
    {
        return Batch::find()->where(['batch_number' => $this->id])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollectionItem()
    {
        return $this->findOne(\app\models\CollectionItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return ProductRelatedImages[] as image URL strings WITH featured image
     */
    public function getProductImages()
    {
        $rawImages =  ProductRelatedImage::find()->where(['product_id' => $this->id ])->all();
        $images = [];
        array_push($images, Url::to('@web/uploads/product') . '/' .$this->product_image);

        foreach($rawImages as $rawImage){
            array_push($images, Url::to('@web/uploads/product') . '/'.$rawImage->image_url);
        }

        return$images;
    }

    /**
     * @return ProductRelatedImages[] as image URL strings without featured image
     */
    public function getProductRelatedImages()
    {
        $rawImages =  ProductRelatedImage::find()->where(['product_id' => $this->id ])->all();
        $images = [];
        foreach($rawImages as $rawImage){
            array_push($images, 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product') . '/'.$rawImage->image_url);
        }
        return$images;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInitialPreviewConfig()
    {

        $rawImages = ProductRelatedImage::find()->where(['product_id' => $this->id])->all();
        $images = [];

        $image = [
            'key' => $this->product_image,
        ];
        array_push($images,$image);
        foreach ($rawImages as $rawImage) {
            $image = [
                'key' => $rawImage->image_url,
            ];
            array_push($images,$image);
        }

//        print_r($images);

        return $images;
    }



//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getCollectionItems()
//    {
//        return CollectionItem::find()->where(['collection_id' => $this->id ])->all();
//    }

    /**
     * @return Ingredients
     */
    public function getIngredients()
    {
        $ingredients = $this->hasMany(\app\models\ProductIngredient::className(), ['product_id' => 'id'])->all();
        $ingredients_to_return = [];
        foreach($ingredients as $ingredient){
            $ingredients_to_return[] = [
                'ingredient_id' => trim($ingredient->ingredient->id),
                'en_ingredient_name' => trim($ingredient->ingredient->getIngredientTranslation(0)),
                'ch_ingredient_name' => trim($ingredient->ingredient->getIngredientTranslation(1))
            ];
        }
        return $ingredients_to_return;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionTags()
    {
        $tags = $this->hasMany(\app\models\ProductNutritionTag::className(), ['product_id' => 'id'])->all();
        $tags_to_return = [];
        foreach($tags as $tag){

            $tags_to_return[] = [
                'nutrition_id' => trim($tag->nutritionTag->id),
                'en_nutrition_tags' => trim($tag->nutritionTag->getNutritionTagTranslation(0)),
                'ch_nutrition_tags' => trim($tag->nutritionTag->getNutritionTagTranslation(1))
            ];
        }


        return $tags_to_return;
    }

    /**
     * @return Nutrition
     */
    public function getNutrition()
    {
        $nutritions = ProductNutrition::find()->where(['product_id' => $this->id])->all();
//        $this->hasMany(ProductNutrition::className(), ['product_id' => 'id'])->all();
        $nutrition_to_return = [];
        foreach($nutritions as $nutrition){
            if($nutrition->avg_qty_per_serving != '' && $nutrition->avg_qty_per_100g != '' ){
                $nutrition_to_return[] = [
                    'nutrition_id' => trim($nutrition->nutrition->id),
                    'en_nutrition' => trim($nutrition->nutrition->getNutritionTranslation(0)),
                    'ch_nutrition' => trim($nutrition->nutrition->getNutritionTranslation(1)),
                    'qty_per_serving' => $nutrition->avg_qty_per_serving,
                    'qty_per_100g' => $nutrition->avg_qty_per_100g,
                ];
            }

        }


        return $nutrition_to_return;
    }

    public function getNoOfReviewsforProduct()
    {
        $raw_reviews = ReviewRating::findAll(['product_id' => $this->id]);
        return count($raw_reviews);
    }

    public function getAverageProductRating()
    {
        $raw_reviews = ReviewRating::find()->where(['product_id' => $this->id])->all();

        $totalReviews = count($raw_reviews);
        $averageRating = 0;

        if($totalReviews > 0){
            $totalRating = 0;
            foreach($raw_reviews as $review){
                $totalRating += $review->rating;
            }
            $averageRating = $totalRating/$totalReviews;
        }else{
            $averageRating =0;
        }
        return round($averageRating,0);
    }

    public function getProductReviews()
    {
        $raw_reviews = ReviewRating::findAll(['product_id' => $this->id]);

        $reviews = [];
        foreach($raw_reviews as $review){
            $user = AppUser::find()->where(['id' => $review->user_id])->one();
            $reviews[] = [
                'review' => $review->review,
                'rating' => $review->rating,
                'review_date' => date('d/M/Y h:i A',$review->created_at ),
                'comment_id' => $review->comment_id,
                'user_id' => $user->email_address,
                'user_name' => $user->first_name.' '.$user->last_name,
            ];
        }
        return $reviews;
    }



    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductIngredients()
    {
        return $this->hasMany(\app\models\ProductIngredient::className(), ['product_id' => 'id'])->all();
    }

    public function getProductNutritionTagsText()
    {
        $tags = $this->hasMany(\app\models\ProductNutritionTag::className(), ['product_id' => 'id'])->all();
        $tags_to_return = '';
        foreach($tags as $tag){
            $tags_to_return .= $tag->nutritionTag->name.', ';
        }


        return rtrim($tags_to_return,', ');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getproduct_nutrition_tags()
    {
        $tags = $this->hasMany(\app\models\ProductNutritionTag::className(), ['product_id' => 'id'])->all();
        $tags_to_return = [];
        foreach($tags as $tag){

            array_push($tags_to_return,$tag->nutritionTag->id);
        }


        return $tags_to_return;//\yii\helpers\ArrayHelper::map(ProductNutritionTag::find()->where(['product_id' => 'id'])->all(), 'id', 'name');//$this->hasMany(\app\models\ProductNutritionTag::className(), ['product_id' => 'id'])->asArray()->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductNutrition()
    {

        $nutritions = ProductNutrition::find()->where(['product_id' => $this->id])->andWhere(['!=','avg_qty_per_serving', ''])->andWhere(['!=','avg_qty_per_100g', ''])->all();
        foreach ($nutritions as $nutrition){
            $nutrition->avg_qty_per_100g = $nutrition->avg_qty_per_100g .' '. substr($nutrition->nutrition->name,strpos($nutrition->nutrition->name,'('),strpos($nutrition->nutrition->name,')')  );
            $nutrition->avg_qty_per_serving = $nutrition->avg_qty_per_serving .' '. substr($nutrition->nutrition->name,strpos($nutrition->nutrition->name,'('),strpos($nutrition->nutrition->name,')')  );
        }
        return $nutritions;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductNature()
    {
        return $this->hasOne(\app\models\ProductNature::className(), ['id' => 'product_nature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(\app\models\Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(\app\models\Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroup()
    {
        return $this->hasOne(\app\models\ProductGroup::className(), ['id' => 'product_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceType()
    {
        return $this->hasOne(\app\models\ServiceType::className(), ['id' => 'service_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShelfLifeUnit()
    {
        return $this->hasOne(\app\models\ShelfLifeUnit::className(), ['id' => 'shelf_life_unit']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(\app\models\ProductType::className(), ['id' => 'product_type_id']);
    }

    /**
     * @return string $imageUrl
     */
    public function getProductImageUrl()
    {
            return 'https://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/product') . '/' . $this->product_image;
    }


    /**
     * @return string $price
     */
    public function getProductPrice($language_id)
    {
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $this->id,'language_id'=>$language_id])->one();
        if(isset($productLocationAttributes)){
            return $productLocationAttributes->price;
        }else {
            return 0.0;
        }

    }

    /**
     * @return string $price
     */
    public function getProductSalePrice($language_id)
    {
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $this->id,'language_id'=>$language_id])->one();
        if(isset($productLocationAttributes)){
            return $productLocationAttributes->sale_price;
        }else {
            return 0.0;
        }

    }

    /**
     * @return string $taxable
     */
    public function getProductTaxable($language_id)
    {
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $this->id,'language_id'=>$language_id])->one();
        if(isset($productLocationAttributes)){
            return $productLocationAttributes->taxable;
        }else {
            return 0;
        }

    }

    /**
     * @return integer $quantity
     */
    public function getBestonQuantity($language_id)
    {
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $this->id,'language_id'=>$language_id])->one();
        if(isset($productLocationAttributes)){
            return $productLocationAttributes->beston_quantity;
        }else {
            return 0;
        }

    }

    /**
     * @return string $price
     */
    public function getEcommerceEnabled($language_id)
    {
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $this->id,'language_id'=>$language_id])->one();
        if(isset($productLocationAttributes)){
            return $productLocationAttributes->ecommerce_enabled;
        }else {
            return 0;
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductPrices()
    {
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $this->id]);
            return $productLocationAttributes;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTranslations()
    {
        return $this->hasMany(\app\models\ProductTranslation::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewRatings()
    {
        return $this->hasMany(\app\models\ReviewRating::className(), ['product' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductQuery(get_called_class());
    }

}
