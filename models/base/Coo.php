<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "coo".
 *
 * @property integer $id
 * @property double $coo_latitude
 * @property double $coo_longitude
 * @property string $city
 * @property string $country
 * @property string $flag
 */
class Coo extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coo';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'coo_latitude', 'coo_longitude', 'city', 'country', 'flag'],
            'create' => ['id', 'coo_latitude', 'coo_longitude', 'city', 'country', 'flag'],
            'update' => ['id', 'coo_latitude', 'coo_longitude', 'city', 'country', 'flag'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coo_latitude', 'coo_longitude'], 'number'],
            [['city', 'country', 'flag'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'coo_latitude' => Yii::t('models', 'Coo Latitude'),
            'coo_longitude' => Yii::t('models', 'Coo Longitude'),
            'city' => Yii::t('models', 'City'),
            'country' => Yii::t('models', 'Country'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\CooQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CooQuery(get_called_class());
    }

}
