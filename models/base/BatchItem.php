<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "batch_items".
 *
 * @property integer $id
 * @property integer $batch_id
 * @property string $qr_code_text
 * @property string $item_status
 * @property integer $box_id
 * @property integer $order_id
 * @property integer $location_id
 *
 * @property \app\models\Product $product
 * @property \app\models\Batch $batch
 */
class BatchItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'batch_items';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'batch_id', 'qr_code_text', 'item_status', 'box_id', 'order_id', 'location_id'],
            'create' => ['id', 'batch_id', 'qr_code_text', 'item_status', 'box_id', 'order_id', 'location_id'],
            'update' => ['id', 'batch_id', 'qr_code_text', 'item_status', 'box_id', 'order_id', 'location_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['batch_id', 'box_id'], 'integer'],
            [['qr_code_text', 'item_status'], 'string', 'max' => 20],
            [['batch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Batch::className(), 'targetAttribute' => ['batch_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'batch_id' => Yii::t('app', 'Batch ID'),
            'qr_code_text' => Yii::t('app', 'Qr Code Text'),
            'item_status' => Yii::t('app', 'Item Status'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatch()
    {
        return $this->hasOne(\app\models\Batch::className(), ['id' => 'batch_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\BatchItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\BatchItemQuery(get_called_class());
    }

}
