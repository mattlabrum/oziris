<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "app_social_media_auth".
 *
 * @property integer $id
 * @property string $provider
 * @property integer $access_id
 * @property integer $user_id
 * @property string $access_token
 * @property string $expiration_date
 *
 * @property \app\models\AppUser $user
 */
class AppSocialMediaAuth extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_social_media_auth';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'provider', 'access_id', 'user_id', 'access_token', 'expiration_date'],
            'create' => ['id', 'provider', 'access_id', 'user_id', 'access_token', 'expiration_date'],
            'update' => ['id', 'provider', 'access_id', 'user_id', 'access_token', 'expiration_date'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['provider', 'access_id', 'user_id', 'access_token', 'expiration_date'], 'required'],
            [['access_id', 'user_id'], 'integer'],
            [['access_token'], 'string'],
            [['provider'], 'string', 'max' => 15],
            [['expiration_date'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'provider' => Yii::t('app', 'Provider'),
            'access_id' => Yii::t('app', 'Access ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'access_token' => Yii::t('app', 'Access Token'),
            'expiration_date' => Yii::t('app', 'Expiration Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\AppSocialMediaAuthQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AppSocialMediaAuthQuery(get_called_class());
    }

}
