<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_nutrition_tags".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $nutrition_tag_id
 *
 * @property \app\models\NutritionTag $nutritionTag
 * @property \app\models\Product $product
 */
class ProductNutritionTag extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_nutrition_tags';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'nutrition_tag_id'],
            'create' => ['id', 'product_id', 'nutrition_tag_id'],
            'update' => ['id', 'product_id', 'nutrition_tag_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'nutrition_tag_id'], 'integer'],
            [['nutrition_tag_id'], 'exist', 'skipOnError' => true, 'targetClass' => NutritionTag::className(), 'targetAttribute' => ['nutrition_tag_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'nutrition_tag_id' => Yii::t('app', 'Nutrition Tag ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNutritionTag()
    {
        return $this->hasOne(\app\models\NutritionTag::className(), ['id' => 'nutrition_tag_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductNutritionTagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductNutritionTagQuery(get_called_class());
    }

}
