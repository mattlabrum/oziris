<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "coupons".
 *
 * @property integer $id
 * @property string $code
 * @property integer $date_created
 * @property integer $date_modified
 * @property string $discount_type
 * @property string $description
 * @property double $amount
 * @property integer $expiry_date
 * @property integer $individual_use
 * @property integer $usage_limit
 * @property integer $usage_limit_per_user
 * @property integer $limit_usage_to_x_items
 * @property integer $free_shipping
 * @property integer $exclude_sale_items
 * @property integer $woo_coupon_id
 * @property double $minimum_amount
 * @property double $maximum_amount
 * @property string $email_restrictions
 *
 *
 * @property \app\models\CouponProduct[] $couponProducts
 */
class Coupon extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupons';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'code', 'date_created', 'date_modified', 'discount_type', 'description', 'amount', 'expiry_date', 'individual_use', 'usage_limit', 'usage_limit_per_user', 'limit_usage_to_x_items', 'free_shipping', 'exclude_sale_items', 'minimum_amount', 'maximum_amount', 'email_restrictions'],
            'create' => ['id', 'code', 'date_created', 'date_modified', 'discount_type', 'description', 'amount', 'expiry_date', 'individual_use', 'usage_limit', 'usage_limit_per_user', 'limit_usage_to_x_items', 'free_shipping', 'exclude_sale_items', 'minimum_amount', 'maximum_amount', 'email_restrictions'],
            'update' => ['id', 'code', 'date_created', 'date_modified', 'discount_type', 'description', 'amount', 'expiry_date', 'individual_use', 'usage_limit', 'usage_limit_per_user', 'limit_usage_to_x_items', 'free_shipping', 'exclude_sale_items', 'minimum_amount', 'maximum_amount', 'email_restrictions'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date_created', 'date_modified', 'individual_use', 'usage_limit', 'usage_limit_per_user', 'limit_usage_to_x_items', 'free_shipping', 'exclude_sale_items'], 'integer'],
            [['amount', 'minimum_amount', 'maximum_amount'], 'number'],
            [['email_restrictions', 'description'], 'string'],
            [['code', 'discount_type'], 'string', 'max' => 255],
            [['code', 'discount_type'], 'required'],
//            [['couponProducts'], 'array'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'code' => Yii::t('models', 'Code'),
            'date_created' => Yii::t('models', 'Date Created'),
            'date_modified' => Yii::t('models', 'Date Modified'),
            'discount_type' => Yii::t('models', 'Discount Type'),
            'description' => Yii::t('models', 'Description'),
            'amount' => Yii::t('models', 'Amount'),
            'expiry_date' => Yii::t('models', 'Expiry Date'),
            'individual_use' => Yii::t('models', 'Individual Use'),
            'usage_limit' => Yii::t('models', 'Usage Limit'),
            'usage_limit_per_user' => Yii::t('models', 'Usage Limit Per User'),
            'limit_usage_to_x_items' => Yii::t('models', 'Limit Usage To X Items'),
            'free_shipping' => Yii::t('models', 'Free Shipping'),
            'exclude_sale_items' => Yii::t('models', 'Exclude Sale Items'),
            'minimum_amount' => Yii::t('models', 'Minimum Amount'),
            'maximum_amount' => Yii::t('models', 'Maximum Amount'),
            'email_restrictions' => Yii::t('models', 'Email Restrictions'),
        ];
    }

    public function getId()
    {
        return $this->hasOne(\app\models\Coupon::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCouponProducts()
    {
        return $this->hasMany(\app\models\CouponProduct::className(), ['coupon_id' => 'id'])->select(['product_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\CouponQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CouponQuery(get_called_class());
    }

}
