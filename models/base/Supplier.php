<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the base-model class for table "supplier".
 *
 * @property integer $id
 * @property string $supplier_name
 * @property double $supplier_location_latitude
 * @property double $supplier_location_longitude
 * @property string $supplier_logo
 * @property string $supplier_description
 * @property integer $deleted_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \app\models\BatchIngredient[] $batchIngredients
 */
class Supplier extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'supplier_name', 'supplier_location_latitude', 'supplier_location_longitude', 'supplier_logo', 'supplier_description', 'deleted_at', 'created_at', 'updated_at', 'city', 'country'],
            'create' => ['id', 'supplier_name', 'supplier_location_latitude', 'supplier_location_longitude', 'supplier_logo', 'supplier_description', 'deleted_at', 'created_at', 'updated_at', 'city', 'country'],
            'update' => ['id', 'supplier_name', 'supplier_location_latitude', 'supplier_location_longitude', 'supplier_logo', 'supplier_description', 'deleted_at', 'created_at', 'updated_at', 'city', 'country'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supplier_name', 'supplier_description','supplier_location_latitude', 'supplier_location_longitude'], 'required'],
            [['supplier_location_latitude', 'supplier_location_longitude'], 'number'],
            [['supplier_description', 'city', 'country'], 'string'],
            [['deleted_at'], 'integer'],
            [['supplier_name'], 'string', 'max' => 55],
            [['supplier_logo'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'supplier_name' => Yii::t('app', 'Supplier Name'),
            'supplier_location_latitude' => Yii::t('app', 'Supplier Location Latitude'),
            'supplier_location_longitude' => Yii::t('app', 'Supplier Location Longitude'),
            'supplier_logo' => Yii::t('app', 'Supplier Logo'),
            'supplier_description' => Yii::t('app', 'Supplier Description'),
            'country' => Yii::t('app', 'Country'),
            'city' => Yii::t('app', 'City'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatchIngredients()
    {
        return $this->hasMany(\app\models\BatchIngredient::className(), ['supplier_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\SupplierQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SupplierQuery(get_called_class());
    }

    /**
     * @return string $imageUrl
     */
    public function getSupplierImageUrl()
    {
        return 'https://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/supplier') . '/' . $this->supplier_logo;
    }

    /**
     * @return string $supplierAddress
     */
    public function getSupplierAddress()
    {

        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($this->supplier_location_latitude).','.trim($this->supplier_location_longitude).'&sensor=false';
        $get     = file_get_contents($url);
//        return $url;
        $geoData = json_decode($get);
//        print_r($geoData);

        return $geoData->results[0]->formatted_address;

//        if(isset($geoData->results->formatted_address)) {
//
//        }
        return null;

    }

}
