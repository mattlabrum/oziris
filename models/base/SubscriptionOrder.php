<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "subscription_orders".
 *
 * @property integer $id
 * @property integer $subscription_id
 * @property integer $subscription_order_id
 *
 * @property \app\models\Order $subscriptionOrder
 * @property \app\models\Subscription $subscription
 */
class SubscriptionOrder extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription_orders';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'subscription_id', 'subscription_order_id'],
            'create' => ['id', 'subscription_id', 'subscription_order_id'],
            'update' => ['id', 'subscription_id', 'subscription_order_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subscription_id', 'subscription_order_id'], 'integer'],
            [['subscription_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Order::className(), 'targetAttribute' => ['subscription_order_id' => 'woo_order_id']],
            [['subscription_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Subscription::className(), 'targetAttribute' => ['subscription_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'subscription_id' => Yii::t('models', 'Subscription ID'),
            'subscription_order_id' => Yii::t('models', 'Subscription Order ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscriptionOrder()
    {
        return $this->hasOne(\app\models\Order::className(), ['woo_order_id' => 'subscription_order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscription()
    {
        return $this->hasOne(\app\models\Subscription::className(), ['id' => 'subscription_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\SubscriptionOrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SubscriptionOrderQuery(get_called_class());
    }

}
