<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "budget".
 *
 * @property integer $id
 * @property integer $month
 * @property integer $year
 * @property string $budget_type
 * @property string $budget_amount
 */
class Budget extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'budget';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'month', 'year', 'budget_type', 'budget_amount'],
            'create' => ['id', 'month', 'year', 'budget_type', 'budget_amount'],
            'update' => ['id', 'month', 'year', 'budget_type', 'budget_amount'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['month', 'year'], 'integer'],
            [['budget_type', 'budget_amount'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'month' => Yii::t('models', 'Month'),
            'year' => Yii::t('models', 'Year'),
            'budget_type' => Yii::t('models', 'Budget Type'),
            'budget_amount' => Yii::t('models', 'Budget Amount'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\BudgetQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\BudgetQuery(get_called_class());
    }

}
