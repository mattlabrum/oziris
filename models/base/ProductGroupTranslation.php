<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_group_translation".
 *
 * @property integer $id
 * @property integer $product_group_id
 * @property integer $language_id
 * @property string $product_group_name
 *
 * @property \app\models\ProductGroup $productGroup
 * @property \app\models\Language $language
 */
class ProductGroupTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_group_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_group_id', 'language_id', 'product_group_name'],
            'create' => ['id', 'product_group_id', 'language_id', 'product_group_name'],
            'update' => ['id', 'product_group_id', 'language_id', 'product_group_name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_group_id', 'language_id', 'product_group_name'], 'required'],
            [['product_group_id', 'language_id'], 'integer'],
            [['product_group_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_group_id' => Yii::t('app', 'Product Group ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'product_group_name' => Yii::t('app', 'Product Group Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroup()
    {
        return $this->hasOne(\app\models\ProductGroup::className(), ['id' => 'product_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductGroupTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductGroupTranslationQuery(get_called_class());
    }

}
