<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_upsell".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $upsell_product_id
 *
 * @property Product $product
 * @property Product $upsellProduct
 */
class ProductUpsell extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_upsell';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'upsell_product_id'],
            'create' => ['id', 'product_id', 'upsell_product_id'],
            'update' => ['id', 'product_id', 'upsell_product_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'upsell_product_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'product_id' => Yii::t('models', 'Product ID'),
            'upsell_product_id' => Yii::t('models', 'Upsell Product ID'),
        ];
    }

    public function getProduct(){
        return Product::find()->where(['id' => $this->product_id])->one();
    }

    public function getUpsellProduct(){
        return Product::find()->where(['id' => $this->upsell_product_id])->one();
    }


    /**
     * @inheritdoc
     * @return \app\models\query\ProductUpsellQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductUpsellQuery(get_called_class());
    }

}
