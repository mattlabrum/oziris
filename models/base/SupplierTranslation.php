<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "supplier_translation".
 *
 * @property integer $id
 * @property string $supplier_name
 * @property string $supplier_description
 * @property integer $language_id
 * @property integer $supplier_id
 */
class SupplierTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supplier_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'supplier_name', 'supplier_description', 'language_id', 'supplier_id'],
            'create' => ['id', 'supplier_name', 'supplier_description', 'language_id', 'supplier_id'],
            'update' => ['id', 'supplier_name', 'supplier_description', 'language_id', 'supplier_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['supplier_name', 'supplier_description'], 'required'],
            [['language_id', 'supplier_id'], 'integer'],
            [['supplier_name', 'supplier_description'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'supplier_name' => Yii::t('app', 'Supplier Name'),
            'supplier_description' => Yii::t('app', 'Supplier Description'),
            'language_id' => Yii::t('app', 'Language ID'),
            'supplier_id' => Yii::t('app', 'Supplier ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\SupplierTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\SupplierTranslationQuery(get_called_class());
    }

}
