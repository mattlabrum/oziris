<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "app_auth_data".
 *
 * @property integer $id
 * @property string $password_hash
 * @property integer $user_id
 *
 * @property \app\models\AppUser $user
 */
class AppAuthDatum extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_auth_data';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'password_hash', 'user_id'],
            'create' => ['id', 'password_hash', 'user_id'],
            'update' => ['id', 'password_hash', 'user_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['password_hash'], 'string'],
            [['user_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'user_id' => Yii::t('app', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\AppAuthDatumQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AppAuthDatumQuery(get_called_class());
    }

}
