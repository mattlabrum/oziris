<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "ingredient".
 *
 * @property integer $id
 * @property string $name
 * @property integer $group_id
 *
 * @property \app\models\BatchIngredient[] $batchIngredients
 * @property \app\models\IngredientGroup $group
 * @property \app\models\ProductIngredient[] $productIngredients
 */
class Ingredient extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredient';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name', 'group_id'],
            'create' => ['id', 'name', 'group_id'],
            'update' => ['id', 'name', 'group_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id'], 'integer'],
            [['group_id' , 'name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => IngredientGroup::className(), 'targetAttribute' => ['group_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'group_id' => Yii::t('app', 'Group ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatchIngredients()
    {
        return $this->hasMany(\app\models\BatchIngredient::className(), ['ingredient_id' => 'id']);
    }

    /**
     * @return IngredientTranslation $ingredientTranslation
     */
    public function getIngredientTranslation($language_id)
    {
        $ingredientTranslation = IngredientTranslation::find()->where(['ingredient_id' => $this->id, 'language_id' => $language_id ])->one();
        if(isset($ingredientTranslation)){
            return $ingredientTranslation->name;
        }else{
            return $this->name;
        }


    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(\app\models\IngredientGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductIngredients()
    {
        return $this->hasMany(\app\models\ProductIngredient::className(), ['ingredient_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\IngredientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\IngredientQuery(get_called_class());
    }

}
