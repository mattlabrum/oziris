<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_type".
 *
 * @property integer $id
 * @property string $icon
 * @property string $type_name
 * @property integer $show_on_menu
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 *
 * @property \app\models\Product[] $products
 * @property \app\models\ProductTypeTranslation[] $productTypeTranslations
 */
class ProductType extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_type';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'icon', 'type_name','show_on_menu', 'created_at', 'updated_at', 'deleted_at'],
            'create' => ['id', 'icon', 'type_name','show_on_menu', 'created_at', 'updated_at', 'deleted_at'],
            'update' => ['id', 'icon', 'type_name','show_on_menu', 'created_at', 'updated_at', 'deleted_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['icon', 'type_name'], 'required'],
            [['created_at','show_on_menu', 'updated_at', 'deleted_at'], 'integer'],
            [['icon', 'type_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'icon' => Yii::t('app', 'Icon'),
            'show_on_menu' => Yii::t('app', 'Show On Menu'),
            'type_name' => Yii::t('app', 'Type Name'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(\app\models\Product::className(), ['product_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductTypeTranslations()
    {
        return $this->hasMany(\app\models\ProductTypeTranslation::className(), ['product_type_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductTypeQuery(get_called_class());
    }

}
