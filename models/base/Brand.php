<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the base-model class for table "brand".
 *
 * @property integer $id
 * @property string $brand_name
 * @property double $brand_location_latitude
 * @property double $brand_location_longitude
 * @property string $brand_logo
 * @property string $brand_video
 * @property string $brand_description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property integer $manufacturer_id
 *
 * @property \app\models\Manufacturer $manufacturer
 * @property \app\models\BrandTranslation[] $brandTranslations
 * @property \app\models\Product[] $products
 * @property \app\models\ReviewRating[] $reviewRatings
 */
class Brand extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_logo', 'brand_video', 'brand_description', 'created_at', 'updated_at', 'deleted_at', 'manufacturer_id'],
            'create' => ['id', 'brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_logo', 'brand_video', 'brand_description', 'created_at', 'updated_at', 'deleted_at', 'manufacturer_id'],
            'update' => ['id', 'brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_logo', 'brand_video', 'brand_description', 'created_at', 'updated_at', 'deleted_at', 'manufacturer_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_logo', 'brand_description', 'manufacturer_id'], 'required'],
            [['brand_location_latitude', 'brand_location_longitude'], 'number'],
            [['created_at', 'updated_at', 'deleted_at', 'manufacturer_id'], 'integer'],
            [['brand_name', 'brand_logo', 'brand_video', 'brand_description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'brand_name' => Yii::t('app', 'Brand Name'),
            'brand_location_latitude' => Yii::t('app', 'Brand Location Latitude'),
            'brand_location_longitude' => Yii::t('app', 'Brand Location Longitude'),
            'brand_logo' => Yii::t('app', 'Brand Logo'),
            'brand_video' => Yii::t('app', 'Brand Video'),
            'brand_description' => Yii::t('app', 'Brand Description'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManufacturer()
    {
        return $this->hasOne(\app\models\Manufacturer::className(), ['id' => 'manufacturer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrandTranslations()
    {
        return $this->hasMany(\app\models\BrandTranslation::className(), ['brand_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(\app\models\Product::className(), ['brand_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewRatings()
    {
        return $this->hasMany(\app\models\ReviewRating::className(), ['brand' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\BrandQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\BrandQuery(get_called_class());
    }

}
