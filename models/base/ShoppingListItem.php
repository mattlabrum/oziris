<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "shopping_list_items".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $shopping_list_id
 * @property Product $product
  */
class ShoppingListItem extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shopping_list_items';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'quantity', 'shopping_list_id'],
            'create' => ['id', 'product_id', 'quantity', 'shopping_list_id'],
            'update' => ['id', 'product_id', 'quantity', 'shopping_list_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'quantity'], 'required'],
            [['product_id', 'quantity', 'shopping_list_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return \app\models\Product::find()->where(['id' => $this->product_id])->one();
    }


    /**
     * @inheritdoc
     * @return \app\models\query\ShoppingListItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ShoppingListItemQuery(get_called_class());
    }

}
