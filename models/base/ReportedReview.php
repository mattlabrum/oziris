<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "reported_review".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $rating_review_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property \app\models\ReviewRating $ratingReview
 * @property \app\models\AppUser $user
 */
class ReportedReview extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reported_review';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'user_id', 'rating_review_id', 'created_at', 'updated_at'],
            'create' => ['id', 'user_id', 'rating_review_id', 'created_at', 'updated_at'],
            'update' => ['id', 'user_id', 'rating_review_id', 'created_at', 'updated_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'rating_review_id'], 'required'],
            [['user_id', 'rating_review_id', 'created_at', 'updated_at'], 'integer'],
            [['rating_review_id'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'rating_review_id' => Yii::t('app', 'Rating Review ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingReview()
    {
        return $this->hasOne(\app\models\ReviewRating::className(), ['id' => 'rating_review_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\AppUser::className(), ['id' => 'user_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ReportedReviewQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ReportedReviewQuery(get_called_class());
    }

}
