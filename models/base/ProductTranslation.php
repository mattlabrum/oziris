<?php

namespace app\models\base;

use bedezign\yii2\audit\AuditTrailBehavior;
use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_translation".
 *
 * @property integer $id
 * @property string $nutrition_text
 * @property integer $product_id
 * @property integer $language_id
 * @property string $product_name
 * @property string $nutrition_image
 * @property string $product_description
 *
 * @property \app\models\Product $product
 * @property \app\models\Language $language
 */
class ProductTranslation extends ActiveRecord
{
    public function behaviors()
    {
        return array(
            AuditTrailBehavior::className(),
        );
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'nutrition_text', 'nutrition_image', 'product_id', 'language_id', 'product_name', 'product_description'],
            'create' => ['id', 'nutrition_text', 'nutrition_image', 'product_id', 'language_id', 'product_name', 'product_description'],
            'update' => ['id', 'nutrition_text', 'nutrition_image', 'product_id', 'language_id', 'product_name', 'product_description'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nutrition_text', 'product_id', 'language_id', 'product_name', 'product_description'], 'required'],
            [['nutrition_text', 'nutrition_image', 'product_description'], 'string'],
            [['product_id', 'language_id'], 'integer'],
            [['product_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nutrition_text' => Yii::t('app', 'Nutrition Text'),
            'product_id' => Yii::t('app', 'Product ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'product_name' => Yii::t('app', 'Product Name'),
            'nutrition_image'  => Yii::t('app', 'Nutrition Image'),
            'product_description' => Yii::t('app', 'Product Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductTranslationQuery(get_called_class());
    }

}
