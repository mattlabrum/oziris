<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "shipping_box".
 *
 * @property integer $id
 * @property string $box_qr
 * @property integer $order_id
 * @property integer $pallet_id
 * @property integer $deleted_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class ShippingBox extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipping_box';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'box_qr', 'order_id', 'pallet_id', 'deleted_at', 'created_at', 'updated_at'],
            'create' => ['id', 'box_qr', 'order_id', 'pallet_id', 'deleted_at', 'created_at', 'updated_at'],
            'update' => ['id', 'box_qr', 'order_id', 'pallet_id', 'deleted_at', 'created_at', 'updated_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'pallet_id', 'deleted_at'], 'integer'],
            [['box_qr'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'box_qr' => Yii::t('models', 'Box Qr'),
            'order_id' => Yii::t('models', 'Order ID'),
            'pallet_id' => Yii::t('models', 'Pallet ID'),
            'created_at' => Yii::t('models', 'Created At'),
            'updated_at' => Yii::t('models', 'Updated At'),
            'deleted_at' => Yii::t('models', 'Deleted At'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ShippingBoxQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ShippingBoxQuery(get_called_class());
    }

}
