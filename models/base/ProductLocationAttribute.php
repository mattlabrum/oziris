<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_location_attributes".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $language_id
 * @property integer $taxable
 * @property double $sale_price
 * @property double $price
 * @property integer $reorder_threshold
 * @property integer $beston_quantity
 * @property integer $ecommerce_enabled
 * @property string $currency
 * @property string $code
 *
 * @property Product $product
 */
class ProductLocationAttribute extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_location_attributes';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'language_id', 'price', 'sale_price', 'reorder_threshold', 'beston_quantity', 'ecommerce_enabled', 'currency', 'code','taxable'],
            'create' => ['id', 'product_id', 'language_id', 'price', 'sale_price', 'reorder_threshold', 'beston_quantity', 'ecommerce_enabled', 'currency', 'code','taxable'],
            'update' => ['id', 'product_id', 'language_id', 'price', 'sale_price','reorder_threshold', 'beston_quantity', 'ecommerce_enabled', 'currency', 'code','taxable'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'language_id', 'reorder_threshold', 'beston_quantity', 'ecommerce_enabled', 'currency'], 'required'],
            [['product_id', 'language_id', 'reorder_threshold', 'beston_quantity', 'ecommerce_enabled','taxable'], 'integer'],
            [['price'], 'number'],
            [['currency', 'code'], 'string', 'max' => 255],
            [['language_id'], 'exist', 'skipOnError' => true, 'targetClass' => Language::className(), 'targetAttribute' => ['language_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'price' => Yii::t('app', 'Price'),
            'reorder_threshold' => Yii::t('app', 'Reorder Threshold'),
            'beston_quantity' => Yii::t('app', 'Beston Quantity'),
            'ecommerce_enabled' => Yii::t('app', 'Ecommerce Enabled'),
            'currency' => Yii::t('app', 'Currency'),
            'code' => Yii::t('app', 'Code'),
            'taxable' => Yii::t('app', 'Taxable'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }
    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductLocationAttributeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductLocationAttributeQuery(get_called_class());
    }


}
