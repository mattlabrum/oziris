<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "hash_tags_translation".
 *
 * @property integer $id
 * @property integer $hash_tags_id
 * @property integer $language_id
 * @property string $hash_tag
 *
 * @property \app\models\HashTag $hashTags
 * @property \app\models\Language $language
 */
class HashTagsTranslation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'hash_tags_translation';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'hash_tags_id', 'language_id', 'hash_tag'],
            'create' => ['id', 'hash_tags_id', 'language_id', 'hash_tag'],
            'update' => ['id', 'hash_tags_id', 'language_id', 'hash_tag'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['hash_tags_id', 'language_id', 'hash_tag'], 'required'],
            [['hash_tags_id', 'language_id'], 'integer'],
            [['hash_tag'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'hash_tags_id' => Yii::t('app', 'Hash Tags ID'),
            'language_id' => Yii::t('app', 'Language ID'),
            'hash_tag' => Yii::t('app', 'Hash Tag'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHashTags()
    {
        return $this->hasOne(\app\models\HashTag::className(), ['id' => 'hash_tags_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(\app\models\Language::className(), ['id' => 'language_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\HashTagsTranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\HashTagsTranslationQuery(get_called_class());
    }

}
