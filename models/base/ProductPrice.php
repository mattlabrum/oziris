<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_price".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $location
 * @property double $price
 *
 * @property \app\models\Product $product

 */
class ProductPrice extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_price';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'location', 'price'],
            'create' => ['id', 'product_id', 'location', 'price'],
            'update' => ['id', 'product_id', 'location', 'price'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'location', 'price'], 'required'],
            [['product_id'], 'integer'],
            [['price'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'location' => Yii::t('app', 'Location ID'),
            'price' => Yii::t('app', 'Price'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(\app\models\Location::className(), ['id' => 'location']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductPriceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductPriceQuery(get_called_class());
    }

}
