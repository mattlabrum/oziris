<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "payment".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \app\models\Order[] $orders
 */
class Payment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name'],
            'create' => ['id', 'name'],
            'update' => ['id', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\app\models\Order::className(), ['payment_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\PaymentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PaymentQuery(get_called_class());
    }

}
