<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "order_status_times".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $status_id
 * @property string $image
 * @property integer $time
 */
class OrderStatusTime extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_status_times';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'order_id', 'status_id', 'image', 'time'],
            'create' => ['id', 'order_id', 'status_id', 'image', 'time'],
            'update' => ['id', 'order_id', 'status_id', 'image', 'time'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'status_id'], 'required'],
            [['order_id', 'status_id', 'time'], 'integer'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'status_id' => Yii::t('app', 'Status ID'),
            'image' => Yii::t('app', 'Image'),
            'time' => Yii::t('app', 'Time'),
        ];
    }



    public function getStatusName()
    {
        return OrderStatus::find()->where(['id' => $this->status_id])->one()->description;
    }
    
    /**
     * @inheritdoc
     * @return \app\models\query\OrderStatusTimeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\OrderStatusTimeQuery(get_called_class());
    }

}
