<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "batch_ingredients".
 *
 * @property integer $id
 * @property integer $batch_id
 * @property integer $ingredient_id
 * @property string $manufacture_date
 * @property double $percentage
 * @property integer $coo_id
 * @property integer $show_on_oziris
 * @property integer $supplier_id
 * @property integer $arrival_at_manufacturer_date
 * @property \app\models\Coo $coo
 * @property \app\models\Supplier $supplier
 * @property \app\models\Ingredient $ingredient
 * @property \app\models\Batch $batch
 */
class BatchIngredient extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'batch_ingredients';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'batch_id', 'ingredient_id', 'manufacture_date', 'supplier_id', 'show_on_oziris' , 'coo_id' , 'percentage' , 'arrival_at_manufacturer_date'],
            'create' => ['id', 'batch_id', 'ingredient_id', 'manufacture_date', 'supplier_id', 'show_on_oziris' , 'coo_id' , 'percentage' , 'arrival_at_manufacturer_date'],
            'update' => ['id', 'batch_id', 'ingredient_id', 'manufacture_date', 'supplier_id', 'show_on_oziris' , 'coo_id' , 'percentage' , 'arrival_at_manufacturer_date'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['batch_id', 'ingredient_id', 'supplier_id', 'show_on_oziris' , 'coo_id'], 'integer'],
            [['percentage'], 'double'],
            [['manufacture_date', 'arrival_at_manufacturer_date'], 'string', 'max' => 25],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Supplier::className(), 'targetAttribute' => ['supplier_id' => 'id']],
            [['ingredient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingredient::className(), 'targetAttribute' => ['ingredient_id' => 'id']],
            [['batch_id'], 'exist', 'skipOnError' => true, 'targetClass' => Batch::className(), 'targetAttribute' => ['batch_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'batch_id' => Yii::t('app', 'Batch ID'),
            'ingredient_id' => Yii::t('app', 'Ingredient ID'),
            'manufacture_date' => Yii::t('app', 'Manufacture Date'),
            'supplier_id' => Yii::t('app', 'Supplier'),
            'arrival_at_manufacturer_date' => Yii::t('app', 'Arrival at Manufacturer date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(\app\models\Supplier::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoo()
    {
        return $this->hasOne(\app\models\Coo::className(), ['id' => 'coo_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredient()
    {
        return $this->hasOne(\app\models\Ingredient::className(), ['id' => 'ingredient_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBatch()
    {
        return $this->hasOne(\app\models\Batch::className(), ['id' => 'batch_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\BatchIngredientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\BatchIngredientQuery(get_called_class());
    }

}
