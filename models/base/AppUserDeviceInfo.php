<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "app_user_device_info".
 *
 * @property integer $id
 * @property string $device_token
 * @property string $device_type
 * @property string $timezone
 * @property string $badge
 * @property integer $user_id
 * @property string $app_identifier
 * @property string $app_name
 */
class AppUserDeviceInfo extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user_device_info';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'device_token', 'device_type', 'timezone', 'badge', 'user_id', 'app_identifier', 'app_name'],
            'create' => ['id', 'device_token', 'device_type', 'timezone', 'badge', 'user_id', 'app_identifier', 'app_name'],
            'update' => ['id', 'device_token', 'device_type', 'timezone', 'badge', 'user_id', 'app_identifier', 'app_name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['device_token', 'device_type','app_identifier', 'app_name'], 'required'],
            [['user_id'], 'integer'],
            [['device_token', 'device_type', 'timezone', 'badge', 'app_identifier', 'app_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'device_token' => Yii::t('app', 'Device Token'),
            'device_type' => Yii::t('app', 'Device Type'),
            'timezone' => Yii::t('app', 'Timezone'),
            'badge' => Yii::t('app', 'Badge'),
            'user_id' => Yii::t('app', 'User ID'),
            'app_identifier' => Yii::t('app', 'App Identifier'),
            'app_name' => Yii::t('app', 'App Name'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\AppUserDeviceInfoQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AppUserDeviceInfoQuery(get_called_class());
    }

}
