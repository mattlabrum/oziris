<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "order_coupons".
 *
 * @property integer $id
 * @property string $order_id
 * @property double $discount_amount
 * @property string $code
 */
class OrderCoupon extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_coupons';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'order_id', 'discount_amount', 'code'],
            'create' => ['id', 'order_id', 'discount_amount', 'code'],
            'update' => ['id', 'order_id', 'discount_amount', 'code'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id'], 'required'],
            [['discount_amount'], 'number'],
            [['order_id', 'code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'order_id' => Yii::t('app', 'Order ID'),
            'discount_amount' => Yii::t('app', 'Discount Amount'),
            'code' => Yii::t('app', 'Code'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\OrderCouponQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\OrderCouponQuery(get_called_class());
    }

}
