<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "ingredient_group".
 *
 * @property integer $id
 * @property string $name
 *
 * @property \app\models\Ingredient[] $ingredients
 */
class IngredientGroup extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ingredient_group';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name'],
            'create' => ['id', 'name'],
            'update' => ['id', 'name'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngredients()
    {
        return $this->hasMany(\app\models\Ingredient::className(), ['group_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\IngredientGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\IngredientGroupQuery(get_called_class());
    }

}
