<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "push_notification".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $message
 * @property integer $for_android
 * @property integer $for_ios
 */
class PushNotification extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'push_notification';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'created_at', 'updated_at', 'message', 'for_android', 'for_ios'],
            'create' => ['id', 'created_at', 'updated_at', 'message', 'for_android', 'for_ios'],
            'update' => ['id', 'created_at', 'updated_at', 'message', 'for_android', 'for_ios'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'message'], 'required'],
            [['created_at', 'updated_at', 'for_android', 'for_ios'], 'integer'],
            [['message'], 'string', 'max' => 120]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'message' => Yii::t('app', 'Message'),
            'for_android' => Yii::t('app', 'For Android'),
            'for_ios' => Yii::t('app', 'For Ios'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\PushNotificationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\PushNotificationQuery(get_called_class());
    }

}
