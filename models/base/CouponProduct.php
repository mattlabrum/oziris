<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "coupon_products".
 *
 * @property integer $id
 * @property integer $coupon_id
 * @property integer $product_id
 *
 * @property \app\models\Coupon $coupon
 */
class CouponProduct extends ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupon_products';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'coupon_id', 'product_id'],
            'create' => ['id', 'coupon_id', 'product_id'],
            'update' => ['id', 'coupon_id', 'product_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coupon_id', 'product_id'], 'integer'],
            [['coupon_id'], 'exist', 'skipOnError' => true, 'targetClass' => \app\models\Coupon::className(), 'targetAttribute' => ['coupon_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('models', 'ID'),
            'coupon_id' => Yii::t('models', 'Coupon ID'),
            'product_id' => Yii::t('models', 'Product ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoupon()
    {
        return $this->hasOne(\app\models\Coupon::className(), ['id' => 'coupon_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\CouponProductQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CouponProductQuery(get_called_class());
    }

}
