<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "collection_item".
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $collection_id
 * @property integer $quantity
 *
 * @property \app\models\Product $collection
 * @property \app\models\Product $product
 */
class CollectionItem extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'collection_item';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'collection_id', 'quantity'],
            'create' => ['id', 'product_id', 'collection_id', 'quantity'],
            'update' => ['id', 'product_id', 'collection_id', 'quantity'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'collection_id', 'quantity'], 'required'],
            [['product_id', 'collection_id', 'quantity'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'collection_id' => Yii::t('app', 'Collection ID'),
            'quantity' => Yii::t('app', 'Quantity'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCollection()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'collection_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\CollectionItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\CollectionItemQuery(get_called_class());
    }

}
