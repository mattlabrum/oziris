<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "app_user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email_address
 * @property string $mobile
 * @property string $country
 * @property integer $is_active
 * @property string $gender
 * @property string $dob
 * @property string $8ston_user_id
 *
 * @property \app\models\AppAuthDatum[] $appAuthData
 * @property \app\models\AppSocialMediaAuth[] $appSocialMediaAuths
 * @property \app\models\AppUserDeviceInfo[] $appUserDeviceInfos
 * @property \app\models\ReportedReview[] $reportedReviews
 * @property \app\models\ReviewRating[] $reviewRatings
 * @property \app\models\ScannedProduct[] $scannedProducts
 */
class AppUser extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_user';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'first_name', 'last_name', 'email_address', 'mobile', 'country', 'is_active', 'gender', 'dob', '8ston_user_id'],
            'create' => ['id', 'first_name', 'last_name', 'email_address', 'mobile', 'country', 'is_active', 'gender', 'dob', '8ston_user_id'],
            'update' => ['id', 'first_name', 'last_name', 'email_address', 'mobile', 'country', 'is_active', 'gender', 'dob', '8ston_user_id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['email_address'], 'required'],
            [['is_active'], 'integer'],
            [['first_name', 'last_name', 'email_address', 'address'], 'string', 'max' => 255],
            [['mobile'], 'string', 'max' => 15],
            [['country', 'dob'], 'string', 'max' => 25],
            [['gender'], 'string', 'max' => 12]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email_address' => Yii::t('app', 'Email Address'),
            'mobile' => Yii::t('app', 'Mobile'),
            'country' => Yii::t('app', 'Country'),
            'is_active' => Yii::t('app', 'Is Active'),
            'gender' => Yii::t('app', 'Gender'),
            'dob' => Yii::t('app', 'Date Of Birth'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppAuthData()
    {
        return $this->hasMany(\app\models\AppAuthDatum::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppSocialMediaAuths()
    {
        return $this->hasMany(\app\models\AppSocialMediaAuth::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppUserDeviceInfos()
    {
        return $this->hasMany(\app\models\AppUserDeviceInfo::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportedReviews()
    {
        return $this->hasMany(\app\models\ReportedReview::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReviewRatings()
    {
        return $this->hasMany(\app\models\ReviewRating::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScannedProducts()
    {
        return $this->hasMany(\app\models\ScannedProduct::className(), ['user_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\AppUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AppUserQuery(get_called_class());
    }

}
