<?php

namespace app\models\base;

//use app\models\BatchIngredient;
use \Yii;
use \yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the base-model class for table "batch".
 *
 * @property integer $id
 * @property string $manufacture_date
 * @property string $recalled_date
 * @property string $qr_code
 * @property string $expiry_date
 * @property string $qr_code_image
 * @property integer $product_id
 * @property integer $quantity
 * @property integer $physicalQuantity
 * @property string $batch_number
 * @property double $overall_coo_percentage
 * @property integer $label_type_text
 * @property integer $cool_image
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted_at
 * @property BatchIngredient[] $ingredients
 *
 * @property \app\models\Product $product
 * @property \app\models\ScannedProduct[] $scannedProducts
 * @property \app\models\BatchItem[] $batchItems
 */
class Batch extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'batch';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'product_id', 'batch_number','batchItems', 'ingredients' , 'quantity' , 'physicalQuantity', 'created_at', 'updated_at', 'deleted_at'],
            'create' => ['id', 'manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'product_id', 'batch_number','batchItems', 'ingredients', 'quantity' , 'physicalQuantity', 'created_at', 'updated_at', 'deleted_at'],
            'update' => ['id', 'manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'product_id', 'batch_number','batchItems', 'ingredients', 'quantity' , 'physicalQuantity', 'created_at', 'updated_at', 'deleted_at'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manufacture_date', 'qr_code', 'product_id', 'batch_number'], 'required'],
            [['product_id', 'created_at', 'quantity' , 'physicalQuantity', 'updated_at', 'deleted_at'], 'integer'],
            [['manufacture_date', 'recalled_date', 'qr_code', 'qr_code_image', 'batch_number'], 'string', 'max' => 255]
        ];
    }

    /**
     * @return string $qrCodeImage
     */
    public function getCoolImageUrl()
    {
        return 'https://' . $_SERVER['HTTP_HOST'] . Url::to('@web/uploads/cool') . '/' . $this->cool_image;
    }

    /**
     * @return string $qrCodeImage
     */
    public function getQrCodeImageUrl()
    {
        return 'https://' . $_SERVER['HTTP_HOST'] . Url::to('@web/upload/qrcode') . '/' . $this->qr_code_image;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'manufacture_date' => Yii::t('app', 'Manufacture Date'),
            'recalled_date' => Yii::t('app', 'Recalled Date'),
            'qr_code' => Yii::t('app', 'Qr Code'),
            'qr_code_image' => Yii::t('app', 'Qr Code Image'),
            'product_id' => Yii::t('app', 'Product ID'),
            'batch_number' => Yii::t('app', 'Batch Number'),
            'quantity' => Yii::t('app', 'Batch Quantity'),
            'physicalQuantity' => Yii::t('app', 'Actual Units Available'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'batchItems' => Yii::t('app', 'Batch Items'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScannedProducts()
    {
        return $this->hasMany(\app\models\ScannedProduct::className(), ['batch_id' => 'id']);
    }


    /**
     * @inheritdoc
     * @return \app\models\query\BatchQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\BatchQuery(get_called_class());
    }

}
