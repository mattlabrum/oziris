<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "tax".
 *
 * @property integer $id
 * @property string $code
 * @property string $title
 * @property double $percentage
 */
class Tax extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'code', 'title', 'percentage'],
            'create' => ['id', 'code', 'title', 'percentage'],
            'update' => ['id', 'code', 'title', 'percentage'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'title', 'percentage'], 'required'],
            [['percentage'], 'number'],
            [['code', 'title'], 'string', 'max' => 15]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'title' => Yii::t('app', 'Title'),
            'percentage' => Yii::t('app', 'Percentage'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\TaxQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\TaxQuery(get_called_class());
    }

}
