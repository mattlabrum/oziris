<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "order_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property \app\models\Order[] $orders
 */
class OrderStatus extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_status';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'name', 'description'],
            'create' => ['id', 'name', 'description'],
            'update' => ['id', 'name', 'description'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(\app\models\Order::className(), ['status_id' => 'id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\OrderStatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\OrderStatusQuery(get_called_class());
    }

}
