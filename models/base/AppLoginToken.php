<?php

namespace app\models\base;

use app\models\base\AppUser;
use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "app_login_token".
 *
 * @property integer $id
 * @property integer $user_id
 * @property AppUser $user
 * @property string $token
 */
class AppLoginToken extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'app_login_token';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'user_id', 'token'],
            'create' => ['id', 'user_id', 'token'],
            'update' => ['id', 'user_id', 'token'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'token'], 'required'],
            [['user_id'], 'integer'],
            [['token'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function getUser()
    {
        return AppUser::find()->where(['id' => $this->user_id])->one();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'token' => Yii::t('app', 'Token'),
        ];
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\AppLoginTokenQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\AppLoginTokenQuery(get_called_class());
    }

}
