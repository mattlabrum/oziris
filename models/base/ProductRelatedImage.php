<?php

namespace app\models\base;

use \Yii;
use \yii\db\ActiveRecord;

/**
 * This is the base-model class for table "product_related_images".
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $image_url
 *
 * @property \app\models\Product $product
 */
class ProductRelatedImage extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_related_images';
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'default' => ['id', 'product_id', 'image_url'],
            'create' => ['id', 'product_id', 'image_url'],
            'update' => ['id', 'product_id', 'image_url'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'image_url'], 'required'],
            [['product_id'], 'integer'],
            [['image_url'], 'string', 'max' => 255],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'product_id' => Yii::t('app', 'Product ID'),
            'image_url' => Yii::t('app', 'Image Url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(\app\models\Product::className(), ['id' => 'product_id']);
    }

    
    /**
     * @inheritdoc
     * @return \app\models\query\ProductRelatedImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ProductRelatedImageQuery(get_called_class());
    }

}
