<?php
namespace app\utils;
/**
 * Created by PhpStorm.
 * User: Windows User
 * Date: 19/11/2015
 * Time: 1:21 PM
 */

/**
 * @param $fileName
 * @param string $delimiter
 * @return array
 */

class ReturnRequiredDatesAndMonths
{

    static function fetchMonthNameFromMonthNumber($countryCode)
    {
        $arr = [
            "01" => "January",
            "02" => "February",
            "03" => "March",
            "04" => "April",
            "05" => "May",
            "06" => "June",
            "07" => "July",
            "08" => "August",
            "09" => "September",
            "10" => "October",
            "11" => "November",
            "12" => "December",
       ];
        return $arr[$countryCode];
    }

    static function getDateRange($first, $last, $step = 'day', $output_format = 'd-m-Y' ) {

        $dates = array();

        $first = date('m/d/Y', $first);
        $last = date('m/d/Y', $last);


        $current = strtotime($first);
        $last = strtotime($last);

        while( $current < $last ) {

            if($step == 'day'){
                $dates[] = [
                    'Sales' => 0,
                    'Day' => date('d', $current),
                    'Month' => date('m', $current),
                    'Year' => date('Y', $current),
                    'cnt' => 0,
                ] ;
            }else if($step == 'month'){
                $dates[] = [
                    'Sales' => 0,
                    'Month' => date('m', $current),
                    'Year' => date('Y', $current),
                    'cnt' => 0,
                ] ;
            }else if($step == 'year'){
                $dates[] = [
                    'Sales' => 0,
                    'Year' => date('Y', $current),
                    'cnt' => 0,
                ] ;
            }


            $current = strtotime('+1 '.$step, $current);
        }

        return $dates;
    }


    static function getNextTwelveMonthsForBudget() {
        $dates = array();
        $step = 0;

        while( $step < 11 ) {
            $dates[] = [
                'Month' => date('m', strtotime('+'.$step.' month', time())),
                'Year' => date('Y', strtotime('+'.$step.' month', time())),
            ] ;
            $step++;
        }
        return $dates;
    }
}


