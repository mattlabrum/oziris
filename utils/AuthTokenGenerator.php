<?php
/**
 * Created by PhpStorm.
 * User: Windows User
 * Date: 5/01/2016
 * Time: 2:00 PM
 */

namespace app\utils;


class AuthTokenGenerator {

    static function generateAuthToken($length = 15) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}