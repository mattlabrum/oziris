<?php
namespace app\utils;
/**
 * Created by PhpStorm.
 * User: Windows User
 * Date: 19/11/2015
 * Time: 1:21 PM
 */

/**
 * @param $fileName
 * @param string $delimiter
 * @return array
 */

class csvToArray
{

    static function str_putcsv($input, $delimiter = ',', $enclosure = '"')
    {
        // Open a memory "file" for read/write...
        $fp = fopen('php://temp', 'r+');
        // ... write the $input array to the "file" using fputcsv()...
        fputcsv($fp, $input, $delimiter, $enclosure);
        // ... rewind the "file" so we can read what we just wrote...
        rewind($fp);
        // ... read the entire line into a variable...
        $data = fread($fp, 1048576);
        // ... close the "file"...
        fclose($fp);
        // ... and return the $data to the caller, with the trailing newline from fgets() removed.
        return rtrim($data, "\n");
    }

    static function csvToArray($fileName, $delimiter = ',', $headerRow = 1)
    {
        $handle = fopen($fileName, 'r');
        $rows = array();
        while ($headerRow > 1) {
            $headerRow--;
            fgetcsv($handle, null, $delimiter);
        }
        if ($headerRow) {
            $header = fgetcsv($handle, null, $delimiter);
        }
        while (($data = fgetcsv($handle, null, $delimiter)) !== FALSE) {
            $row = array();
            if ($headerRow) {
                foreach ($header as $key => $heading) {
                    $heading = trim($heading);
                    $row[$heading] = $data[$key];
                }
                $rows[] = $row;
            } else {
                $rows[] = $data;
            }
        }
        fclose($handle);
        return $rows;
    }

    static function in_array_field($needle, $needle_field, $haystack) {
        $return = -1;
        foreach ($haystack as $key=>$item){

//            echo $item[$needle_field];
//            echo '<br>';
            if (isset($item[$needle_field]) && $item[$needle_field] === $needle){
                $return = $key;
                break;
            }
        }
        return $return;
    }

}