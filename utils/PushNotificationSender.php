<?php
/**
 * Created by PhpStorm.
 * User: Windows User
 * Date: 12/08/2016
 * Time: 2:03 PM
 */

namespace app\utils;


use app\models\OrderStatus;

class PushNotificationSender {


    static function sendIOSNotification($devices, $notification,$order_id){

        $passphrase = 'Oz1r1s';
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'apple_push_notification_production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
//        stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');
        $message = $notification;
        foreach ($devices as $device) {
            $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err,
                $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            if (!$fp) {
                return false;
            }
            $body['aps'] = array(
                'alert' => $message,
                'sound' => 'default'
            );
            $body['order_id'] = $order_id;
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $device->device_token) . pack('n', strlen($payload)) . $payload;
            fwrite($fp, $msg, strlen($msg));
            fclose($fp);
        }

    }

    static function sendAndroidNotification($devices, $notification)
    {
        $registration_ids = [];
        foreach($devices as $device)
            array_push($registration_ids,$device->device_token);

        // API access key from Google API's Console
        define('API_ACCESS_KEY', 'AIzaSyDZznaCTsjEkoWKP37KaNED7AbZQx8kQug');

        $msg = array
        (
            'message' => $notification,
            'vibrate' => 1,
            'sound' => 1,

        );
        $fields = array
        (
            'registration_ids' => $registration_ids,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        $result =  json_decode($result);
        if ($result->success > 0) {
            return true;
        } else {
            return false;
        }
    }



}