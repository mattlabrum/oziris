<?php
/**
 * Created by PhpStorm.
 * User: Windows User
 * Date: 12/08/2016
 * Time: 2:03 PM
 */

namespace app\utils;

class BestonSMSSender
{

    static function sendSMSNotification($phone_no, $msg)
    {
        $key = "ef8b639d-b119-4b4c-b73b-6b39d0e626ac";
        $secret = "Mxe+BbORcUWANtTAVwwwDQ==";
        $user = "application\\" . $key . ":" . $secret;
        $message = array("from" => "Beston", "message" => $msg);
        $data = json_encode($message);

        $ch = curl_init('https://messagingapi.sinch.com/v1/sms/' . $phone_no);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERPWD, $user);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        } else {
            return $result;
        }
        curl_close($ch);
    }
}