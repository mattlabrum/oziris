<?php
/* @var $this yii\web\View */


use kartik\file\FileInput;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Home');
?>


<div class="site-index ">

    <h1><?= getenv('APP_NAME') ?></h1>

    <h2 class="lead"><?= Yii::t('app', 'Up and running.') ?></h2>

</div>
