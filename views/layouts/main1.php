<?php
use app\assets\AppAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
#use app\assets\AppAsset;


$viewSidebar = '@app/views/layouts/_sidebar';
$viewFooter = '@app/views/layouts/_footer';
$viewNavbar = '@app/views/layouts/_navbar';
$viewContent = '@app/views/layouts/_content';
    
   

AppAsset::register($this);

$regex = '|(\\' . DIRECTORY_SEPARATOR . '[^\\' . DIRECTORY_SEPARATOR . ']*\\' . DIRECTORY_SEPARATOR . '[^\\' . DIRECTORY_SEPARATOR . ']*\.php)$|';
preg_match($regex, __FILE__, $matches);
require(Yii::getAlias('@vendor/cornernote/yii2-ace/src/views' . $matches[1]));
