<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$controller = Yii::$app->controller;
?>

<div id="sidebar" class="sidebar responsive">


    <ul class="nav nav-list">

        <?php if(!Yii::$app->user->can('accounts') && !Yii::$app->user->isGuest ){ ?>

            <li class="">
                <a href="<?= Yii::$app->homeUrl ?>">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>
                <b class="arrow"></b>
            </li>


            <li class="<?php echo Yii::$app->controller->id == 'product' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/product/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Products
            </a>
            <b class="arrow"></b>
        </li>
        <li class="<?php echo Yii::$app->controller->id == 'product-group' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/product-group/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Product Groups
            </a>
            <b class="arrow"></b>
        </li>
        <li class="<?php echo Yii::$app->controller->id == 'product-type' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/product-type/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Product Types
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Nutrition Menu Items Start--------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'nutrition' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/nutrition/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Nutrition
            </a>
        </li>
        <!-----------------------------Nutrition Menu Items End--------------------------------------->
        <!-----------------------------Nutrition Menu Items Start--------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'nutrition-tag' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/nutrition-tag/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Nutrition Tags
            </a>
        </li>
        <!-----------------------------Nutrition Menu Items End--------------------------------------->
        <!-----------------------------Ingredient Menu Items Start--------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'ingredient' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/ingredient/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Ingredient
            </a>
        </li>
        <!-----------------------------Ingredient Menu Items End--------------------------------------->
        <!-----------------------------Batch Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'batch' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/batch/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Batches
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Batch Menu Items End----------------------------------------->

        <!-----------------------------Brand Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'brand' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/brand/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Brands
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Brand Menu Items End----------------------------------------->

        <!-----------------------------Manufacturer Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'manufacturer' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/manufacturer/index']) ?>">
                <i class="menu-icon fa fa-industry"></i>
                Manufacturers
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Manufacturer Menu Items End----------------------------------------->

        <!-----------------------------Supplier Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'supplier' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/supplier/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Supplier
            </a>
        </li>
        <!-----------------------------Supplier Menu Items End----------------------------------------->

        <!-----------------------------COO Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'coo' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/coo/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                CoO
            </a>
        </li>
        <!-----------------------------COO Menu Items End----------------------------------------->

        <!-----------------------------Coupons Menu Items Start----------------------------------------->
            <li class="<?php echo Yii::$app->controller->id == 'coupon' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
                <a href="<?= Url::to(['/admin/coupon/index']) ?>">
                    <i class="menu-icon fa fa-percent"></i>
                    Coupons
                </a>
            </li>
        <!-----------------------------COO Menu Items End----------------------------------------->

        <!-----------------------------Subscription Menu Items Start----------------------------------------->
            <li class="<?php echo Yii::$app->controller->id == 'subscription' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
                <a href="<?= Url::to(['/admin/subscription/index']) ?>">
                    <i class="menu-icon fa fa-percent"></i>
                    Subscription
                </a>
            </li>
        <!-----------------------------Subscription Menu Items End----------------------------------------->

            <div class="hr sidebar-hr"></div>
        <!--        ---------------------------E-Commerce Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'order' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/order/index']) ?>">
                <i class="menu-icon fa fa-search"></i>
                Orders
            </a>
            <b class="arrow"></b>
        </li>

        <li class="<?php echo Yii::$app->controller->id == 'order' && Yii::$app->controller->action->id == 'stock-analysis' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/order/stock-analysis']) ?>">
                <i class="menu-icon fa fa-search"></i>
                Stock Management
            </a>
            <b class="arrow"></b>
        </li>

        <li class="<?php echo Yii::$app->controller->id == 'reports' && Yii::$app->controller->action->id == 'ecommerce' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/order/ecommerce-reports']) ?>">
                <i class="menu-icon fa fa-search"></i>
                eCommerce Reports
            </a>
            <b class="arrow"></b>
        </li>
        <li class="<?php echo Yii::$app->controller->id == 'budget' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/budget/index']) ?>">
                <i class="menu-icon fa fa-search"></i>
                Budget
            </a>
            <b class="arrow"></b>
        </li>


        <!--        ---------------------------E-Commerce Menu Items End----------------------------------------->
        <div class="hr sidebar-hr"></div>
        <!-----------------------------Admin User Menu Item Start--------------------------------------->
<!--        <li class="--><?php //echo Yii::$app->controller->id == 'user' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?><!--">-->
<!--            <a href="--><?//= Url::to(['admin/index']) ?><!--">-->
<!--                <i class="menu-icon fa fa-dropbox"></i>-->
<!--                Admin Users-->
<!--            </a>-->
<!--            <b class="arrow"></b>-->
<!--        </li>-->
        <!-----------------------------Collections Menu Items End----------------------------------------->

        <!-----------------------------Translation Summary Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'translation-summary' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/translation-summary/index']) ?>">
                <i class="menu-icon fa fa-dropbox"></i>
                Translation Summary
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Translation Summary  Menu Items End----------------------------------------->

        <!-----------------------------Reports Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'reports' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/reports/index']) ?>">
                <i class="menu-icon fa fa-bar-chart"></i>
                Reports
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Reports Menu Items End----------------------------------------->

        <!-----------------------------Scanned Product Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'scanned-product' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/scanned-product/index']) ?>">
                <i class="menu-icon fa fa-qrcode"></i>
                Scanned Products
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Scanned Product Menu Items End----------------------------------------->

        <!-----------------------------Rating and Reviews Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'review-rating' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/review-rating/index']) ?>">
                <i class="menu-icon fa fa-commenting"></i>
                Rating & Reviews
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Rating and Reviews Menu Items End----------------------------------------->

        <!-----------------------------Reported Reviews Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'reported-review' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/reported-review/index']) ?>">
                <i class="menu-icon fa fa-ban"></i>
                Reported Reviews
            </a>
            <b class="arrow"></b>
        </li>
        <!-----------------------------Reported Reviews Menu Items End----------------------------------------->

        <!--        ---------------------------User Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'app-user' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/app-user/index']) ?>">
                <i class="menu-icon fa fa-users"></i>
                App Users
            </a>
            <b class="arrow"></b>
        </li>
        <!--        ---------------------------User Menu Items End----------------------------------------->

        <!---->
<!--        ---------------------------Push Notifications Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'push-notification' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/push-notification/index']) ?>">
                <i class="menu-icon fa fa-bell-o"></i>
                Push Notifications
            </a>
            <b class="arrow"></b>
        </li>
<!--        ---------------------------Push Notifications Menu Items End----------------------------------------->

<!--        ---------------------------Audit Trail Menu Items Start----------------------------------------->
        <li class="<?php echo Yii::$app->controller->id == 'audit-trail' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
            <a href="<?= Url::to(['/admin/audit-trail/index']) ?>">
                <i class="menu-icon fa fa-search"></i>
                Audit
            </a>
            <b class="arrow"></b>
        </li>
        <!--        ---------------------------Audit Trail Menu Items End----------------------------------------->
            <li class="<?php echo Yii::$app->controller->id == 'app-setting' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
                <a href="<?= Url::to(['/admin/app-setting/update']) ?>">
                    <i class="menu-icon fa fa-search"></i>
                    App Settings
                </a>
                <b class="arrow"></b>
            </li>
        <?php }else if(Yii::$app->user->can('accounts')){?>

            <li class="<?php echo Yii::$app->controller->id == 'order' && Yii::$app->controller->action->id == 'index' ? 'active' : '' ?>">
                <a href="<?= Url::to(['/admin/order/index']) ?>">
                    <i class="menu-icon fa fa-search"></i>
                    Orders
                </a>
                <b class="arrow"></b>
            </li>

            <li class="<?php echo Yii::$app->controller->id == 'order' && Yii::$app->controller->action->id == 'stock-analysis' ? 'active' : '' ?>">
                <a href="<?= Url::to(['/admin/order/stock-analysis']) ?>">
                    <i class="menu-icon fa fa-search"></i>
                    Stock Management
                </a>
                <b class="arrow"></b>
            </li>

            <li class="<?php echo Yii::$app->controller->id == 'order' && Yii::$app->controller->action->id == 'ecommerce-reports' ? 'active' : '' ?>">
                <a href="<?= Url::to(['/admin/order/ecommerce-reports']) ?>">
                    <i class="menu-icon fa fa-search"></i>
                    eCommerce Reports
                </a>
                <b class="arrow"></b>
            </li>

        <?php }?>

    </ul>

</div>

