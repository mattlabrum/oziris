<?php
use yii\widgets\Breadcrumbs;
use cornernote\ace\widgets\Alert;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<div class="main-content">
    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

    </div>
</div>
