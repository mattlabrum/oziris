<?php

use yii\db\Migration;

class m170504_000751_add_tax_column_to_order_items_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_items', 'tax', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('order_items', 'tax');
    }
}
