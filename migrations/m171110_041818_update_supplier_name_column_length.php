<?php

use yii\db\Migration;
use yii\db\Schema;

class m171110_041818_update_supplier_name_column_length extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->alterColumn('supplier', 'supplier_name',Schema::TYPE_STRING."(55)");
    }

    public function safeDown()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->alterColumn('supplier', 'supplier_name',Schema::TYPE_STRING."(15)");
    }

}
