<?php

use yii\db\Migration;
use yii\db\Schema;

class m161113_161549_create_product_attributes_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_location_attributes}}',
            [
                'id' => Schema::TYPE_PK . "",
                'product_id' => Schema::TYPE_INTEGER .  " NOT NULL",
                'language_id' => Schema::TYPE_INTEGER .  " NOT NULL",
                'price' => Schema::TYPE_FLOAT . " NULL",
                'reorder_threshold' => Schema::TYPE_INTEGER .  " NOT NULL",
                'beston_quantity' => Schema::TYPE_INTEGER .  " NOT NULL",
                'ecommerce_enabled' => Schema::TYPE_INTEGER .  " NOT NULL",
                'currency' => Schema::TYPE_STRING .  " NOT NULL",
                'code' => Schema::TYPE_STRING . " NULL",
            ],
            $tableOptions
        );

        $this->createIndex('idx_language_id', '{{%product_location_attributes}}','language_id',0);
        $this->addForeignKey('fk_language_id', '{{%product_location_attributes}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {
        $this->dropTable('{{%product_location_attributes}}');
    }
}
