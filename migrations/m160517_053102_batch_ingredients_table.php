<?php

use yii\db\Migration;
use yii\db\Schema;

class m160517_053102_batch_ingredients_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%batch_ingredients}}',
            [
                'id'=> Schema::TYPE_PK."",
                'batch_id'=> Schema::TYPE_INTEGER."(15)",
                'ingredient_id'=> Schema::TYPE_INTEGER."(15)",
                'manufacture_date'=> Schema::TYPE_STRING."(25)",
                'supplier_id'=> Schema::TYPE_INTEGER."(15)",
            ]
        );
        $this->createIndex('idx_batch_id', '{{%batch_ingredients}}','batch_id',0);
        $this->addForeignKey('fk_batch_ingredients_product_id', '{{%batch_ingredients}}', 'batch_id', 'batch', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_ingredient_id', '{{%batch_ingredients}}','ingredient_id',0);
        $this->addForeignKey('fk_batch_ingredients_ingredient_id', '{{%batch_ingredients}}', 'ingredient_id', 'ingredient', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_supplier_id', '{{%batch_ingredients}}','supplier_id',0);
        $this->addForeignKey('fk_batch_ingredients_supplier_id', '{{%batch_ingredients}}', 'supplier_id', 'supplier', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_batch_ingredients_product_id', '{{%batch_ingredients}}');
        $this->dropIndex('idx_batch_id', '{{%batch_ingredients}}');

        $this->dropForeignKey('fk_batch_ingredients_ingredient_id', '{{%batch_ingredients}}');
        $this->dropIndex('idx_ingredient_id', '{{%batch_ingredients}}');

        $this->dropForeignKey('fk_batch_ingredients_supplier_id', '{{%batch_ingredients}}');
        $this->dropIndex('idx_supplier_id', '{{%batch_ingredients}}');

        $this->dropTable('{{%batch_ingredients}}');
    }
}
