<?php

use yii\db\Migration;

class m160729_044335_alter_reported_review_remove_index extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('rating_review_id', '{{%reported_review}}');
        $this->dropIndex('user_id', '{{%reported_review}}');
    }

    public function safeDown()
    {
    }
}
