<?php

use yii\db\Migration;

class m160831_003741_add_preorder_enabled_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'preorder_enabled', $this->smallInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'preorder_enabled');
    }
}
