<?php

use yii\db\Migration;
use yii\db\Schema;

class m160516_061222_create_ingredient_group_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%ingredient_group}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(255) NOT NULL",
            ]
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%ingredient_group}}');
    }
}
