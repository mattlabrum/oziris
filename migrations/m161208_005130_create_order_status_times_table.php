<?php

use yii\db\Migration;
use yii\db\Schema;

class m161208_005130_create_order_status_times_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
            '{{%order_status_times}}',
            [
                'id' => Schema::TYPE_PK . "",
                'order_id' => Schema::TYPE_INTEGER .  " NOT NULL",
                'status_id' => Schema::TYPE_INTEGER .  " NOT NULL",
                'image' => Schema::TYPE_STRING .  " NULL",
                'time' => Schema::TYPE_INTEGER .  " NULL",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%order_status_times}}');
    }
}
