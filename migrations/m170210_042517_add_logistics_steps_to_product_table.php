<?php

use yii\db\Migration;

class m170210_042517_add_logistics_steps_to_product_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'logistics', $this->text()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'logistics');
    }}
