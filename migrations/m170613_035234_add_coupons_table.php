<?php

use yii\db\Migration;
use yii\db\Schema;

class m170613_035234_add_coupons_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%coupons}}',
            [
                'id' => Schema::TYPE_PK . "",
                "code"=> Schema::TYPE_STRING . "",
                "date_created"=> Schema::TYPE_BIGINT . "",
                "date_modified"=> Schema::TYPE_BIGINT . "",
                "discount_type"=> Schema::TYPE_STRING . "",//"percent",
                "description"=> Schema::TYPE_STRING . "",
                "amount"=> Schema::TYPE_FLOAT . "",
                "expiry_date"=> Schema::TYPE_STRING . "",
                "individual_use"=> Schema::TYPE_BOOLEAN . "",
                "usage_limit"=> Schema::TYPE_INTEGER . "",
                "usage_limit_per_user"=> Schema::TYPE_INTEGER . "",
                "limit_usage_to_x_items"=> Schema::TYPE_INTEGER . "",
                "free_shipping"=> Schema::TYPE_BOOLEAN . "",
                "exclude_sale_items"=> Schema::TYPE_BOOLEAN . "",
                "minimum_amount"=> Schema::TYPE_FLOAT . "",
                "maximum_amount"=> Schema::TYPE_FLOAT . "",
                "email_restrictions"=> Schema::TYPE_TEXT,
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%coupons}}');
    }
}
