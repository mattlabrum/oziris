<?php

use yii\db\Migration;

class m160919_002422_add_beston_quantity_reorder_quantity_reorder_notification_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'beston_quantity', $this->integer()->defaultValue(500));
        $this->addColumn('product', 'reorder_threshold', $this->integer()->defaultValue(100));
        $this->addColumn('product', 'reorder_notification', $this->smallInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'beston_quantity');
        $this->dropColumn('product', 'reorder_threshold');
        $this->dropColumn('product', 'reorder_notification');
    }
}
