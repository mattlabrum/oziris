<?php

use yii\db\Migration;

class m160627_023040_add_qr_code_text_column_to_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'qr_code_text', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'qr_code_text');
    }
}
