<?php

use yii\db\Migration;

class m170131_002532_add_coo_columns_to_batch_ingredients extends Migration
{
    public function safeUp()
    {
        $this->addColumn('batch_ingredients', 'coo_id', $this->integer(11));
        $this->addColumn('batch_ingredients', 'percentage', $this->double(2));
        $this->addColumn('batch_ingredients', 'show_on_oziris', $this->smallInteger(2));
    }

    public function safeDown()
    {
        $this->dropColumn('batch_ingredients', 'coo_id');
        $this->dropColumn('batch_ingredients', 'percentage');
        $this->dropColumn('batch_ingredients', 'show_on_oziris');
    }
}
