<?php

use yii\db\Migration;

class m160531_063714_add_nutrition_serve_columns_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'serves_per_pack', $this->string(10,'NULL'));
        $this->addColumn('product', 'serve_size', $this->string(10,'NULL'));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'serves_per_pack');
        $this->dropColumn('product', 'serve_size');
    }
}
