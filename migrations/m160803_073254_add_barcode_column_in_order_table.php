<?php

use yii\db\Migration;

class m160803_073254_add_barcode_column_in_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'barcode', $this->string()->null()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'barcode');
    }
}
