<?php

use yii\db\Migration;

class m170130_004502_add_percentage_column_to_product_ingredients extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product_ingredients', 'percentage', $this->double(2)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('product_ingredients', 'percentage');
    }
}
