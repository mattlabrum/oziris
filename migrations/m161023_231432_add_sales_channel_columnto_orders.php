<?php

use yii\db\Migration;

class m161023_231432_add_sales_channel_columnto_orders extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'trading_channel', $this->string()->defaultValue('B2C'));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'trading_channel');
    }
}
