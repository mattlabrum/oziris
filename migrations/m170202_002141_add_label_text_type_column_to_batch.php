<?php

use yii\db\Migration;

class m170202_002141_add_label_text_type_column_to_batch extends Migration
{
    public function safeUp()
    {
        $this->addColumn('batch', 'label_type_text', $this->smallInteger(2));
    }

    public function safeDown()
    {
        $this->dropColumn('batch', 'label_type_text');
    }
}
