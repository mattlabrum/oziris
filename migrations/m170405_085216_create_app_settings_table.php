<?php

use yii\db\Migration;
use yii\db\Schema;

class m170405_085216_create_app_settings_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%app_settings}}',
            [
                'id' => Schema::TYPE_PK . "",
                'promo_url' => Schema::TYPE_STRING . "(255) NULL",
                'promo_image' => Schema::TYPE_STRING . "(255) NULL",
                'promo_product_id' => Schema::TYPE_INTEGER . "(11) NULL",
                'promo_search_string' => Schema::TYPE_STRING . "(255) NULL",
            ],
            $tableOptions
        );

        $this->insert('app_settings',array(
            'id' =>1,
        ));

    }

    public function safeDown()
    {
        $this->dropTable('{{%app_settings}}');
    }
}
