<?php

use yii\db\Migration;
use yii\db\Schema;

class m170713_035229_create_subscription_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%subscription}}',
            [
                'id' => Schema::TYPE_PK . "",
                'start_date' => Schema::TYPE_STRING . "(25) NULL",
                'subscription_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'next_payment_date' => Schema::TYPE_STRING . "(25) NULL",
                'billing_period' => Schema::TYPE_STRING . "(15) NULL",
                'billing_interval' => Schema::TYPE_STRING . "(15) NULL",
                'status' => Schema::TYPE_STRING . "(15) NULL",
                'subscription_name' => Schema::TYPE_STRING . "(255) NULL",
                'delivery_day' => Schema::TYPE_STRING . "(15) NULL",
                'next_delivery_date' => Schema::TYPE_STRING . "(25) NULL",
                'customer_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'subscription_billing_address_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'subscription_shipping_address_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'payment_method' => Schema::TYPE_STRING . "(15) NULL",
                'payment_title' => Schema::TYPE_STRING . "(15) NULL",
                'stript_customer_id' => Schema::TYPE_STRING . "(15) NULL",
                'stripe_card_id' => Schema::TYPE_STRING . "(15) NULL",
            ],
            $tableOptions
        );

//        $this->createIndex('idx_woo_order_id', '{{%order}}','woo_order_id',0);
        $this->createIndex('idx_subscription_id', '{{%subscription}}','subscription_id',0);
        $this->createIndex('idx_customer_id', '{{%subscription}}','customer_id',0);
        $this->addForeignKey('fk_subscription_customer_id', '{{%subscription}}', 'customer_id', 'app_user', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_subscription_billing_address_id', '{{%subscription}}','subscription_billing_address_id',0);
        $this->addForeignKey('fk_subscription_subscription_billing_address_id', '{{%subscription}}', 'subscription_billing_address_id', 'address', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_subscription_shipping_address_id', '{{%subscription}}','subscription_shipping_address_id',0);
        $this->addForeignKey('fk_subscription_subscription_shipping_address_id', '{{%subscription}}', 'subscription_shipping_address_id', 'address', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable('{{%subscription}}');
    }
}
