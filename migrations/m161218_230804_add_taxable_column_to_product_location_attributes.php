<?php

use yii\db\Migration;

class m161218_230804_add_taxable_column_to_product_location_attributes extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product_location_attributes', 'taxable', $this->integer()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('product_location_attributes', 'taxable');
    }
}
