<?php

use yii\db\Migration;

class m170509_045014_add_actual_order_item_price_to_order_items extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_items', 'actual_price', $this->string());
        $this->addColumn('order_items', 'actual_tax', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('order_items', 'actual_price');
        $this->dropColumn('order_items', 'actual_tax');
    }
}
