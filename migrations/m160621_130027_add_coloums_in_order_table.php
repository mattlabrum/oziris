<?php

use yii\db\Migration;

class m160621_130027_add_coloums_in_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'qr_code', $this->text());
        $this->addColumn('order', 'order_preferred_time', $this->string(15));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'qr_code');
        $this->dropColumn('order', 'order_preferred_time');
    }
}
