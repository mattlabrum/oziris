<?php

use yii\db\Migration;
use yii\db\Schema;

class m170609_063010_add_cross_sell_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_cross_sell}}',
            [
                'id' => Schema::TYPE_PK . "",
                'product_id' => Schema::TYPE_INTEGER . "(10) NULL",
                'cross_sell_product_id' => Schema::TYPE_INTEGER . "(10) NULL",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%product_cross_sell}}');
    }
}
