<?php

use yii\db\Migration;
use yii\db\Schema;

class m170608_061517_create_upsell_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_upsell}}',
            [
                'id' => Schema::TYPE_PK . "",
                'product_id' => Schema::TYPE_INTEGER . "(10) NULL",
                'upsell_product_id' => Schema::TYPE_INTEGER . "(10) NULL",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%product_upsell}}');
    }
}

