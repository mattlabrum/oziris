<?php

use yii\db\Migration;

class m160428_032308_add_column_woo_order_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'woo_order_id', $this->integer(15));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'woo_order_id');
    }
}

