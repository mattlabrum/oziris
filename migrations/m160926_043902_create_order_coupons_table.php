<?php

use yii\db\Migration;
use yii\db\Schema;

class m160926_043902_create_order_coupons_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%order_coupons}}',
            [
                'id' => Schema::TYPE_PK . "",
                'order_id' => Schema::TYPE_STRING .  " NOT NULL",
                'discount_amount' => Schema::TYPE_FLOAT . " NULL",
                'code' => Schema::TYPE_STRING . " NULL",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%order_coupons}}');
    }

}
