<?php

use yii\db\Migration;

class m170511_013433_add_non_food_item_field_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'is_eatable', $this->smallInteger(1));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'is_eatable');
    }
}
