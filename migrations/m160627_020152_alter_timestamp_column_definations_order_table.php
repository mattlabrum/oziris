<?php

use yii\db\Migration;
use yii\db\Schema;

class m160627_020152_alter_timestamp_column_definations_order_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->alterColumn('order', 'created_at',Schema::TYPE_STRING."(255)");
        $this->alterColumn('order', 'updated_at',Schema::TYPE_STRING."(255)");
        $this->alterColumn('order', 'completed_at',Schema::TYPE_STRING."(255)");
    }

    public function safeDown()
    {
    }
}
