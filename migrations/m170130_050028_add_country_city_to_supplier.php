<?php

use yii\db\Migration;

class m170130_050028_add_country_city_to_supplier extends Migration
{
    public function safeUp()
    {
        $this->addColumn('supplier', 'city', $this->string(255));
        $this->addColumn('supplier', 'country', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('supplier', 'city');
        $this->dropColumn('supplier', 'country');
    }
}
