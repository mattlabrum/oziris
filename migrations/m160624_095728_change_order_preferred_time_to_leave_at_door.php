<?php

use yii\db\Migration;

class m160624_095728_change_order_preferred_time_to_leave_at_door extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('order', 'order_preferred_time', 'leave_at_door');
    }
}
