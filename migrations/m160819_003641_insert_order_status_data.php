<?php

use yii\db\Migration;

class m160819_003641_insert_order_status_data extends Migration
{
    public function safeUp()
    {
        $this->insert('order_status',array(
            'name'=> 'ReadyToDispatch',
            'description'=> 'Ready to Dispatch',
        ));
    }

    public function safeDown()
    {
        echo "m160502_061317_insert_setup_data does not support migration down.\n";
        return false;
    }
}
