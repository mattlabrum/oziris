<?php

use yii\db\Migration;

class m160721_063645_remove_comment_id_column_reported_review extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('reported_review', 'comment_id');
    }
}
