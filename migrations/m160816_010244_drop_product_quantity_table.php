<?php

use yii\db\Migration;

class m160816_010244_drop_product_quantity_table extends Migration
{
    public function safeUp()
    {
        $this->dropTable('product_quantity');
        $this->addColumn('batch', 'quantity', $this->integer());
    }

    public function safeDown()
    {

    }
}
