<?php

use yii\db\Migration;
use yii\db\Schema;

class m160517_053012_product_ingredients_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_ingredients}}',
            [
                'id'=> Schema::TYPE_PK."",
                'product_id'=> Schema::TYPE_INTEGER."(15)",
                'ingredient_id'=> Schema::TYPE_INTEGER."(15)",
            ]
        );
        $this->createIndex('idx_product_id', '{{%product_ingredients}}','product_id',0);
        $this->addForeignKey('fk_product_ingredients_product_id', '{{%product_ingredients}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_ingredient_id', '{{%product_ingredients}}','ingredient_id',0);
        $this->addForeignKey('fk_product_ingredients_ingredient_id', '{{%product_ingredients}}', 'ingredient_id', 'ingredient', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_product_ingredients_product_id', '{{%product_ingredients}}');
        $this->dropIndex('idx_product_id', '{{%product_ingredients}}');

        $this->dropForeignKey('fk_product_ingredients_ingredient_id', '{{%product_ingredients}}');
        $this->dropIndex('idx_ingredient_id', '{{%product_ingredients}}');

        $this->dropTable('{{%product_ingredients}}');
    }
}
