<?php

use yii\db\Migration;

class m160929_060326_add_continent_column_scanned_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scanned_product', 'continent', $this->string()->null());
        $this->addColumn('address', 'continent', $this->string()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('scanned_product', 'continent');
        $this->dropColumn('address', 'continent');
    }
}
