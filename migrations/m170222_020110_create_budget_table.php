<?php

use yii\db\Migration;
use yii\db\Schema;

class m170222_020110_create_budget_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%budget}}',
            [
                'id' => Schema::TYPE_PK . "",
                'month' => Schema::TYPE_INTEGER . " NULL",
                'year' => Schema::TYPE_INTEGER . " NULL",
                'budget_type' => Schema::TYPE_STRING . "(255) NULL",
                'budget_amount' => Schema::TYPE_STRING . "(255) NULL",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%budget}}');
    }

}
