<?php

use yii\db\Migration;

class m160826_063403_add_is_featured_and_is_top_seller_columns_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'is_top_seller', $this->smallInteger()->defaultValue(0));
        $this->addColumn('product', 'is_featured', $this->smallInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'is_top_seller');
        $this->dropColumn('product', 'is_featured');
    }
}
