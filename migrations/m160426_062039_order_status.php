<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062039_order_status extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%order_status}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(255) NOT NULL",
                'description'=> Schema::TYPE_STRING."(255)",
                ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%order_status}}');
    }
}
