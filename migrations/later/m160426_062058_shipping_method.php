<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062058_shipping_method extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%shipping_method}}',
            [
                'id'=> Schema::TYPE_PK."",
                'title'=> Schema::TYPE_STRING."(255) NOT NULL",
                'shipping_range_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_shipping_method_shipping_range_id', '{{%shipping_method}}');
        $this->dropIndex('idx_shipping_range_id', '{{%shipping_range}}');

        $this->dropTable('{{%shipping_method}}');
    }
}
