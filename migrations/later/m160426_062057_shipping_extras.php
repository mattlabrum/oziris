<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062057_shipping_extras extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%shipping_extras}}',
            [
                'id'=> Schema::TYPE_PK."",
                'shipping_method_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'extras_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                ],
            $tableOptions
        );


        $this->createIndex('idx_shipping_method_id', '{{%shipping_extras}}','shipping_method_id',0);
        $this->createIndex('idx_extras_id', '{{%shipping_extras}}','extras_id',0);

        $this->addForeignKey('fk_shipping_extras_to_shipping_method_id', '{{%shipping_extras}}', 'shipping_method_id', 'shipping_method', 'id', 'NO ACTION','NO ACTION');
        $this->addForeignKey('fk_shipping_extras_to_extras_id', '{{%shipping_extras}}', 'extras_id', 'extras', 'id', 'NO ACTION','NO ACTION');



    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_shipping_extras_to_shipping_method_id', '{{%shipping_extras}}');
        $this->dropIndex('idx_shipping_method_id', '{{%shipping_extras}}');

        $this->dropForeignKey('fk_shipping_extras_to_extras_id', '{{%shipping_extras}}');
        $this->dropIndex('idx_extras_id', '{{%shipping_extras}}');

        $this->dropTable('{{%shipping_extras}}');
    }
}
