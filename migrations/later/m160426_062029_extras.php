<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062029_extras extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%extras}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(15) NOT NULL",
                'amount'=> Schema::TYPE_DOUBLE." NOT NULL",
                ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%extras}}');
    }
}
