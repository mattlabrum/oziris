<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062059_shipping_range extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%shipping_range}}',
            [
                'id'=> Schema::TYPE_PK."",
                'distance_from'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'distance_to'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'amount'=> Schema::TYPE_DOUBLE."(11) NOT NULL",
                ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%shipping_range}}');
    }
}
