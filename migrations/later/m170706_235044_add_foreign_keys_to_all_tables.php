<?php

use yii\db\Migration;

class m170706_235044_add_foreign_keys_to_all_tables extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        // Foreign Keys on Product Class
//        $this->addForeignKey('fk_product_group_group_id', '{{%product}}', 'product_group_id', 'product_group', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_product_type_type_id', '{{%product}}', 'product_type_id', 'product_type', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_brand_brand_id', '{{%product}}', 'brand_id', 'brand', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_manufacturer_manufacturer_id', '{{%product}}', 'manufacturer_id', 'manufacturer', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_service_type_service_type_id', '{{%product}}', 'service_type_id', 'service_type', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_shelf_life_shelf_life_unit_id', '{{%product}}', 'shelf_life_unit', 'shelf_life_unit', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_product_nature_product_nature_id', '{{%product}}', 'product_nature_id', 'product_nature', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on AppUser Class
//        $this->createIndex('idx_user_id', '{{%app_auth_data}}','user_id',0);
//        $this->addForeignKey('fk_app_auth_data_user_id', '{{%app_auth_data}}', 'user_id', 'app_user', 'id', 'NO ACTION','NO ACTION');
//
//        // Foreign Keys on AppLoginToken Class
//        $this->createIndex('idx_user_id', '{{%app_login_token}}','user_id',0);
//        $this->addForeignKey('fk_app_login_token_user_id', '{{%app_login_token}}', 'user_id', 'app_user', 'id', 'NO ACTION','NO ACTION');
//
//        // Foreign Keys on AppSocialMediaAuth Class
//        $this->createIndex('idx_user_id', '{{%app_social_media_auth}}','user_id',0);
//        $this->addForeignKey('fk_app_social_media_auth_user_id', '{{%app_social_media_auth}}', 'user_id', 'app_user', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on AppUserDeviceInfo Class
//        $this->createIndex('idx_user_id', '{{%app_user_device_info}}','user_id',0);
//        $this->addForeignKey('fk_app_user_device_info_user_id', '{{%app_user_device_info}}', 'user_id', 'app_user', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on Batch Class
//        $this->addForeignKey('fk_batch_product_id', '{{%batch}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on BatchIngredient Class
//        $this->addForeignKey('fk_batch_ingredient_batch_id', '{{%batch_ingredients}}', 'batch_id', 'batch', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_batch_ingredient_ingredient_id', '{{%batch_ingredients}}', 'ingredient_id', 'ingredient', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_batch_ingredient_supplier_id', '{{%batch_ingredients}}', 'supplier_id', 'supplier', 'id', 'NO ACTION','NO ACTION');
//        $this->createIndex('idx_coo_id', '{{%batch_ingredients}}','coo_id',0);
//        $this->addForeignKey('fk_batch_ingredient_coo_id', '{{%batch_ingredients}}', 'coo_id', 'coo', 'id', 'NO ACTION','NO ACTION');
//
//        // Foreign Keys on BatchItems Class
//        $this->addForeignKey('fk_batch_items_batch_id', '{{%batch_items}}', 'batch_id', 'batch', 'id', 'NO ACTION','NO ACTION');

//        $this->createIndex('idx_order_id', '{{%batch_items}}','order_id',0);
//        $this->createIndex('idx_woo_order_id', '{{%order}}','woo_order_id',0);
//        $this->addForeignKey('fk_batch_items_order_id', '{{%batch_items}}', 'order_id', 'order', 'woo_order_id', 'NO ACTION','NO ACTION');

//        $this->createIndex('idx_location_id', '{{%batch_items}}','location_id',0);
//        $this->addForeignKey('fk_batch_items_batch_id', '{{%batch_items}}', 'location_id', 'language', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on Brand Class
//        $this->addForeignKey('fk_brand_manufacture_id', '{{%brand}}', 'manufacturer_id', 'manufacturer', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on BrandTranslation
//        $this->addForeignKey('fk_brand_translation_brand_id', '{{%brand_translation}}', 'brand_id', 'brand', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_brand_translation_batch_id', '{{%brand_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on CollectionItem
//        $this->addForeignKey('fk_collection_item_product_id', '{{%collection_item}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_collection_item_collection_id', '{{%collection_item}}', 'collection_id', 'product', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on CouponProduct
//        $this->addForeignKey('fk_coupon_product_product_id', '{{%coupon_products}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_coupon_product_coupon_id', '{{%coupon_products}}', 'coupon_id', 'coupon', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on Ingredient
//        $this->addForeignKey('fk_ingredient_group_id', '{{%ingredient}}', 'group_id', 'ingredient_group', 'id', 'NO ACTION','NO ACTION');
//
//        // Foreign Keys on IngredientGroupTranslation
//        $this->createIndex('idx_ingredient_group_id', '{{%ingredient_group_translation}}','ingredient_group_id',0);
//        $this->createIndex('idx_language_id', '{{%ingredient_group_translation}}','language_id',0);
//
//        $this->addForeignKey('fk_ingredient_group_translation_group_id', '{{%ingredient_group_translation}}', 'ingredient_group_id', 'ingredient_group', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_ingredient_group_translation_language_id', '{{%ingredient_group_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on IngredientTranslation
//        $this->createIndex('idx_ingredient_id', '{{%ingredient_translation}}','ingredient_id',0);
//        $this->createIndex('idx_language_id', '{{%ingredient_translation}}','language_id',0);
//
//        $this->addForeignKey('fk_ingredient_translation_group_id', '{{%ingredient_translation}}', 'ingredient_id', 'ingredient', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_ingredient_translation_language_id', '{{%ingredient_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');
//
//        // Foreign Keys on ManufacturerTranslation
//        $this->addForeignKey('fk_manufacturer_translation_language_id', '{{%manufacturer_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on NutritionTagTranslation
//        $this->addForeignKey('fk_nutrition_tag_translation_nutrition_tag_id', '{{%nutrition_tag_translation}}', 'nutrition_tag_id', 'nutrition_tag', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_nutrition_tag_translation_language_id', '{{%nutrition_tag_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

        // Foreign Keys on NutritionTranslation
//        $this->addForeignKey('fk_nutrition_translation_nutrition_id', '{{%nutrition_translation}}', 'nutrition_id', 'nutrition', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_nutrition_translation_language_id', '{{%nutrition_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');
//
//        // Foreign Keys on Order
//        $this->addForeignKey('fk_order_status_id', '{{%order}}', 'status_id', 'order_status', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_order_shipping_id', '{{%order}}', 'shipping_id', 'shipping', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_order_customer_id', '{{%order}}', 'customer_id', 'app_user', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_order_shipping_address_id', '{{%order}}', 'shipping_address_id', 'address', 'id', 'NO ACTION','NO ACTION');
//        $this->addForeignKey('fk_order_billing_address_id', '{{%order}}', 'billing_address_id', 'address', 'id', 'NO ACTION','NO ACTION');

//        $this->createIndex('idx_order_id', '{{%order_coupons}}','order_id',0);
//        $this->createIndex('idx_order_id', '{{%order_items}}','order_id',0);
//
//        $this->createIndex('idx_order_id', '{{%order_status_times}}','order_id',0);
//        $this->createIndex('idx_status_id', '{{%order_status_times}}','status_id',0);

//        $this->createIndex('idx_product_id', '{{%product_cross_sell}}','product_id',0);
//        $this->createIndex('idx_cross_sell_product_id', '{{%product_cross_sell}}','cross_sell_product_id',0);
//
//        $this->createIndex('idx_product_id', '{{%product_location_attributes}}','product_id',0);

//        $this->createIndex('idx_product_id', '{{%product_upsell}}','product_id',0);
//        $this->createIndex('idx_upsell_product_id', '{{%product_upsell}}','upsell_product_id',0);
//
//        $this->createIndex('idx_user_id', '{{%reported_review}}','user_id',0);
//        $this->createIndex('idx_rating_review_id', '{{%reported_review}}','rating_review_id',0);
//
//        $this->createIndex('idx_shopping_list_id', '{{%shopping_list_items}}','shopping_list_id',0);
//
//        $this->createIndex('idx_supplier_id', '{{%supplier_translation}}','supplier_id',0);
//        $this->createIndex('idx_language_id', '{{%supplier_translation}}','language_id',0);

    }

    public function safeDown()
    {

        $this->dropIndex('product_name_id', '{{%product}}');
        $this->dropForeignKey('fk_product_group_group_id', '{{%product}}');

    }

}
