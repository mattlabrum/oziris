<?php

use yii\db\Migration;

class m170503_055132_add_price_column_to_order_items_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_items', 'price', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('order_items', 'price');
    }
}
