<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_034126_create_nutrition_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%nutrition}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(255) NULL",
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%nutrition}}');
    }
}
