<?php

use yii\db\Migration;
use yii\db\Schema;

class m170713_040839_create_subscription_orders_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%subscription_orders}}',
            [
                'id' => Schema::TYPE_PK . "",
                'subscription_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'subscription_order_id' => Schema::TYPE_INTEGER . "(15) NULL",
            ],
            $tableOptions
        );

        $this->createIndex('idx_subscription_id', '{{%subscription_orders}}','subscription_id',0);
        $this->addForeignKey('fk_subscription_orders_subscription_id', '{{%subscription_orders}}', 'subscription_id', 'subscription', 'subscription_id', 'NO ACTION','NO ACTION');


        $this->createIndex('idx_subscription_order_id', '{{%subscription_orders}}','subscription_order_id',0);
        $this->addForeignKey('fk_subscription_orders_subscription_order_id', '{{%subscription_orders}}', 'subscription_order_id', 'order', 'woo_order_id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {
        $this->dropIndex('idx_woo_order_id', '{{%order}}');
        $this->dropTable('{{%subscription_orders}}');
    }
}
