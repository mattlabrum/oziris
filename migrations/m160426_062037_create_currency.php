<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062037_create_currency extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%currency}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(15) NOT NULL",
                'sign'=> Schema::TYPE_STRING."(15) NOT NULL",
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%currency}}');
    }
}
