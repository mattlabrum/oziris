<?php

use yii\db\Migration;

class m160502_043631_add_tax_id_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'tax_id', $this->integer()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'tax_id');
    }
}
