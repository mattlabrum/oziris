<?php

use yii\db\Migration;
use yii\db\Schema;

class m161221_054915_create_box_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
            '{{%shipping_box}}',
            [
                'id' => Schema::TYPE_PK . "",
                'box_qr' => Schema::TYPE_STRING .  " NULL",
                'order_id' => Schema::TYPE_INTEGER .  " NULL",
                'pallet_id' => Schema::TYPE_INTEGER .  " NULL",
                'created_at' => Schema::TYPE_BIGINT .  " NULL",
                'updated_at' => Schema::TYPE_BIGINT .  " NULL",
                'deleted_at' => Schema::TYPE_BIGINT .  " NULL",
            ],
            $tableOptions
        );

        $this->addColumn('batch_items', 'box_id', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropTable('{{%shipping_box}}');
        $this->dropColumn('brand_items', 'box_id');
    }
}
