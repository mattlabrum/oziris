<?php

use yii\db\Migration;
use yii\db\mysql\Schema;

class m160516_061529_create_ingredient_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%ingredient}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(255)",
                'group_id'=> Schema::TYPE_INTEGER."(20)",
            ]
        );
        $this->createIndex('idx_ingredient_group_id', '{{%ingredient}}','group_id',0);
        $this->addForeignKey('fk_ingredient_group_id', '{{%ingredient}}', 'group_id', 'ingredient_group', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_ingredient_group_id', '{{%ingredient}}');
        $this->dropIndex('idx_ingredient_group_id', '{{%ingredient}}');
        $this->dropTable('{{%ingredient}}');
    }
}
