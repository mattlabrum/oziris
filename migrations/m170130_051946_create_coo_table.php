<?php

use yii\db\Migration;
use yii\db\Schema;

class m170130_051946_create_coo_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%coo}}',
            [
                'id' => Schema::TYPE_PK . "",
                'coo_latitude' => Schema::TYPE_FLOAT . " NULL",
                'coo_longitude' => Schema::TYPE_FLOAT . " NULL",
                'city' => Schema::TYPE_STRING . "(255) NULL",
                'country' => Schema::TYPE_STRING . "(255) NULL",
                'flag' => Schema::TYPE_STRING . "(255) NULL",
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%coo}}');
    }
}
