<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_045739_create_nutrition_tags_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%nutrition_tags}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(255)",
            ]
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%nutrition_tags}}');
    }
}
