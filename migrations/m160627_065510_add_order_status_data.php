<?php

use yii\db\Migration;

class m160627_065510_add_order_status_data extends Migration
{
    public function safeUp()
    {
        $this->insert('order_status',array(
            'name'=> 'Pending Payment',
            'description'=> 'Pending Payment',
        ));
        $this->insert('order_status',array(
            'name'=> 'On Hold',
            'description'=> 'On Hold',
        ));
        $this->insert('order_status',array(
            'name'=> 'Completed',
            'description'=> 'Completed',
        ));
        $this->insert('order_status',array(
            'name'=> 'Cancelled',
            'description'=> 'Cancelled',
        ));
        $this->insert('order_status',array(
            'name'=> 'Refunded',
            'description'=> 'Refunded',
        ));
        $this->insert('order_status',array(
            'name'=> 'Failed',
            'description'=> 'Failed',
        ));
        $this->insert('order_status',array(
        'name'=> 'Dispatch',
        'description'=> 'Dispatch',
        ));

    }
}
