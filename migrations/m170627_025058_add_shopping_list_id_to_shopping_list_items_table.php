<?php

use yii\db\Migration;

class m170627_025058_add_shopping_list_id_to_shopping_list_items_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('shopping_list_items', 'shopping_list_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('shopping_list_items', 'shopping_list_id');
    }
}
