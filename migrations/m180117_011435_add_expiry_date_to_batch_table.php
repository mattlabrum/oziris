<?php

use yii\db\Migration;

class m180117_011435_add_expiry_date_to_batch_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('batch', 'expiry_date', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('batch', 'expiry_date');
    }
}
