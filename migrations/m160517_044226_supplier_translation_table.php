<?php

use yii\db\Migration;
use yii\db\Schema;

class m160517_044226_supplier_translation_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%supplier_translation}}',
            [
                'id'=> Schema::TYPE_PK."",
                'supplier_name'=> Schema::TYPE_STRING."(15) NOT NULL",
                'supplier_description'=> Schema::TYPE_STRING."(15) NOT NULL",
                'language_id'=> Schema::TYPE_INTEGER."(20)",
                'supplier_id'=> Schema::TYPE_INTEGER."(20)",
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%supplier_translation}}');
    }
}