<?php

use yii\db\Migration;

class m160601_001658_insert_nutrition_setup_data extends Migration
{
    public function safeUp()
    {
        $this->insert('nutrition',array('name' =>'Energy',));
        $this->insert('nutrition',array('name' =>'',));
        $this->insert('nutrition',array('name' =>'Protein',));
        $this->insert('nutrition',array('name' =>'Fat, total',));
        $this->insert('nutrition',array('name' =>' - saturated',));
        $this->insert('nutrition',array('name' =>'Carbohydrates',));
        $this->insert('nutrition',array('name' =>' - sugars',));
        $this->insert('nutrition',array('name' =>' Sodium',));
        $this->insert('nutrition',array('name' =>' Calcium',));
        $this->insert('nutrition',array('name' =>' -trans fat',));
        $this->insert('nutrition',array('name' =>'Polyunsaturated',));
        $this->insert('nutrition',array('name' =>'Monousaturated',));
        $this->insert('nutrition',array('name' =>'Dietary Fibre',));

    }

    public function safeDown()
    {
        echo "m160502_061317_insert_setup_data does not support migration down.\n";
        return false;
    }
}
