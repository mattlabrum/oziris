<?php

use yii\db\Migration;
use yii\db\Schema;

class m170613_064531_add_coupon_products_table extends Migration
{

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%coupon_products}}',
            [
                'id' => Schema::TYPE_PK . "",
                "coupon_id"=> Schema::TYPE_INTEGER . "",
                "product_id"=> Schema::TYPE_INTEGER . ""
            ],
            $tableOptions
        );
        $this->createIndex('idx_coupon_id', '{{%coupon_products}}','coupon_id',0);
        $this->addForeignKey('fk_coupon_product_id', '{{%coupon_products}}', 'coupon_id', 'coupons', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable('{{%coupon_products}}');
    }

}
