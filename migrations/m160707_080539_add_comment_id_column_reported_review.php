<?php

use yii\db\Migration;

class m160707_080539_add_comment_id_column_reported_review extends Migration
{
    public function safeUp()
    {
        $this->addColumn('reported_review', 'comment_id', $this->integer(15));
    }

    public function safeDown()
    {
        $this->dropColumn('reported_review', 'comment_id');
    }
}
