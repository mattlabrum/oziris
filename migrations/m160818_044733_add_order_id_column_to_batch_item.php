<?php

use yii\db\Migration;

class m160818_044733_add_order_id_column_to_batch_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('batch_items', 'order_id', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('batch_items', 'order_id');
    }
}
