<?php

use yii\db\Migration;

class m170727_042820_add_8ston_user_id_to_app_user_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('app_user', '8ston_user_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('app_user', '8ston_user_id');
    }
}
