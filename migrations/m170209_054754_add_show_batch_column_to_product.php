<?php

use yii\db\Migration;

class m170209_054754_add_show_batch_column_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'show_batch', $this->smallInteger(2));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'show_batch');
    }
}
