<?php

use yii\db\Migration;

class m160711_070440_add_name_columns_to_address_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('address', 'first_name', $this->string(255));
        $this->addColumn('address', 'last_name', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('review_rating', 'comment_id');
        $this->dropColumn('review_rating', 'comment_id');
    }
}
