<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_062454_create_product_nutrition_tags_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_nutrition_tags}}',
            [
                'id'=> Schema::TYPE_PK."",
                'product_id'=> Schema::TYPE_INTEGER,
                'nutrition_tag_id'=> Schema::TYPE_INTEGER,
            ]
        );
        $this->createIndex('idx_product_id', '{{%product_nutrition_tags}}','product_id',0);
        $this->addForeignKey('fk_product_nutrition_tags_product_id', '{{%product_nutrition_tags}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_nutrition_tag_id', '{{%product_nutrition_tags}}','nutrition_tag_id',0);
        $this->addForeignKey('fk_product_nutrition_tags_nutrition_tag_id', '{{%product_nutrition_tags}}', 'nutrition_tag_id', 'nutrition_tags', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_product_nutrition_tags_product_id', '{{%product_nutrition_tags}}');
        $this->dropIndex('idx_product_id', '{{%product_nutrition_tags}}');

        $this->dropForeignKey('fk_product_nutrition_tags_nutrition_tag_id', '{{%product_nutrition_tags}}');
        $this->dropIndex('idx_nutrition_tag_id', '{{%product_nutrition_tags}}');

        $this->dropTable('{{%product_nutrition_tags}}');
    }
}
