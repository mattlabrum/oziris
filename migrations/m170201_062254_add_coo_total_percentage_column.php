<?php

use yii\db\Migration;

class m170201_062254_add_coo_total_percentage_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('batch', 'overall_coo_percentage', $this->double());
    }

    public function safeDown()
    {
        $this->dropColumn('batch', 'overall_coo_percentage');
    }
}
