<?php

use yii\db\Migration;

class m170210_030546_add_date_of_arrival_at_manufacturer_to_batch_ingredient extends Migration
{
    public function safeUp()
    {
        $this->addColumn('batch_ingredients', 'arrival_at_manufacturer_date', $this->string(25)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('batch_ingredients', 'arrival_at_manufacturer_date');
    }
}
