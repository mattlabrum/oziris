<?php

use yii\db\Migration;

class m160509_042152_drop_warehouse_table_add_warehouse_column extends Migration
{
    public function safeUp()
    {

        $this->dropForeignKey('product_quantity_ibfk_2', 'product_quantity');
        $this->dropIndex('warehouse_id', 'product_quantity');

        $this->dropColumn('product_quantity', 'warehouse_id');

        $this->dropTable('warehouse');

        $this->addColumn('product_quantity', 'warehouse', $this->string(25));

    }

    public function safeDown()
    {

    }
}
