<?php

use yii\db\Migration;
use yii\db\Schema;

class m160620_033426_alter_coloum_length_shipping_name extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->alterColumn('shipping', 'name',Schema::TYPE_STRING."(255)");
    }

    public function safeDown()
    {
    }
}
