<?php

use yii\db\Migration;
use yii\db\Schema;

class m161221_054937_create_pallet_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
            '{{%shipping_pallet}}',
            [
                'id' => Schema::TYPE_PK . "",
                'pallet_qr' => Schema::TYPE_STRING .  " NULL",
                'created_at' => Schema::TYPE_BIGINT .  " NULL",
                'updated_at' => Schema::TYPE_BIGINT .  " NULL",
                'deleted_at' => Schema::TYPE_BIGINT .  " NULL",
            ],
            $tableOptions
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{%shipping_pallet}}');
    }
}
