<?php

use yii\db\Migration;

class m160728_045655_add_address_columns_in_order_table extends Migration
{
    public function safeUp()
    {

        $this->addColumn('order', 'shipping_address_id', $this->integer());
        $this->createIndex('idx_shipping_address_id', '{{%order}}','shipping_address_id',0);
        $this->addForeignKey('fk_order_shipping_address_id', '{{%order}}', 'shipping_address_id', 'address', 'id', 'NO ACTION','NO ACTION');


        $this->addColumn('order', 'billing_address_id', $this->integer());
        $this->createIndex('idx_billing_address_id', '{{%order}}','billing_address_id',0);
        $this->addForeignKey('fk_billing_address_id', '{{%order}}', 'billing_address_id', 'address', 'id', 'NO ACTION','NO ACTION');


        $this->dropColumn('address', 'order_id');

    }

    public function safeDown()
    {

    }
}
