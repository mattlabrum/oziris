<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062011_address extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%address}}',
            [
                'id'=> Schema::TYPE_PK."",
                'address_1'=> Schema::TYPE_STRING."(255) NOT NULL",
                'address_2'=> Schema::TYPE_STRING."(255)",
                'customer_id'=> Schema::TYPE_INTEGER."(20)",
                'type'=> Schema::TYPE_STRING."(255)",
                'suburb'=> Schema::TYPE_STRING."(50) NOT NULL",
                'city'=> Schema::TYPE_STRING."(50) NOT NULL",
                'state'=> Schema::TYPE_STRING."(50) NOT NULL",
                'postcode'=> Schema::TYPE_STRING."(10) NOT NULL",
                'country'=> Schema::TYPE_STRING."(50) NOT NULL",
            ]
        );
        $this->createIndex('idx_customer_id', '{{%address}}','customer_id',0);
        $this->addForeignKey('fk_address_customer_id', '{{%address}}', 'customer_id', 'app_user', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_address_customer_id', '{{%address}}');
        $this->dropIndex('idx_customer_id', '{{%address}}');
        $this->dropTable('{{%address}}');
    }
}


