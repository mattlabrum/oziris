<?php

use yii\db\Migration;
use yii\db\Schema;

class m160829_021717_create_nutrition_translation_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%nutrition_translation}}',
            [
                'id'=> Schema::TYPE_PK."",
                'language_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'created_at'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'updated_at'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'deleted_at'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'nutrition_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'name'=> Schema::TYPE_STRING."(255) NOT NULL",
            ],
            $tableOptions
        );

        $this->createIndex('idx_language_id', '{{%nutrition_translation}}','language_id',0);
        $this->addForeignKey('fk_nutrition_translation_language_id', '{{%nutrition_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_nutrition_id', '{{%nutrition_translation}}','nutrition_id',0);
        $this->addForeignKey('fk_nutrition_translation_nutrition_id', '{{%nutrition_translation}}', 'nutrition_id', 'nutrition', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
    }
}
