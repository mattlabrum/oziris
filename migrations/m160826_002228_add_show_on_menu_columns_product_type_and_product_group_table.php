<?php

use yii\db\Migration;

class m160826_002228_add_show_on_menu_columns_product_type_and_product_group_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product_type', 'show_on_menu', $this->smallInteger()->defaultValue(0));
        $this->addColumn('product_group', 'show_on_menu', $this->smallInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('product_type', 'show_on_menu');
        $this->dropColumn('product_group', 'show_on_menu');
    }
}
