<?php

use yii\db\Migration;
use yii\db\Schema;

class m160630_050732_create_product_related_images_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_related_images}}',
            [
                'id'=> Schema::TYPE_PK."",
                'product_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'image_url'=> Schema::TYPE_STRING."(255) NOT NULL",
            ],
            $tableOptions
        );

        $this->createIndex('idx_product_id', '{{%product_related_images}}','product_id',0);
        $this->addForeignKey('fk_product_related_images_product_id', '{{%product_related_images}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_related_images_product_id', '{{%product_related_images}}');
        $this->dropIndex('idx_product_id', '{{%product_related_images}}');

        $this->dropTable('{{%product_related_images}}');
    }
}
