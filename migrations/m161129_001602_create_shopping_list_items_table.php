<?php

use yii\db\Migration;
use yii\db\Schema;

class m161129_001602_create_shopping_list_items_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
            '{{%shopping_list_items}}',
            [
                'id' => Schema::TYPE_PK . "",
                'product_id' => Schema::TYPE_INTEGER .  " NOT NULL",
                'quantity' => Schema::TYPE_INTEGER .  " NOT NULL",
            ],
            $tableOptions
        );
        $this->createIndex('idx_product_id', '{{%shopping_list_items}}','product_id',0);
        $this->addForeignKey('fk_product_id', '{{%shopping_list_items}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable('{{%shopping_list}}');
    }
}
