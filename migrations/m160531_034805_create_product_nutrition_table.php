<?php

use yii\db\Migration;
use yii\db\Schema;

class m160531_034805_create_product_nutrition_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%product_nutritions}}',
            [
                'id'=> Schema::TYPE_PK."",
                'product_id'=> Schema::TYPE_INTEGER,
                'nutrition_id'=> Schema::TYPE_INTEGER,
                'avg_qty_per_serving'=> Schema::TYPE_STRING,
                'avg_qty_per_100g'=> Schema::TYPE_STRING,
            ]
        );
        $this->createIndex('idx_product_id', '{{%product_nutritions}}','product_id',0);
        $this->addForeignKey('fk_product_nutritions_product_id', '{{%product_nutritions}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_nutrition_id', '{{%product_nutritions}}','nutrition_id',0);
        $this->addForeignKey('fk_product_nutritions_nutrition_id', '{{%product_nutritions}}', 'nutrition_id', 'nutrition', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {

        $this->dropForeignKey('fk_product_nutritions_product_id', '{{%product_nutritions}}');
        $this->dropIndex('idx_product_id', '{{%product_nutritions}}');

        $this->dropForeignKey('fk_product_nutritions_nutrition_id', '{{%product_nutritions}}');
        $this->dropIndex('idx_nutrition_id', '{{%product_nutritions}}');

        $this->dropTable('{{%product_nutritions}}');
    }
}
