<?php

use yii\db\Migration;
use yii\db\Schema;

class m161129_001547_create_shopping_list_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';
        $this->createTable(
            '{{%shopping_list}}',
            [
                'id' => Schema::TYPE_PK . "",
                'user_id' => Schema::TYPE_INTEGER .  " NOT NULL",
                'name' => Schema::TYPE_STRING . " NOT NULL",
            ],
            $tableOptions
        );
        $this->createIndex('idx_user_id', '{{%shopping_list}}','user_id',0);
        $this->addForeignKey('fk_user_id', '{{%shopping_list}}', 'user_id', 'app_user', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable('{{%shopping_list}}');
    }
}
