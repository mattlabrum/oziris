<?php

use yii\db\Migration;

class m160426_053623_drop_columns_from_app_user extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('app_user', 'address');
        $this->dropColumn('app_user', 'city');
        $this->dropColumn('app_user', 'state');
        $this->dropColumn('app_user', 'zipcode');

    }

    public function safeDown()
    {
        $this->addColumn('app_user', 'city', $this->string(25)->defaultValue('Un-disclosed'));
        $this->addColumn('app_user', 'state', $this->string(25)->defaultValue('Un-disclosed'));
        $this->addColumn('app_user', 'zipcode', $this->string(25)->defaultValue('Un-disclosed'));
        $this->addColumn('app_user', 'address', $this->string(255)->defaultValue('Un-disclosed'));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
