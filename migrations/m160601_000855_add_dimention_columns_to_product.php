<?php

use yii\db\Migration;

class m160601_000855_add_dimention_columns_to_product extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'width', $this->string(10,'NULL'));
        $this->addColumn('product', 'height', $this->string(10,'NULL'));
        $this->addColumn('product', 'length', $this->string(10,'NULL'));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'length');
        $this->dropColumn('product', 'width');
        $this->dropColumn('product', 'height');
    }
}
