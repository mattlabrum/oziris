<?php

use yii\db\Migration;

class m160505_045853_remove_location_table_add_location_product_price extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {

        $this->dropForeignKey('product_price_ibfk_2', 'product_price');
        $this->dropIndex('location_id', 'product_price');

        $this->dropColumn('product_price', 'location_id');

        $this->dropTable('location');

        $this->addColumn('product_price', 'location', $this->string(25));

    }

    public function safeDown()
    {

    }
}
