<?php

use yii\db\Migration;
use yii\db\Schema;

class m170713_040826_create_subscription_items_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%subscription_items}}',
            [
                'id' => Schema::TYPE_PK . "",
                'subscription_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'subscription_item_id' => Schema::TYPE_INTEGER . "(15) NULL",
                'quantity' => Schema::TYPE_INTEGER . "(15) NULL",
                'price' => Schema::TYPE_FLOAT . " NULL",

            ],
            $tableOptions
        );

        $this->createIndex('idx_subscription_id', '{{%subscription_items}}','subscription_id',0);
        $this->addForeignKey('fk_subscription_items_subscription_id', '{{%subscription_items}}', 'subscription_id', 'subscription', 'subscription_id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_subscription_item_id', '{{%subscription_items}}','subscription_item_id',0);
        $this->addForeignKey('fk_subscription_items_subscription_item_id', '{{%subscription_items}}', 'subscription_item_id', 'product', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropTable('{{%subscription_items}}');
    }
}
