<?php

use yii\db\Migration;

class m161208_054214_add_sorting_column_to_order_status extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_status', 'sort', $this->integer()->defaultValue(0));
        $this->addColumn('order_status_times', 'sort', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('order_status', 'sort');
        $this->dropColumn('order_status_times', 'sort');
    }
}
