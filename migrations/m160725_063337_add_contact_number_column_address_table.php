<?php

use yii\db\Migration;

class m160725_063337_add_contact_number_column_address_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('address', 'contact_no', $this->string(255));
        $this->addColumn('address', 'email', $this->string(255));
    }

    public function safeDown()
    {

    }

}
