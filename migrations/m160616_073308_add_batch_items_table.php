<?php

use yii\db\Migration;
use yii\db\Schema;

class m160616_073308_add_batch_items_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%batch_items}}',
            [
                'id'=> Schema::TYPE_PK."",
                'batch_id'=> Schema::TYPE_INTEGER."(20)",
                'qr_code_text'=> Schema::TYPE_STRING."(20)",
                'item_status'=> Schema::TYPE_STRING."(20)",
            ]
        );
        $this->createIndex('idx_batch_id', '{{%batch_items}}','batch_id',0);
        $this->addForeignKey('fk_batch_id', '{{%batch_items}}', 'batch_id', 'batch', 'id', 'NO ACTION','NO ACTION');

    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_batch_id', '{{%batch_items}}');
        $this->dropIndex('idx_batch_id', '{{%batch_items}}');
        $this->dropTable('{{%batch_items}}');
    }
}
