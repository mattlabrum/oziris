<?php

use yii\db\Schema;
use yii\db\Migration;

class m160427_071331_order extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%order}}',
            [
                'id'=> Schema::TYPE_PK."",
                'status_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'currency_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'subtotal'=> Schema::TYPE_DOUBLE." NOT NULL",
                'shipping_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'customer_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'payment_id'=> Schema::TYPE_INTEGER."(11) NULL",
                'order_note'=> Schema::TYPE_TEXT." NULL",
                'customer_ip'=> Schema::TYPE_STRING."(20) NULL",
                'customer_user_agent'=> Schema::TYPE_TEXT." NULL",
                'tax_amount'=> Schema::TYPE_DOUBLE." NOT NULL",
                'total_amount'=> Schema::TYPE_DOUBLE." NOT NULL",
                ],
            $tableOptions
        );

        $this->createIndex('idx_status_id', '{{%order}}','status_id',0);
        $this->addForeignKey('fk_order_status_id', '{{%order}}', 'status_id', 'order_status', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_currency_id', '{{%order}}','currency_id',0);
        $this->addForeignKey('fk_order_currency_id', '{{%order}}', 'currency_id', 'currency', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_shipping_id', '{{%order}}','shipping_id',0);
        $this->addForeignKey('fk_order_shipping_id', '{{%order}}', 'shipping_id', 'shipping', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_customer_id', '{{%order}}','customer_id',0);
        $this->addForeignKey('fk_order_customer_id', '{{%order}}', 'customer_id', 'app_user', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_payment_id', '{{%order}}','payment_id',0);
        $this->addForeignKey('fk_order_payment_id', '{{%order}}', 'payment_id', 'payment', 'id', 'NO ACTION','NO ACTION');



    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_status_id', '{{%order}}');
        $this->dropIndex('idx_status_id', '{{%order}}');

        $this->dropForeignKey('fk_order_currency_id', '{{%order}}');
        $this->dropIndex('idx_currency_id', '{{%order}}');

        $this->dropForeignKey('fk_order_shipping_id', '{{%order}}');
        $this->dropIndex('idx_shipping_id', '{{%order}}');


        $this->dropForeignKey('fk_order_customer_id', '{{%order}}');
        $this->dropIndex('idx_customer_id', '{{%order}}');

        $this->dropForeignKey('fk_order_payment_id', '{{%order}}');
        $this->dropIndex('idx_payment_id', '{{%order}}');

        $this->dropTable('{{%order}}');
    }
}
