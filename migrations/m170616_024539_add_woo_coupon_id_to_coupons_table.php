<?php

use yii\db\Migration;

class m170616_024539_add_woo_coupon_id_to_coupons_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('coupons', 'woo_coupon_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('coupons', 'woo_coupon_id');
    }
}
