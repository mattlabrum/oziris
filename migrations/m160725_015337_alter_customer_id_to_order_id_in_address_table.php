<?php

use yii\db\Migration;

class m160725_015337_alter_customer_id_to_order_id_in_address_table extends Migration
{
    public function safeUp()
    {

        $this->dropForeignKey('fk_address_customer_id', 'address');
        $this->dropIndex('idx_customer_id', 'address');
        $this->dropColumn('address', 'customer_id');

        $this->addColumn('address', 'order_id', $this->integer(11));
    }

    public function safeDown()
    {

    }
}
