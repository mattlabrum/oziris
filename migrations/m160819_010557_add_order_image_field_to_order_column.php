<?php

use yii\db\Migration;

class m160819_010557_add_order_image_field_to_order_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'order_image', $this->string(100)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'order_image');
    }
}
