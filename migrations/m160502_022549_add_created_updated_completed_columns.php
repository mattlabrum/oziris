<?php

use yii\db\Migration;

class m160502_022549_add_created_updated_completed_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'created_at', $this->bigInteger());
        $this->addColumn('order', 'updated_at', $this->bigInteger());
        $this->addColumn('order', 'completed_at', $this->bigInteger());
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'created_at');
        $this->dropColumn('order', 'updated_at');
        $this->dropColumn('order', 'completed_at');
    }
}

