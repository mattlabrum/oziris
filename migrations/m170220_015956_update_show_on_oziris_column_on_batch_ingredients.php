<?php

//use app\models\BatchIngredient;
use app\models\BatchIngredient;
use yii\db\Migration;
use yii\db\Schema;

class m170220_015956_update_show_on_oziris_column_on_batch_ingredients extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->alterColumn('batch_ingredients', 'show_on_oziris',$this->smallInteger(2)->defaultValue(1));

        $ingredients = BatchIngredient::find()->all();

        echo count($ingredients);

        foreach($ingredients as $ingredient){
            $ingredient->show_on_oziris = 1;
            $ingredient->save();
        }

    //        UPDATE `batch_ingredients` SET `show_on_oziris`=1 WHERE `show_on_oziris` IS NULL
    }

    public function safeDown()
    {
    }
}
