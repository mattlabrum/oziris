<?php

use yii\db\Migration;

class m170724_032115_add_shipping_price_colum_to_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'shipping_amount', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'shipping_amount');
    }
}
