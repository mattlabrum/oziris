<?php

use yii\db\Migration;

class m170205_235527_add_cool_image_name_column_to_batch extends Migration
{
    public function safeUp()
    {
        $this->addColumn('batch', 'cool_image', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('batch', 'cool_image');
    }
}
