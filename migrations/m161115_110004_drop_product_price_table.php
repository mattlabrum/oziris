<?php

use yii\db\Migration;

class m161115_110004_drop_product_price_table extends Migration
{
    public function safeUp()
    {
        $this->dropTable('product_price');
        $this->dropColumn('product', 'beston_quantity');
        $this->dropColumn('product', 'reorder_threshold');
        $this->dropColumn('product', 'ecommerce_enabled');
    }

    public function safeDown()
    {

    }
}
