<?php

use yii\db\Migration;

class m161205_043834_add_location_column_to_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'location_id', $this->string()->defaultValue(2));
        $this->addColumn('batch_items', 'location_id', $this->string()->defaultValue(2));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'location_id');
        $this->dropColumn('batch_items', 'location_id');
    }
}
