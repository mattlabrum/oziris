<?php

use yii\db\Migration;

class m160701_055740_add_woo_id_column_rating_review_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('review_rating', 'comment_id', $this->integer(15));
    }

    public function safeDown()
    {
        $this->dropColumn('review_rating', 'comment_id');
    }
}
