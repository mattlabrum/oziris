<?php

use yii\db\Schema;
use yii\db\Migration;

class m160426_062061_tax extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%tax}}',
            [
                'id'=> Schema::TYPE_PK."",
                'code'=> Schema::TYPE_STRING."(15) NOT NULL",
                'title'=> Schema::TYPE_STRING."(15) NOT NULL",
                'percentage'=> Schema::TYPE_DOUBLE." NOT NULL",
                ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%tax}}');
    }
}
