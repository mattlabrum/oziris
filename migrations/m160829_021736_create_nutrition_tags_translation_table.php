<?php

use yii\db\Migration;
use yii\db\Schema;

class m160829_021736_create_nutrition_tags_translation_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%nutrition_tag_translation}}',
            [
                'id'=> Schema::TYPE_PK."",
                'language_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'created_at'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'updated_at'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'deleted_at'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'nutrition_tag_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'name'=> Schema::TYPE_STRING."(255) NOT NULL",
            ],
            $tableOptions
        );

        $this->createIndex('idx_language_id', '{{%nutrition_tag_translation}}','language_id',0);
        $this->addForeignKey('fk_nutrition_tag_translation_language_id', '{{%nutrition_tag_translation}}', 'language_id', 'language', 'id', 'NO ACTION','NO ACTION');

        $this->createIndex('idx_nutrition_tag_id', '{{%nutrition_tag_translation}}','nutrition_tag_id',0);
        $this->addForeignKey('fk_nutrition_tag_translation_nutrition_id', '{{%nutrition_tag_translation}}', 'nutrition_tag_id', 'nutrition_tags', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
    }

}
