<?php

use yii\db\Migration;
use yii\db\Schema;

class m160517_041205_ingredient_group_translation_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%ingredient_group_translation}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(15) NOT NULL",
                'language_id'=> Schema::TYPE_INTEGER."(20)",
                'ingredient_group_id'=> Schema::TYPE_INTEGER."(20)",
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%ingredient_group_translation}}');
    }
}
