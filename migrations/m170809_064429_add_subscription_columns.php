<?php

use yii\db\Migration;

class m170809_064429_add_subscription_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'subscription_alias', $this->string());
        $this->addColumn('order', 'parent_billing_period', $this->string());
        $this->addColumn('order', 'parent_billing_interval', $this->string());
        $this->addColumn('order', 'parent_schedule_next_payment', $this->string());
        $this->addColumn('order', 'parent_subscription_delivery_day', $this->string());
        $this->addColumn('order', 'subscription_delivery_date', $this->string());
        $this->addColumn('order', 'subscription_id', $this->string());




    }

    public function safeDown()
    {
        $this->dropColumn('order', 'subscription_alias');
        $this->dropColumn('order', 'parent_billing_period');
        $this->dropColumn('order', 'parent_billing_interval');
        $this->dropColumn('order', 'parent_schedule_next_payment');
        $this->dropColumn('order', 'parent_subscription_delivery_day');
        $this->dropColumn('order', 'subscription_delivery_date');
        $this->dropColumn('order', 'subscription_id');

    }
}
