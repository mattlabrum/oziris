<?php

use yii\db\Migration;

class m160803_002203_add_is_beston_pure_product_column_in_product_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'is_beston_pure_product', $this->smallInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'is_beston_pure_product');
    }
}
