<?php

use yii\db\Migration;

class m160502_061317_insert_setup_data extends Migration
{

    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->insert('shipping',array(
            'name' =>'Flat Rate',
            'amount' => 10.00,
        ));
        $this->insert('payment',array(
            'name'=> 'Credit card (CBA)',
        ));
        $this->insert('order_status',array(
            'name'=> 'Processing',
            'description'=> 'Processing Order',
        ));
        $this->insert('currency',array(
            'name'=> 'AUD',
            'sign'=> '$',
        ));
        $this->insert('tax',array(
            'code'=> 'AU-TAX-1',
            'title'=> 'Tax',
            'percentage'=> '1',
        ));

    }

    public function safeDown()
    {
        echo "m160502_061317_insert_setup_data does not support migration down.\n";
        return false;
    }

}
