<?php

use yii\db\Migration;
use yii\db\Schema;

class m170727_054050_alter_user_email_address_to_accept_null extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

//        $this->alterColumn('{{%app_user}}', 'email_address', ' DROP NOT NULL'); //for drop not null
//        $this->alterColumn('{{%app_user}}', 'email_address', ' SET DEFAULT NULL'); //for set default null value

        $this->alterColumn('app_user', 'email_address',Schema::TYPE_STRING."( 255) NULL");
    }

    public function safeDown()
    {
    }
}
