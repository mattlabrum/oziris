<?php

use yii\db\Migration;
use yii\db\Schema;

class m170124_003037_add_created_column_to_audit_data_table extends Migration
{
    const TABLE = '{{%audit_data}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'created', Schema::TYPE_DATETIME);
    }
}
