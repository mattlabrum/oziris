<?php

use yii\db\Migration;
use yii\db\Schema;

class m160517_042445_supplier_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%supplier}}',
            [
                'id' => Schema::TYPE_PK . "",
                'supplier_name' => Schema::TYPE_STRING . "(15) NOT NULL",
                'supplier_location_latitude' => Schema::TYPE_FLOAT . " NULL",
                'supplier_location_longitude' => Schema::TYPE_FLOAT . " NULL",
                'supplier_logo' => Schema::TYPE_STRING . "(255) NULL",
                'supplier_description' => Schema::TYPE_TEXT . " NOT NULL",
                'created_at' => Schema::TYPE_INTEGER . "(15) NOT NULL",
                'updated_at' => Schema::TYPE_INTEGER . "(15) NOT NULL",
                'deleted_at' => Schema::TYPE_INTEGER . "(15) NULL",
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%supplier}}');
    }
}





