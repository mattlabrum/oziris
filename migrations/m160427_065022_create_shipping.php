<?php

use yii\db\Schema;
use yii\db\Migration;

class m160427_065022_create_shipping extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%shipping}}',
            [
                'id'=> Schema::TYPE_PK."",
                'name'=> Schema::TYPE_STRING."(15) NOT NULL",
                'amount'=> Schema::TYPE_DOUBLE." NOT NULL",
            ],
            $tableOptions
        );

    }

    public function safeDown()
    {
        $this->dropTable('{{%extras}}');
    }
}
