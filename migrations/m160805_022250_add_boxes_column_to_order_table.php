<?php

use yii\db\Migration;

class m160805_022250_add_boxes_column_to_order_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'boxes', $this->integer()->null()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'boxes');
    }
}
