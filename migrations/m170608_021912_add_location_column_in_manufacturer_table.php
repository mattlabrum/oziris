<?php

use yii\db\Migration;

class m170608_021912_add_location_column_in_manufacturer_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('manufacturer', 'location', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('manufacturer', 'location');
    }
}