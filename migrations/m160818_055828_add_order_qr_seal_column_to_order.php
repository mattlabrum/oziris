<?php

use yii\db\Migration;

class m160818_055828_add_order_qr_seal_column_to_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'order_qr_seal', $this->string(20)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'order_qr_seal');
    }
}
