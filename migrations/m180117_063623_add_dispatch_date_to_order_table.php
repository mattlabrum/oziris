<?php

use yii\db\Migration;

class m180117_063623_add_dispatch_date_to_order_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $this->addColumn('order', 'dispatch_date', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'dispatch_date');
    }
}
