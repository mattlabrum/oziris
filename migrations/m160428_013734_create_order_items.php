<?php

use yii\db\Schema;
use yii\db\Migration;

class m160428_013734_create_order_items extends Migration
{
    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%order_items}}',
            [
                'id'=> Schema::TYPE_PK."",
                'product_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'order_id'=> Schema::TYPE_INTEGER."(11) NOT NULL",
                'quantity'=> Schema::TYPE_INTEGER."(11) NOT NULL",
            ],
            $tableOptions
        );

        $this->createIndex('idx_product_id', '{{%order_items}}','product_id',0);
        $this->addForeignKey('fk_order_items_product_id', '{{%order_items}}', 'product_id', 'product', 'id', 'NO ACTION','NO ACTION');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_items_product_id', '{{%order_items}}');
        $this->dropIndex('idx_product_id', '{{%order_items}}');

        $this->dropTable('{{%order_items}}');
    }
}
