<?php

$config = [
    'id' => getenv('APP_ID'),
    'name' => getenv('APP_NAME'),
    'basePath' => dirname(__DIR__),
    'vendorPath' => '@app/../vendor',
    'runtimePath' => '@app/../runtime',
    'bootstrap' => ['log'],
    'aliases' => [
        '@admin' => '@app/modules/admin',
    ],
    'components' => [
        'assetManager' => [
            'forceCopy' => false, // Note: May degrade performance with Docker or VMs
            'linkAssets' => true, // Note: May also publish files, which are excluded in an asset bundle
            'dirMode' => YII_ENV_PROD ? 0777 : null, // Note: For using mounted volumes or shared folders
            'bundles' => YII_ENV_PROD ? require(__DIR__ . '/assets-prod.php') : null,
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages', // if advanced application, set @frontend/messages
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        //'main' => 'main.php',
                    ],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => getenv('DATABASE_DSN'),
            'username' => getenv('DATABASE_USER'),
            'password' => getenv('DATABASE_PASSWORD'),
            'charset' => 'utf8',
            'tablePrefix' => getenv('DATABASE_TABLE_PREFIX'),
        ],
        'formatter' => [
            'dateFormat' => 'php:d/m/Y',
        ],
        'imap' => [
            'class' => 'unyii2\imap\Imap',
            'connection' => [
                'imapPath' => '{syd1.email-hosting.net.au:993/imap/ssl}INBOX',
                'imapLogin' => 'operations@bestonmarketplace.com.au',
                'imapPassword' => 'jvZqmEqVn4',
                'serverEncoding'=>'default', // utf-8 default.
                'attachmentsDir'=>'/'
            ]
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
//            'viewPath' => '@app/mail',
            'useFileTransport' => false,//set this property to false to send mails to real email addresses
            //comment the following array to send mail using php's mail function
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'syd1.email-hosting.net.au',
                'username' => 'noreply@bestonmarketplace.com.au',
                'password' => '69fallinguglyturtles',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => getenv('APP_PRETTY_URLS') ? true : false,
            'showScriptName' => getenv('YII_ENV_TEST') ? true : false,
            'rules' => [
                '<controller:[\w\-]+>/<id:\d+>' => '<controller>/view',
                '<controller:[\w\-]+>/<action:[\w\-]+>/<id:\d+>' => '<controller>/<action>',
                '<controller:[\w\-]+>/<action:[\w\-]+>' => '<controller>/<action>',
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@vendor/dektrium/yii2-user/views' => '@app/views/user',
                    '@yii/gii/views/layouts' => '@admin/views/layouts',
                ],
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
            'layout' => '@admin/views/layouts/main',
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'defaultRoute' => 'profile',
            'admins' => ['admin@oziris.com.au','apfeiffer@bestonglobalfoods.com.au','apfeiffer@bestonglobalfoods.com.au','awanduri@bestonglobalfoods.com.au'],
            'enableFlashMessages' => true,
            'controllerMap' => [
                'security' => [
                    'class' => 'app\controllers\SecurityController',
                    'layout' => 'ace',
                ],
            ],
        ],

        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
//            'enableFlashMessages' => true,
        ],
        'audit' => [
            'class' => 'bedezign\yii2\audit\Audit',
            'ignoreActions' => ['*'],
        ],
    ],
    'params' => [
        'adminEmail' => getenv('APP_ADMIN_EMAIL'),
        'supportEmail' => getenv('APP_SUPPORT_EMAIL'),
        'yii.migrations' => [
            '@bedezign/yii2/audit/migrations',
            '@dektrium/user/migrations',
            '@yii/rbac/migrations',
        ]
    ]

];


$web = [
    'components' => [
        'log' => [
            'traceLevel' => getenv('YII_TRACE_LEVEL'),
            'targets' => [
                // log route handled by nginx process
                [
                    'class' => 'dmstr\log\SyslogTarget',
                    'prefix' => function () {
                        return '';
                    },
                    'levels' => YII_DEBUG ? ['error', 'warning', 'info'] : ['error', 'warning'],
                    'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
                    'enabled' => YII_DEBUG ? true : false,
                ],
                // standard file log route
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => YII_DEBUG ? ['error', 'warning', 'info'] : ['error', 'warning'],
                    'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
                    'logFile' => '@app/runtime/logs/web.log',
                    'dirMode' => 0777
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
            // web error handler
//            'class' => '\bedezign\yii2\audit\components\web\ErrorHandler',
            // console error handler
            //'class' => '\bedezign\yii2\audit\components\console\ErrorHandler',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => getenv('APP_COOKIE_VALIDATION_KEY'),
        ],
        'user' => [
            'identityClass' => 'dektrium\user\models\User',
        ],
    ]
];


$console = [
    'controllerNamespace' => 'app\commands',
    'controllerMap' => [
        'migrate' => 'dmstr\console\controllers\MigrateController',
        'fixture' => [
            'class' => 'yii\console\controllers\FixtureController',
            'namespace' => 'app\tests\codeception\fixtures',
        ],
        'faker' => [
            'class' => 'yii\faker\FixtureController',
            'namespace' => 'app\tests\codeception\fixtures',
            'templatePath' => '@app/tests/codeception/fixtures/templates',
            'fixtureDataPath' => '@app/tests/codeception/fixtures/data',
        ],
    ],
    'components' => [
        'log' => [
            'traceLevel' => getenv('YII_TRACE_LEVEL'),
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'prefix' => function () {
                        return '[console]';
                    },
                    'levels' => YII_DEBUG ? ['error', 'warning', 'info'] : ['error', 'warning'],
                    'logVars' => ['_GET', '_POST', '_FILES', '_COOKIE', '_SESSION'],
                    'logFile' => '@app/runtime/logs/console.log',
                    'dirMode' => 0777
                ],
            ],
        ],
    ]
];


$allowedIPs = [
    '127.0.0.1',
    '178.79.174.116',
    '192.168.*',
];

if (php_sapi_name() == 'cli') {
    // Console application
    $config = \yii\helpers\ArrayHelper::merge($config, $console);
} else {
    // Web application
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => YII_ENV_DEV ? $allowedIPs : array(),
    ];
    $config = \yii\helpers\ArrayHelper::merge($config, $web);
}

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => $allowedIPs,
        'generators' => [
            'giiant-model' => [
                'class' => 'schmunk42\giiant\generators\model\Generator',
                'templates' => [
                    'cornernote' => '@vendor/cornernote/yii2-gii/src/giiant/model/cornernote',
                ],
            ],
            'giiant-crud' => [
                'class' => 'schmunk42\giiant\generators\crud\Generator',
                'templates' => [
                    'cornernote' => '@vendor/cornernote/yii2-gii/src/giiant/crud/cornernote',
                ],
            ],
        ],
    ];
}

if (file_exists(__DIR__ . '/local.php')) {
    // Local configuration, if available
    $local = require(__DIR__ . '/local.php');
    $config = \yii\helpers\ArrayHelper::merge($config, $local);
}

return $config;
