<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductNutritionTag $model
 */

$this->title = Yii::t('app', 'Create Product Nutrition Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Nutrition Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-nutrition-tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
