<?php

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\Marker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ScannedProduct $model
 */

$this->title = Yii::t('app', 'Scanned Product');
$this->params['heading'] = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Scanned Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>

<div class="row">
    <?= $this->render('_menu', compact('model')); ?>
    <div class="col-lg-1"></div>
    <div class="col-lg-10">

        <hr class="hr">
        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="batch-view text-center">
                    <?php

                    $coord = new LatLng(['lat' => $model->scanned_latitude , 'lng' => $model->scanned_longitude]);
                    $map = new Map([
                        'center' => $coord,
                        'zoom' => 16,
                        'width' => '100%',
                        'containerOptions' => [
                            'align' => 'center'
                        ],
                    ]);

                    // Lets add a marker now
                    $marker = new Marker([
                        'position' => $coord,
                        'title' => 'Scan Location',
                    ]);

                    // Add marker to the map
                    $map->addOverlay($marker);

                    echo $map->display();

                    ?>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>



    </div>



<div class="scanned-product-view">





</div>
