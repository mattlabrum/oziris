<?php

/**
 * @var yii\web\View $this
 */

use app\models\OrderStatus;
use app\models\search\BudgetSearch;
use app\widgets\JavaScript;
use kartik\daterange\DateRangePicker;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Budget');
?>

<div class="brand-index">

    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Import') . ' ' . Yii::t('app', $this->title.' Entries'), ['class' => 'btn btn-oziris', 'data-toggle' => 'modal', 'data-target' => '#activity-modal']) ?>
                        <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Generate') . ' ' . Yii::t('app', 'Sample CSV'), ['class' => 'btn btn-oziris', 'id' => 'btnExport']) ?>
                        <a class='btn btn-oziris hidden ' id="btnDownload">Download CSV</a>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>
    <div class="text-center">
        <?php

//        $searchModel = new BudgetSearch();
//        $dataProvider = $searchModel->search(Yii::$app->request->get());
//
//            echo GridView::widget([
//            'dataProvider' => $dataProvider,
//            'id' => 'budget_table',
//            'export' => false,
//            'filterModel' => $searchModel,
//            'bordered'=>true,
//            'striped'=>true,
//            'condensed'=>true,
//            'responsive'=>true,
//            'floatHeader'=>true,
//            'floatHeaderOptions'=>['top'=>'0'],
//            'hover'=>true,
//            'columns' => [
//                [
//                    'class' => yii\grid\DataColumn::className(),
//                    'attribute' => 'month',
//                    'label' => 'Order Date',
//                    'contentOptions' => ['style' => 'width:200px;'],
//                    'filter' => DateRangePicker::widget([
//                        'model' => $searchModel,
//                        'attribute' => 'month',
//                        'convertFormat' => true,
//                        'pluginOptions' => [
//                            'locale' => [
//                                'format' => 'd-M-y'
//                            ],
//                        ],
//                    ]),
//                    'value' => function ($model) {
//                        return Html::a(date('d-M-y h:i a', $model->month), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
//                    },
//                    'format' => 'raw',
//                ],
//                [
//                    'class' => yii\grid\DataColumn::className(),
//                    'attribute' => 'budget_type',
//                    'filter'=>ArrayHelper::map(OrderStatus::find()->asArray()->all(), 'id', 'description'),
//                    'contentOptions' => ['style' => 'width:150px;'],
//                    'value' => function ($model) {
//                        return Html::a($model->budget_type, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
//                    },
//                    'format' => 'raw',
//                ],
//                [
//                    'class' => yii\grid\DataColumn::className(),
//                    'attribute' => 'order_note',
//                    'value' => function ($model) {
//                        return Html::a($model->order_note, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
//                    },
//                    'format' => 'raw',
//                ],
//                [
//                    'class' => yii\grid\DataColumn::className(),
//                    'attribute' => 'total_amount',
//                    'label' => 'Total',
//                    'contentOptions' => ['style' => 'width:50px;'],
//                    'value' => function ($model) {
//                        $formatter = new NumberFormatter('en_AU', NumberFormatter::CURRENCY);
//                        return Html::a($formatter->formatCurrency($model->total_amount, 'AUD'), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
//                    },
//                    'format' => 'raw',
//                ],
//
//                ['class' => ActionColumn::className(),
//                    'template' => '{update}&nbsp;&nbsp;{duplicate}',
//                    'buttons' => [
//                        'duplicate' => function ($url, $model) {
//                            return Html::a('<span class="glyphicon glyphicon-print"></span> ', ['report', 'id' => $model->woo_order_id], ['title' => Yii::t('app', 'Print Order'),]);
//                        }
//                    ],
//                    'contentOptions' => ['nowrap' => 'nowrap']
//                ],
//            ],
//        ]);
        ?>

    </div>
    <?php echo $this->render('../import-modal/modal'); ?>
</div>


<?php JavaScript::begin(); ?>
    <script>

        $('#import-file').on('change', function (event) {
            var file_data = $('#import-file').prop('files')[0];
            $('#txtFileInput').html($('#import-file')[0].files[0].name);
            var form_data = new FormData();
            form_data.append('file', file_data);

            $.ajax({
                url: '<?= Url::to(['csv-import/upload-file']); ?>', // point to server-side PHP script
                dataType: 'json',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (php_script_response) {
                    viewImportData();
                    console.log('Success : ' + php_script_response); // display response from the PHP script, if any
                },
                error: function (php_script_response) {
                    console.log('Error : ' + php_script_response);  // display response from the PHP script, if any
                }
            });
        });


        function viewImportData() {
            $.ajax({
                url: '<?= Url::to(['csv-import/import-data']); ?>', // point to server-side PHP script
                data: {fileName: $('#import-file')[0].files[0].name},
                dataType: 'json',
                type: 'POST',
                success: function (php_script_response) {
                    htmlString = '';
                    htmlString = "<div class='table-responsive'>";
                    htmlString += "<table class='table table-striped table-bordered table-condensed table-hover'>";
                    htmlString += "<tr>";
                    htmlString += "<th class='table-header text-center'>Month/Year</th>";
                    htmlString += "<th class='table-header text-center'>Budget Type</th>";
                    htmlString += "<th class='table-header text-center'>Budget Amount</th>";
                    htmlString += "</tr>";

                    for (i = 0; i < php_script_response.length; i++) {
                        htmlString += "<tr class='table-row'>";
                        htmlString += "<td class='text-center'>" + php_script_response[i].Month +'/'+ php_script_response[i].Year +"</td>";
                        htmlString += "<td class='text-center'>" + php_script_response[i].BudgetType + "</td>";
                        htmlString += "<td class='text-center'>" + php_script_response[i].BudgetAmount + "</td>";
                        htmlString += "</tr>";
                    }

                    htmlString += "</table>";
                    htmlString += "</div>";
                    $('#display-data-table').html(htmlString);

                    console.log("Success"); // display response from the PHP script, if any
//                console.log(php_script_response); // display response from the PHP script, if any
                },
                error: function (php_script_response) {
                    htmlString = "Error Loading Data Please Try Again!";

                    $('#display-data-table').html(htmlString);
                    console.log("error"); // display response from the PHP script, if any
                    console.log(php_script_response);  // display response from the PHP script, if any
                }
            });

        }

        $('#btnSaveData').on('click', function (event) {
            $('#btnSaveData').attr('disabled','disabled');
            $.ajax({
                url: '<?= Url::to(['budget/import-budget']); ?>', // point to server-side PHP script
                dataType: 'html',  // what to expect back from the PHP script, if anything
                data: {fileName: $('#import-file')[0].files[0].name},
                type: 'POST',
                success: function (php_script_response) {
                    alert(php_script_response);

                    $( '#activity-modal' ).modal( 'hide' ).data( 'bs.modal', null );
                    $('#btnSaveData').removeAttr('disabled');
                    htmlString = '';
                    $('#display-data-table').html(htmlString);
                    $('#import-file').val('');
                    $('#txtFileInput').text('');

                    location.reload(true);
                    console.log("success"); // display response from the PHP script, if any
                    console.log(php_script_response);  // display response from the PHP script, if any
                },
                error: function (php_script_response) {
                    console.log("error"); // display response from the PHP script, if any
                    console.log(php_script_response);  // display response from the PHP script, if any
                }
            });
        });

        $('#btnClose').on('click', function (event) {
            htmlString = '';
            $( '#activity-modal' ).modal( 'hide' ).data( 'bs.modal', null );
            $('#display-data-table').html(htmlString);
            $('#import-file').val('');
            $('#txtFileInput').text('');
        });


        $('#btnExport').on('click', function (event) {
            $.ajax({
                url: '<?= Url::to(['budget/generate-sample-csv']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (php_script_response) {
                    if (php_script_response) {
                        $('#btnDownload').removeClass('hidden');
                        $('#btnExport').addClass('hidden');

                    }
                    console.log(php_script_response); // display response from the PHP script, if any
                },
                error: function (php_script_response) {
                    console.log('error'); // display response from the PHP script, if any
                    console.log(php_script_response);  // display response from the PHP script, if any
                }
            });
        });

        $('#btnDownload').on('click', function (event) {
            $('#btnDownloadOrderItemCSV').attr({target: '_blank', href: ''});
            $('#btnDownload').attr({target: '_blank', href: '<?= Url::to('@web/csv/budget.csv'); ?>'});
            $('#btnDownload').addClass('hidden');
            $('#btnExport').removeClass('hidden');
        });

    </script>
<?php JavaScript::end(); ?>