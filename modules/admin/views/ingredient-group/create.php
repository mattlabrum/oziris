<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\IngredientGroup $model
 */

$this->title = Yii::t('app', 'Create Ingredient Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-group-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
