<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\IngredientGroup $model
 */

$this->title = Yii::t('app', 'Ingredient Group') . ' ' . $model->name;
$this->params['heading'] = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="ingredient-group-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\IngredientGroup'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?php $this->beginBlock('Ingredients'); ?>
    <div class="clearfix">
        <div class="pull-right">
            <?= Html::a(
                '<span class="fa fa-list"></span> ' . Yii::t('app', 'List All') . ' Ingredients',
                ['ingredient/index'],
                ['class' => 'btn text-muted btn-xs']
            ) ?>
            <?= Html::a(
                '<span class="fa fa-plus"></span> ' . Yii::t('app', 'Create') . ' Ingredient',
                ['ingredient/create', 'Ingredient' => ['group_id' => $model->id], 'ru' => ReturnUrl::getToken()],
                ['class' => 'btn btn-success btn-xs']
            ); ?>
        </div>
    </div>
    <?php Pjax::begin(['id' => 'pjax-Ingredients', 'enableReplaceState' => false, 'linkSelector' => '#pjax-Ingredients ul.pagination a, th a', 'clientOptions' => ['pjax:success' => 'function(){alert("yo")}']]) ?>
    <?= '<div class="table-responsive">' . \yii\grid\GridView::widget([
        'layout' => '{summary}{pager}<br/>{items}{pager}',
        'dataProvider' => new \yii\data\ActiveDataProvider(['query' => $model->getIngredients(), 'pagination' => ['pageSize' => 20, 'pageParam'=>'page-ingredients']]),
        'pager'        => [
            'class'          => yii\widgets\LinkPager::className(),
            'firstPageLabel' => Yii::t('app', 'First'),
            'lastPageLabel'  => Yii::t('app', 'Last')
        ],
        'columns' => [[
        'class'      => 'yii\grid\ActionColumn',
        'template'   => '{view} {update}',
        'contentOptions' => ['nowrap'=>'nowrap'],
        'urlCreator' => function ($action, $model, $key, $index) {
            // using the column name as key, not mapping to 'id' like the standard generator
            $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
            $params[0] = 'ingredient' . '/' . $action;
            return $params;
        },
        'buttons'    => [
            
        ],
        'controller' => 'ingredient'
    ],
            'id',
            'name',
    ]
    ]) . '</div>' ?>
    <?php Pjax::end() ?>
    <?php $this->endBlock() ?>


    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> IngredientGroup',
                'content' => $this->blocks['app\models\IngredientGroup'],
                'active' => true,
            ],
            [
                'label' => '<small><span class="fa fa-paperclip"></span> Ingredients</small>',
                'content' => $this->blocks['Ingredients'],
                'active' => false,
            ],
        ]
    ]);
    ?>

</div>
