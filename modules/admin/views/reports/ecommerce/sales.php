<?php
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Sales');
?>

<div class="site-index">
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <label style="padding-top: 10px ; padding-bottom: 10px" class="pull-left data-table-label-counter"><?= $this->title ?></label>
                        </td>
                        <td>
                            <select id="sale-duration" class="pull-right">
                                <option value="month">View : Monthly</option>
                                <option value="year">View : Yearly</option>
                                <option value="day">View : Daily</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <div>
                    <div id="saleChartLegend"></div>
                    <canvas style='max-height: 400px' id="sales-report-line-chart"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>