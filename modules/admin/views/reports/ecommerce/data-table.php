<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Home');
?>
<div class="site-index ">

        <div class="col-lg-12 oziris-block">
            <div class="row">
                <label style="padding-left: 10px ; padding-top: 10px" class="pull-left data-table-label-counter">Sales </label>
            </div>

            <div class="col-lg-2">

                <label class="data-table-label">Gross Sale</label><br>
                <label class="data-table-text-ecommerce-reports">$ </label><label id="gross_sale" class="data-table-text-ecommerce-reports">0.0</label>
            </div>
            <div class="col-lg-2">
                <label class="data-table-label">Average Daily Sale</label><br>
                <label class="data-table-text-ecommerce-reports">$ </label><label id="avg_daily_sale" class="data-table-text-ecommerce-reports">0.0</label>
            </div>
            <div class="col-lg-2">
                <label class="data-table-label">Net Sales</label><br>
                <label class="data-table-text-ecommerce-reports">$ </label><label id="net_sales" class="data-table-text-ecommerce-reports">0.0</label>
            </div>
            <div class="col-lg-2">
                <label class="data-table-label">Total Tax</label><br>
                <label class="data-table-text-ecommerce-reports">$ </label><label id="total_tax" class=" data-table-text-ecommerce-reports">0.0</label>
            </div>
            <div class="col-lg-2">
                <label class="data-table-label">Average Spent Per Order</label><br>
                <label class="data-table-text-ecommerce-reports">$ </label><label id="avg_spent_per_order" class="data-table-text-ecommerce-reports">0.0</label>
            </div>
            <div class="col-lg-2">
            <!--
                <label class="data-table-label">Total Coupons Redeemed</label><br>
                <label class="data-table-text-ecommerce-reports">$ </label><label id="coupons_redeemed" class="data-table-text-ecommerce-reports">0.0</label>
            -->
            </div>
        </div>
<div class="col-lg-12" style="padding-top: 5px ; padding-bottom: 5px" ></div>
        <div class="col-lg-12 oziris-block" style="padding-top: 5px">
            <div class="row">
                <label style="padding-left: 10px ; padding-top: 10px" class="pull-left data-table-label-counter">Orders </label>
            </div>

            <div class="col-lg-2">
                <label class="data-table-label">Processing</label><br>
                <label id="total_orders_processing" class=" data-table-text-ecommerce-reports">0</label>
            </div>

            <div class="col-lg-2">
                <label class="data-table-label">Ready To Dispatch</label><br>
                <label id="total_orders_ready_to_dispatch" class=" data-table-text-ecommerce-reports">0</label>
            </div>

            <div class="col-lg-2">
                <label class="data-table-label">Dispatched</label><br>
                <label id="total_orders_dispatched" class=" data-table-text-ecommerce-reports">0</label>
            </div>

            <div class="col-lg-2">
                <label class="data-table-label">Completed</label><br>
                <label id="total_orders_completed" class=" data-table-text-ecommerce-reports">0</label>
            </div>

            <div class="col-lg-2">
                <label class="data-table-label">Cancelled</label><br>
                <label id="total_orders_cancelled" class=" data-table-text-ecommerce-reports">0</label>
            </div>

            <div class="col-lg-2">
                <label class="data-table-label">Total Orders</label><br>
                <label id="total_orders" class=" data-table-text-ecommerce-reports">0</label>
            </div>
        </div>
</div>


<?php JavaScript::begin(); ?>

    <script>
//        populateDataTable();
//
//        function populateDataTable() {
//
//            $.ajax({
//                url: '<?//= Url::to(['api/oziris-reports-api/populate-data-table']); ?>//', // point to server-side PHP script
//                type: 'post',
//                success: function (data) {
//                    console.log(data);
//                    $('#number_of_users').text(data[0].total_users);
//                    $('#number_of_products').text(data[0].total_products);
//                    $('#number_of_counterfeit_scans').text(data[0].total_counterfeit_scans);
//                    $('#number_of_scans').text(data[0].total_scans);
//                    $('#number_of_rating_reviews').text(data[0].review_ratings);
//                },
//                error: function (data) {
//                    console.log(data);
//                }
//            });
//        }

    </script>
<?php JavaScript::end(); ?>