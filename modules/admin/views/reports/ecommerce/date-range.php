<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use dmstr\helpers\Html;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\Url;

?>
    <div class="site-index">
        <div class="col-lg-11">
            <div class="row">

                <div id="continent-div" class="col-lg-3 reports-dropdown-background">
                    <?=
                    Select2::widget([
                        'id' => 'continent',
                        'class' => 'disabled',
                        'name' => 'continent',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Continent : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>

                </div>
                <div id="country-div" class="col-lg-3 reports-dropdown-background">
                    <?=
                    Select2::widget([
                        'id' => 'country',
                        'name' => 'country',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Country : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>
                </div>
                <div id="state-div" class="col-lg-3 reports-dropdown-background">
                    <?=

                    Select2::widget([
                        'id' => 'state',
                        'name' => 'state',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'State : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>
                </div>

                <div id="postcode-div" class="col-lg-3 reports-dropdown-background">
                    <?=
                    Select2::widget([
                        'id' => 'postcode',
                        'name' => 'postcode',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Post Code : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>
                </div>

            </div>

            <div class="row">
                <div id="age-div" class="col-lg-3 reports-dropdown-background">
                    <?=
                    Select2::widget([
                        'id' => 'age',
                        'name' => 'age',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Age : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-lg-3 reports-dropdown-background">
                    <?=
                    Select2::widget([
                        'id' => 'gender',
                        'name' => 'gender',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Gender : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>
                </div>
                <div id="channel-div" class="col-lg-3 reports-dropdown-background">
                    <?=
                    Select2::widget([
                        'id' => 'channel',
                        'name' => 'channel',
                        'theme' => Select2::THEME_CLASSIC,
                        'options' => [
                            'placeholder' => Yii::t('app', 'Trading Channel : All'),
                            'multiple' => false,
                        ],
                    ]);
                    ?>
                </div>
                <div class="col-lg-3 reports-dropdown-background">

                    <div class="input-group drp-container" id="daterange-container" style="width: 100%">

                        <?= DateRangePicker::widget([
                            'name' => 'date_range_1',
                            'id' => 'date_range',
                            'convertFormat' => true,
                            'useWithAddon' => true,
                            'initRangeExpr' => true,
                            'presetDropdown' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd-M-y',
                                    'separator' => ' - ',
                                ],
                                'opens' => 'center'
                            ],
                            'pluginEvents' => [
                                "cancel.daterangepicker" => "function() { $('#date_range').val(''); }",
                                "apply.daterangepicker" => "function() { applyDataFilters() }",
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-1">
            <div class="row">
                <div class=" reports-dropdown-background">
                    <button type="button" id="btn-filter" style="width: 90%; height: 28px"
                            class="btn-report-filter btn-oziris-normal">Filter
                    </button>
                </div>
            </div>

            <div  class="row">
                <div class=" reports-dropdown-background">
                    <button type="button" id="btn-clear" style="width: 90%; height: 28px"
                            class="btn-report-filter btn-oziris-normal">Clear
                    </button>
                </div>
            </div>

        </div>
    </div>

