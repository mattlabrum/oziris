<?php

$legend = "Legend"

?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center col-lg-4">
                <div class="row">
                    <label style="padding-top: 10px ; padding-bottom: 10px"
                           class="data-table-label-counter" id="lbl-order">Orders by Continent</label>
                </div>
                <div class="row">
                    <canvas height="150px" width="150px" id="states-pie-chart-orders"/>
                </div>
            </div>
            <div class="text-center col-lg-4">
                <div class="row">
                    <label style="padding-top: 10px ; padding-bottom: 10px"
                           class="data-table-label-counter" id="lbl-sale">Sales by Continent</label>
                </div>
                <div class="row">
                    <canvas height="150px" width="150px" id="states-pie-chart-sales"/>
                </div>
            </div>
            <div class="text-center col-lg-4">
                <div class="row">
                    <label style="padding-top: 10px ; padding-bottom: 10px"
                           class="data-table-label-counter center-text"><?= $legend ?></label>
                </div>
                <div class="row">
                    <div id="statesPieChartLegend">
                    </div>
                </div>

            </div>
            <div class="col-lg-1">

            </div>
        </div>
    </div>
</div>
