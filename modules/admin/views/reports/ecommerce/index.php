<?php
/**
 * @var $this yii\web\View
 * @var $model EcommerceForm
 */

use app\models\form\EcommerceForm;
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Reports');
?>
<script src="../../../src/assets/web/js/chartjs/Chart.js"></script>
    <div>
        <div class="row">
            <div class="col-lg-12">
                <table width="100%">
                    <tr>
                        <td>
                            <h1 class="pull-left"><?= $this->title ?></h1>
                        </td>
                    </tr>
                </table>
                <hr class="hr">
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-lg-12 text-center">
        <div class="row reports-dropdown-background" style="padding-top: 5px ; padding-bottom: 5px">
                <?= $this->render('date-range'); ?>
        </div>

        <div class="row" style="padding-top: 5px ; padding-bottom: 5px">
            <div>
                <?= $this->render('data-table'); ?>
            </div>
        </div>

        <div class="row" style="padding-top: 5px ; padding-bottom: 5px">
            <div class="col-lg-6 oziris-block">
                <?= $this->render('sales'); ?>
            </div>
            <div class="col-lg-6 oziris-block">
                <?= $this->render('orders'); ?>
            </div>
        </div>

        <div class="row" style="padding-top: 5px ; padding-bottom: 5px">
            <div class="col-lg-6 oziris-block">
                <?= $this->render('states-pie-charts'); ?>
            </div>
            <div class="col-lg-6 oziris-block">
                <?= $this->render('channel-pie-charts'); ?>
            </div>
        </div>

        <div class="row" style="padding-top: 5px ; padding-bottom: 5px">
            <div class="col-lg-6 oziris-block">
                <?= $this->render('type-pie-charts'); ?>
            </div>
            <div class="col-lg-6 oziris-block">
                <?= $this->render('group-pie-charts'); ?>
            </div>
        </div>

        <div class="row" style="padding-top: 5px ; padding-bottom: 5px">
            <div class="col-lg-6 oziris-block">
                <?= $this->render('nature-pie-charts'); ?>
            </div>
            <div class="col-lg-6 oziris-block">

            </div>
        </div>

    </div>
</div>
<?php JavaScript::begin(); ?>

<script>

    $('#sale-duration').on('change', getSalesComparison);
    $('#order-duration').on('change', getOrdersComparison);
    $('#btn-filter').on('click', applyDataFilters);
    $('#btn-clear').on('click', initializeDropDowns);

    $('#daterange-placeholder').on('click', hidePlaceHolderTextInput);


    function hidePlaceHolderTextInput() {
        $('#daterange-placeholder').addClass('hidden');
        $('#date_range').show();
    }

    initializeDropDowns();

    function initializeDropDowns() {
        $('#continent-div').addClass('disabled');
        $('#country-div').addClass('disabled');
        $('#state-div').addClass('disabled');
        $('#postcode-div').addClass('disabled');
        $('#age-div').addClass('disabled');
        $('#gender-div').addClass('disabled');
        $('#channel-div').addClass('disabled');
        $('#product_group-div').addClass('disabled');
        $('#product_type-div').addClass('disabled');
        $('#product_nature-div').addClass('disabled');


        $('#date_range').val('');

        populateContinentDropDown();
        populateCountryDropDown();
        populateStateDropDown();
        populatePostCodeDropDown();
        populateAgeDropDown();
        populateGenderDropDown();
        populateTradingChannelDropDown();

        applyDataFilters();

        $('#daterange-placeholder').removeClass('hidden');
        $('#date_range').hide();
    }

    function getFilterData(){
        continent = $('#continent').find(":selected").text();
        country = $('#country').find(":selected").text();
        state = $('#state').find(":selected").text();
        postcode = $('#postcode').find(":selected").text();

        age = $('#age').find(":selected").text();
        gender = $('#gender').find(":selected").text();
        channel = $('#channel').find(":selected").text();
        saleduration = $('#sale-duration').find(":selected").val();
        orderduration = $('#order-duration').find(":selected").val();

        chartfilter = '';
        chartfilterdata = '';

        date_range = $('#date_range').val();
        date_from = date_range.substr(0, (date_range.indexOf(' ')));
        date_to = date_range.substr((date_range.indexOf(' ') + 3), date_range.length);

        data =  {continent, country, state , postcode ,age,gender,date_from,date_to,saleduration,orderduration, channel};
        return data;
    }


    function applyDataFilters() {
        getSalesData();
        getSalesComparison();
        getOrdersComparison();
        getStatesPieChartData('');
        getChannelPieChartData();
        getProductTypePieChartData();
        getProductGroupPieChartData();
        getProductNaturePieChartData();
    }

    function getSalesData() {

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-sales-data']); ?>', // point to server-side PHP script
            data: data,
            success: function (data) {
                console.log(data);
                $('#gross_sale').text(data['sale']['grossSale']);
                $('#avg_daily_sale').text(data['sale']['avgDailySales']);
                $('#net_sales').text(data['sale']['netSales']);
                $('#total_tax').text(data['sale']['totalTax']);
                $('#avg_spent_per_order').text(data['sale']['avgSalesPerOrder']);
//                $('#coupons_redeemed').text(data['coupons']);

                $('#total_orders_processing').text(data['orders']['totalOrdersProcessing']);
                $('#total_orders_ready_to_dispatch').text(data['orders']['totalOrdersReadyToDispatch']);
                $('#total_orders_dispatched').text(data['orders']['totalOrdersDispatched']);
                $('#total_orders_completed').text(data['orders']['totalOrdersCompleted']);
                $('#total_orders_cancelled').text(data['orders']['totalOrdersCancelled']);
                $('#total_orders').text(data['orders']['totalOrders']);
            },
            error: function (data) {
                console.log('Error : ' + data);  // display response from the PHP script, if any
            }
        });

    }


    function getSalesComparison() {

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-sales-comparison']); ?>', // point to server-side PHP script
            data: data,
            success: function (data) {
                console.log(data);
                if (window.saleLineChart != null) {
                    window.saleLineChart.destroy();
                }
                var ctx = document.getElementById("sales-report-line-chart").getContext("2d");
                window.saleLineChart = new Chart(ctx).Line(data, {
                    responsive: true
                });
                document.getElementById('saleChartLegend').innerHTML = saleLineChart.generateLegend();
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function getOrdersComparison() {

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-orders-comparison']); ?>', // point to server-side PHP script
            data: data,
            success: function (data) {

                if (window.orderLineChart != null) {
                    window.orderLineChart.destroy();
                }
                var ctx = document.getElementById("order-report-line-chart").getContext("2d");
                window.orderLineChart = new Chart(ctx).Line(data, {
                    responsive: true
                });
                document.getElementById('orderChartLegend').innerHTML = orderLineChart.generateLegend();
                console.log(data);
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function populateTradingChannelDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-trading-channel-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                console.log(data);
                $('#channel').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#channel').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#channel-div').removeClass('disabled');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }


    function populatePostCodeDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-postcode-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                console.log(data);
                $('#postcode').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#postcode').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#postcode-div').removeClass('disabled');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }


    function populateStateDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-state-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                console.log(data);
                $('#state').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#state').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#state-div').removeClass('disabled');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }


    function populateContinentDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-continent-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                console.log(data);
                $('#continent').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#continent').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#continent-div').removeClass('disabled');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }


    function populateAgeDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-age-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                console.log(data);
                $('#age').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#age').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#age-div').removeClass('disabled');
            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function populateGenderDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-gender-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                console.log(data);
                $('#gender').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#gender').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#gender-div').removeClass('disabled');

            },
            error: function (data) {
                console.log(data);
            }
        });
    }

    function populateCountryDropDown() {

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-ecommerce-country-dropdown-data']); ?>', // point to server-side PHP script
            type: 'post',
            success: function (data) {
                $('#country').find('option').remove().end();
                $.each(data.results, function (i, item) {
                    $('#country').append($('<option>', {
                        value: item.id,
                        text: item.text
                    }));
                });
                $('#country-div').removeClass('disabled');
            }
        });
    }

    document.getElementById("channel-pie-chart-orders").ondblclick = function(evt)
    {
        var activePoints = channelOrderPieChart.getSegmentsAtEvent(evt);
        if(activePoints.length > 0)
        {
            var clickedElementindex = activePoints[0]["_saved"]["label"];
            console.log(clickedElementindex);
            var selectedOption = $("#channel option:contains("+clickedElementindex+")")[0].value;
        }
        $('#channel').val(selectedOption).change();
    }

    document.getElementById("states-pie-chart-orders").ondblclick = function(evt)
    {
        updateLocationPieChartOnClick(evt)
    }

    document.getElementById("states-pie-chart-sales").ondblclick = function(evt)
    {
        updateLocationPieChartOnClick(evt)
    }


    function updateLocationPieChartOnClick(evt){
        var activePoints = stateOrderPieChart.getSegmentsAtEvent(evt);
        if(activePoints.length > 0)
        {
            var clickedElementindex = activePoints[0]["_saved"]["label"];
            getStatesPieChartData(clickedElementindex);
        }
    }

    function getStatesPieChartData(filter){
        $.loader.open();
        data = getFilterData();

//        console.log('hello'+filter);

        if(filter == ''){
            data.chartfilter = 'postcode';
        }else if( document.getElementById("lbl-order").innerHTML == "Orders by Continent" && filter != '' ){
            data.chartfilter = 'continent';
        }else if( document.getElementById("lbl-order").innerHTML == "Orders by Country" ){
            data.chartfilter = 'country';
        }else if( document.getElementById("lbl-order").innerHTML == "Orders by State" ){
            data.chartfilter = 'state';
        }else if( document.getElementById("lbl-order").innerHTML == "Orders by Postcode" ){
            data.chartfilter = 'postcode';
        }



        data.chartfilterdata = filter;
        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-total-sales-by-state-pie-chart-data']); ?>',
            data : data,
            success: function (data) {
                console.log(data);


                if (window.stateOrderPieChart != null) {
                    window.stateOrderPieChart.destroy();
                }
                var ctx = document.getElementById("states-pie-chart-orders").getContext("2d");
                window.stateOrderPieChart = new Chart(ctx).Doughnut(data['orders'], {
                    responsive: false,
                    percentageInnerCutout: 0,
                    segmentShowStroke: false
                });

                if (window.stateSalesPieChart != null) {
                    window.stateSalesPieChart.destroy();
                }
                var ctx = document.getElementById("states-pie-chart-sales").getContext("2d");
                window.stateSalesPieChart = new Chart(ctx).Doughnut(data['sales'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                if(filter == ''){
                    document.getElementById("lbl-order").innerHTML = "Orders by Continent"
                    document.getElementById("lbl-sale").innerHTML = "Orders by Continent"
                }else if( document.getElementById("lbl-order").innerHTML == "Orders by Continent" && filter != '' ){
                    document.getElementById("lbl-order").innerHTML = "Orders by Country"
                    document.getElementById("lbl-sale").innerHTML = "Orders by Country"
                }else if( document.getElementById("lbl-order").innerHTML == "Orders by Country" ){
                    document.getElementById("lbl-order").innerHTML = "Orders by State"
                    document.getElementById("lbl-sale").innerHTML = "Orders by State"
                }else if( document.getElementById("lbl-order").innerHTML == "Orders by State" ){
                    document.getElementById("lbl-order").innerHTML = "Orders by Postcode"
                    document.getElementById("lbl-sale").innerHTML = "Orders by Postcode"
                }else if( document.getElementById("lbl-order").innerHTML == "Orders by Postcode" ){
                    document.getElementById("lbl-order").innerHTML = "Orders by Continent"
                    document.getElementById("lbl-sale").innerHTML = "Orders by Continent"}





                document.getElementById('statesPieChartLegend').innerHTML = stateOrderPieChart.generateLegend();
                $.loader.close();

            },
            error: function (data) {
                console.log('Error : ' + data);
            }
        });
    }

    function getChannelPieChartData(){

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-total-sales-by-channel-pie-chart-data']); ?>',
            data : data,
            success: function (data1) {
                console.log(data1);

                if (window.channelOrderPieChart != null) {
                    window.channelOrderPieChart.destroy();
                }
                var ctx = document.getElementById("channel-pie-chart-orders").getContext("2d");
                window.channelOrderPieChart = new Chart(ctx).Doughnut(data1['orders'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                if (window.channelSalesPieChart != null) {
                    window.channelSalesPieChart.destroy();
                }
                var ctx = document.getElementById("channel-pie-chart-sales").getContext("2d");
                window.channelSalesPieChart = new Chart(ctx).Doughnut(data1['sales'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                document.getElementById('channelPieChartLegend').innerHTML = channelOrderPieChart.generateLegend();
            },
            error: function (data1) {
                console.log('Error : ' + data1);
            }
        });
    }

    function getProductTypePieChartData(){

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-total-sales-by-product-type-pie-chart-data']); ?>',
            data : data,
            success: function (data) {
                console.log(data);

                if (window.typeOrderPieChart != null) {
                    window.typeOrderPieChart.destroy();
                }
                var ctx = document.getElementById("type-pie-chart-orders").getContext("2d");
                window.typeOrderPieChart = new Chart(ctx).Doughnut(data['orders'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                if (window.typeSalesPieChart != null) {
                    window.typeSalesPieChart.destroy();
                }
                var ctx = document.getElementById("type-pie-chart-sales").getContext("2d");
                window.typeSalesPieChart = new Chart(ctx).Doughnut(data['sales'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                document.getElementById('typePieChartLegend').innerHTML = typeOrderPieChart.generateLegend();
            },
            error: function (data) {
                console.log('Error : ' + data);
            }
        });
    }

    function getProductGroupPieChartData(){

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-total-sales-by-product-group-pie-chart-data']); ?>',
            data : data,
            success: function (data) {
                console.log(data);

                if (window.groupOrderPieChart != null) {
                    window.groupOrderPieChart.destroy();
                }
                var ctx = document.getElementById("group-pie-chart-orders").getContext("2d");
                window.groupOrderPieChart = new Chart(ctx).Doughnut(data['orders'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                if (window.groupSalesPieChart != null) {
                    window.groupSalesPieChart.destroy();
                }
                var ctx = document.getElementById("group-pie-chart-sales").getContext("2d");
                window.groupSalesPieChart = new Chart(ctx).Doughnut(data['sales'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                document.getElementById('groupPieChartLegend').innerHTML = groupOrderPieChart.generateLegend();
            },
            error: function (data) {
                console.log('Error : ' + data);
            }
        });
    }

    function getProductNaturePieChartData(){

        data = getFilterData();

        $.ajax({
            url: '<?= Url::to(['api/reports/oziris-reports-api/get-total-sales-by-product-nature-pie-chart-data']); ?>',
            data : data,
            success: function (data) {
                console.log(data);

                if (window.natureOrderPieChart != null) {
                    window.natureOrderPieChart.destroy();
                }
                var ctx = document.getElementById("nature-pie-chart-orders").getContext("2d");
                window.natureOrderPieChart = new Chart(ctx).Doughnut(data['orders'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                if (window.natureSalesPieChart != null) {
                    window.natureSalesPieChart.destroy();
                }
                var ctx = document.getElementById("nature-pie-chart-sales").getContext("2d");
                window.natureSalesPieChart = new Chart(ctx).Doughnut(data['sales'], {
                    responsive: false,
                    segmentShowStroke: false,
                    percentageInnerCutout: 0
                });

                document.getElementById('naturePieChartLegend').innerHTML = natureOrderPieChart.generateLegend();
            },
            error: function (data) {
                console.log('Error : ' + data);
            }
        });
    }


</script>

<?php JavaScript::end(); ?>
