<?php

$orders = "Qty Sold By Product Group";
$sales = "Sales By Product Group";
$legend = "Legend"

?>
<div class="site-index">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center col-lg-4">
                <div class="row">
                    <label style="padding-top: 10px ; padding-bottom: 10px"
                           class="data-table-label-counter"><?= $orders ?></label>
                </div>
                <div class="row">
                    <canvas height="150px" width="150px" id="group-pie-chart-orders"/>
                </div>
            </div>
            <div class="text-center col-lg-4">
                <div class="row">
                    <label style="padding-top: 10px ; padding-bottom: 10px"
                           class="data-table-label-counter"><?= $sales ?></label>
                </div>
                <div class="row">
                    <canvas height="150px" width="150px" id="group-pie-chart-sales"/>
                </div>
            </div>
            <div class="text-center col-lg-4">
                <div class="row">
                    <label style="padding-top: 10px ; padding-bottom: 10px"
                           class="data-table-label-counter center-text"><?= $legend ?></label>
                </div>
                <div class="row">
                    <div id="groupPieChartLegend">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
