<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;

$this->title = Yii::t('app', 'Reports');
?>
    <script src="../../../src/assets/web/js/chartjs/Chart.js"></script>
    <div >
    <div class="row">
        <div class="col-lg-12">
            <table width="100%">
                <tr>
                    <td>
                        <h1 class="pull-left"><?= $this->title ?></h1>
                    </td>
                    <td>
                        <div class="pull-right">
                            <?= Html::a('<span class="fa fa-print"></span> ' . Yii::t('app', 'Print'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
                        </div>
                    </td>
                </tr>
            </table>
            <hr class="hr">
        </div>
    </div>

    <div class="col-lg-12 text-center">
        <div class="row">
            <div class="col-lg-3 oziris-block box">
                <?= $this->render('charts/countries'); ?>
            </div>
            <div class="col-lg-3 oziris-block box">
                <?= $this->render('charts/cities'); ?>
            </div>
            <div class="col-lg-3 oziris-block" >
                <?= $this->render('charts/age-range'); ?>
            </div>
            <div class="col-lg-3 oziris-block">
                <?= $this->render('charts/gender'); ?>
            </div>
        </div>
        <div class="row" style="padding-top: 10px">
            <?= $this->render('charts/dropdowns'); ?>
        </div>
        <div class="row" style="padding-top: 10px ; padding-bottom: 10px">
            <div class="oziris-block">
                <?= $this->render('charts/data-table'); ?>
            </div>
        </div>
        <div class="row oziris-block">
            <div class="col-lg-6" style="border-right: 1px solid #bebebe;">
                <?= $this->render('charts/most-popular-products'); ?>
            </div>
            <div class="col-lg-6 ">
                <?= $this->render('charts/total-scans-of-product'); ?>
            </div>
        </div>
        <div class="row" style="padding-top: 10px">
            <div class="oziris-block">
                <?= $this->render('charts/product-scan-calender'); ?>
            </div>
        </div>
    </div>
</div>


<?php JavaScript::begin(); ?>
    <script>
        var options = {
            segmentShowStroke : true,//Boolean - Whether we should show a stroke on each segment
            segmentStrokeColor : "#fff",//String - The colour of each segment stroke
            segmentStrokeWidth : 2,//Number - The width of each segment stroke
            percentageInnerCutout : 75, //Number - The percentage of the chart that we cut out of the middle. This is 0 for Pie charts
            animationSteps : 100, //Number - Amount of animation steps
            animationEasing : "easeOutBounce", //String - Animation easing effect
            animateRotate : true, //Boolean - Whether we animate the rotation of the Doughnut
            animateScale : false, //Boolean - Whether we animate scaling the Doughnut from the centre
            showXAxisLabel : false,
            responsive:true

        };
    </script>

<?php JavaScript::end(); ?>