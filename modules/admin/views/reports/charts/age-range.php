<?php
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Age Range');
?>
<div class="site-index" style="height: 180px">

    <div class="row">
        <div class="col-lg-12">

            <div class="col-lg-6">
                <div class="row">
                    <h1 class="pull-left h7"><?= $this->title ?></h1>
                </div>
                <div id="content-age-range" class="row">

                </div>
            </div>

            <div class="col-lg-6" style="padding-top: 20px">
                <div class="row">
                    <canvas height="110px" width="150px" id="age-range-chart"/>
                </div>
            </div>
        </div>
    </div>

    <?php JavaScript::begin(); ?>

    <script>

        $.ajax({

            url: '<?= Url::to(['api/reports/oziris-reports-api/get-age-range-report-data']); ?>', // point to server-side PHP script
            dataType: 'json',  // what to expect back from the PHP script, if anything
            contentType: false,
            processData: false,
            type: 'post',
            success: function (php_script_response) {
                viewAgeRangeData(php_script_response);
            },
            error: function (php_script_response) {
                console.log('Error : ' + php_script_response);  // display response from the PHP script, if any
            }
        });

        function viewAgeRangeData(php_script_response){
            htmlString = php_script_response.length;
            htmlString = "<div class='table-responsive text-left'>";
            htmlString += "<table style='width: 100%;font-weight: bolder'>";

            for (i = 0; i < php_script_response.length; i++) {
                htmlString += "<tr>";
                htmlString += "<td width='30px'>" + php_script_response[i].label + "</td>";
                htmlString += "<td width='30px'>" + Math.round((php_script_response[i].value/php_script_response[i].total_users)*100) + "% </td>";
                htmlString += "</tr>";
            }
            htmlString += "</table>";
            htmlString += "</div>";
            $('#content-age-range').html(htmlString);

            var ctx = document.getElementById("age-range-chart").getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(php_script_response, options);

            console.log("Success"); // display response from the PHP script, if any
            console.log(php_script_response); // display response from the PHP script, if any
        }
    </script>

    <?php JavaScript::end(); ?>


</div>
