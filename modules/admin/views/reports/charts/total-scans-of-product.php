<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Total Product Scans');
?>
<div class="site-index ">
    <div class="row">
        <div class="col-lg-12">
            <table style="width: 100%">
                <tr>
                    <td>
                        <h1 class="pull-left h7"><?= $this->title ?></h1>
                    </td>
                    <td>
                        <select id="sort-total-scans" class="pull-right">
                            <option value="DESC">Sort : Most Popular</option>
                            <option value="ASC">Sort : Least Popular</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <hr>
    </div>

    <div class="row">
        <div id="content-total-product-scans" class="col-lg-12">

        </div>
    </div>

</div>


<?php JavaScript::begin(); ?>
<script>

    $('#sort-total-scans').on('change',getTotalProductScans);
        getTotalProductScans();

        function getTotalProductScans(){
            city = ($('#city').find(":selected").text() != 'City : All') ? $('#city').find(":selected").text() : '';
            country = ($('#country').find(":selected").text() != 'Country : All') ? $('#country').find(":selected").text() : '';
            age = ($('#age').find(":selected").text() != 'Age Range : All') ? $('#age').find(":selected").text() : '';
            gender = ($('#gender').find(":selected").text() != 'Gender : All') ? $('#gender').find(":selected").text() : '';
            date_range = $('#date_range').val();
            sort = $('#sort-total-scans').find(":selected").val();

            date_from = date_range.substr(0,(date_range.indexOf(' ')));
            date_to = date_range.substr((date_range.indexOf(' ')+3),date_range.length);

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/get-most-popular-scanned-products']); ?>', // point to server-side PHP script
                data: { city,country,age,gender,date_from,date_to,sort, limit :0 },
                type: 'post',
                success: function (data) {

                    htmlString = '';
                    htmlString = "<div class='product-scan'>";
                    htmlString += "<table style='text-align: center' >";
                    htmlString += "<tbody>";
                    for (i = 0; i < data.labels.length; i++) {
                        htmlString += "<tr style='height: 30px'>";
                        htmlString += "<td class='pull-left'><strong>" + data.labels[i] + "</strong></td>";
                        htmlString += "<td class='pull-right' >" + data.datasets[0].data[i] + "</td>";
                        htmlString += "</tr>";
                    }
                    htmlString += "</tbody>";
                    htmlString += "</table>";
                    htmlString += "</div>";
                    $('#content-total-product-scans').html(htmlString);

                    console.log("Success"); // display response from the PHP script, if any
                    console.log(data); // display response from the PHP script, if any



                },
                error: function (data) {
                    console.log(data);
                }
            });
        }




</script>

<?php JavaScript::end(); ?>

