<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Product Scan Cities');
?>

<div class="site-index" style="height: 180px">

    <div class="row">
        <div class="col-lg-12">
            <table style="width: 100%">
                <tr>
                    <td>
                        <h1 class="pull-left h7"><?= $this->title ?></h1>
                    </td>
                    <td>
                        <div class="pull-right">
                            <?= Html::button(Yii::t('app', 'View All'), ['class' => 'btn btn-oziris round', 'id' => 'btnViewAllCities']) ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <hr>
    </div>

    <div class="row">
        <div id="content-city" class="col-lg-12">

        </div>
    </div>


</div>
<?php echo $this->render('../../reports/modal/cities-modal'); ?>
<?php JavaScript::begin(); ?>
<script>
    $.ajax({

        url: '<?= Url::to(['api/reports/oziris-reports-api/get-cities-report-data']); ?>', // point to server-side PHP script
        dataType: 'json',  // what to expect back from the PHP script, if anything
        contentType: false,
        processData: false,
        type: 'post',
        success: function (php_script_response) {
            viewCitiesData(php_script_response);
            viewCitiesModalData(php_script_response);
        },
        error: function (php_script_response) {
            console.log('Error : ' + php_script_response);  // display response from the PHP script, if any
        }
    });


    function viewCitiesData(php_script_response){

        htmlString = '';

        if(php_script_response.length<=4){
            length = php_script_response.length;
        }else {
            length = 4
        }
        htmlString = '';
        htmlString += "<div class='row' style='font-weight: bolder'>";
        for (i = 0; i < length; i++) {
            htmlString += "<div class='col-lg-4'>" + php_script_response[i].city + "</div>";
            htmlString += "<div class='col-lg-4'>" + php_script_response[i].users + "</div>";
            htmlString += "<div class='col-lg-4'>" + Math.round((php_script_response[i].users/php_script_response[i].total_users)*100) + "% </div>";
        }
        htmlString += "</div>";
        $('#content-city').html(htmlString);

        console.log("Success"); // display response from the PHP script, if any
        console.log(php_script_response); // display response from the PHP script, if any
    }

    function viewCitiesModalData(php_script_response){
        htmlString = '';
        htmlString += "<div class='row' style='font-weight: bolder'>";
        for (i = 0; i < php_script_response.length; i++) {
            htmlString += "<div class='col-lg-4'>" + php_script_response[i].city + "</div>";
            htmlString += "<div class='col-lg-4'>" + php_script_response[i].users + "</div>";
            htmlString += "<div class='col-lg-4'>" + Math.round((php_script_response[i].users/php_script_response[i].total_users)*100) + "% </div>";
        }
        htmlString += "</div>";

        $('#content-cities-modal').html(htmlString);

        console.log("Success"); // display response from the PHP script, if any
        console.log(php_script_response); // display response from the PHP script, if any
    }

    $('#btnViewAllCities').on('click', function(ev) {
        $('#cities-modal').modal('show');
    });



</script>

<?php JavaScript::end(); ?>
