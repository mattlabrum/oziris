<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Product Scan Calender');
?>
<div class="site-index">

    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <table style="width: 100%">
                    <tr>
                        <td>
                            <h1 class="pull-left h7"><?= $this->title ?></h1>
                        </td>
                        <td>
                            <select id="duration" class="pull-right">
                                <option value="day">View : Daily</option>
                                <option value="month">View : Monthly</option>
                                <option value="year">View : Yearly</option>
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-12">
                <div>
                    <canvas style='max-height: 500px' id="scanned-product-by-duration"></canvas>
                </div>
            </div>
        </div>
    </div>


    <?php JavaScript::begin(); ?>
    <script>

        $('#duration').on('change', getScannedProductsByDuration);
        getScannedProductsByDuration();
        function getScannedProductsByDuration() {
            city = ($('#city').find(":selected").text() != 'City : All') ? $('#city').find(":selected").text() : '';
            country = ($('#country').find(":selected").text() != 'Country : All') ? $('#country').find(":selected").text() : '';
            age = ($('#age').find(":selected").text() != 'Age Range : All') ? $('#age').find(":selected").text() : '';
            gender = ($('#gender').find(":selected").text() != 'Gender : All') ? $('#gender').find(":selected").text() : '';
            date_range = $('#date_range').val();
            duration = $('#duration').find(":selected").val();

            date_from = date_range.substr(0, (date_range.indexOf(' ')));
            date_to = date_range.substr((date_range.indexOf(' ') + 3), date_range.length);

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/get-scanned-products-by-duration']); ?>', // point to server-side PHP script
                data: {city, country, age, gender, date_from, date_to, duration},
                type: 'post',
                success: function (data) {
                    if (window.myLine != null) {
                        window.myLine.destroy();
                    }

                    var ctx = document.getElementById("scanned-product-by-duration").getContext("2d");
                    window.myLine = new Chart(ctx).Line(data, {
                        responsive: true
                    });
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });


        }


    </script>
    <?php JavaScript::end(); ?>

</div>