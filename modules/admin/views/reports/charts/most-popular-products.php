<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Most Popular Product Scans');
?>
<div class="site-index ">

    <div class="row">
        <div class="col-lg-12">
            <table style="width: 100%">
                <tr>
                    <td>
                        <h1 class="pull-left h7"><?= $this->title ?></h1>
                    </td>
                    <td>
                        <select id="sort-most-popular" class="pull-right">
                            <option value="DESC">Sort : Most Popular</option>
                            <option value="ASC">Sort : Least Popular</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <hr>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div>
                <canvas style='max-height: 250px' id="most-popular-product"></canvas>
            </div>
        </div>
    </div>

</div>

<?php JavaScript::begin(); ?>
    <script>
        $('#sort-most-popular').on('change',getMostPopularScannedProducts);
        getMostPopularScannedProducts();
        function getMostPopularScannedProducts (){
            city = ($('#city').find(":selected").text() != 'City : All') ? $('#city').find(":selected").text() : '';
            country = ($('#country').find(":selected").text() != 'Country : All') ? $('#country').find(":selected").text() : '';
            age = ($('#age').find(":selected").text() != 'Age Range : All') ? $('#age').find(":selected").text() : '';
            gender = ($('#gender').find(":selected").text() != 'Gender : All') ? $('#gender').find(":selected").text() : '';
            date_range = $('#date_range').val();
            sort = $('#sort-most-popular').find(":selected").val();

            date_from = date_range.substr(0,(date_range.indexOf(' ')));
            date_to = date_range.substr((date_range.indexOf(' ')+3),date_range.length);

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/get-most-popular-scanned-products']); ?>', // point to server-side PHP script
                data: { city,country,age,gender,date_from,date_to,sort, limit :10 },
                type: 'post',
                success: function (data) {
                    if(window.myBar != null){
                        window.myBar.destroy();
                    }

                    var ctx = document.getElementById("most-popular-product").getContext("2d");
                    window.myBar = new Chart(ctx).Bar(data, options);
                    console.log(data);
                },
                error: function (data) {
                    console.log(data);
                }
            });


        }


    </script>
<?php JavaScript::end(); ?>