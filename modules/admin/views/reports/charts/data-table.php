<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Home');
?>
<div class="site-index ">

    <div class="row" style="padding-top: 15px; padding-bottom: 15px">

        <div class="col-lg-3">
            <label class="data-table-label">No. of Users</label><br>
            <label id="number_of_users" class="data-table-text"></label>
        </div>
        <div class="col-lg-2">
            <label class="data-table-label">No. of Products</label><br>
            <label id="number_of_products" class="data-table-text"></label>
        </div>
        <div class="col-lg-2">
            <label class="data-table-label-counter">No. Of Non-Traceable Scans</label><br>
            <label id="number_of_counterfeit_scans" class="data-table-text-counter"></label>
        </div>
        <div class="col-lg-2">
            <label class="data-table-label">No. of Scans</label><br>
            <label id="number_of_scans" class="data-table-text"></label>
        </div>
        <div class="col-lg-3">
            <label class="data-table-label">No. of Product Reviews and Ratings</label><br>
            <label id="number_of_rating_reviews" class="data-table-text"></label>
        </div>
    </div>

</div>


<?php JavaScript::begin(); ?>

    <script>
        populateDataTable();

        function populateDataTable() {

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/populate-data-table']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (data) {
                    console.log(data);
                    $('#number_of_users').text(data[0].total_users);
                    $('#number_of_products').text(data[0].total_products);
                    $('#number_of_counterfeit_scans').text(data[0].total_counterfeit_scans);
                    $('#number_of_scans').text(data[0].total_scans);
                    $('#number_of_rating_reviews').text(data[0].review_ratings);
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

    </script>
<?php JavaScript::end(); ?>