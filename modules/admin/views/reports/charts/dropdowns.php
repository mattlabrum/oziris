<?php
/* @var $this yii\web\View */
use app\widgets\JavaScript;
use dmstr\helpers\Html;
use kartik\daterange\DateRangePicker;
use kartik\select2\Select2;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Home');
?>
    <div class="site-index">

        <div id="city-div" class="col-lg-2 reports-dropdown-background">
            <?=
            Select2::widget([
                'id' => 'city',
                'class' => 'disabled',
                'name' => 'city',
                'theme' => Select2::THEME_CLASSIC,
                'options' => [
                    'placeholder' => Yii::t('app', 'City : All'),
                    'multiple' => false,
                ],
            ]);
            ?>

        </div>
        <div id="country-div" class="col-lg-2 reports-dropdown-background">
            <?=
            Select2::widget([
                'id' => 'country',
                'name' => 'country',
                'theme' => Select2::THEME_CLASSIC,
                'options' => [
                    'placeholder' => Yii::t('app', 'Country : All'),
                    'multiple' => false,
                ],
            ]);
            ?>

        </div>
        <div class="col-lg-3 reports-dropdown-background">
            <?php
            echo '<div class="input-group drp-container" style="width: 100%">';
            echo DateRangePicker::widget([
                'name' => 'date_range_1',
                'id' => 'date_range',
                'convertFormat' => true,
                'useWithAddon' => true,
                'initRangeExpr' => true,
                'presetDropdown' => true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-M-y',
                        'separator' => ' - ',
                    ],
                    'opens' => 'center'
                ]
            ]);
            echo '</div>';
            ?>

        </div>
        <div id="age-div" class="col-lg-2 reports-dropdown-background">
            <?=

            Select2::widget([
                'id' => 'age',
                'name' => 'age',
                'theme' => Select2::THEME_CLASSIC,
                'options' => [
                    'placeholder' => Yii::t('app', 'Age Range : All'),
                    'multiple' => false,
                ],
            ]);
            ?>
        </div>
        <div class="col-lg-2 reports-dropdown-background">
            <?=
            Select2::widget([
                'id' => 'gender',
                'name' => 'gender',
                'theme' => Select2::THEME_CLASSIC,
                'options' => [
                    'placeholder' => Yii::t('app', 'Gender : All'),
                    'multiple' => false,
                ],
            ]);
            ?>
        </div>

    <div class="col-lg-1 reports-dropdown-background">
        <button type="button"  id="btn-filter" style="width: 90%; height: 28px" class="btn-report-filter btn-oziris-normal">Filter</button>
    </div>

    </div>

<?php JavaScript::begin(); ?>

    <script>

        $('#city-div').addClass('disabled');
        $('#country-div').addClass('disabled');
        $('#age-div').addClass('disabled');
        $('#gender-div').addClass('disabled');
        $('#btn-filter').on('click',applyDataFilters);

        //populateCitiesDropDown();
        populateCountryDropDown();
        populateAgeDropDown();
        populateGenderDropDown();

        function populateAgeDropDown() {

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/populate-age-drop-down']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (data) {
                    console.log(data);
                    $('#age').find('option').remove().end();
                    $.each(data.results, function (i, item) {
                        $('#age').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });
                    $('#age-div').removeClass('disabled');
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

        function populateGenderDropDown() {

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/populate-gender-drop-down']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (data) {
                    console.log(data);
                    $('#gender').find('option').remove().end();
                    $.each(data.results, function (i, item) {
                        $('#gender').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });
                    $('#gender-div').removeClass('disabled');

                },
                error: function (data) {
                    console.log(data);
                }
            });
        }


        function populateCitiesDropDown() {

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/populate-city-drop-down']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (data) {
                    $('#city').find('option').remove().end();
                    $.each(data.results, function (i, item) {
                        $('#city').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });
                    $('#city-div').removeClass('disabled');
                }
            });
        }

        function populateCountryDropDown() {

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/populate-country-drop-down']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (data) {
                    $('#country').find('option').remove().end();
                    $.each(data.results, function (i, item) {
                        $('#country').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });
                    $('#country-div').removeClass('disabled');
                }
            });
        }

        function applyDataFilters() {

            city = $('#city').find(":selected").text();
            country = $('#country').find(":selected").text();
            age = $('#age').find(":selected").text();
            gender = $('#gender').find(":selected").text();
            date_range = $('#date_range').val();

            date_from = date_range.substr(0,(date_range.indexOf(' ')));
            date_to = date_range.substr((date_range.indexOf(' ')+3),date_range.length);

            console.log('city : '+city+' country : '+country+' age : '+age+' gender : '+gender+' date_from : '+date_from+' date_to : '+date_to);

            $.ajax({
                url: '<?= Url::to(['api/reports/oziris-reports-api/update-table-data-report']); ?>', // point to server-side PHP script
                data: { city,country,age,gender,date_from,date_to },
                type: 'post',
                success: function (data) {
                    console.log(data);
                    $('#number_of_users').text(data[3].users);
                    $('#number_of_counterfeit_scans').text(data[1].counterfeit_scans);
                    $('#number_of_scans').text(data[0].scanned_product);
                    $('#number_of_rating_reviews').text(data[2].review_ratings);

                    getMostPopularScannedProducts();
                    getTotalProductScans();
                },
                error: function (data) {
                    console.log(data);
                }
            });
        }

    </script>
<?php JavaScript::end(); ?>