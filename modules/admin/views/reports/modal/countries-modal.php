<div id="countries-modal" class="modal fade text-left" xmlns="http://www.w3.org/1999/html">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-globe"></span> View All User Countries</h4>
            </div>
            <div id="content-country-modal" class="modal-body text-center">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-oziris-normal round btn-xs" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
