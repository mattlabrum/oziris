<?php

/**
 * @var yii\web\View $this
 * @var app\models\form\NutritionForm $model
 */

use kartik\detail\DetailView;

$this->title = Yii::t('app', 'Nutrition') . ' ' . $model->nutrition->name;
?>


<div class="brand-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->nutrition]) ?>
    <hr class="hr">

    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-10">
                <?php
                echo DetailView::widget([
                    'model' => $model->nutrition,
                    'attributes' => [
                        [
                            'group' => true,
                            'label' => 'Nutrition Information',
                            'rowOptions' => ['class' => 'product-info-header']
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'format' => 'raw',
                                    'value' => '<kbd>' . $model->nutrition->id . '</kbd>',
                                    'labelColOptions' => ['style' => 'width:30%'],
                                    'valueColOptions' => ['style' => 'width:70%'],
                                    'displayOnly' => true,
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'format' => 'raw',
                                    'value' => $model->nutrition->name,
                                    'labelColOptions' => ['style' => 'width:30%'],
                                    'valueColOptions' => ['style' => 'width:70%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ]
                    ],
                    'mode' => 'view',
                    'bordered' => false,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'fadeDelay' => 200,
                    'class'=> 'text-left',
                ]);
                ?>
                <?= DetailView::widget([
                    'model' => $model->getNutritionTranslation(),
                    'attributes' => [
                        'name',
                    ],
                ]);
                ?>

            </div>


            <div class="col-lg-1"></div>

        </div>
    </div>
</div>
