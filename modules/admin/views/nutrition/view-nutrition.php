<?php

/**
 * @var yii\web\View $this
 * @var app\models\ProductNutrition[] $model
 * @var yii\bootstrap\ActiveForm $form
 */
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use Symfony\Component\Console\Input\Input;

?>

<div class="text-center" id="product-nutrition-div">
    <div class="row">
            <h1 class="pull-left">Nutrition Information</h1>
            <button id="saveNutritionTable" class="pull-right">Generate Nutrition Information</button>
    </div>
    <div id="nutrition-table" class="row">
        <table style="border:  1px solid #000000;border-spacing: 0px"  bgcolor="#FFFFFF" class="table">
            <thead>
            <tr style="1px solid #000000;">
                <th bgcolor="#808080" style="color: white" colspan="3">Nutritional Information</th>
            </tr>
            <tr style="border-top: 1px solid #000000;">
                <th bgcolor="#FFFFFF" colspan="3"><?= $serves_per_pack?> Serves per pack, Serving size <?= $serve_size?></th>
            </tr>

            <tr style="border-top: 1px solid #000000;!important;">
                <th style="border: 1px solid #000000;!important;">Average Quantity</th>
                <th style="border: 1px solid #000000;!important;" >per Serving </th>
                <th style="border: 1px solid #000000;!important;">per 100g (or 100 mL)</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $index => $nutrition) {
                ?>
                <tr style="border: 1px solid #000000;">
                    <td class="text-left" style="border-right: 1px solid #000000;" style="width: 40%"><label class="control-label"><?= $nutrition->nutrition->name ?></label></td>
                    <td style="width: 30% ; border-right: 1px solid #000000;"><label class="control-label"><?= $nutrition->avg_qty_per_serving ?></label></td>
                    <td style="width: 30% ; border-right: 1px solid #000000;"><label class="control-label"><?= $nutrition->avg_qty_per_100g?></label></td>
                </tr>
            <?php } ?>

            <?php
                $ingredientString = 'Ingredients : ';
                foreach($ingredients as $ingredient){
                    $ingredientString .= $ingredient->ingredient->name.', ';
                }

            ?>

            <tr style="1px solid #000000;">
                <td colspan="3"><p style="  font-size: 10px"><?= $nutrition_text ?> <br/><br/> <?= $ingredientString ?></p></td>
            </tr>
            </tbody>

        </table>
    </div>
</div>

