<?php

/**
 * @var yii\web\View $this
 * @var app\models\ProductNutrition[] $model
 * @var yii\bootstrap\ActiveForm $form
 */
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use Symfony\Component\Console\Input\Input;

?>

<div class="text-center" id="product-nutrition-div">

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
    <?= Html::hiddenInput('no_of_nutrition', count($model)); ?>
    <div class="row">
            <h1 class="pull-left">Nutrition Information</h1>
    </div>
    <div class="row">
        <table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
            <thead>
            <tr>
                <th colspan="3">Nutrition Information</th>
            </tr>

            <tr>
                <th></th>
                <th>Average Quantity </br> per Serving </th>
                <th>Average Quantity </br> per 100g (or 100 mL)</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $index => $nutrition) { ?>
                <tr>
                    <td class="text-left" style="width: 40%"><label class="control-label"><?= $nutrition->nutrition->name ?></label></td>
                    <?= Html::hiddenInput('nutrition_id-'.$index, $nutrition->nutrition_id); ?>
                    <td style="width: 30%"><?= Html::input('text','nutrition_per_serve-'.$index,$nutrition->avg_qty_per_serving); ?></td>
                    <td style="width: 30%"><?= Html::input('text','nutrition_per_100g-'.$index,$nutrition->avg_qty_per_100g); ?></td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>
</div>

