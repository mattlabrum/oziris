<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Nutrition $model
 */

$this->title = Yii::t('app', 'Create Nutrition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutritions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutrition-create">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]) ?>
    <?= $this->render('_form', ['model' => $model,]); ?>

</div>



