<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\NutritionForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Nutrition') . ' - ' . $model->getNutrition()->name;
?>
<div class="nutrition-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->getNutrition()]) ?>
    <?php echo $this->render('_form', ['model' => $model,]); ?>

</div>
