<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ShelfLifeUnit $model
 */

$this->title = Yii::t('app', 'Shelf Life Unit') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shelf Life Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shelf-life-unit-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\ShelfLifeUnit'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> ShelfLifeUnit',
                'content' => $this->blocks['app\models\ShelfLifeUnit'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
