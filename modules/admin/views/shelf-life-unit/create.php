<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ShelfLifeUnit $model
 */

$this->title = Yii::t('app', 'Create Shelf Life Unit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Shelf Life Units'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shelf-life-unit-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
