<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\ProductForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Product') . ' ' . $model->product->product_number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product->id, 'url' => ['view', 'id' => $model->product->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="product-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->product]); ?>
    <?php echo $this->render('_form', ['model' => $model,]); ?>

</div>
