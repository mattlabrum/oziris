<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Brand $model
 */

?>

<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a('<span class="fa fa-arrow-left"></span> ' . Yii::t('app', 'Back'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>

                        <?php if (Yii::$app->controller->action->id != 'view' && Yii::$app->controller->action->id != 'create' && Yii::$app->controller->action->id != 'duplicate' && Yii::$app->controller->action->id != 'resend') { ?>
                            <?= Html::a('<span class="fa fa-eye"></span> ' . Yii::t('app', 'View'), ['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal']) ?>
                        <?php } ?>
                        <?php if (Yii::$app->controller->action->id != 'update' && Yii::$app->controller->action->id != 'create' && Yii::$app->controller->action->id != 'duplicate'&& Yii::$app->controller->action->id != 'resend') { ?>
                            <?= Html::a('<span class="fa fa-pencil"></span> ' . Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal']) ?>
                        <?php } ?>
                            <?= Html::a('<span class="fa fa-eye"></span> ' . Yii::t('app', 'Update Beston'), ['update-bmp-product', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal']) ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div>
<div class="clearfix"></div>