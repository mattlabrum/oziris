<div id="bmp-update-modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" id="btnBulkUpdateCross">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-import"></span> Update All BMP Products</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div id="data-preview">
                        <div style="height: 300px ; overflow: scroll" id="display-response">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-oziris-normal hidden" id="btnBulkUpdateCancel">Cancel</button>
                <button type="button" id="btnUpdateBMPProducts" class="btn btn-oziris-save">Save changes</button>
            </div>
        </div>
    </div>
</div>

