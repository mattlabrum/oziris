<?php

use app\models\CollectionItem;
use app\models\ProductLocationAttribute;
use app\models\ProductPrice;
use app\models\ProductQuantity;
use app\models\ProductTranslation;
use app\widgets\JavaScript;
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use kartik\detail\DetailView;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\models\form\ProductForm $model
 */


$this->title = Yii::t('app', 'Product') . ' - ' . $model->product->product_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <?= $this->render('@app/modules/admin/views/product/_menu', ['model' => $model->product]); ?>
    <hr class="hr">
<br>

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="col-lg-2" style="padding-right: 25px">
                <div class="row">
                    <label class="control-label">Product Image</label>
                </div>
                <div class="row">
                    <img class="img-responsive img-thumbnail"
                         src="<?= yii\helpers\Url::to('@web/uploads/product') . '/' . $model->product->product_image ?>"
                         alt="<?= $model->product->product_name ?>">
                </div>
                <div class="row" style="padding-top: 40px">
                    <label class="control-label">Nutrition Image</label>
                </div>
                <div class="row">
                    <img class="img-responsive img-thumbnail"
                         src="<?= yii\helpers\Url::to('@web/uploads/product') . '/' . $model->product->nutrition_image ?>"
                         alt="Chania">
                </div>

                <div class="row" style="padding-top: 40px">
                    <label class="control-label">Product QR Code</label>
                </div>
                <div class="row">
                    <img class="img-responsive img-thumbnail"
                         src="<?= yii\helpers\Url::to('@web/upload/qrcode') . '/' . $model->product->getDefaultBatch()->qr_code_image ?>"
                         alt="Chania">
                </div>

                <?php
                if (isset($model->productTranslation->nutrition_image) && $model->productTranslation->nutrition_image != '') {
                    echo '<div class="row" style="padding-top: 20px">';
                    echo '<label class="control-label">Translated Nutrition Image</label>';
                    echo '</div>';
                    echo '<div class="row">';
                    echo "<img class='img-responsive img-thumbnail' src=" . yii\helpers\Url::to('@web/uploads/product') . '/' . $model->productTranslation->nutrition_image . ">";
                    echo '</div>';
                }
                ?>

            </div>
            <div class="col-lg-10" style="padding-left: 25px">

                <div class="row">
                    <?php
                    echo DetailView::widget([
                        'model' => $model->getProduct(),
                        'attributes' => [
                            [
                                'group' => true,
                                'label' => 'Product Information',
                                'rowOptions' => ['class' => 'product-info-header']
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'is_collection',
                                        'format' => 'raw',
                                        'value' => $model->product->is_collection ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                                        'widgetOptions' => [
                                            'pluginOptions' => [
                                                'onText' => 'Yes',
                                                'offText' => 'No',
                                            ]
                                        ],
                                        'labelColOptions' => ['style' => 'width:15.8%'],
                                    ],
                                    [
                                        'attribute' => 'is_beston_pure_product',
                                        'format' => 'raw',
                                        'value' => $model->product->is_beston_pure_product ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                                        'widgetOptions' => [
                                            'pluginOptions' => [
                                                'onText' => 'Yes',
                                                'offText' => 'No',
                                            ]
                                        ],
                                        'labelColOptions' => ['style' => 'width:15.8%'],
                                    ],
                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'id',
                                        'format' => 'raw',
                                        'value' => '<kbd>' . $model->product->id . '</kbd>',
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => true,
                                    ],
                                    [
                                        'attribute' => 'product_number',
                                        'format' => 'raw',
                                        'value' => $model->product->product_number,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'barcode',
                                        'format' => 'raw',
                                        'value' => $model->product->barcode,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],

                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'product_name',
                                        'format' => 'raw',
                                        'value' => $model->product->product_name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'product_group_id',
                                        'format' => 'raw',
                                        'value' => $model->product->productGroup->product_group_name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'product_type_id',
                                        'format' => 'raw',
                                        'value' => $model->product->productType->type_name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],

                            [
                                'attribute' => 'product_description',
                                'format' => 'raw',
                                'value' => '<span class="text-left"><em>' . $model->product->product_description . '</em></span>',
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'brand_id',
                                        'format' => 'raw',
                                        'value' => $model->product->brand->brand_name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'manufacturer_id',
                                        'format' => 'raw',
                                        'value' => $model->product->manufacturer->manufacturer_name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'service_type_id',
                                        'format' => 'raw',
                                        'value' => $model->product->serviceType->name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'unit_size',
                                        'format' => 'raw',
                                        'value' => $model->product->unit_size,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'unit_size_kg',
                                        'format' => 'raw',
                                        'value' => $model->product->unit_size_kg,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'unit_per_box',
                                        'format' => 'raw',
                                        'value' => $model->product->unit_per_box,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'shelf_life',
                                        'format' => 'raw',
                                        'value' => $model->product->shelf_life,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'shelf_life_unit',
                                        'format' => 'raw',
                                        'value' => $model->product->shelfLifeUnit->name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'product_nature_id',
                                        'format' => 'raw',
                                        'value' => $model->product->productNature->name,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'sample_availability',
                                        'format' => 'raw',
                                        'value' => $model->product->sample_availability,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'annual_supply',
                                        'format' => 'raw',
                                        'value' => $model->product->annual_supply,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'available_since',
                                        'format' => 'raw',
                                        'value' => $model->product->available_since,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'length',
                                        'format' => 'raw',
                                        'value' => $model->product->length,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'width',
                                        'format' => 'raw',
                                        'value' => $model->product->width,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'height',
                                        'format' => 'raw',
                                        'value' => $model->product->height,
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],
                            [
                                'label' => 'product_nutrition_tags',
                                'format' => 'raw',
                                'value' => '<span class="text-left"><em><b>' . $model->product->getProductNutritionTagsText() . '</b></em></span>',
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'minimum_order',
                                        'format' => 'raw',
                                        'value' => $model->product->minimum_order,
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'created_at',
                                        'format' => ['date', 'php:d/m/yy'],
                                        'value' => $model->product->created_at,
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'updated_at',
                                        'format' => ['date', 'php:d/m/yy'],
                                        'value' => $model->product->updated_at,
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'displayOnly' => false
                                    ],
                                ],
                            ],
                            [
                                'columns' => [
                                    [
                                        'attribute' => 'productQuantity',
                                        'format' => 'raw',
                                        'value' => $model->product->getProductQuantity(),
                                        'valueColOptions' => ['style' => 'width:10%'],
                                        'labelColOptions' => ['style' => 'width:15%'],
                                        'displayOnly' => false
                                    ],
                                    [
                                        'attribute' => 'is_top_seller',
                                        'format' => 'raw',
                                        'value' => $model->product->is_top_seller ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                                        'widgetOptions' => [
                                            'pluginOptions' => [
                                                'onText' => 'Yes',
                                                'offText' => 'No',
                                            ]
                                        ],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'labelColOptions' => ['style' => 'width:15%'],
                                    ],
                                    [
                                        'attribute' => 'is_featured',
                                        'format' => 'raw',
                                        'value' => $model->product->is_featured ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                                        'widgetOptions' => [
                                            'pluginOptions' => [
                                                'onText' => 'Yes',
                                                'offText' => 'No',
                                            ]
                                        ],
                                        'valueColOptions' => ['style' => 'width:20%'],
                                        'labelColOptions' => ['style' => 'width:15%'],
                                    ],
                                ],
                            ]
                        ],
                        'mode' => 'view',
                        'bordered' => false,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hover' => true,
                        'hAlign' => 'left',
                        'vAlign' => 'middle',
                        'fadeDelay' => 200,
                        'class' => 'text-left',
                    ]);
                    ?>
                </div>
                <!--Body content-->
                <div class="row">
                    <?php
                    $collections = CollectionItem::findAll(['collection_id' => $model->product->id]);
                    $attributes = [];

                    $row = [
                        'group' => true,
                        'label' => 'Colelction Items ',
                        'rowOptions' => ['class' => 'product-info-header']
                    ];
                    array_push($attributes, $row);

                    foreach ($collections as $collection) {
                        $column = [
                            [
                                'attribute' => 'product_name',
                                'format' => 'raw',
                                'value' => $collection->product->product_name,
                                'labelColOptions' => ['style' => 'width:12%'],
                                'valueColOptions' => ['style' => 'width:18%'],
                                'displayOnly' => false
                            ],
                            [
                                'attribute' => 'product_name',
                                'label' => 'Manufacturer Name',
                                'format' => 'raw',
                                'value' => $collection->product->manufacturer->manufacturer_name,
                                'labelColOptions' => ['style' => 'width:15%'],
                                'valueColOptions' => ['style' => 'width:18%'],
                                'displayOnly' => false
                            ],
                            [
                                'attribute' => 'product_name',
                                'label' => 'Brand Name',
                                'format' => 'raw',
                                'value' => $collection->product->brand->brand_name,
                                'labelColOptions' => ['style' => 'width:12%'],
                                'valueColOptions' => ['style' => 'width:18%'],
                                'displayOnly' => false
                            ],
                            [
                                'attribute' => 'product_name',
                                'label' => 'Qty',
                                'format' => 'raw',
                                'value' => $collection->quantity,
                                'labelColOptions' => ['style' => 'width:5%'],
                                'valueColOptions' => ['style' => 'width:5%'],
                                'displayOnly' => false
                            ]
                        ];
                        array_push($attributes, ['columns' => $column]);
                    }

                    if (count($collections) > 0) {
                        echo DetailView::widget([
                            'model' => $model->getProduct(),
                            'attributes' => $attributes,
                            'mode' => 'view',
                            'bordered' => true,
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'hAlign' => 'left',
                            'vAlign' => 'middle',
                            'fadeDelay' => 200,
                            'container' => ['id' => 'collections'],
                        ]);

                    }
                    ?>

                </div>
                <!--Body content-->
                <div class="row">
                    <?php
                    $translations = ProductTranslation::findAll(['product_id' => $model->product->id]);
                    $attributes = [];

                    foreach ($translations as $translation) {

                        $row = [
                            'group' => true,
                            'label' => 'Product Translation ' . $translation->language->name,
                            'rowOptions' => ['class' => 'product-info-header']
                        ];
                        array_push($attributes, $row);


                        $column = [
                            'attribute' => 'product_name',
                            'format' => 'raw',
                            'value' => $translation->product_name,
                            'labelColOptions' => ['style' => 'width:20%'],
                            'valueColOptions' => ['style' => 'width:80%'],
                            'displayOnly' => false
                        ];
                        array_push($attributes, $column);
                        $column = [
                            'attribute' => 'product_description',
                            'format' => 'raw',
                            'value' => $translation->product_description,
                            'labelColOptions' => ['style' => 'width:15%'],
                            'valueColOptions' => ['style' => 'width:20%'],
                            'displayOnly' => false
                        ];
                        array_push($attributes, $column);
                        $column = [
                            'attribute' => 'nutrition_text',
                            'format' => 'raw',
                            'value' => $translation->nutrition_text,
                            'labelColOptions' => ['style' => 'width:15%'],
                            'valueColOptions' => ['style' => 'width:20%'],
                            'displayOnly' => false
                        ];
                        array_push($attributes, $column);
                    }

                    if (count($translations) > 0) {
                        echo DetailView::widget([
                            'model' => $model->getProductTranslation(),
                            'attributes' => $attributes,
                            'mode' => 'view',
                            'bordered' => true,
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'hAlign' => 'left',
                            'vAlign' => 'middle',
                            'fadeDelay' => 200,
                            'container' => ['id' => 'translations'],
                        ]);

                    }
                    ?>

                </div>
                <!-- End !-->

                <div class="row">
                    <?php
                    $prices = ProductLocationAttribute::findAll(['product_id' => $model->product->id]);
                    $attributes = [];

                    $row = [
                        'group' => true,
                        'label' => 'Prices',
                        'rowOptions' => ['class' => 'product-info-header']
                    ];
                    array_push($attributes, $row);


                    foreach ($prices as $price) {
                        $column = [
                            'attribute' => 'language_id',
                            'format' => 'raw',
                            'label' => 'Region',
                            'value' => $price->language_id,
                            'labelColOptions' => ['style' => 'width:20%'],
                            'valueColOptions' => ['style' => 'width:80%'],
                            'displayOnly' => false
                        ];
                        array_push($attributes, $column);
                        $column = [
                            'attribute' => 'price',
                            'format' => 'raw',
                            'value' => $price->price,
                            'labelColOptions' => ['style' => 'width:15%'],
                            'valueColOptions' => ['style' => 'width:20%'],
                            'displayOnly' => false
                        ];
                        array_push($attributes, $column);
                        $column = [
                            'attribute' => 'ecommerce_enabled',
                            'format' => 'raw',
                            'value' => $price->ecommerce_enabled ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                            'widgetOptions' => [
                                'pluginOptions' => [
                                    'onText' => 'Yes',
                                    'offText' => 'No',
                                ]
                            ],
                            'labelColOptions' => ['style' => 'width:15.8%'],
                        ];
                        array_push($attributes, $column);
                    }

                    if (count($prices) > 0) {
                        echo DetailView::widget([
                            'model' => $model->getProductLocationAttributes(),
                            'attributes' => $attributes,
                            'mode' => 'view',
                            'bordered' => true,
                            'striped' => true,
                            'condensed' => true,
                            'responsive' => true,
                            'hAlign' => 'left',
                            'vAlign' => 'middle',
                            'fadeDelay' => 200,
                            'container' => ['id' => 'quantities'],
                        ]);

                    }
                    ?>

                </div>
                <div class="row">
                    <div class="col-lg-1" >

                    </div>
                    <div class="col-lg-3" >
                        <?php
                        if (count($model->getProduct()->productNutrition) > 0) {
                            echo $this->render('../nutrition/view-nutrition', ['model' => $model->getProduct()->productNutrition, 'serves_per_pack' => $model->getProduct()->serves_per_pack , 'serve_size' => $model->getProduct()->serve_size, 'nutrition_text' => $model->getProduct()->nutrition_text, 'ingredients' => $model->getProduct()->productIngredients]);
                        }
                        ?>
                    </div>
                    <div class="col-lg-1" >

                    </div>
                    <div class="col-lg-6" style="padding-left: 25px">
                        <?php
                        if (count($model->getProduct()->productIngredients) > 0) {
                            echo $this->render('../ingredient/view-ingredients', ['model' => $model->getProduct()->productIngredients]);
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>

<?php

echo $this->render('../audit-trail/AuditGrid', [
    'params' => [
        'search_string' => $_GET,
        'type' => 'app\models\Product',
        'query' => $model->product->getAuditTrails(),
        'filter' => [
            'is_collection', 'product_number', 'barcode', 'product_group_id', 'product_name',
            'product_description', 'product_type_id', 'brand_id', 'manufacturer_id',
            'service_type_id', 'unit_size', 'unit_size_kg', 'unit_per_box', 'shelf_life',
            'shelf_life_unit', 'product_nature_id', 'sample_availability', 'annual_supply',
            'minimum_order', 'available_since', 'product_image', 'nutrition_image', 'nutrition_text',
            'price', 'quantity','beston_quantity','ecommerce_enabled','currency','language_id','taxable',
            'sale_price','reorder_threshold','ecommerce_enabled','currency', 'percentage', 'ingredient_id', 'code'
            , 'nutrition_id', 'avg_qty_per_serving', 'avg_qty_per_100g'
        ]
    ],
//
    // which columns to show
    'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
    // set to false to hide filter
])



?>

<?php JavaScript::begin(); ?>
    <script>



        $('#saveNutritionTable').on('click', function (event) {
            console.log('working ... ');
            html2canvas([document.getElementById('nutrition-table')], {
                onrendered: function(canvas) {
                    var data = canvas.toDataURL('image/png');
                    save_img(data);
                }
            });

        });




        function save_img(data){
            $.ajax({
                url: '<?= Url::to(['upload-nutrition-image']); ?>', // point to server-side PHP script
                type: 'post',
                data: {img: data, product_id: <?= $model->getProduct()->id?>},
                success: function (data) {
                    alert("Nutrition Image generated successfully");
                    return data;
                }
            });

        }


    </script>
<?php JavaScript::end(); ?>