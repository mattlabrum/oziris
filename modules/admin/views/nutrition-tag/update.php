<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\NutritionTagForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Nutrition Tag') . ' ' . $model->getNutritionTag()->name;
?>
<div class="nutrition-tag-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->getNutritionTag()]) ?>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
