<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\NutritionTag $model
 */

$this->title = Yii::t('app', 'Create Nutrition Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutrition Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutrition-tag-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
