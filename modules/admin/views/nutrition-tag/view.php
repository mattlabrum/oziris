<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\NutritionTagForm $model
 */

$this->title = Yii::t('app', 'Nutrition Tag') . ' ' . $model->getNutritionTag()->name;
?>

<div class="brand-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->getNutritionTag()]) ?>
    <hr class="hr">

    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-10">
                <?= DetailView::widget([
                    'model' => $model->getNutritionTag(),
                    'attributes' => [
                        'id',
                        'name',
                    ],
                ]); ?>
                <?= DetailView::widget([
                    'model' => $model->getNutritionTagTranslation(),
                    'attributes' => [
                        'name',
                    ],
                ]);
                ?>

            </div>


            <div class="col-lg-1"></div>

        </div>
    </div>
</div>

