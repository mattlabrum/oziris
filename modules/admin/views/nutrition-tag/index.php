<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;
use yii\grid\GridView;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\NutritionTagsSearch $searchModel
 */

$this->title = Yii::t('app', 'Nutrition Tags');
?>

<div class="nutrition-tag-index">
    <div class="clearfix">
        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">

                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="text-center">
        <?= \kartik\grid\GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'responsive' => true,
            'hover'=>true,
            'export' => false,
            'columns' => [

                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'id',
                    'value' => function ($model) {
                        return Html::a($model->id, ['nutrition-tag/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'name',
                    'label' => 'Tag',
                    'value' => function ($model) {
                        return Html::a($model->name, ['nutrition-tag/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>
</div>