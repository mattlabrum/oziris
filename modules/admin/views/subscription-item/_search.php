<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/eeda5c365686c9888dbc13dbc58f89a1
 *
 * @package default
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 *
 * @var yii\web\View $this
 * @var app\models\search\SubscriptionItemsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="subscription-item-search">

    <?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

    		<?php echo $form->field($model, 'id') ?>

		<?php echo $form->field($model, 'subscription_id') ?>

		<?php echo $form->field($model, 'subscription_item_id') ?>

		<?php echo $form->field($model, 'quantity') ?>

		<?php echo $form->field($model, 'price') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('cruds', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('cruds', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
