<?php
/**
 * /vagrant/digitalnoir/oziris-backend/src/../runtime/giiant/d4b4964a63cc95065fa0ae19074007ee
 *
 * @package default
 */
use kartik\detail\DetailView;


/**
 *
 * @var yii\web\View $this
 * @var app\models\Coo $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('models', 'Country Of Origin');
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Coos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'View');
?>
<div class="coo-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]) ?>

    <hr class="hr">
    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-10">
                <?php
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'group' => true,
                            'label' => 'Country of Origin Information',
                            'rowOptions' => ['class' => 'product-info-header']
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'format' => 'raw',
                                    'value' => '<kbd>' . $model->id . '</kbd>',
                                    'labelColOptions' => ['style' => 'width:50%'],
                                    'valueColOptions' => ['style' => 'width:50%'],
                                    'displayOnly' => true,
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'coo_latitude',
                                    'format' => 'raw',
                                    'value' => $model->coo_latitude,
                                    'labelColOptions' => ['style' => 'width:50%'],
                                    'valueColOptions' => ['style' => 'width:50%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'coo_longitude',
                                    'format' => 'raw',
                                    'value' => $model->coo_longitude,
                                    'labelColOptions' => ['style' => 'width:50%'],
                                    'valueColOptions' => ['style' => 'width:50%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ]                        ,
                        [
                            'columns' => [
                                [
                                    'attribute' => 'city',
                                    'format' => 'raw',
                                    'value' => $model->city,
                                    'labelColOptions' => ['style' => 'width:50%'],
                                    'valueColOptions' => ['style' => 'width:50%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'country',
                                    'format' => 'raw',
                                    'value' => $model->country,
                                    'labelColOptions' => ['style' => 'width:50%'],
                                    'valueColOptions' => ['style' => 'width:50%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],

                    ],
                    'mode' => 'view',
                    'bordered' => false,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'fadeDelay' => 200,
                    'class'=> 'text-left',
                ]);
                ?>
            </div>
            <div class="col-lg-1"></div>

        </div>
    </div>
    <hr/>
</div>
