<?php
/**
 * /vagrant/digitalnoir/oziris-backend/src/../runtime/giiant/fcd70a9bfdf8de75128d795dfc948a74
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var app\models\Coo $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'COO') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Coo'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit');
?>
<div class="supplier-create">
    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]) ?>
    <?= $this->render('_form', ['model' =>$model,]); ?>
</div>