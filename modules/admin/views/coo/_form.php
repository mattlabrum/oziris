<?php

use cornernote\returnurl\ReturnUrl;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
 *
 * @var yii\web\View $this
 * @var app\models\Coo $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="coo-form">



    <?php $form = ActiveForm::begin([
        'id' => 'Supplier',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-4',
                'wrapper' => 'col-lg-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'enableClientValidation' => false,
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>


    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
            <h1>Country of Origin</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row" style="padding-top: 20px">
                <div class="col-lg-5">

                    <?php echo $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                    <?php echo $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
                    <?php echo $form->field($model, 'flag')->fileInput() ?>

                </div>
                <div class="col-lg-7">
                    <?php echo $form->field($model, 'coo_latitude')->textInput() ?>
                    <?php echo $form->field($model, 'coo_longitude')->textInput() ?>

                </div>

            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>


    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-check"></span> ' . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if ($model->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <?php ActiveForm::end(); ?>





</div>
