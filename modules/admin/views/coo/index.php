<?php
/**
 * /vagrant/digitalnoir/oziris-backend/src/../runtime/giiant/a0a12d1bd32eaeeb8b2cff56d511aa22
 *
 * @package default
 */


use cornernote\returnurl\ReturnUrl;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CooSearch $searchModel
 */
$this->title = Yii::t('models', 'Coos');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="coo-index">
    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a('<span class="fa fa-plus"></span> ' . Yii::t('app', 'Create') . ' ' . Yii::t('app', 'CoO'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-oziris']) ?>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="text-center">
        <?= \kartik\grid\GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'responsive' => true,
            'hover'=>true,
            'export' => false,
            'columns' => [

                [
                    'class' => DataColumn::className(),
                    'attribute' => 'id',
                    'value' => function ($model) {
                        return Html::a($model->id, ['supplier/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'coo_latitude',
                    'value' => function ($model) {
                        return Html::a($model->coo_latitude, ['supplier/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'coo_longitude',
                    'value' => function ($model) {
                        return Html::a($model->coo_longitude, ['supplier/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'city',
                    'value' => function ($model) {
                        return Html::a($model->city, ['supplier/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'country',
                    'value' => function ($model) {
                        return Html::a($model->country, ['supplier/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                ['class' => ActionColumn::className(),
                    'template' => '{update} &nbsp;&nbsp; {duplicate}',
                    'buttons' => [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-duplicate"></span> ', ['duplicate', 'id' => $model->id], ['title' => Yii::t('app', 'Duplicate'),]);
                        }
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>

</div>
