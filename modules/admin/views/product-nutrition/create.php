<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductNutrition $model
 */

$this->title = Yii::t('app', 'Create Product Nutrition');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Nutritions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-nutrition-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
