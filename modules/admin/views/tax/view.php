<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Tax $model
 */

$this->title = Yii::t('app', 'Tax') . ' ' . $model->title;
$this->params['heading'] = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Taxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="tax-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\Tax'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'code',
            'title',
            'percentage',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> Tax',
                'content' => $this->blocks['app\models\Tax'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
