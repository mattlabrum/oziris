    <?php
    use app\widgets\JavaScript;
    use dmstr\helpers\Html;
    use kartik\select2\Select2;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Brand $model
 * @var app\models\search\Product $searchModel
 */
$this->title = Yii::t('app', 'Stock Transfer');
?>
<div class="brand-index">
    <div class="clearfix">
        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a( Yii::t('app', 'Stock Analysis'), ['stock-analysis'], ['class' => 'btn btn-oziris']) ?>
                        <?= Html::a( Yii::t('app', 'Stock Transfer'), ['stock-transfer'], ['class' => 'btn btn-oziris']) ?>
                        <?= Html::a( Yii::t('app', 'B2B Sale'), ['b2b-transfer'], ['class' => 'btn btn-oziris']) ?>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>
    <div id="stock-data-grid">

        <div class="row">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="row text-center">
                    <div class="col-lg-6">
                        <label class="control-label pull-right" style="padding-top: 5px">Please select the type of internal order : </label>
                    </div>
                    <div class="col-lg-6">
                        <select id="orderTypeDropDown" class="pull-left width-50">
                            <option value="transfer">Stock Transfer</option>
                            <option value="b2b">B2B Sale Order</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-lg-1"></div>
        </div>

<hr class="sidebar-hr">

        <?= $this->render('b2b-transfer'); ?>

        <hr class="sidebar-hr">

        <div class="row">


            <?= $this->render('stock-transfer-address'); ?>
        </div>

        <hr class="sidebar-hr">

        <div class="row">
            <?= $this->render('stock-transfer-items'); ?>
        </div>

        <hr class="sidebar-hr">

            <div class="row hidden" id="btnSave" style="padding-top: 15px">
                <div class="col-lg-1"></div>
                <div class="col-lg-10 align-right">
                    <?= Html::button(Yii::t('app', 'Save Order'), [
                        'id' => 'btnSaveOrderItem',
                        'class' => 'btn btn-oziris-save btn-xs',
                        'style' => ['width' => '200px']
                    ]); ?>
                </div>
                <div class="col-lg-1"></div>
            </div>

    </div>
    <div class="clearfix">
        <hr class="hr">
    </div>
</div>

    <?php JavaScript::begin(); ?>
    <script>

        $("#orderTypeDropDown").change(function () {

            $("#btnSave").removeClass('hidden');
            $("#orderItemsDiv").removeClass('hidden');

            if($('#orderTypeDropDown').val() == 'b2b'){
                $("#b2b-transfer").removeClass('hidden');
                $("#stock-transfer").addClass('hidden');

            }else{
                $("#stock-transfer").removeClass('hidden');
                $("#b2b-transfer").addClass('hidden');
            }

        });


    </script>
    <?php JavaScript::end(); ?>