<?php

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Address $to_address
 * @var app\models\Address $from_address
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div id="stock-transfer" class="hidden">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="row text-center">
                <div class="col-lg-6 pull-right">
                    <div>
                        <label for="from" class="control-label">From : </label>
                    </div>
                    <div>
                        <?=
                        Select2::widget([
                            'id' => "from",
                            'name' => "from",
                            'data' => \yii\helpers\ArrayHelper::map(app\models\Language::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Type to autocomplete'),
                                'multiple' => false,
                            ],
                        ]);
                        ?>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div>
                        <label for="to" class="control-label">To : </label>
                    </div>
                    <div>
                        <?=
                        Select2::widget([
                            'id' => "to",
                            'name' => "to",
                            'data' => \yii\helpers\ArrayHelper::map(app\models\Language::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Type to autocomplete'),
                                'multiple' => false,
                            ],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
</div>

