<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Order $model
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div class="order-form">

    <?php $form = ActiveForm::begin([
        'id' => 'Order',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-2',
                'wrapper' => 'col-lg-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
    <?= $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10  oziris-block">

            <div class="row">

                <div class="col-md-12">
                    <h1>Contact Name</h1>

                    <div class="col-md-6" style="padding-top: 20px">
                        <?= $form->field($model->billingAddress, 'first_name')->textInput(['maxlength' => true, 'name' => 'first_name']) ?>
                    </div>
                    <div class="col-md-6" style="padding-top: 20px">
                        <?= $form->field($model->billingAddress, 'last_name')->textInput(['maxlength' => true, 'name' => 'last_name']) ?>
                    </div>
                </div>

                <div class="col-md-6">
                    <h1>Billing Address</h1>
                    <?= $form->field($model->billingAddress, 'address_1')->textInput(['maxlength' => true, 'name' => 'address_1']) ?>
                    <?= $form->field($model->billingAddress, 'suburb')->textInput(['maxlength' => true, 'name' => 'suburb']) ?>
                    <?= $form->field($model->billingAddress, 'state')->textInput(['maxlength' => true, 'name' => 'state']) ?>
                    <?= $form->field($model->billingAddress, 'postcode')->textInput(['maxlength' => true, 'name' => 'postcode']) ?>
                    <?= $form->field($model->billingAddress, 'country')->textInput(['maxlength' => true, 'name' => 'country']) ?>
                    <?= $form->field($model->billingAddress, 'email')->textInput(['maxlength' => true, 'name' => 'email']) ?>
                    <?= $form->field($model->billingAddress, 'contact_no')->textInput(['maxlength' => true, 'name' => 'contact_no']) ?>
                </div>
                <div class="col-md-6">
                    <h1>Shipping Address</h1>
                    <?= $form->field($model->shippingAddress, 'address_1')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model->shippingAddress, 'suburb')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model->shippingAddress, 'state')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model->shippingAddress, 'postcode')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model->shippingAddress, 'country')->textInput(['maxlength' => true]) ?>
                </div>
            </div>


            <div class="row" style="padding-top: 20px">
                <div class="col-lg-12">
                    <?= $form->field($model, 'order_note')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <?php if (!$model->isNewRecord){
            echo $this->render('../collection-item/order-item',['model' => $model]);
        }
     ?>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-arrow-right"></span> ' . ($model->isNewRecord ? Yii::t('app', 'Next') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if ($model->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
