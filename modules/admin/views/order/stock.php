<?php
use app\models\Language;
use app\widgets\JavaScript;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Brand $model
 * @var app\models\search\Product $searchModel
 */
$this->title = Yii::t('app', 'Stock');
?>
<div class="brand-index">
    <div class="clearfix">
        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?php /*Html::a(Yii::t('app', 'Stock Transfer'), ['stock-transfer', 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal'])*/ ?>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>
    <div id="stock-data-grid">

        <div class="text-center">
            <?= \kartik\grid\GridView::widget([
                'layout' => '{summary}{pager}{items}{pager}',
                'dataProvider' => $dataProvider,
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'firstPageLabel' => Yii::t('app', 'First'),
                    'lastPageLabel' => Yii::t('app', 'Last'),
                ],
                'filterModel' => $searchModel,
                'responsive' => true,
                'hover' => true,
                'export' => false,
                'columns' => [
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'id',
                        'contentOptions' => ['style' => 'width:80px;'],
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'product_number',
                        'contentOptions' => ['style' => 'width:150px;'],
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'product_name',
                        'contentOptions' => ['style' => 'width:250px;'],
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'location_id',
                        'filter'=>ArrayHelper::map(Language::find()->asArray()->all(), 'id', 'name'),
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($model) {

                            $location_attributes = $model->getproductLocationAttributes()->where( ['language_id' =>((isset($_REQUEST['ProductSearch']) ? $_REQUEST['ProductSearch']['location_id'] : 2)), 'product_id' =>$model->id ])->one() ;
                            if (isset($location_attributes)) {
                                return $location_attributes->language->name;
                            }

                        },
                        'format' => 'raw',
                    ],
//                    [
//                        'class' => yii\grid\DataColumn::className(),
//                        'label' => 'Prices',
//                        'contentOptions' => ['style' => 'width:120px;'],
//                        'value' => function ($model) {
//                            $allPrices = "";
//                            foreach ($model->getProductPrices()->all() as $price) {
//                                if ($price->language_id != "") {
//                                    $allPrices = $allPrices . "<b>" . $price->language_id . "</b> : " . $price->price . "<br>";
//                                }
//                            }
//                            return $allPrices;
//                        },
//                        'format' => 'raw',
//                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'No of Batches',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return count($model->getBatches()->all()) - 1;
                        },
                        'format' => 'raw',
                    ],
//                    [
//                        'class' => yii\grid\DataColumn::className(),
//                        'label' => 'E-commerce Quantity',
//                        'contentOptions' => ['style' => 'width:60px;'],
//                        'value' => function ($model) {
//                            return $model->beston_quantity;
//                        },
//                        'format' => 'raw',
//                    ],
//                    [
//                        'class' => yii\grid\DataColumn::className(),
//                        'label' => 'Re Order Threshold',
//                        'contentOptions' => ['style' => 'width:60px;'],
//                        'value' => function ($model) {
//                            return $model->reorder_threshold;
//                        },
//                        'format' => 'raw',
//                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Batch Level Quantity',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return $model->getProductBatchLevelQuantity();
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Actual Quantity',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return $model->getProductQuantity();
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Theo Quantity',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return $model->getTheoraticalQuantity();
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Sold Quantity',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return $model->getProductSoldQuantity();
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Discarded Quantity',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return ($model->getProductDiscardQuantity() < 5) ? '<span class="label label-success">' . $model->getProductDiscardQuantity() . '</span>' : '<span class="label label-danger">' . $model->getProductDiscardQuantity() . '</span>';

                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Variance',
                        'contentOptions' => ['style' => 'width:60px;'],
                        'value' => function ($model) {
                            return ($model->getProductVariance() < 5 && $model->getProductVariance() > -5) ? '<span class="label label-success">' . $model->getProductVariance() . '</span>' : '<span class="label label-danger">' . $model->getProductVariance() . '</span>';

                        },
                        'format' => 'raw',
                    ],

                ],
            ]); ?>
        </div>

    </div>
    <div class="clearfix">
        <hr class="hr">
        <table width="100%">
            <tr>
                <td>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Export') . ' ' . Yii::t('app', 'To CSV'), ['class' => 'btn btn-oziris', 'id' => 'btnExport']) ?>
                        <a class='btn btn-oziris hidden ' id="btnDownload">Download CSV</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>

<?php JavaScript::begin(); ?>
<script>

    $('#btnDownload').on('click', function (event) {
        $('#btnDownload').attr({target: '_blank', href: '<?= Url::to('@web/csv/stock.csv'); ?>'});
    });


    $('#btnExport').on('click', function (event) {
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if ($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined) {
                console.log($('thead tr td:nth-child(' + i + ') input').attr('name'));
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
//                console.log(element);

                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }

        searchField['location_id'] = $('select[name="ProductSearch[location_id]"]').val();
        console.log(searchField);
        $.ajax({
            url: '<?= Url::to(['order/generate-stock-csv']); ?>', // point to server-side PHP script
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                console.log(php_script_response); // display response from the PHP script, if any
                if (php_script_response) {
                    $('#btnExport').addClass('hidden');
                    $('#btnDownload').removeClass('hidden');
                }

            },
            error: function (php_script_response) {
                console.log('error'); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            }
        });
    });
</script>
<?php JavaScript::end(); ?>
