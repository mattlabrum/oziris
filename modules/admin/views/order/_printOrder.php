<?php
function calculateItemQuantity($items){
    $quantity = 0;

    foreach($items as $item){
        $quantity += $item['Quantity'];
    }
    return $quantity;
}

function prepareCollectionPrintItem($item,$collection_quantity){
    $itm = [
        'OzirisID' => $item->product->id,
        'ProductName' => $item->product->product_name,
        'Brand' => $item->product->brand->brand_name,
        'Manufacturer' => $item->product->manufacturer->manufacturer_name,
        'Collection' => (isset($item->collection) ? $item->collection->product_name : ''),
        'Quantity' => $item->quantity*$collection_quantity,
    ];
    return $itm;
}

function preparePrintItem($item){
    $itm = [
        'OzirisID' => $item->product->id,
        'ProductName' => $item->product->product_name,
        'Brand' => $item->product->brand->brand_name,
        'Manufacturer' => $item->product->manufacturer->manufacturer_name,
        'Collection' => (isset($item->collection) ? $item->collection->product_name : ''),
        'Quantity' => $item->quantity,
    ];
    return $itm;
}
?>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body lang="zh-CN">
<?php foreach( $orders as $key => $order ) {?>

<table width="100%">
    <tr>
        <td style="vertical-align: top" width="33.333%">
            <img style=" padding-top: 5px" src="images/beston-logo.png" alt="Beston Market Place">
        </td>
        <td style=" padding-left: 1px;">
            <?php $imgsrc = 'order/qrcode/'.$order->qr_code?>
            <img src="<?= $imgsrc?>"  alt='Order QR Code' width='25%' height='25%'>
        </td>
        <td width="33.333%" style="text-align: right; vertical-align: top">
            <table>
                <tr>
                    <th style="text-align: right; padding: 5px" width="140px">
                        Order Date
                    </th>
                    <td style="border: solid 1px #808080; text-align: left; padding: 5px" width="140px">
                        <?php
                        echo date('d-M-y h:i a', $order->created_at); ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right; padding: 5px" width="140px">
                        Order Number
                    </th>
                    <td style="border: solid 1px #808080; text-align: left; padding: 5px" width="140px">
                        <?= $order->woo_order_id ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right; padding: 5px" width="140px">
                        Customer
                    </th>
                    <td style="border: solid 1px #808080; text-align: left; padding: 5px" width="140px">

<!--                        (isset() : $order->getShippingAddress()->one()->first_name ? '')-->

                        <?php
                        $shipping = $order->getShippingAddress()->one();
                        echo (isset( $shipping ) ? $shipping->first_name : '' )   . ' ' . (isset( $shipping ) ? $shipping->last_name : '' );
                        ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right; padding: 5px" width="140px">
                        Customer Contact
                    </th>
                    <td style="border: solid 1px #808080; text-align: left; padding: 5px" width="140px">


                        <?php
                        $billing = $order->getBillingAddress()->one();
                        echo (isset( $billing ) ? $billing->contact_no : '' ) ;
                         ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right; padding: 5px" width="140px">
                        Payment Status
                    </th>
                    <td style="border: solid 1px #808080; text-align: left; padding: 5px" width="140px">
                        <?= (isset($order->payment) ? $order->payment->name : '' ) ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align: right; padding: 5px" width="140px">
                        Leave At Door
                    </th>
                    <td style="border: solid 1px #808080; text-align: left; padding: 5px" width="140px">
                        <?= (($order->leave_at_door == '1') ? 'Yes' : 'No'); $order->leave_at_door ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table width="100%">
    <tr>
        <td width="33.3333%">
            <table width="100%">
                <tr>
                    <th style="background-color: #000000; color: #ffffff">
                        Shipping Address
                    </th>
                </tr>
                <tr>
                    <td>
                        <b><?=  (isset( $shipping ) ? $shipping->first_name : '' )   . ' ' . (isset( $shipping ) ? $shipping->last_name : '' ) ?></b><br>
                        <?= (isset( $shipping ) ? $shipping->address_1 : '' ) ?><br>
                        <?= (isset( $shipping ) ? $shipping->address_2 : '' );

                        ?>
                        <?= (isset( $shipping ) ? $shipping->suburb : '' ) . ' ' . (isset( $shipping ) ? $shipping->state : '' ) . ' ' . (isset( $shipping ) ? $shipping->postcode : '' ) ?>
                        <br>
                        <?= (isset( $shipping ) ? $shipping->country : '' ) ?><br>

                    </td>
                </tr>
            </table>
        </td>
        <td width="33.3333%" style="vertical-align: top">
            <table width="100%">
                <tr>
                    <th style="background-color: #000000; color: #ffffff">
                        Order Note
                    </th>
                </tr>
                <tr>
                    <td>
                        <?= $order->order_note ?><br>
                    </td>
                </tr>
            </table>
        </td>
        <td width="33.3333%">
            <table width="100%">
                <tr>
                    <th style="background-color: #000000; color: #ffffff">
                        Billing Address
                    </th>
                </tr>
                <tr>
                    <td>

                        <b><?= (isset( $billing ) ? $billing->first_name : '' ) . ' ' . (isset( $billing ) ? $billing->last_name : '' ) ?></b><br>
                        <?= (isset( $billing ) ? $billing->address_1 : '' ) ?><br>
                        <?= (isset( $billing ) ? $billing->address_2 : '' ) ?>
                        <?= (isset( $billing ) ? $billing->suburb : '' ) . ' ' . (isset( $billing ) ? $billing->state : '' ) . ' ' . (isset( $billing ) ? $billing->postcode : '' ) ?>
                        <br>
                        <?= (isset( $billing ) ? $billing->country : '' ) ?><br>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>



<div style="padding-left: 30px; padding-right: 30px" >
    <h4 style="text-decoration: underline">Ordered Products</h4>
    <br/>
    <table style='font-size:70%' class='table-bordered'>
        <tr>
            <th style="text-align: center ; height: 25px" width="100px">Oziris ID</th>
            <th style="text-align: center ; height: 25px" width="200px">Product Name</th>
            <th style="text-align: center ; height: 25px" width="100px">Quantity</th>
        </tr>
        <tbody>


<?php

$dryItems = [];
$frozenItems = [];
$chilledItems = [];
$orderItems = $order->getOrderItems();







foreach( $orderItems as $raw_item ){
?>

    <tr>
        <th style="text-align: center ; height: 25px" width="100px"><?= $raw_item->product->id ?></th>
        <th style="text-align: center ; height: 25px" width="200px"><?= $raw_item->product->product_name ?></th>
        <th style="text-align: center ; height: 25px" width="100px"><?= $raw_item->quantity ?></th>
    </tr>

    <?php


    if($raw_item->product->is_collection){

        foreach ($raw_item->product->getCollectionItemsForReport() as $raw_collection_item) {
            if ($raw_collection_item->product->productNature->name == 'Live' || $raw_collection_item->product->productNature->name == 'Dry') {
                array_push($dryItems, prepareCollectionPrintItem($raw_collection_item,$raw_item->quantity));
            }
            else if ($raw_collection_item->product->productNature->name == 'Chilled') {
                array_push($chilledItems, prepareCollectionPrintItem($raw_collection_item,$raw_item->quantity));
            } else if ($raw_collection_item->product->productNature->name == 'Frozen') {
                array_push($frozenItems, prepareCollectionPrintItem($raw_collection_item,$raw_item->quantity));
            }
        }
    }
    else{
        if ($raw_item->product->productNature->name == 'Live' || $raw_item->product->productNature->name == 'Dry') {
            array_push($dryItems, preparePrintItem($raw_item));
        } else if ($raw_item->product->productNature->name == 'Chilled') {
            array_push($chilledItems, preparePrintItem($raw_item));
        } else if ($raw_item->product->productNature->name == 'Frozen') {
            array_push($frozenItems, preparePrintItem($raw_item));
        }
    }
}
?>

        </tbody>
    </table>
</div >

    <div style="padding-left: 30px; padding-right: 30px ; text-decoration: underline" >
        <br/>
        <h4>Product Breakdown</h4>
        <br/>
    </div>

<?php if (count($chilledItems) > 0) { ?>
<div style="padding-left: 30px; padding-right: 30px" >
    <h4>Chilled Items</h4>
    <table class='table-bordered'>
        <tr>
            <th style="text-align: center ; height: 40px" width="100px">Oziris ID</th>
            <th style="text-align: center" width="200px">Product Name</th>
            <th style="text-align: center" width="200px">Brand</th>
            <th style="text-align: center" width="200px">Manufacturer</th>
            <th style="text-align: center" width="200px">Collection</th>
            <th style="text-align: center" width="100px">Quantity</th>
        </tr>
        <tbody>

        <?php foreach ($chilledItems as $item) { ?>
            <tr>
                <th style="text-align: center ; height: 40px" width="100px"><?= $item['OzirisID'] ?></th>
                <th style="text-align: center" width="200px"><?= $item['ProductName'] ?></th>
                <th style="text-align: center" width="200px"><?= $item['Brand'] ?></th>
                <th style="text-align: center" width="200px"><?= $item['Manufacturer'] ?></th>
                <th style="text-align: center" width="200px"><?= $item['Collection'] ?></th>
                <th style="text-align: center ; height: 40px" width="100px"><?= $item['Quantity'] ?></th>

            </tr>
        <?php } ?>
        </tbody>
    </table>
    <p style="text-align: right;">Total Chilled Items : <?= calculateItemQuantity($chilledItems)?></p>
</div >
    <?php } ?>

    <?php if (count($frozenItems) > 0) { ?>
    <div style="padding-left: 30px; padding-right: 30px" >
        <h4>Frozen Items</h4>
        <table class='table-bordered'>
            <tr>
                <th style="text-align: center ; height: 40px" width="100px">Oziris ID</th>
                <th style="text-align: center" width="200px">Product Name</th>
                <th style="text-align: center" width="200px">Brand</th>
                <th style="text-align: center" width="200px">Manufacturer</th>
                <th style="text-align: center" width="200px">Collection</th>
                <th style="text-align: center" width="100px">Quantity</th>
            </tr>
            <tbody>

            <?php foreach ($frozenItems as $item) { ?>
                <tr>
                    <th style="text-align: center ; height: 40px" width="100px"><?= $item['OzirisID'] ?></th>
                    <th style="text-align: center" width="200px"><?= $item['ProductName'] ?></th>
                    <th style="text-align: center" width="200px"><?= $item['Brand'] ?></th>
                    <th style="text-align: center" width="200px"><?= $item['Manufacturer'] ?></th>
                    <th style="text-align: center" width="200px"><?= $item['Collection'] ?></th>
                    <th style="text-align: center ; height: 40px" width="100px"><?= $item['Quantity'] ?></th>

                </tr>
            <?php } ?>
            </tbody>
        </table>
        <p style="text-align: right;">Total Frozen Items : <?= calculateItemQuantity($frozenItems)?></p>
        </div >
        <?php } ?>

        <?php if (count($dryItems) > 0) { ?>
        <div style="padding-left: 30px; padding-right: 30px" >
            <h4>Dry Items</h4>
            <table class='table-bordered'>
                <tr>
                    <th style="text-align: center ; height: 40px" width="100px">Oziris ID</th>
                    <th style="text-align: center" width="200px">Product Name</th>
                    <th style="text-align: center" width="200px">Brand</th>
                    <th style="text-align: center" width="200px">Manufacturer</th>
                    <th style="text-align: center" width="200px">Collection</th>
                    <th style="text-align: center" width="100px">Quantity</th>
                </tr>
                <tbody>

                <?php foreach ($dryItems as $item) { ?>
                    <tr>
                        <th style="text-align: center ; height: 40px" width="100px"><?= $item['OzirisID'] ?></th>
                        <th style="text-align: center" width="200px"><?= $item['ProductName'] ?></th>
                        <th style="text-align: center" width="200px"><?= $item['Brand'] ?></th>
                        <th style="text-align: center" width="200px"><?= $item['Manufacturer'] ?></th>
                        <th style="text-align: center" width="200px"><?= $item['Collection'] ?></th>
                        <th style="text-align: center ; height: 40px" width="100px"><?= $item['Quantity'] ?></th>

                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <p style="text-align: right;">Total Dry Items : <?= calculateItemQuantity($dryItems)?></p>
        </div >
            <?php }?>

    <?php if(($key+1) != count($orders)){
            echo '<pagebreak />';
    } ?>

        <?php }?>
</body>