<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>

<body lang="zh-CN">
<?php
foreach($orders as $order){
?>

    <table width="100%">
        <tr style="border: 1px solid #000000">
            <th style="vertical-align: middle; text-align: left">
                <img width="120px" src="images/beston-logo.png" alt="Beston Market Place">
            </th>
            <th style="vertical-align: middle; text-align: left">
                <img width="130px" src="images/oziris-logo.png" alt="OZIRIS">
            </th>
            <th style="vertical-align: top ; text-align: right">
                <?php $imgsrc = 'order/qrcode/'.$order->qr_code?>
                <img src="<?= $imgsrc?>"  alt='Order QR Code' width='100px' height='100px'>
            </th>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: center">
                <p style="font-size: 16px; font-weight: bolder ; color: #ffffff ;background-color: #000000;">www.bestonmarketplace.com.au</p>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
        <tr>
            <td colspan="3" height="100px" style="vertical-align: top">
                <table width="100%" >
                    <tr>
                        <th style="font-size: 14px; font-weight: bolder ; background-color: #000000; color: #ffffff">
                            Shipping Address
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <p style="text-align: justify ; font-size: 14px">
                                <b><?= $order->getShippingAddress()->one()->first_name . ' ' . $order->getShippingAddress()->one()->last_name ?></b><br>
                                <span><?= $order->getShippingAddress()->one()->address_1 ?></span><br>
                                <?php if ($order->getShippingAddress()->one()->address_2 != '') {
                                    echo $order->getShippingAddress()->one()->address_2;
                                }
                                ?>
                                <?= $order->getShippingAddress()->one()->suburb . ' ' . $order->getShippingAddress()->one()->state . ' ' . $order->getShippingAddress()->one()->postcode ?>
                                <br>
                                <?= $order->getShippingAddress()->one()->country ?>
                                <br>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="100px" style="vertical-align: top">
                <table width="100%">
                    <tr>
                        <th style="font-size: 14px; font-weight: bolder ; background-color: #000000; color: #ffffff;">
                            Order Information
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <p style="text-align: justify ; font-size: 14px">
                                <b>Order Number : </b><?= $order->woo_order_id ?><br>
                                <b>Leave at door : </b><?= (($order->leave_at_door == '1') ? 'Yes' : 'No'); $order->leave_at_door ?><br>
                                <br>
                                <br>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" height="100px" style="vertical-align: top" >
                <table width="100%">
                    <tr>
                        <th style="font-size: 14px; font-weight: bolder ; background-color: #000000; color: #ffffff">
                            Order Note
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <p style="text-align: justify ; font-size: 14px">
                                <?= $order->order_note ?><br>
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="vertical-align: top ; text-align: center">
                <p style="font-size: 16px; font-weight: bolder ; color: #ffffff ;background-color: #000000;">TASTY & FRAGILE HANDLE WITH CARE</p>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br>
            </td>
        </tr>
        <tr style="border: 1px solid #000000">
            <td colspan="3" style="vertical-align: top ; text-align: center">
                <?php $barcode = preg_replace('/\s+/', '', 'order/barcode/'.$order->barcode).'.png' ?>
                <img src="<?= $barcode?>"  height="45px" alt='Order Bar Code'>
            </td>
        </tr>

    </table>

    <pagebreak />
<?php
}
?>
</body>

</html>