<?php

use app\models\OrderStatus;
use app\widgets\JavaScript;
use cornernote\returnurl\ReturnUrl;
use kartik\daterange\DateRangePicker;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Orders');
?>
<div class="address-create">

    <div class="product-index">
        <div class="clearfix">
            <div class="row">
                <div class="col-lg-12">
                    <table width="100%">
                        <tr>
                            <td>
                                <h1 class="pull-left"><?= $this->title ?></h1>
                            </td>
                            <td>
                                <div class="pull-right">

                                    <?= Html::button('Generate Manifest', ['class' => 'btn btn-oziris', 'id' => 'btnGenerateManifest']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadManifest">Download Manifest</a>
                                    <?= Html::button('Generate Labels', ['class' => 'btn btn-oziris', 'id' => 'btnPrintLabel']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadLabel">Download Labels</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr class="hr">
        </div>
        <div class="table-responsive text-center">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'export' => false,
                'filterModel' => $searchModel,
                'bordered'=>true,
                'striped'=>true,
                'condensed'=>true,
                'responsive'=>true,
                'hover'=>true,
                'columns' => [
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'woo_order_id',
                        'contentOptions' => ['style' => 'width:70px;'],
                        'value' => function ($model) {
                            return Html::a($model->woo_order_id, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'created_at',
                        'label' => 'Order Date',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd-M-y'
                                ],
                            ],
                        ]),
                        'value' => function ($model) {
                            return Html::a(date('d-M-y h:i a', $model->created_at), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'status_id',
                        'filter'=>ArrayHelper::map(OrderStatus::find()->asArray()->all(), 'id', 'name'),
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($model) {
                           return Html::a($model->status->name, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'updated_at',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'updated_at',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd-M-y'
                                ],
                            ],
                        ]),
                        'value' => function ($model) {
                            if((integer)$model->updated_at>0){
                                return Html::a(date('d-M-y h:i a', (integer)$model->updated_at), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                            }else{
                                return '';
                            }

                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'customer_id',
                        'label' => 'Customer Email',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'value' => function ($model) {
                                if($model->customer->email_address && $model->customer->email_address != 'default'){
                                    return Html::a($model->customer->email_address, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                                }else if($model->customer->email_address == 'default'){
                                    return Html::a("Guest Checkout", ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                                }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'contact_no',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'value' => function ($model) {
                                if($model->getBillingAddress()->one()){
                                    return Html::a($model->getBillingAddress()->one()->contact_no, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                                }else{
                                    return '';
                                }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'order_note',
                        'value' => function ($model) {
                            return Html::a($model->order_note, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'total_amount',
                        'label' => 'Total',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'value' => function ($model) {
                            $formatter = new NumberFormatter('en_AU', NumberFormatter::CURRENCY);
                            return Html::a($formatter->formatCurrency($model->total_amount, 'AUD'), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],

                    ['class' => ActionColumn::className(),
                        'template' => '{update}&nbsp;&nbsp;{duplicate}',
                        'buttons' => [
                            'duplicate' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-print"></span> ', ['report', 'id' => $model->woo_order_id], ['title' => Yii::t('app', 'Print Order'),]);
                            }
                        ],
                        'contentOptions' => ['nowrap' => 'nowrap']
                    ],
                ],
            ]); ?>
        </div>

    </div>
</div>


<?php JavaScript::begin(); ?>
<script>

    $('#btnGenerateManifest').on('click', function (event) {
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                console.log($('thead tr td:nth-child(' + i + ') input').attr('name'));
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
//                console.log(element);

                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }

        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();
        console.log(searchField);
        $.ajax({
            url: '<?= Url::to(['order/generate-manifest']); ?>', // point to server-side PHP script
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                if (php_script_response) {
                    $('#btnGenerateManifest').addClass('hidden');
                    $('#btnDownloadManifest').removeClass('hidden');
                }
                console.log(php_script_response); // display response from the PHP script, if any
            },
            error: function (php_script_response) {
                console.log('error'); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            }
        });
    });


    $('#btnPrintLabel').on('click', function (event) {
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }
        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();

        $.ajax({
            url: '<?= Url::to(['order/print-label']); ?>',
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                    $('#btnPrintLabel').addClass('hidden');
                    $('#btnDownloadLabel').removeClass('hidden');
            },
            error: function (php_script_response) {
            }
        });
    });


    $('#btnDownloadManifest').on('click', function (event) {
        $('#btnGenerateManifest').removeClass('hidden');
        $('#btnDownloadManifest').addClass('hidden');

        $('#btnDownloadManifest').attr({target: '_blank', href: '<?= Url::to('@web/csv/manifest.csv'); ?>'});
    });

    $('#btnDownloadLabel').on('click', function (event) {
        $('#btnPrintLabel').removeClass('hidden');
        $('#btnDownloadLabel').addClass('hidden');

        $('#btnDownloadLabel').attr({target: '_blank', href: '<?= Url::to('@web/label.pdf'); ?>'});
    });

</script>
<?php JavaScript::end(); ?>

