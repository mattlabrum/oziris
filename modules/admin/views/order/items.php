<?php

/**
 * @var yii\web\View $this
 * @var app\models\Order $model
 * @var yii\bootstrap\ActiveForm $form
 */
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use kartik\detail\DetailView;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\grid\ActionColumn;

?>

<div class="collection-item-form text-center" id="order-items-div">
    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <h1 class="pull-left">Order Items</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="text-center" style="padding-top: 20px">
                <?= GridView::widget([
                    'layout' => '{pager}{items}{pager}',
                    'export' => false,
                    'responsive' => true,
                    'hover'=>true,
                    'dataProvider' => $model->getOrderItemsDataProvider(),
                    'pager' => [
                        'class' => yii\widgets\LinkPager::className(),
                        'firstPageLabel' => Yii::t('app', 'First'),
                        'lastPageLabel' => Yii::t('app', 'Last'),
                    ],
                    'columns' => [
                        [
                            'class' => DataColumn::className(),
                            'value' => function ($model) {
                                return Html::img(yii\helpers\Url::to('@web/uploads/product').'/'.$model->product->product_image,['style' => 'width:100px']) ;
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => DataColumn::className(),
                            'label' => 'Product Name',
                            'value' => function ($model) {
                                return Html::a($model->product->product_name, ['product/view', 'id' => $model->product->id,], ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => DataColumn::className(),
                            'label' => 'Quantity',
                            'value' => function ($model) {
                                return Html::a($model->quantity, ['product/view', 'id' => $model->product->id,], ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                        [
                            'class' => DataColumn::className(),
                            'label' => 'Price',
                            'value' => function ($model) {
                                return Html::a($model->product->getProductPrice(2), ['product/view', 'id' => $model->product->id,], ['data-pjax' => 0]);
                            },
                            'format' => 'raw',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
</div>