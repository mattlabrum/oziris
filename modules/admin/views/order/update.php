<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Order $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Order') . ' ' . $model->woo_order_id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>



<div class="order-update">
    <?= $this->render('_menu', ['model' => $model]); ?>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
