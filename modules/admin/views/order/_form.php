<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Order $model
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div class="order-form">

    <?php $form = ActiveForm::begin([
        'id' => 'Order',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-2',
                'wrapper' => 'col-lg-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
    <?= $form->errorSummary($model); ?>


    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">

            <div class="row" style="padding-top: 20px">

                <div class="col-lg-10">
                    <div class="row text-center">
                        <div class="<?= ( isset($model->subscription_id) ? 'col-lg-3' : 'col-lg-4') ?>">
                            <table class="table-responsive" style="height: 203px ; border : solid 1px #808080;">
                                <tr style="background-color: #000000; color: #ffffff">
                                    <th colspan="2" class="text-center" style="padding: 5px" width="130px">
                                        Order Information
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="130px">
                                        Order Date
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="130px">
                                        <?php
                                        echo date('d-M-y h:i a', $model->created_at); ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="140px">
                                        Order Number
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="140px">
                                        <?= $model->woo_order_id ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="140px">
                                        Payment Status
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="140px">
                                        <?= (isset($model->payment) ? $model->payment->name : '' ) ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="140px">
                                        Total Amount
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="140px">
                                        <?php
                                        $formatter = new NumberFormatter('en_AU', NumberFormatter::CURRENCY);
                                        echo $formatter->formatCurrency($model->total_amount, 'AUD'), PHP_EOL;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="140px">
                                        Leave At Door
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="140px">
                                        <?= (($model->leave_at_door == '1') ? 'Yes' : 'No'); $model->leave_at_door ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="<?= ( isset($model->subscription_id) ? 'col-lg-3' : 'col-lg-4') ?>">
                            <table class="table-responsive" style="height: 203px ; border : solid 1px #808080;">
                                <tr style="background-color: #000000; color: #ffffff">
                                    <th colspan="2" class="text-center" style="padding: 5px" width="130px">
                                        Shipping Information
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="130px">
                                        Customer Name
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="130px">
                                        <?= $model->getShippingAddress()->one()->first_name . ' ' . $model->getShippingAddress()->one()->last_name ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 10px ; vertical-align: top" width="130px">
                                        Shipping Address
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="130px">
                                        <?= $model->getShippingAddress()->one()->address_1 ?><br>
                                        <?php if ($model->getShippingAddress()->one()->address_2 != '') {
                                            echo $model->getShippingAddress()->one()->address_2;
                                        }
                                        ?>
                                        <?= $model->getShippingAddress()->one()->suburb . ' ' . $model->getShippingAddress()->one()->state . ' ' . $model->getShippingAddress()->one()->postcode ?>
                                        <br>
                                        <?= $model->getShippingAddress()->one()->country ?><br><br><br>
                                    </td>
                                </tr>

                            </table>
                        </div>

                        <div class="<?= ( isset($model->subscription_id) ? 'col-lg-3' : 'col-lg-4') ?>">
                            <table class="table-responsive" style="height: 203px ; border : solid 1px #808080;">
                                <tr style="background-color: #000000; color: #ffffff">
                                    <th colspan="2" class="text-center" style="padding: 5px" width="130px">
                                        Billing Information
                                    </th>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="130px">
                                        Name
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="130px">
                                        <?= $model->getBillingAddress()->one()->first_name . ' ' . $model->getBillingAddress()->one()->last_name ?>
                                    </td>
                                </tr>

                                <tr>
                                    <th style="text-align: right; padding: 10px ; vertical-align: top" width="130px">
                                        Billing Address
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="130px">
                                        <?= $model->getBillingAddress()->one()->address_1 ?><br>
                                        <?php if ($model->getBillingAddress()->one()->address_2 != '') {
                                            echo $model->getBillingAddress()->one()->address_2;
                                        }
                                        ?>
                                        <?= $model->getBillingAddress()->one()->suburb . ' ' . $model->getBillingAddress()->one()->state . ' ' . $model->getBillingAddress()->one()->postcode ?>
                                        <br>
                                        <?= $model->getBillingAddress()->one()->country ?><br>
                                    </td>
                                </tr>

                                <tr>
                                    <th style="text-align: right; padding: 5px" width="140px">
                                        Contact Number
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="140px">
                                        <?= $model->getBillingAddress()->one()->contact_no ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th style="text-align: right; padding: 5px" width="140px">
                                        Contact E-mail
                                    </th>
                                    <td style="text-align: left; padding: 5px" width="140px">
                                        <?= $model->getBillingAddress()->one()->email;
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <?php if(isset($model->subscription_id)) { ?>

                            <div class="<?= ( isset($model->subscription_id) ? 'col-lg-3' : 'col-lg-4') ?>">
                                <table class="table-responsive" style="height: 203px ; border : solid 1px #808080;">
                                    <tr style="background-color: #000000; color: #ffffff">
                                        <th colspan="2" class="text-center" style="padding: 5px" width="130px">
                                            Subscription Information
                                        </th>
                                    </tr>
                                    <tr>
                                        <th style="text-align: right; padding: 5px" width="130px">
                                            Subscription ID
                                        </th>
                                        <td style="text-align: left; padding: 5px" width="130px">
                                            <?= $model->subscription_id ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: right; padding: 5px" width="140px">
                                            Billing Period
                                        </th>
                                        <td style="text-align: left; padding: 5px" width="140px">
                                            <?= $model->parent_billing_period;
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="text-align: right; padding: 5px" width="140px">
                                            Billing Interval
                                        </th>
                                        <td style="text-align: left; padding: 5px" width="140px">
                                            <?= $model->parent_billing_interval;
                                            ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th style="text-align: right; padding: 5px" width="140px">
                                            Delivery Day
                                        </th>
                                        <td style="text-align: left; padding: 5px" width="140px">
                                            <?= $model->parent_subscription_delivery_day;
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th style="text-align: right; padding: 5px" width="140px">
                                           Delivery Date
                                        </th>
                                        <td style="text-align: left; padding: 5px" width="140px">
                                            <?= date('d-m-Y', strtotime($model->subscription_delivery_date)); ?>
                                        </td>
                                    </tr>
                                </table>
                            </div>


                        <?php } ?>



                    </div>
                </div>

                <div class="col-lg-2" style="width: 200px; height:180px" >
                    <?= Html::img(yii\helpers\Url::to('@web/order/qrcode').'/'.$model->qr_code, ['style'=>['width'=> '100%','height'=> '100%' ]]) ; ?>
                </div>

            </div>

            <div class="row" style="padding-top: 20px">
                <div class="col-lg-12">
                    <?= $form->field($model, 'order_note')->textarea(['rows' => 6]) ?>
                    <?= // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::activeField
                    $form->field($model, 'status_id')->widget(\kartik\select2\Select2::classname(), [
                        'name' => 'class_name',
                        'model' => $model,
                        'attribute' => 'status_id',
                        'data' => \yii\helpers\ArrayHelper::map(app\models\OrderStatus::find()->all(), 'id', 'description'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Type to autocomplete'),
                            'multiple' => false,
                        ]
                    ]); ?>
                </div>
            </div>


            <div class="row" style="padding-top: 20px">
                <div class="col-lg-12">

                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <?= $this->render('../collection-item/order-item',['model' => $model]); ?>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-check"></span> ' . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if ($model->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
