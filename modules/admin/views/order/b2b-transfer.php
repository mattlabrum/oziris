<?php

use kartik\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div id="b2b-transfer" class="hidden">
    <div class="col-lg-1"></div>
    <div class="col-lg-8">
        <div class="col-lg-2">
            <label style="padding-top: 10px" class="control-label">Email Address</label>
        </div>
        <div class="col-lg-6">
            <?= Html::textInput('', null, ['id' => 'txtEmailAddress', 'style'=>'width:100%;' ]) ?>
        </div>
    </div>
    <div class="col-lg-2"></div>
</div>

