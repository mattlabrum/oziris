<?php

use app\models\Language;
use app\models\OrderStatus;
use app\widgets\JavaScript;
use cornernote\returnurl\ReturnUrl;
use kartik\daterange\DateRangePicker;
use kartik\grid\ActionColumn;
use kartik\grid\GridView;
use kartik\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$this->title = Yii::t('app', 'Orders');
?>
<div class="address-create">

    <div class="product-index">
        <div class="clearfix">
            <div class="row">
                <div class="col-lg-12">
                    <table width="100%">
                        <tr>
                            <td>
                                <h1 class="pull-left"><?= $this->title ?></h1>
                            </td>
                            <td>
                                <div class="pull-right">

                                    <?= Html::a('<span class="btn btn-oziris">Create B2B Order</span> ', ['create']); ?>

                                    <?= Html::button('Generate Manifest', ['class' => 'btn btn-oziris', 'id' => 'btnGenerateManifest']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadManifest">Download Manifest</a>

                                    <?= Html::button('Generate Labels', ['class' => 'btn btn-oziris', 'id' => 'btnPrintLabel']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadLabel">Download Labels</a>

                                    <?= Html::button('Print Orders', ['class' => 'btn btn-oziris', 'id' => 'btnPrintOrders']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadPrintOrders">Download Print Orders</a>

                                    <?= Html::button('Generate Order CSV', ['class' => 'btn btn-oziris', 'id' => 'btnGenerateOrderCSV']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadOrderCSV">Download Order CSV</a>

                                    <?= Html::button('Generate Order Item CSV', ['class' => 'btn btn-oziris', 'id' => 'btnGenerateOrderItemCSV']) ?>
                                    <a class='btn btn-oziris hidden' id="btnDownloadOrderItemCSV">Download Order Item CSV</a>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <hr class="hr">
        </div>
        <div class="table-responsive text-center">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'order_table',
                'export' => false,
                'filterModel' => $searchModel,
                'bordered'=>true,
                'striped'=>true,
                'condensed'=>true,
                'responsive'=>true,
                'floatHeader'=>true,
                'floatHeaderOptions'=>['top'=>'0'],
                'hover'=>true,
                'columns' => [
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'woo_order_id',
                        'contentOptions' => ['style' => 'width:70px;'],
                        'value' => function ($model) {
                            return Html::a($model->woo_order_id, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'created_at',
                        'label' => 'Order Date',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'created_at',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd-M-y'
                                ],
                            ],
                        ]),
                        'value' => function ($model) {
                            return Html::a(date('d-M-y h:i a', $model->created_at), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'status_id',
                        'filter'=>ArrayHelper::map(OrderStatus::find()->asArray()->all(), 'id', 'description'),
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($model) {
                           return Html::a($model->status->description, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'updated_at',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'filter' => DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'updated_at',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'locale' => [
                                    'format' => 'd-M-y'
                                ],
                            ],
                        ]),
                        'value' => function ($model) {
                            if((integer)$model->updated_at>0){
                                return Html::a(date('d-M-y h:i a', (integer)$model->updated_at), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                            }else{
                                return '';
                            }

                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'First Name',
                        'attribute' => 'first_name',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'value' => function ($model) {
                            if($model->customer->email_address && $model->customer->email_address != 'default'){
                                return Html::a($model->customer->first_name , ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                            }else if($model->customer->email_address == 'default'){
                                return Html::a($model->getShippingAddress()->one()->first_name , ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                            }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'label' => 'Last Name',
                        'attribute' => 'last_name',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'value' => function ($model) {
                            if($model->customer->email_address && $model->customer->email_address != 'default'){
                                return Html::a($model->customer->last_name , ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                            }else if($model->customer->email_address == 'default'){
                                return Html::a($model->getShippingAddress()->one()->last_name , ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                            }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'customer_id',
                        'label' => 'Customer Email',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'value' => function ($model) {
                                if($model->customer->email_address && $model->customer->email_address != 'default'){
                                    return Html::a($model->customer->email_address, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                                }else if($model->customer->email_address == 'default'){
                                    return Html::a($model->getBillingAddress()->one()->email." (Guest)", ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                                }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'contact_no',
                        'contentOptions' => ['style' => 'width:200px;'],
                        'value' => function ($model) {
                                if($model->getBillingAddress()->one()){
                                    return Html::a($model->getBillingAddress()->one()->contact_no, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                                }else{
                                    return '';
                                }
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'order_note',
                        'value' => function ($model) {
                            return Html::a($model->order_note, ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'subscription_id',
                        'label' => 'Subscription ID',
                        'value' => function ($model) {
                            return Html::a((isset($model->subscription_id) ? $model->subscription_id : '' ), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'subscription_delivery_date',
                        'label' => 'Delivery Date',
                        'value' => function ($model) {
                            return Html::a((isset($model->subscription_delivery_date) ? $model->subscription_delivery_date : '' ), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'dispatch_date',
                        'label' => 'Dispatch Date',
                        'value' => function ($model) {
                            return Html::a((isset($model->dispatch_date) ? $model->dispatch_date : '' ), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'location_id',
                        'label' => 'Order Location',
                        'filter'=>ArrayHelper::map(Language::find()->asArray()->all(), 'id', 'name'),
                        'contentOptions' => ['style' => 'width:150px;'],
                        'value' => function ($model) {
                            return Html::a((($model->woo_order_id < 10000000) ? 'Australia' : 'China'), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'class' => yii\grid\DataColumn::className(),
                        'attribute' => 'total_amount',
                        'label' => 'Total',
                        'contentOptions' => ['style' => 'width:50px;'],
                        'value' => function ($model) {
                            $formatter = new NumberFormatter('en_AU', NumberFormatter::CURRENCY);
                            return Html::a($formatter->formatCurrency($model->total_amount, 'AUD'), ['order/update', 'id' => $model->id,], ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                    ],

                    ['class' => ActionColumn::className(),
                        'template' => '{update}&nbsp;&nbsp;{duplicate}',
                        'buttons' => [
                            'duplicate' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-print"></span> ', ['report', 'id' => $model->woo_order_id], ['title' => Yii::t('app', 'Print Order'),]);
                            }
                        ],
                        'contentOptions' => ['nowrap' => 'nowrap']
                    ],
                ],
            ]); ?>
        </div>

    </div>
</div>


<?php JavaScript::begin(); ?>
<script>

    $('#btnGenerateManifest').on('click', function (event) {
        $.loader.open();
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                console.log($('thead tr td:nth-child(' + i + ') input').attr('name'));
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
//                console.log(element);

                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }

        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();
        console.log(searchField);
        $.ajax({
            url: '<?= Url::to(['order/generate-manifest']); ?>', // point to server-side PHP script
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                if (php_script_response) {
                    $('#btnGenerateManifest').addClass('hidden');
                    $('#btnDownloadManifest').removeClass('hidden');
                    $.loader.close();
                }
                console.log(php_script_response); // display response from the PHP script, if any
            },
            error: function (php_script_response) {
                $.loader.close();
                console.log('error'); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            }
        });
    });

    $('#btnPrintLabel').on('click', function (event) {
        $.loader.open();
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }
        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();

        $.ajax({
            url: '<?= Url::to(['order/print-label']); ?>',
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                $('#btnPrintLabel').addClass('hidden');
                $('#btnDownloadLabel').removeClass('hidden');
                $.loader.close();
            },
            error: function (php_script_response) {
                $.loader.close();
            }
        });
    })

    $('#btnPrintOrders').on('click', function (event) {
        $.loader.open();
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }
        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();

        $.ajax({
            url: '<?= Url::to(['order/print-orders']); ?>',
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                console.log(php_script_response);
                    $('#btnPrintOrders').addClass('hidden');
                    $('#btnDownloadPrintOrders').removeClass('hidden');
                $.loader.close();
            },
            error: function (php_script_response) {
                $.loader.close();
            }
        });
    });

    $('#btnDownloadPrintOrders').on('click', function (event) {
        $('#btnPrintOrders').removeClass('hidden');
        $('#btnDownloadPrintOrders').addClass('hidden');

        $('#btnDownloadPrintOrders').attr({target: '_blank', href: '<?= Url::to('@web/orders.pdf'); ?>'});
    });

    $('#btnDownloadManifest').on('click', function (event) {
        $('#btnGenerateManifest').removeClass('hidden');
        $('#btnDownloadManifest').addClass('hidden');

        $('#btnDownloadManifest').attr({target: '_blank', href: '<?= Url::to('@web/csv/manifest.csv'); ?>'});
    });

    $('#btnDownloadLabel').on('click', function (event) {
        $('#btnPrintLabel').removeClass('hidden');
        $('#btnDownloadLabel').addClass('hidden');

        $('#btnDownloadLabel').attr({target: '_blank', href: '<?= Url::to('@web/label.pdf?v='.time()); ?>'});
    });

    $('#btnGenerateOrderCSV').on('click', function (event) {
        $.loader.open();
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                console.log($('thead tr td:nth-child(' + i + ') input').attr('name'));
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
//                console.log(element);

                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }

        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();
        searchField['location_id'] = $('select[name="OrderSearch[location_id]"]').val();
        console.log(searchField);
        $.ajax({
            url: '<?= Url::to(['order/generate-order-csv']); ?>', // point to server-side PHP script
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                console.log(php_script_response); // display response from the PHP script, if any
                if (php_script_response) {
                    $('#btnGenerateOrderCSV').addClass('hidden');
                    $('#btnDownloadOrderCSV').removeClass('hidden');
                    $.loader.close();
                }

            },
            error: function (php_script_response) {
                console.log('error'); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
                $.loader.close();
            }
        });
    });

    $('#btnGenerateOrderItemCSV').on('click', function (event) {
        $.loader.open();
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            if($('thead tr td:nth-child(' + i + ') input').attr('name') != undefined){
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }
        }
        searchField['status_id'] = $('select[name="OrderSearch[status_id]"]').val();
        searchField['location_id'] = $('select[name="OrderSearch[location_id]"]').val();
        $.ajax({
            url: '<?= Url::to(['order/generate-order-item-csv']); ?>', // point to server-side PHP script
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                console.log(php_script_response); // display response from the PHP script, if any

                if (php_script_response) {
                    $('#btnGenerateOrderItemCSV').addClass('hidden');
                    $('#btnDownloadOrderItemCSV').removeClass('hidden');
                    $.loader.close();
                }

            },
            error: function (php_script_response) {
                console.log('error'); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
                $.loader.close();
            }
        });
    });

    $('#btnDownloadOrderItemCSV').on('click', function (event) {
        $('#btnGenerateOrderItemCSV').removeClass('hidden');
        $('#btnDownloadOrderItemCSV').addClass('hidden');
        $('#btnDownloadOrderItemCSV').attr({target: '_blank', href: '<?= Url::to('@web/csv/order-item.csv'); ?>'});
    });

    $('#btnDownloadOrderCSV').on('click', function (event) {
        $('#btnGenerateOrderCSV').removeClass('hidden');
        $('#btnDownloadOrderCSV').addClass('hidden');

        $('#btnDownloadOrderCSV').attr({target: '_blank', href: '<?= Url::to('@web/csv/order.csv'); ?>'});
    });


</script>
<?php JavaScript::end(); ?>

