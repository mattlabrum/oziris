<?php

use app\widgets\JavaScript;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\models\PushNotification $model
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div class="push-notification-form">

    <?php $form = ActiveForm::begin([
        'id' => 'PushNotification',
        'layout' => 'horizontal',
        'enableClientValidation' => false,
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-4',
                'wrapper' => 'col-lg-8',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>

    <?= $form->errorSummary($model); ?>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
            <h1>Push Notification</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row center-block" style="padding-top: 20px ">
                <div class="col-lg-10">
                    <div class="row">


                        <div class="col-lg-10">
                            <?= $form->field($model, 'message')->textarea(['maxlength' => true,'rows' =>4]) ?>
                        </div>

                        <div class="col-lg-2">
                            <?= $form->field($model, 'for_ios')->checkbox() ?>
                            <?= $form->field($model, 'for_android')->checkbox() ?>
                        </div>
                    </div>



                </div>

            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-check"></span> ' . ($model->isNewRecord ? Yii::t('app', 'Send') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if($model->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <?php ActiveForm::end(); ?>

</div>



