<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\PushNotification $model
 */

$this->title = Yii::t('app', 'Push Notification') . ' ' . $model->id;
$this->params['heading'] = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Push Notifications'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="push-notification-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]); ?>



    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <hr class="hr">
        <div class="row">
            <div class="col-lg-12">
                <div class="batch-view">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'message',
                            'created_at',
                            'for_ios',
                            'for_android',
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-1"></div>

</div>
