<?php

use kartik\grid\ActionColumn;
use kartik\grid\BooleanColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\PushNotification $model
 * @var app\models\search\PushNotificationSearch $searchModel
 */

$this->title = Yii::t('app', 'Push Notifications History');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="push-notification-index">

    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a('<span class="fa fa-plus"></span> ' . Yii::t('app', 'Send') . ' ' . Yii::t('app', 'Notification'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-oziris']) ?>
                    </div>
                </td>
            </tr>
        </table>

    </div>

    <div class="table-responsive text-center">
        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'export' => false,
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'responsive' => true,
            'hover'=>true,
            'columns' => [
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'id',
                    'options' => ['width' => '50px']
                ],
                'message',
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'created_at',
                    'format' =>  ['datetime', 'php:d-M-Y h:m A'],
                    'options' => ['width' => '200px']
                ],
                [
                    'class' => BooleanColumn::className(),
                    'attribute' => 'for_ios',
                    'value' => function ($model) {
                        if ( $model->for_ios) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => BooleanColumn::className(),
                    'attribute' => 'for_android',
                    'value' => function ($model) {
                        if ( $model->for_android) {
                            return true;
                        } else {
                            return false;
                        }
                    },
                    'format' => 'raw',
                ],
                ['class' => ActionColumn::className(),
                    'template' => '{resend}&nbsp;&nbsp;{delete}',
                    'buttons' => [
                        'resend' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-duplicate"></span> ', ['resend', 'id' => $model->id], ['title' => Yii::t('app', 'Resend'),]);
                        }
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>

</div>