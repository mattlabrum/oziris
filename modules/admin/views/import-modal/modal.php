<div id="activity-modal" class="modal modal-wide fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-import"></span> Import CSV</h4>
            </div>
            <div class="modal-body">
                <div>
                    <div id="fields">

                        <div>
                            <button class="btn btn-file1 btn-oziris">
                                <span>Choose file</span>
                                <input id="import-file" type="file"/>
                            </button>
                            <span id="txtFileInput"></span>
                        </div>

                    </div>

                    <div id="data-preview">
                        <hr>
                        <div id="display-data-table">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-oziris-normal" id="btnClose">Close</button>
                <button type="button" id="btnSaveData" class="btn btn-oziris-save">Save changes</button>
            </div>
        </div>
    </div>
</div>
