<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\ProductGroupForm $model
 */

$this->title = Yii::t('app', 'Product Group') . ' ' . $model->productgroup->product_group_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-group-view">

    <?= $this->render('@app/views/layouts/_menu', ['model'=>$model->productgroup]); ?>
    <?= DetailView::widget([
        'model' => $model->productgroup,
        'attributes' => [
            'product_group_name',
            [
                'attribute' => 'show_on_menu',
                'format' => 'raw',
                'value' => $model->productgroup->show_on_menu ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                'widgetOptions' => [
                    'pluginOptions' => [
                        'onText' => 'Yes',
                        'offText' => 'No',
                    ]
                ],
                'labelColOptions' => ['style' => 'width:15.8%'],
            ],
            [
                'attribute' => 'created_at',
                'format' =>  ['datetime', 'php:d/m/yy h:m:s '],
                'options' => ['width' => '100px']
            ],
            [
                'attribute' => 'updated_at',
                'format' =>  ['datetime', 'php:d/m/yy h:m:s '],
                'options' => ['width' => '100']
            ],
        ],
    ]); ?>

    <?= DetailView::widget([
        'model' => $model->productgrouptranslation,
        'attributes' => [
            'product_group_name',
        ],
    ]);
    ?>
</div>
