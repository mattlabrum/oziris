<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductGroup $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Product Group') . ' ' . $model->productgroup->product_group_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->productgroup->id, 'url' => ['view', 'id' => $model->productgroup->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="product-group-update">

    <?= $this->render('@app/views/layouts/_menu', ['model'=>$model->productgroup]); ?>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
