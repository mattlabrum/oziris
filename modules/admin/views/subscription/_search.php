<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/eeda5c365686c9888dbc13dbc58f89a1
 *
 * @package default
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 *
 * @var yii\web\View $this
 * @var app\models\search\SubscriptionSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="subscription-search">

    <?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

    		<?php echo $form->field($model, 'id') ?>

		<?php echo $form->field($model, 'start_date') ?>

		<?php echo $form->field($model, 'next_payment_date') ?>

		<?php echo $form->field($model, 'billing_period') ?>

		<?php echo $form->field($model, 'billing_interval') ?>

		<?php // echo $form->field($model, 'status') ?>

		<?php // echo $form->field($model, 'subscription_name') ?>

		<?php // echo $form->field($model, 'delivery_day') ?>

		<?php // echo $form->field($model, 'next_delivery_date') ?>

		<?php // echo $form->field($model, 'customer_id') ?>

		<?php // echo $form->field($model, 'subscription_billing_address_id') ?>

		<?php // echo $form->field($model, 'subscription_shipping_address_id') ?>

		<?php // echo $form->field($model, 'payment_method') ?>

		<?php // echo $form->field($model, 'payment_title') ?>

		<?php // echo $form->field($model, 'stript_customer_id') ?>

		<?php // echo $form->field($model, 'stripe_card_id') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('cruds', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('cruds', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
