<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\IngredientGroupTranslation $model
 */

$this->title = Yii::t('app', 'Create Ingredient Group Translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient Group Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-group-translation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
