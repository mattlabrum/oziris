<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\IngredientGroupTranslation $model
 */

$this->title = Yii::t('app', 'Ingredient Group Translation') . ' ' . $model->name;
$this->params['heading'] = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient Group Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="ingredient-group-translation-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\IngredientGroupTranslation'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'language_id',
            'ingredient_group_id',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> IngredientGroupTranslation',
                'content' => $this->blocks['app\models\IngredientGroupTranslation'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
