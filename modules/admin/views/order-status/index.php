<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\OrderStatusSearch $searchModel
 */

$this->title = Yii::t('app', 'Order Statuses');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-status-index">

    <div class="clearfix">
        <div class="row">
            <div class="col-lg-12">
                <table width="100%">
                    <tr>
                        <td>
                            <h1 class="pull-left"><?= $this->title ?></h1>
                        </td>
                        <td>
                            <div class="pull-right">
                                <?= Html::a( Yii::t('app', 'Manage Orders'), ReturnUrl::getUrl(['order/index']), ['class' => 'btn btn-oziris']) ?>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <hr class="hr">
    </div>

    <div class="table-responsive text-center">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'export' => false,
            'filterModel' => $searchModel,
            'bordered'=>true,
            'striped'=>true,
            'condensed'=>true,
            'responsive'=>true,
            'hover'=>true,
            'columns' => [
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{create} &nbsp;&nbsp; {update} &nbsp;&nbsp; {delete}',
                    'buttons' => [
                        'create' => function () {
                            return Html::a('<span class="glyphicon glyphicon-plus-sign"></span> ', ['create'], ['title' => Yii::t('app', 'Create'),]);
                        }


                    ],
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
                'id',
                'name',
                'description',
            ],
        ]); ?>
    </div>

</div>