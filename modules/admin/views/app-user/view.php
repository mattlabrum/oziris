<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\AppUser $model
 */

$this->title = Yii::t('app', 'App User') . ' { ' . $model->first_name . ' ' . $model->last_name . ' } ';
$this->params['heading'] = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>


<div class="row">
    <?= $this->render('@app/views/layouts/_menu', compact('model')); ?>
    <div class="col-lg-1"></div>
    <div class="col-lg-10">

        <hr class="hr">
        <div class="row">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'first_name',
                    'last_name',
                    'dob',
                    'gender',
                    'email_address:email',
                    'is_active',
                    'mobile',
                    'country',
                ],
            ]); ?>
        </div>
    </div>
    <div class="col-lg-1"></div>
</div>
