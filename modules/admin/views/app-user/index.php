<?php

use app\widgets\JavaScript;
use kartik\grid\ActionColumn;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\AppUser $model
 * @var app\models\search\AppUserSearch $searchModel
 */

$this->title = Yii::t('app', 'App Users');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="app-user-index">

    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Export') . ' ' . Yii::t('app', 'To CSV'), ['class' => 'btn btn-oziris', 'id' => 'btnExport']) ?>
                        <a class='btn btn-oziris hidden ' id="btnDownload">Download CSV</a>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="table-responsive text-center">
        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'export' => false,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'responsive' => true,
            'hover'=>true,
            'columns' => [
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'first_name',
                    'value' => function ($model) {
                            return Html::a($model->first_name, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'last_name',
                    'value' => function ($model) {
                        return Html::a($model->last_name, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'dob',
                    'value' => function ($model) {
                        return Html::a($model->dob, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'gender',
                    'value' => function ($model) {
                        return Html::a($model->gender, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'email_address',
                    'value' => function ($model) {
                        return Html::a($model->email_address, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'mobile',
                    'value' => function ($model) {
                        return Html::a($model->mobile, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'country',
                    'value' => function ($model) {
                        return Html::a($model->country, ['app-user/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'created_at',
                    'value' => function ($model) {
                        return $model->created_at;
                    },
                    'format' =>  ['date', 'php:d/m/Y h:i'],
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'deleted_at',
                    'label' => 'Status',
                      'value' => function ($model) {
                        if ( $model->deleted_at == null) {
                            return Html::a('<span class="glyphicon glyphicon-ok translated"></span>', ['app-user/change-user-status', 'id' => $model->id,],['title'=>Yii::t('app', 'Disable User'), 'data-confirm' => Yii::t('kvgrid', 'Are you sure to disable this user ?')]);
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['app-user/change-user-status', 'id' => $model->id,],['title'=>Yii::t('app', 'Enable User'), 'data-confirm' => Yii::t('kvgrid', 'Are you sure to enable this user ?')]);
                        }
                    },
                    'format' => 'raw',
                ],
                ['class' => ActionColumn::className(),
                    'template' => '{user-scans} &nbsp;&nbsp; {update}',
                    'buttons' => [
                        'user-scans' => function ($url, $model) {
                            $scannedProductSearch = [
                                'user_id' => $model->id
                            ];
                            return Html::a('<span class="glyphicon glyphicon-list-alt"></span> ', ['list-user-scans', 'ScannedProductSearch'=> $scannedProductSearch], ['title' => Yii::t('app', 'User Scans')]);
                        }
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>

</div>
<?php JavaScript::begin(); ?>
    <script>
        $('#btnExport').on('click', function (event) {
            searchField = {};
            length = $('thead tr td').children().length;
            for (i = 1; i <= length; i++) {
                element = $('thead tr td:nth-child(' + i + ') input').attr('name');
                console.log(element);

                key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
                searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
            }

            $.ajax({
                url: '<?= Url::to(['app-user/generate-csv']); ?>', // point to server-side PHP script
                data: searchField,
                type: 'post',
                success: function (php_script_response) {
                    if (php_script_response) {
                        $('#btnDownload').removeClass('hidden');
                    }
                    console.log(php_script_response); // display response from the PHP script, if any
                },
                error: function (php_script_response) {
                    console.log('error'); // display response from the PHP script, if any
                    console.log(php_script_response);  // display response from the PHP script, if any
                }
            });
        });
        $('#btnDownload').on('click', function (event) {
            $('#btnDownload').attr({target: '_blank', href: '<?= Url::to('@web/csv/appusers.csv'); ?>'});
        });

    </script>
<?php JavaScript::end(); ?>