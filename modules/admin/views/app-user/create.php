<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\AppUser $model
 */

$this->title = Yii::t('app', 'Create App User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="app-user-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
