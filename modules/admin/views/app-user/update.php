<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\AppUser $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'App User') . ' { ' . $model->first_name .' '.$model->last_name .' } ';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'App Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="app-user-update">

    <?= $this->render('@app/views/layouts/_menu', [
        'model' => $model,
    ]); ?>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
