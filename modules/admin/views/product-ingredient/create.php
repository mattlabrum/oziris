<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductIngredient $model
 */

$this->title = Yii::t('app', 'Create Product Ingredient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Ingredients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-ingredient-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
