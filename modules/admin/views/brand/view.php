<?php

use app\models\BrandTranslation;
use dmstr\bootstrap\Tabs;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\form\BrandForm $model;
 */

$this->title = Yii::t('app', 'Brand') . ' ' . $model->brand->brand_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->brand]) ?>
    <hr class="hr">

    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-2 text-center no-padding-right">
                <div style="padding: 10px">
                    <div class="row">
                        <label class="control-label">Brand Image</label>
                    </div>
                    <div class="row">
                        <img class="img-responsive img-thumbnail" src="<?= yii\helpers\Url::to('@web/uploads/brand').'/'.$model->getBrand()->brand_logo?>" alt="<?= $model->getBrand()->brand_logo?>">
                    </div>
                </div>

            </div>
            <div class="col-lg-8">
                <?php
                echo DetailView::widget([
                    'model' => $model->getBrand(),
                    'attributes' => [
                        [
                            'group' => true,
                            'label' => 'Brand Information',
                            'rowOptions' => ['class' => 'product-info-header']
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'format' => 'raw',
                                    'value' => '<kbd>' . $model->brand->id . '</kbd>',
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => true,
                                ],
                                [
                                    'attribute' => 'brand_name',
                                    'format' => 'raw',
                                    'value' => $model->brand->brand_name,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                                [
                                    'attribute' => 'manufacturer_id',
                                    'format' => 'raw',
                                    'value' => $model->brand->manufacturer->manufacturer_name,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'brand_location_latitude',
                                    'format' => 'raw',
                                    'value' => $model->brand->brand_location_latitude,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => false
                                ],
                                [
                                    'attribute' => 'brand_location_longitude',
                                    'format' => 'raw',
                                    'value' => $model->brand->brand_location_longitude,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],

                        [
                            'attribute' => 'brand_description',
                            'format' => 'raw',
                            'value' => '<span class="text-left"><em>' . $model->brand->brand_description . '</em></span>',
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'brand_video',
                                    'format' => 'raw',
                                    'value' => $model->brand->brand_video,
                                    'labelColOptions' => ['style' => 'width:20%'],
                                    'valueColOptions' => ['style' => 'width:80%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                    ],
                    'mode' => 'view',
                    'bordered' => false,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'fadeDelay' => 200,
                    'class'=> 'text-left',
                ]);
                ?>
            </div>
            <div class="col-lg-1"></div>

        </div>

        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-2 text-center no-padding-right">
            </div>
            <div class="col-lg-8">
                <?php
                $translations = BrandTranslation::findAll(['brand_id' => $model->brand->id]);
                $attributes = [];

                foreach($translations as $translation){

                    $row = [
                        'group' => true,
                        'label' => 'Brand Translation '.$translation->language->name,
                        'rowOptions' => ['class' => 'product-info-header']
                    ];
                    array_push($attributes,$row);


                    $column= [
                        'attribute' => 'brand_name',
                        'format' => 'raw',
                        'value' => $translation->brand_name,
                        'labelColOptions' => ['style' => 'width:20%'],
                        'valueColOptions' => ['style' => 'width:80%'],
                        'displayOnly' => false
                    ];
                    array_push($attributes,$column);
                    $column= [
                        'attribute' => 'brand_description',
                        'format' => 'raw',
                        'value' => $translation->brand_description,
                        'labelColOptions' => ['style' => 'width:15%'],
                        'valueColOptions' => ['style' => 'width:20%'],
                        'displayOnly' => false
                    ];
                    array_push($attributes,$column);
                }

                if(count($translations)>0){
                    echo DetailView::widget([
                        'model' => $model->getBrandTranslation(),
                        'attributes' => $attributes,
                        'mode' => 'view',
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hAlign' => 'left',
                        'vAlign' => 'middle',
                        'fadeDelay' => 200,
                        'container' => ['id' => 'translations'],
                    ]);

                }
                ?>

            </div>
            <div class="col-lg-1"></div>
       </div>
    </div>
</div>




        <?php
        echo $this->render('../audit-trail/AuditGrid', [
            'params' => [
                'search_string' => $_GET,
                'query' => $model->brand->getAuditTrails(),
                'filter' => ['brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_description', 'manufacturer_id', 'brand_logo', 'brand_video']
            ],
//
            // which columns to show
            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
            // set to false to hide filter
        ]);
        ?>
