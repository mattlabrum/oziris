<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\BrandForm $model
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div class="brand-form">
    <?php $form = ActiveForm::begin([
        'id' => 'Brand',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-4',
                'wrapper' => 'col-lg-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'enableClientValidation' => false,
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
            <h1>Brand</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row" style="padding-top: 20px">
                <div class="col-lg-5">
                    <?= $form->field($model->brand, 'brand_name')->textInput(['maxlength' => true]) ?>
                    <?=
                    $form->field($model->brand, 'manufacturer_id')->widget(\kartik\select2\Select2::classname(), [
                        'name' => 'class_name',
                        'model' => $model,
                        'attribute' => 'manufacturer_id',
                        'data' => \yii\helpers\ArrayHelper::map(app\models\Manufacturer::findAll(['deleted_at' => null]), 'id', 'manufacturer_name'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Type to autocomplete'),
                            'multiple' => false,
                        ]
                    ]); ?>
                    <?= $form->field($model->brand, 'brand_location_latitude')->textInput() ?>
                    <?= $form->field($model->brand, 'brand_location_longitude')->textInput() ?>
                    <?= $form->field($model->brand, 'brand_video')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-7">
                    <?= $form->field($model->brand, 'brand_description')->textarea(array('rows' => 7)); ?>
                    <?= $form->field($model->brand, 'brand_logo')->fileInput(['accept' => 'image/*']); ?>
                </div>

            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <h1>Brand Translation</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>
        <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row" style="padding-top: 20px">
                <div class="col-lg-5">
                    <?=
                    $form->field($model->brandtranslation, 'language_id')->widget(\kartik\select2\Select2::classname(), [
                        'name' => 'class_name',
                        'model' => $model,
                        'attribute' => 'language_id',
                        'data' => \yii\helpers\ArrayHelper::map(app\models\Language::find()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Type to autocomplete'),
                            'multiple' => false,
                        ]
                    ]); ?>
                    <?= $form->field($model->brandtranslation, 'brand_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-7">
                    <?= $form->field($model->brandtranslation, 'brand_description')->textarea(array('rows' => 6)); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-check"></span> ' . ($model->brand->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if ($model->brand->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
