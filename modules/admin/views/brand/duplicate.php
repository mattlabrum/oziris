<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\BrandForm $model
 */

$this->title = Yii::t('app', 'Duplicate') . ' ' . Yii::t('app', 'Brand') . ' ' . $model->brand->brand_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->brand->brand_name, 'url' => ['view', 'id' => $model->brand->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Duplicate');
?>
<div class="brand-duplicate">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->brand,]); ?>
    <?php echo $this->render('_form', ['model' => $model,]); ?>

</div>
