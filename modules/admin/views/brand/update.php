<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\BrandForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Brand') . ' ' . $model->brand->brand_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Brands'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->brand->id, 'url' => ['view', 'id' => $model->brand->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="brand-update">

    <?= $this->render('@app/views/layouts/_menu', [
        'model' => $model->brand,
    ]); ?>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
