<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ReviewRating $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Review Rating') . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Review Ratings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="review-rating-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]); ?>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
