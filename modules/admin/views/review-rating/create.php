<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ReviewRating $model
 */

$this->title = Yii::t('app', 'Create Review Rating');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Review Ratings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="review-rating-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
