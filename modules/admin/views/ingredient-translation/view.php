<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\IngredientTranslation $model
 */

$this->title = Yii::t('app', 'Ingredient Translation') . ' ' . $model->name;
$this->params['heading'] = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="ingredient-translation-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\IngredientTranslation'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'language_id',
            'ingredient_id',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> IngredientTranslation',
                'content' => $this->blocks['app\models\IngredientTranslation'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
