<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\IngredientTranslation $model
 */

$this->title = Yii::t('app', 'Create Ingredient Translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ingredient-translation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
