<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;
use yii\grid\GridView;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Language $model
 * @var app\models\search\LanguageSearch $searchModel
 */

$this->title = Yii::t('app', 'Languages');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="language-index">

    <div class="clearfix">

        <p class="pull-left">
            <?= Html::a('<span class="fa fa-plus"></span> ' . Yii::t('app', 'Create') . ' ' . Yii::t('app', 'Language'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-success']) ?>
            <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Search') . ' ' . Yii::t('app', 'Languages'), ['class' => 'btn btn-info', 'data-toggle' => 'modal', 'data-target' => '#language-searchModal']) ?>
        </p>

        <div class="pull-right">

            <?=
            ButtonDropdown::widget([
                'id' => 'giiant-relations',
                'encodeLabel' => false,
                'label' => '<span class="fa fa-paperclip"></span> ' . Yii::t('app', 'Relations'),
                'dropdown' => [
                    'options' => [
                        'class' => 'dropdown-menu-right'
                    ],
                    'encodeLabels' => false,
                    'items' => [
                        [
                            'label' => '<i class="fa fa-arrow-right"></i> Brand Translation',
                            'url' => [
                                'brand-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-right"></i> Hash Tags Translation',
                            'url' => [
                                'hash-tags-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-right"></i> Manufacturer Translation',
                            'url' => [
                                'manufacturer-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-left"></i> Product Description Translation',
                            'url' => [
                                'product-description-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-left"></i> Product Group Translation',
                            'url' => [
                                'product-group-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-right"></i> Product Name Translation',
                            'url' => [
                                'product-name-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-right"></i> Product Translation',
                            'url' => [
                                'product-translation/index',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-arrow-right"></i> Product Type Translation',
                            'url' => [
                                'product-type-translation/index',
                            ],
                        ],
                    ],
                ],
            ]);
            ?>

        </div>

    </div>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="table-responsive">
        <?= GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\ActionColumn',
                    'urlCreator' => function ($action, $model, $key, $index) {
                        // using the column name as key, not mapping to 'id' like the standard generator
                        $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string)$key];
                        $params[0] = Yii::$app->controller->id ? Yii::$app->controller->id . '/' . $action : $action;
                        $params['ru'] = ReturnUrl::getToken();
                        return Url::toRoute($params);
                    },
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
                'id',
                'name',
                'logo',
                'code',
            ],
        ]); ?>
    </div>

</div>