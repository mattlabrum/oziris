<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ReportedReview $model
 */

$this->title = Yii::t('app', 'Create Reported Review');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Reported Reviews'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reported-review-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
