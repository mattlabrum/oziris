<?php
/**
 * /vagrant/code/OzirisBackend/src/../runtime/giiant/fcd70a9bfdf8de75128d795dfc948a74
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var app\models\AppSetting $model
 */
$this->title = Yii::t('models', 'App Setting') . " " . $model->id . ', ' . Yii::t('cruds', 'Edit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'App Setting'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'Edit');
?>
<div class="giiant-crud app-setting-update">

    <h1>
        <?php echo Yii::t('models', 'Oziris Settings') ?>
    </h1>

    <hr />

    <?php echo $this->render('_form', [
		'model' => $model,
	]); ?>

</div>
