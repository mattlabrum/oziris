<?php
/**
 * /vagrant/code/OzirisBackend/src/../runtime/giiant/eeda5c365686c9888dbc13dbc58f89a1
 *
 * @package default
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 *
 * @var yii\web\View $this
 * @var app\models\search\AppSettingsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="app-setting-search">

    <?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

    		<?php echo $form->field($model, 'id') ?>

		<?php echo $form->field($model, 'promo_image') ?>

		<?php echo $form->field($model, 'promo_url') ?>

		<?php echo $form->field($model, 'promo_product_id') ?>

		<?php echo $form->field($model, 'promo_search_string') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('cruds', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('cruds', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
