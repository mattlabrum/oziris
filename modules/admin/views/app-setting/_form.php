<?php
/**
 * /vagrant/code/OzirisBackend/src/../runtime/giiant/4b7e79a8340461fe629a6ac612644d03
 *
 * @package default
 */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
 *
 * @var yii\web\View $this
 * @var app\models\AppSetting $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="app-setting-form">

    <?php $form = ActiveForm::begin([
		'id' => 'AppSetting',
		'layout' => 'horizontal',
		'enableClientValidation' => true,
		'errorSummaryCssClass' => 'error-summary alert alert-danger',
        'options' => ['enctype'=>'multipart/form-data']
	]
);
?>

    <div class="">
        <p>


<!-- attribute promo_image -->
			<?= $form->field($model, 'promo_image')->fileInput(['accept' => 'image/*']);?>

<!-- attribute promo_url -->
			<?php echo $form->field($model, 'promo_url')->textInput(['maxlength' => true]) ?>

<!-- attribute promo_product_id -->

            <?= // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::activeField
            $form->field($model, 'promo_product_id')->widget(\kartik\select2\Select2::classname(), [
                'name' => 'class_name',
//                        'id' => 'product_id',
                'class' => 'over',
                'model' => $model,
                'attribute' => 'promo_product_id',
                'data' => \yii\helpers\ArrayHelper::map(app\models\Product::findAll(['deleted_at' => null]), 'id', 'product_name'),
                'options' => [
                    'placeholder' => Yii::t('app', 'Type to autocomplete'),
                    'multiple' => false,
                ],
            ]); ?>




<!-- attribute promo_search_string -->
			<?php echo $form->field($model, 'promo_search_string')->textInput(['maxlength' => true]) ?>
        </p>


        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?php echo Html::submitButton(
	'<span class="glyphicon glyphicon-check"></span> ' .
	($model->isNewRecord ? Yii::t('cruds', 'Create') : Yii::t('cruds', 'Save')),
	[
		'id' => 'save-' . $model->formName(),
		'class' => 'btn btn-success'
	]
);
?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
