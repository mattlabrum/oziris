<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/d4b4964a63cc95065fa0ae19074007ee
 *
 * @package default
 */


use kartik\detail\DetailView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;

/**
 *
 * @var yii\web\View $this
 * @var app\models\Coupon $model
 */
$copyParams = $model->attributes;

$this->title = Yii::t('models', 'Coupon');
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Coupons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => (string)$model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('cruds', 'View');
?>




<div>
    <div class="row">

        <div class="clearfix crud-navigation">
            <?= $this->render('@app/views/layouts/_menu', ['model' => $model]) ?>
        </div>

        <hr class="hr">

        <div class="col-lg-1 no-padding-right">

        </div>
        <div class="col-lg-10">

            <div class="giiant-crud coupon-view">

                <!-- flash message -->
                <?php if (\Yii::$app->session->getFlash('deleteError') !== null) : ?>
                    <span class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                        <?php echo \Yii::$app->session->getFlash('deleteError') ?>
        </span>
                <?php endif; ?>





                <?php $this->beginBlock('app\models\Coupon'); ?>


                <?php echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'code',
                        'date_created',
                        'date_modified',
                        'discount_type',
                        'description',
                        'amount',
                        'expiry_date',
                        'individual_use',
                        'usage_limit',
                        'usage_limit_per_user',
                        'limit_usage_to_x_items',
                        'free_shipping',
                        'exclude_sale_items',
                        'minimum_amount',
                        'maximum_amount',
                        'email_restrictions:ntext',
                    ],
                ]); ?>


                <hr/>

                <?php $this->endBlock(); ?>



                <?php $this->beginBlock('CouponProducts'); ?>
                <?php Pjax::begin(['id'=>'pjax-CouponProducts', 'enableReplaceState'=> false, 'linkSelector'=>'#pjax-CouponProducts ul.pagination a, th a', 'clientOptions' => ['pjax:success'=>'function(){alert("yo")}']]) ?>
                <?php echo
                    '<div class="table-responsive">'
                    . \yii\grid\GridView::widget([
                        'layout' => '{summary}{pager}<br/>{items}{pager}',
                        'dataProvider' => new \yii\data\ActiveDataProvider([
                            'query' => $model->getCouponProducts(),
                            'pagination' => [
                                'pageSize' => 20,
                                'pageParam'=>'page-couponproducts',
                            ]
                        ]),
                        'pager'        => [
                            'class'          => yii\widgets\LinkPager::className(),
                            'firstPageLabel' => Yii::t('cruds', 'First'),
                            'lastPageLabel'  => Yii::t('cruds', 'Last')
                        ],
                        'columns' => [
                            [
                                'class'      => 'yii\grid\ActionColumn',
                                'template'   => '{view} {update}',
                                'contentOptions' => ['nowrap'=>'nowrap'],
                                'urlCreator' => function ($action, $model, $key, $index) {
                                    // using the column name as key, not mapping to 'id' like the standard generator
                                    $params = is_array($key) ? $key : [$model->primaryKey()[0] => (string) $key];
                                    $params[0] = 'coupon-product' . '/' . $action;
                                    $params['CouponProduct'] = ['coupon_id' => $model->primaryKey()[0]];
                                    return $params;
                                },
                                'buttons'    => [

                                ],
                                'controller' => 'coupon-product'
                            ],
                            'id',
                            'product_id',
                        ]
                    ])
                    . '</div>'
                ?>
                <?php Pjax::end() ?>
                <?php $this->endBlock() ?>


                <?php echo Tabs::widget(
                    [
                        'id' => 'relation-tabs',
                        'encodeLabels' => false,
                        'items' => [
                            [
                                'label'   => '<b class=""># '.$model->code.'</b>',
                                'content' => $this->blocks['app\models\Coupon'],
                                'active'  => true,
                            ],
                            [
                                'content' => $this->blocks['CouponProducts'],
                                'label'   => '<small>Coupon Products <span class="badge badge-default">'.count($model->getCouponProducts()->asArray()->all()).'</span></small>',
                                'active'  => false,
                            ],
                        ]
                    ]
                );
                ?>
            </div>

        </div>
        <div class="col-lg-1"></div>
    </div>
</div>









<!---->
<?php
//echo $this->render('../audit-trail/AuditGrid', [
//    'params' => [
//        'search_string' => $_GET,
//        'query' => $model->brand->getAuditTrails(),
//        'filter' => ['brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_description', 'manufacturer_id', 'brand_logo', 'brand_video']
//    ],
////
//    // which columns to show
//    'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
//    // set to false to hide filter
//]);
//?>
