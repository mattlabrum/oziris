<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/eeda5c365686c9888dbc13dbc58f89a1
 *
 * @package default
 */


use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 *
 * @var yii\web\View $this
 * @var app\models\search\CouponsSearch $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="coupon-search">

    <?php $form = ActiveForm::begin([
		'action' => ['index'],
		'method' => 'get',
	]); ?>

    		<?php echo $form->field($model, 'id') ?>

		<?php echo $form->field($model, 'code') ?>

		<?php echo $form->field($model, 'date_created') ?>

		<?php echo $form->field($model, 'date_modified') ?>

		<?php echo $form->field($model, 'discount_type') ?>

		<?php // echo $form->field($model, 'description') ?>

		<?php // echo $form->field($model, 'amount') ?>

		<?php // echo $form->field($model, 'expiry_date') ?>

		<?php // echo $form->field($model, 'individual_use') ?>

		<?php // echo $form->field($model, 'usage_limit') ?>

		<?php // echo $form->field($model, 'usage_limit_per_user') ?>

		<?php // echo $form->field($model, 'limit_usage_to_x_items') ?>

		<?php // echo $form->field($model, 'free_shipping') ?>

		<?php // echo $form->field($model, 'exclude_sale_items') ?>

		<?php // echo $form->field($model, 'minimum_amount') ?>

		<?php // echo $form->field($model, 'maximum_amount') ?>

		<?php // echo $form->field($model, 'email_restrictions') ?>

    <div class="form-group">
        <?php echo Html::submitButton(Yii::t('cruds', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton(Yii::t('cruds', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
