<?php

use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;
use zhuravljov\widgets\DatePicker;

/**
 *
 * @var yii\web\View $this
 * @var app\models\Coupon $model
 * @var yii\widgets\ActiveForm $form
 */
?>


<?php $form = ActiveForm::begin([
    'id' => 'Coupon',
    'layout' => 'horizontal',
    'enableClientValidation' => false,
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-lg-4',
            'wrapper' => 'col-lg-8',
            'error' => '',
            'hint' => '',
        ],
    ]
]); ?>



<div class="coupon-form">

    <?php echo $form->errorSummary($model); ?>
    <div class="row">

        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row center-block" style="padding-top: 20px ">
                <div class="col-lg-10">

                    <div class="row">
                        <div class="col-md-4 text-center">
                            <?php echo $form->field($model, 'free_shipping')->checkbox() ?>
                        </div>

                        <div class="col-md-4 text-center">
                            <?php echo $form->field($model, 'individual_use')->checkbox() ?>
                        </div>

                        <div class="col-md-4 text-center">
                            <?php echo $form->field($model, 'exclude_sale_items')->checkbox() ?>
                        </div>

                    </div>

                    <?php echo $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

                    <?php echo $form->field($model, 'discount_type')->widget(Select2::classname(), [
                        'data' => [
                            "fixed_cart" => "Cart Discount",
                            "percent" => "Cart % Discount",
                            "fixed_product" => "Product Discount",
                            "percent_product" => "Product % Discount",
                            "sign_up_fee" => "Sign Up Fee Discount",
                            "sign_up_fee_percent" => "Sign Up Fee % Discount",
                            "recurring_fee" => "Recurring Product Discount",
                            "recurring_percent" => "Recurring Product % Discount",
                        ],
                        'options' => ['placeholder' => 'Select a discount type ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]); ?>
                    <?php echo $form->field($model, 'description')->textarea(['rows' => 6]) ?>
                    <?php echo $form->field($model, 'amount')->textInput() ?>
                    <?= $form->field($model, 'expiry_date')->widget(DateTimePicker::className(),[
                        'type' => DateTimePicker::TYPE_INPUT,
                        'pluginOptions' => [
                            'autoclose'=>true,
                            'format' => 'dd-M-yyyy'
                        ]
                    ]) ?>

                    <?php echo $form->field($model, 'couponProducts')->widget(\kartik\select2\Select2::classname(), [
                        'attribute' => 'couponProducts',
                        'data' => \yii\helpers\ArrayHelper::map(app\models\Product::find()->where(['deleted_at' => null])->all(), 'id', 'product_name'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Type to autocomplete'),
                            'multiple' => true,
                        ],
                        'pluginOptions' => [
                            'tags' => true,
                        ],
                    ]); ?>

                    <?php echo $form->field($model, 'usage_limit')->textInput([
                            'title' => 'How many times the coupon can be used.',
                            'data-toggle' => 'tooltip',
                            'data-placement' => 'right']
                    ) ?>
                    <?php echo $form->field($model, 'usage_limit_per_user')->textInput() ?>
                    <?php echo $form->field($model, 'limit_usage_to_x_items')->textInput() ?>


                    <?php echo $form->field($model, 'minimum_amount')->textInput() ?>
                    <?php echo $form->field($model, 'maximum_amount')->textInput() ?>
                    <?php echo $form->field($model, 'email_restrictions')->textarea(['rows' => 6]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-check"></span> ' . ($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if ($model->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), 'index', ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <?php ActiveForm::end(); ?>

</div>