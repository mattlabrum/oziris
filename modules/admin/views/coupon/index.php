<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/a0a12d1bd32eaeeb8b2cff56d511aa22
 *
 * @package default
 */


use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;

/**
 *
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\CouponsSearch $searchModel
 */
$this->title = Yii::t('models', 'Coupons');
$this->params['breadcrumbs'][] = $this->title;

if (isset($actionColumnTemplates)) {
	$actionColumnTemplate = implode(' ', $actionColumnTemplates);
	$actionColumnTemplateString = $actionColumnTemplate;
} else {
	Yii::$app->view->params['pageButtons'] = Html::a('<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('cruds', 'New'), ['create'], ['class' => 'btn btn-success']);
	$actionColumnTemplateString = "{view} {update} {delete}";
}
$actionColumnTemplateString = '<div class="action-buttons">'.$actionColumnTemplateString.'</div>';
?>







<div class="brand-index">

    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a('<span class="fa fa-plus"></span> ' . Yii::t('app', 'Create') . ' ' . Yii::t('app', 'Coupon'), ['create'], ['class' => 'btn btn-oziris']) ?>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="text-center">
        <?= \kartik\grid\GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'responsive' => true,
            'hover'=>true,
            'export' => false,
            'columns' => [
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'id',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->id, ['coupon/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'code',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->code, ['coupon/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'discount_type',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->discount_type, ['coupon/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'description',
                    'value' => function ($model) {
                        return Html::a($model->description, ['coupon/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'amount',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->amount, ['coupon/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} &nbsp;&nbsp; {delete}',
                    'buttons' => [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-duplicate"></span> ', ['duplicate', 'id' => $model->id], ['title' => Yii::t('app', 'Duplicate'),]);
                        }
                    ],
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>
</div>