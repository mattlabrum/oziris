<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/fccccf4deb34aed738291a9c38e87215
 *
 * @package default
 */


use yii\helpers\Html;

/**
 *
 * @var yii\web\View $this
 * @var app\models\Coupon $model
 */
$this->title = Yii::t('cruds', 'Create Coupon');
$this->params['breadcrumbs'][] = ['label' => Yii::t('models', 'Coupons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


?>
<div class="coupon-create">
    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]) ?>
    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>