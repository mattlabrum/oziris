<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductNature $model
 */

$this->title = Yii::t('app', 'Product Nature') . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Natures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-nature-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\ProductNature'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> ProductNature',
                'content' => $this->blocks['app\models\ProductNature'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
