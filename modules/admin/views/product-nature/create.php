<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductNature $model
 */

$this->title = Yii::t('app', 'Create Product Nature');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Natures'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-nature-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
