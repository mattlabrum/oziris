<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/4b7e79a8340461fe629a6ac612644d03
 *
 * @package default
 */


use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use yii\helpers\StringHelper;

/**
 *
 * @var yii\web\View $this
 * @var app\models\OrderCoupon $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="order-coupon-form">

    <?php $form = ActiveForm::begin([
		'id' => 'OrderCoupon',
		'layout' => 'horizontal',
		'enableClientValidation' => true,
		'errorSummaryCssClass' => 'error-summary alert alert-danger'
	]
);
?>

    <div class="">
        <?php $this->beginBlock('main'); ?>

        <p>


<!-- attribute id -->
			<?php echo $form->field($model, 'id')->textInput() ?>

<!-- attribute order_id -->
			<?php echo $form->field($model, 'order_id')->textInput(['maxlength' => true]) ?>

<!-- attribute discount_amount -->
			<?php echo $form->field($model, 'discount_amount')->textInput() ?>

<!-- attribute code -->
			<?php echo $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </p>
        <?php $this->endBlock(); ?>

        <?php echo
Tabs::widget(
	[
		'encodeLabels' => false,
		'items' => [
			[
				'label'   => Yii::t('models', 'OrderCoupon'),
				'content' => $this->blocks['main'],
				'active'  => true,
			],
		]
	]
);
?>
        <hr/>

        <?php echo $form->errorSummary($model); ?>

        <?php echo Html::submitButton(
	'<span class="glyphicon glyphicon-check"></span> ' .
	($model->isNewRecord ? Yii::t('cruds', 'Create') : Yii::t('cruds', 'Save')),
	[
		'id' => 'save-' . $model->formName(),
		'class' => 'btn btn-success'
	]
);
?>

        <?php ActiveForm::end(); ?>

    </div>

</div>
