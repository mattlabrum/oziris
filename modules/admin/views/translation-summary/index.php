<?php

use app\widgets\JavaScript;
use kartik\grid\DataColumn;
use kartik\grid\GridView;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Product $model
 * @var app\models\search\ProductSearch $searchModel
 */

$this->title = Yii::t('app', 'Translation Summary');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="product-index">
    <div class="clearfix">
        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="text-center table-responsive" style="width: 80%">
        <?=
        GridView::widget([
            'id' => 'translation-summary',
            'dataProvider' => $dataProvider,
            'responsive' => true,
            'hover' => true,
            'export' => false,
            'filterModel' => $searchModel,
            'columns' => [
                // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::columnFormat
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'id',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->id, ['product/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],

                [
                'class' => DataColumn::className(),
                'attribute' => 'product_number',
                'width' => '100px',
                'value' => function ($model) {
                    if ($rel = $model->getId()->one()) {
                        return Html::a($rel->product_number, ['product/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                    } else {
                        return '';
                    }
                },
                'format' => 'raw',
            ],
                // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::columnFormat
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'product_group_id',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ( $rel = \app\models\ProductGroupTranslation::findOne(['product_group_id' => $model->productGroup->id])) {
                            return '<span class="glyphicon glyphicon-ok" style="color: #248556"></span>';
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove" style="color: #aa4543"></span>', ['product-group/update', 'id' => $model->productGroup->id,]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'product_name',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ( $rel = \app\models\ProductTranslation::findOne(['product_id' => $model->id])) {
                            return '<span class="glyphicon glyphicon-ok translated"></span>';
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['product/update', 'id' => $model->id,]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'product_description_id',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ( $rel = \app\models\ProductTranslation::findOne(['product_id' => $model->id])) {
                            return '<span class="glyphicon glyphicon-ok translated"></span>';
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['product/update', 'id' => $model->id,]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'brand_id',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ( $rel = \app\models\BrandTranslation::findOne(['brand_id' => $model->brand->id])) {
                            return '<span class="glyphicon glyphicon-ok translated"></span>';
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['brand/update', 'id' => $model->brand->id,]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'manufacturer_id',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ( $rel = \app\models\ManufacturerTranslation::findOne(['manufacturer_id' => $model->manufacturer->id])) {
                            return '<span class="glyphicon glyphicon-ok translated"></span>';
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['manufacturer/update', 'id' => $model->manufacturer->id,]);
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'nutrition_text',
                    'width' => '100px',
                    'value' => function ($model) {
                        if ( $rel = \app\models\ProductTranslation::findOne(['product_id' => $model->id])) {
                            return '<span class="glyphicon glyphicon-ok translated"></span>';
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['product/update', 'id' => $model->id,]);
                        }
                    },
                    'format' => 'raw',
                ],
            ],
        ]);


        ?>
        Products Per Page :
        <select onchange="changePagination()">
            <option value="10">Show 10</option>
            <option value="20">Show 20</option>
            <option value="30">Show 30</option>
            <option value="40">Show 40</option>
        </select>

    </div>
</div>


<?php JavaScript::begin(); ?>
    <script>
        function changePagination(){

        }
    </script>
<?php JavaScript::end(); ?>