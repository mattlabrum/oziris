<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\NutritionTagTranslation $model
 */

$this->title = Yii::t('app', 'Create Nutrition Tag Translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutrition Tag Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutrition-tag-translation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
