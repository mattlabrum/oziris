<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\NutritionTagTranslation $model
 */

$this->title = Yii::t('app', 'Nutrition Tag Translation') . ' ' . $model->name;
$this->params['heading'] = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutrition Tag Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="nutrition-tag-translation-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\NutritionTagTranslation'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'language_id',
            'deleted_at',
            'nutrition_tag_id',
            'name',
            'created_at',
            'updated_at',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> NutritionTagTranslation',
                'content' => $this->blocks['app\models\NutritionTagTranslation'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
