<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\NutritionTranslation $model
 */

$this->title = Yii::t('app', 'Nutrition Translation') . ' ' . $model->name;
$this->params['heading'] = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutrition Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->params['heading'];
?>
<div class="nutrition-translation-view">

    <?= $this->render('_menu', compact('model')); ?>
    <?php $this->beginBlock('app\models\NutritionTranslation'); ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'language_id',
            'deleted_at',
            'nutrition_id',
            'name',
            'created_at',
            'updated_at',
        ],
    ]); ?>

    <?php $this->endBlock(); ?>

    <?= Tabs::widget([
        'id' => 'relation-tabs',
        'encodeLabels' => false,
        'items' => [
            [
                'label' => '<span class="fa fa-asterisk"></span> NutritionTranslation',
                'content' => $this->blocks['app\models\NutritionTranslation'],
                'active' => true,
            ],
        ]
    ]);
    ?>

</div>
