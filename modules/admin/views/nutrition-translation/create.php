<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\NutritionTranslation $model
 */

$this->title = Yii::t('app', 'Create Nutrition Translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Nutrition Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nutrition-translation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
