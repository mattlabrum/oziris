<?php

/**
 * @var yii\web\View $this
 * @var app\models\ProductIngredient[] $model
 * @var yii\bootstrap\ActiveForm $form
 */
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use Symfony\Component\Console\Input\Input;

?>

<div class="text-center" id="product-ingredient-div">
    <div class="row">
            <h1 class="pull-left">Ingredients</h1>
    </div>
    <div class="row">
        <table class="kv-grid-table table table-bordered table-striped kv-table-wrap">
            <thead>
            <tr>
                <th colspan="2">Ingredients</th>
            </tr>
        <tr>
                <th>Ingredient Name</th>
                <th>Ingredient Group</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($model as $index => $ingredient) { ?>
                <tr>
                    <td style="width: 30%"><label class="control-label"><?= $ingredient->ingredient->name ?></label></td>
                    <td style="width: 30%"><label class="control-label"><?= $ingredient->ingredient->group->name?></label></td>
                </tr>
            <?php } ?>

            </tbody>
        </table>
    </div>
</div>

