<?php

/**
 * @var yii\web\View $this
 * @var app\models\BatchIngredient[] $model
 * @var yii\bootstrap\ActiveForm $form
 */
use app\widgets\JavaScript;
use kartik\datetime\DateTimePicker;
use kartik\helpers\Html;
use kartik\select2\Select2;

?>

<div class="batch-ingredient-form">

    <?= Html::hiddenInput('no_of_ingredients', count($model)); ?>
    <div class="row text-center" style="padding-bottom: 10px; padding-right: 50px">

        <div class="col-lg-1">
            <label class="control-label" style="text-align: center">Ingredient</label>
        </div>
        <div class="col-lg-1 text-center">
            <label class="control-label" style="text-align: center">Ingredient Group</label>
        </div>
        <div class="col-lg-2">
            <label class="control-label" style="text-align: center">Arrival at Manufacturer Date</label>
        </div>
        <div class="col-lg-1">
            <label class="control-label" style="text-align: center">%</label>
        </div>
        <div class="col-lg-2">
            <label class="control-label" style="text-align: center">CoO</label>
        </div>
        <div class="col-lg-2">
            <label class="control-label" style="text-align: center">Supplier</label>
        </div>
        <div class="col-lg-2">
            <label class="control-label" style="text-align: center">Manufacturing Date</label>
        </div>
        <div class="col-lg-1">
            <label class="control-label" style="text-align: center">Show Supplier on OZIRIS</label>
        </div>


    </div>

    <?php
    foreach ($model as $index => $ing) { ?>
        <div class="row text-center" style="padding-right: 50px">
            <div class="col-lg-1">
                <label class="control-label"><?= $ing->ingredient->name?></label>
                <?= Html::hiddenInput("ingredient_id-$index", $ing->ingredient->id); ?>
            </div>
            <div class="col-lg-1">
                <label class="control-label"><?= $ing->ingredient->group->name?></label>
            </div>
            <div class="col-lg-2">
                <?= DateTimePicker::widget([
                    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                    'id' => "arrival-date$index",
                    'name' => "arrival_at_manufacturer_date-$index",
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'format' => 'mm/dd/yyyy hh:ii',
                        'minuteStep' => 1,
                    ],
                ]) ?>
            </div>
            <div class="col-lg-1">
                <?= Html::input('text','percentage-'.$index,$ing->percentage,['style' => ['width' => '100px', 'text-align' => 'center']]); ?>
            </div>
            <div class="col-lg-2">
                <?=
                Select2::widget([
                    'id' => "coo_id-$index",
                    'name' => "coo_id-$index",
                    'data' => \yii\helpers\ArrayHelper::map(app\models\Coo::find()->all(), 'id', 'country'),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Type to autocomplete'),
                        'multiple' => false,
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-2">
                <?=
                Select2::widget([
                    'id' => "supplier_id-$index",
                    'name' => "supplier_id-$index",
                    'data' => \yii\helpers\ArrayHelper::map(app\models\Supplier::find()->all(), 'id', 'supplier_name'),
                    'options' => [
                        'placeholder' => Yii::t('app', 'Type to autocomplete'),
                        'multiple' => false,
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-2">
                <?= DateTimePicker::widget([
                    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                    'id' => "manufacturing-date$index",
                    'name' => "ingredient_date-$index",
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        'format' => 'mm/dd/yyyy hh:ii',
                        'minuteStep' => 1,
                    ],
                ]) ?>
            </div>
            <div class="col-lg-1">
                <?php
                    $options = [];
                if($ing->show_on_oziris != 0){
                    $options = [
                        'checked' => 1
                    ];
                }
                    echo Html::input('checkbox','show_on_oziris-'.$index,$ing->show_on_oziris , $options);
                ?>
            </div>
        </div>
    <?php } ?>


</div>

<?php JavaScript::begin(); ?>
    <script>
        <?php foreach ($model as $index => $ing) {?>
        $('#supplier_id-<?= $index ?>').val("<?= $ing->supplier_id ?>").change();
        $('#coo_id-<?= $index ?>').val("<?= $ing->coo_id ?>").change();

//        arrival_at_manufacturer_date

        $('#manufacturing-date<?= $index ?>').val("<?= $ing->manufacture_date ?>");
        $('#arrival-date<?= $index ?>').val("<?= $ing->arrival_at_manufacturer_date ?>");



        <?php } ?>
    </script>
<?php JavaScript::end(); ?>