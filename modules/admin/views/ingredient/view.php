<?php

/**
 * @var yii\web\View $this
 * @var app\models\form\IngredientForm $model
 */

use kartik\detail\DetailView;
    $this->title = Yii::t('app', 'Ingredient') . ' - ' . $model->getIngredient()->name;
?>
<div class="ingredient-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->getIngredient()]) ?>
    <hr class="hr">

    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-10">
                <?php
                echo DetailView::widget([
                    'model' => $model->getIngredient(),
                    'attributes' => [
                        [
                            'group' => true,
                            'label' => 'Ingredients',
                            'rowOptions' => ['class' => 'product-info-header']
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'format' => 'raw',
                                    'value' => $model->getIngredient()->id,
                                    'labelColOptions' => ['style' => 'width:30%'],
                                    'valueColOptions' => ['style' => 'width:70%'],
                                    'displayOnly' => false
                                ],
                            ]
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'format' => 'raw',
                                    'label' => 'Ingredient Name',
                                    'value' => $model->getIngredient()->name,
                                    'labelColOptions' => ['style' => 'width:30%'],
                                    'valueColOptions' => ['style' => 'width:70%'],
                                    'displayOnly' => false
                                ],
                            ]
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'name',
                                    'format' => 'raw',
                                    'label' => 'Ingredient Group',
                                    'value' => $model->getIngredient()->group->name,
                                    'labelColOptions' => ['style' => 'width:30%'],
                                    'valueColOptions' => ['style' => 'width:70%'],
                                    'displayOnly' => false
                                ],
                            ]
                        ],

                    ],
                    'mode' => 'view',
                    'bordered' => false,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'fadeDelay' => 200,
                    'class'=> 'text-left',
                ]);
                ?>
                <?= DetailView::widget([
                    'model' => $model->getIngredientTranslation(),
                    'attributes' => [
                        'name',
                    ],
                ]);
                ?>
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
</div>
