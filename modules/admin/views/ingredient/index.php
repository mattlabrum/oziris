<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ButtonDropdown;
use yii\grid\GridView;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\search\IngredientSearch $searchModel
 */

$this->title = Yii::t('app', 'Ingredients');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="ingredient-index">

    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a('<span class="fa fa-plus"></span> ' . Yii::t('app', 'Create') . ' ' . Yii::t('app', 'Ingredient'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-oziris']) ?>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="text-center">
        <?= \kartik\grid\GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'responsive' => true,
            'hover'=>true,
            'export' => false,
            'columns' => [
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'id',
                    'value' => function ($model) {
                        return Html::a($model->id, ['ingredient/view', 'id' => $model->id,], ['data-pjax' => 0]);
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'group_id',
                    'label' => 'Ingredient Group',
                    'value' => function ($model) {
                        return Html::a($model->group->name, ['ingredient/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'name',
                    'label' => 'Ingredient Name',
                    'value' => function ($model) {
                        return Html::a($model->name, ['ingredient/view', 'id' => $model->id,], ['data-pjax' => 0]);

                    },
                    'format' => 'raw',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update}',
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]); ?>
    </div>

</div>
