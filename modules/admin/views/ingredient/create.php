<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\Ingredient $model
 */

$this->title = Yii::t('app', 'Create Ingredient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ingredient'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="ingredient-create">
    <?= $this->render('@app/views/layouts/_menu', ['model' => $model]) ?>
    <?= $this->render('_form', ['model' => $model,]); ?>
</div>
