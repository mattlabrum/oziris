<?php

use app\widgets\JavaScript;
use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var app\models\Product $model
 * @var yii\bootstrap\ActiveForm $form
 */

?>

    <div class="text-center" id="order-items-div">

        <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>
        <div class="row">
                <h1 class="pull-left">Ingredients</h1>
        </div>

        <div class="row oziris-block">
            <div class="row">
                <div style="padding-left: 30px" class="col-lg-4">
                    <label class="control-label">Select Ingredient Group</label>
                    <?=
                        Select2::widget([
                            'id' => 'ingredient-group',
                            'name' => 'ingredient-group',
                            'data' => \yii\helpers\ArrayHelper::map(app\models\IngredientGroup::find()->all(), 'id', 'name'),
                            'options' => [
                                'placeholder' => Yii::t('app', 'Type to autocomplete'),
                                'multiple' => false,
                            ],
                            'pluginOptions' => [
                                'tags' => true,
                            ],
                            'pluginEvents' => [
                                'select2:select' => 'function(e) { populateIngredients(); }',
                            ]
                        ]);
                    ?>
                </div>

                <div class="col-lg-4" id="ingredients-div">
                    <label class="control-label">Select Ingredient</label>
                    <?=
                        Select2::widget([
                            'id' => 'ingredient',
                                'name' => 'ingredient',
                                'data' => \yii\helpers\ArrayHelper::map(app\models\Ingredient::find()->all(), 'id', 'name'),
                                'options' => [
                                    'placeholder' => Yii::t('app', 'Type to autocomplete'),
                                    'multiple' => false,
                                ],
                                'pluginOptions' => [
                                    'tags' => true,
                                ],
                        ]);
                    ?>
                </div>

                <div class="col-lg-2" id="ingredients-div">
                    <label class="control-label">Percentage</label>
                    <input type="text" id="txtIngredientPercentage">
                </div>

                <div class="col-lg-2" style="padding-top: 30px; padding-right: 30px">
                    <?= Html::button(Yii::t('app', 'Add'), [
                        'id' => 'btnAddIngredient',
                        'class' => 'btn btn-oziris-save btn-xs',
                        'style' => ['width' => '100%']
                    ]); ?>
                </div>
            </div>

            <div class="row" style="padding-top: 10px; padding-right: 30px ; padding-left: 30px">

                <table id="tblIngredients" class="kv-grid-table table table-bordered table-striped kv-table-wrap">
                    <thead>
                    <tr>
                        <th>Ingredient Group</th>
                        <th>Ingredient</th>
                        <th>Percentage</th>
                        <th width="50px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
<?php JavaScript::begin(); ?>
    <script>

        function generateIngredientsRow(data) {
            var row = '';

            row += "<tr>";
            row += "<td hidden='hidden'>" + data['id'] + "</td>";
            row += "<td>" + data['ingredient-group'] + "</td>";
            row += "<td>" + data['ingredient'] + "</td>";
            row += "<td> <input data='" + data.id + "' type='text' id='product" + data['id'] + "' value='"+data['percentage']+"'> </input> </td>";
            row += "<td><button type='button' style='width: 95px' id='update-percentage' class='glyphicon glyphicon-edit btn btn-oziris-save btn-xs' data='" + data.id + "'></button><button type='button' style='width: 95px' id='delete-ingredient' class='glyphicon glyphicon-trash btn btn-oziris-delete btn-oziris-delete-sm btn-xs' data='" + data.id + "'></button></td>";
            row += "</tr>";

            return row;
        }


        function populateIngredientGroups() {

            $('#ingredients-div').addClass('disabled');
            $.ajax({
                url: '<?= Url::to(['ingredient/populate-ingredient-groups-dropdown']); ?>', // point to server-side PHP script
                type: 'post',
                success: function (data) {
                    $('#ingredient-group').find('option').remove().end();
                    $('#ingredient').find('option').remove().end();
                    $.each(data.results, function (i, item) {
                        $('#ingredient-group').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });
                }
            });
        }

        function populateIngredients() {

            $('#ingredients-div').addClass('disabled');
            $.ajax({
                url: '<?= Url::to(['ingredient/populate-ingredients-dropdown']); ?>', // point to server-side PHP script
                data: {
                    group_id: $('#ingredient-group').find(":selected").val()
                },
                type: 'post',
                success: function (data) {
                    $('#ingredient').find('option').remove().end();
                    $.each(data.results, function (i, item) {
                        $('#ingredient').append($('<option>', {
                            value: item.id,
                            text: item.text
                        }));
                    });

                    $('#ingredients-div').removeClass('disabled');
                }
            });
        }

        function getAllItems() {

            var htmlString = '';
            $.ajax({
                url: '<?= Url::to(['ingredient/get-product-ingredients']); ?>', // point to server-side PHP script
                data: {product_id: <?= $model->id; ?>},
                type: 'post',
                dataType: 'json',
                success: function (data) {
                    console.log('ingredients');
//                        console.log(data.ingredients);
                    if (data != null) {
                        $.each(data.ingredients, function (i, item) {
                            console.log(item);
                            htmlString += generateIngredientsRow(item);
                        });
                    }
                    $("#tblIngredients").find('tbody').append(htmlString);
                }
            });

        }

        if ($('#save-ProductForm').text().trim() == 'Save') {
            getAllItems();
            $("#btnAddIngredient").click(function () {
                var htmlString = '';
                if ($('#ingredient').find(":selected").val() == '' || $('#ingredient').find(":selected").text() == '') {
                    alert('Please Select an Ingredient');
                } else {
                    $.ajax({
                        url: '<?= Url::to(['ingredient/add-product-ingredient']); ?>', // point to server-side PHP script
                        data: {
                            group: $('#ingredient-group').find(":selected").text(),
                            percentage: $('#txtIngredientPercentage').val(),
                            ingredient: $('#ingredient').find(":selected").text(),
                            product_id: <?= ($model->id); ?>
                        },
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            if (data != null) {
                                if(data == false){
                                    alert('Selected Ingredient already added to Product');
                                }else{
                                    $('#ingredient-group').val('').change();
                                    $('#ingredient').val('').change();
                                    populateIngredientGroups();
                                    htmlString += generateIngredientsRow(data);
                                    $("#tblIngredients").find('tbody').append(htmlString);


                                }
                            }
                        }
                    });
                }
            });

            $(document).on("click", "#update-percentage", function () {

                id = $(this).attr('data');
                percentage = $("#product"+id).val();

                if (confirm("Are you sure?")) {
                    $.ajax({
                        url: '<?= Url::to(['ingredient/update-percentage']); ?>', // point to server-side PHP script
                        data: {id , percentage},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            if (data == 1){
                                alert("Ingredient Percentage updated successfully!");
                            }

                        }
                    });
                }
            });



            $(document).on("click", "#delete-ingredient", function () {
                console.log($(this).parent().parent());
                if (confirm("Are you sure?")) {
                    row = $(this).parent().parent();
                    $.ajax({
                        url: '<?= Url::to(['ingredient/delete-product-ingredient']); ?>', // point to server-side PHP script
                        data: {id: $(this).attr('data')},
                        type: 'post',
                        dataType: 'json',
                        success: function (data) {
                            if (data == '1') {
                                console.log(this);
                                row.remove();

                            }
                        }
                    });
                }
            });

            function generateTotals() {

//                $('#lblSubTotal').text(subtotal);
//                $('#lblTax').text(tax);
//                $('#lblTotal').text(total);
            }


        }

    </script>
<?php JavaScript::end(); ?>