<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\IngredientForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Ingredient') . ' ' . $model->getIngredient()->name;

?>
<div class="ingredient-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->getIngredient()]) ?>
    <?php echo $this->render('_form', ['model' => $model,]); ?>

</div>
