<?php
use app\widgets\JavaScript;
use app\widgets\xj\Text;
use app\widgets\xj\QRcode;
use cornernote\returnurl\ReturnUrl;
use kartik\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use dmstr\bootstrap\Tabs;

/**
 * @var yii\web\View $this
 * @var app\models\Batch $model
 */

$this->title = Yii::t('app', 'Batch') . ' - ' . $model->batch_number;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Batches'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <table width="100%">
                <tr>
                    <td>
                        <h1 class="pull-left"><?= $this->title ?></h1>
                    </td>
                    <td>
                        <div class="pull-right">
                            <?= Html::a('<span class="fa fa-arrow-left"></span> ' . Yii::t('app', 'Back'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
                            <?php if (Yii::$app->controller->action->id != 'view' && Yii::$app->controller->action->id != 'create' && Yii::$app->controller->action->id != 'duplicate' && Yii::$app->controller->action->id != 'resend') { ?>
                                <?= Html::a('<span class="fa fa-eye"></span> ' . Yii::t('app', 'View'), ['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal']) ?>
                            <?php } ?>
                            <?php if (Yii::$app->controller->action->id != 'update' && Yii::$app->controller->action->id != 'create' && Yii::$app->controller->action->id != 'duplicate'&& Yii::$app->controller->action->id != 'resend') { ?>
                                <?= Html::a('<span class="fa fa-pencil"></span> ' . Yii::t('app', 'Update'), ['update', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal']) ?>
                            <?php } ?>
                            <?= Html::a('<span class="fa fa-print"></span> ' . Yii::t('app', 'Print'), ['print-batch', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()], ['class' => 'btn btn-oziris-normal']) ?>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <div class="clearfix"></div>




    <div class="col-lg-1"></div>
    <div class="col-lg-10">

        <hr class="hr">
        <div class="row">
            <div class="col-lg-2 text-center no-padding-right">
                <div style="padding: 10px">
                    <div class="row">
                        <label class="control-label">Product Image</label>
                    </div>
                    <div class="row">
                        <img class="img-responsive img-thumbnail" src="<?= yii\helpers\Url::to('@web/uploads/product').'/'.$model->product->product_image?>" alt="<?=$model->product->product_name?>">
                    </div>
                    <div class="row" style="padding-top: 40px">
                        <label class="control-label">QR Code</label>
                    </div>
                    <div class="row">
                        <img class="img-responsive img-thumbnail" src="<?= yii\helpers\Url::to('@web/upload/qrcode').'/'.$model->qr_code_image ?>" alt="<?= $model->qr_code ?>">
                        <a class='btn btn-oziris btn-sm' download href="<?= yii\helpers\Url::to('@web/upload/qrcode').'/'.$model->qr_code_image ?>" type="application/octet-stream">Download QR Code</a>
                    </div>
                    <?php if($model->label_type_text != 0 && $model->overall_coo_percentage >= 50){ ?>
                    <div class="row" style="padding-top: 40px">
                        <label class="control-label">CoO Label</label>
                        <div id="canvas" style="background-image: url(../../images/border.png); background-color: white ; background-size: cover; height: 400px; width: 200px; padding-bottom: 30px">
                            <div>
                                <img src="<?= yii\helpers\Url::to('@web/images/triangle.png') ?>" style="background-size: cover; height: 175px; width: 175px; padding-top: 20px ; padding-bottom: 20px;">
                            </div>

                            <div class="parent">
                                <img class="image2" src="<?= yii\helpers\Url::to('@web/images/yellow.png') ?>" style="height: 60px; width: <?= (($model->overall_coo_percentage/100)*163) ?>px; max-width: 163px;">
                                <img class="image1" src="<?= yii\helpers\Url::to('@web/images/percentbar.png') ?>" style="height: 60px; width: 175px;">
                            </div>

                            <div>
                                <?php
                                $lblText = '';
                                $fontSize = 21;
                                if($model->label_type_text == 1){
                                    $fontSize = 22;
                                    $lblText = "Made in Australia from at least ". round($model->overall_coo_percentage)  ."% Australian ingredients";
                                }else if($model->label_type_text == 2){
                                    $lblText = "Made in Australia from at least ". round($model->overall_coo_percentage)  ."% Australian ingredients";
                                }else if($model->label_type_text == 3){
                                    $fontSize = 28;
                                    $lblText = "Produced in Australia";
                                }else if($model->label_type_text == 4){
                                    $fontSize = 28;
                                    $lblText = "Grown in Australia";
                                }else if($model->label_type_text == 5){
                                    $fontSize = 28;
                                    $lblText = "Product of Australia";
                                }else if($model->label_type_text == 6){
                                    $fontSize = 22;
                                    $lblText = "Made in Australia from 100% Australian ingredients";
                                }
                                ?>
                                <p style="padding-top: 10px ;padding-left: 10px ; padding-right: 10px; font-size: <?= $fontSize?>px ; font-weight: bolder"><?= $lblText ?></p>
                            </div>

                        </div>
                    </div>
                    <?php } ?>

                </div>
            </div>



            <div class="col-lg-10">
                <div class="batch-view">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::attributeFormat
//            [
//                'format' => 'html',
//                'attribute' => 'id',
//                'value' => ($model->getId()->one() ? Html::a($model->getId()->one()->id, ['scanned-product/view', 'id' => $model->getId()->one()->id,]) : '<span class="label label-warning">?</span>'),
//            ],
                            'manufacture_date',
                            'recalled_date',
                            'qr_code',
                            // generated by schmunk42\giiant\generators\crud\providers\RelationProvider::attributeFormat
                            [
                                'format' => 'html',
                                'attribute' => 'product_id',
                                'label' => 'Product Name',
                                'value' => ($model->product->product_name ? $model->product->id.' - '.$model->product->product_name : '<span class="label label-warning">?</span>'),
                            ],
                            'batch_number',
                            [
                                'attribute' => 'created_at',
                                'format' =>  ['datetime', 'php:d/m/yy h:m:s '],
                                'options' => ['width' => '100']
                            ],
                            [
                                'attribute' => 'updated_at',
                                'format' =>  ['datetime', 'php:d/m/yy h:m:s '],
                                'options' => ['width' => '100']
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>



    </div>
    <div class="col-lg-1"></div>
</div>

<?php

//    echo $this->render('@bedezign/yii2/audit/views/_audit_trails', [

echo $this->render('../audit-trail/AuditGrid', [
    'params' => [
        'search_string' => $_GET,
        'query' => $model->getAuditTrails(),
        'filter' => ['manufacture_date', 'recalled_date', 'qr_code', 'product_id', 'batch_number']
    ],
//
    // which columns to show
    'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
    // set to false to hide filter
])



?>

<?php JavaScript::begin(); ?>
<script>



    uploadCool();

    function uploadCool() {

        html2canvas([document.getElementById('canvas')], {
            onrendered: function(canvas) {
                var data = canvas.toDataURL('image/png');
                save_img(data);
            }
        });


//        div_content = document.querySelector("#canvas");
//        html2canvas(div_content).then(function (canvas) {
//            data = canvas.toDataURL('image/png');
//            save_img(data);
//        });
    }

    function save_img(data){
        $.ajax({
            url: '<?= Url::to(['upload-cool']); ?>', // point to server-side PHP script
            type: 'post',
            data: {img: data, batch_id: <?= $model->id?>},
            success: function (data) {
                return data;
            }
        });

    }


</script>
<?php JavaScript::end(); ?>