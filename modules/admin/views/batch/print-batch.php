<?php use app\models\BatchIngredient;
use yii\helpers\Url;

?>
<table width="100%" style="padding-top: 5px ; padding-bottom: 5px">
    <tr>
        <td align="center">
            <img style="max-width: 200px"
                 src="<?= 'https://' . $_SERVER['HTTP_HOST'] . Url::to('@web/images/beston-batch-report.png'); ?>" alt="Beston Market Place">
        </td>
    </tr>
</table>
<hr>

<table width="100%">
    <tr>
        <td style="border: solid 1px #808080;vertical-align: top" width="80%">
            <table width="100%">
                <tr>
                    <td>
                        <table width="100%">
                            <tr style="width: 100%">
                                <th colspan="4" style="background-color: #000000; color: #ffffff">
                                    Batch Information
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    Batch Number

                                </th>
                                <th>
                                    Product

                                </th>
                                <th>
                                    Manufacturing Date

                                </th>
                                <th>
                                    Recall Date

                                </th>
                            </tr>
                            <tr>
                                <td style="text-align: center">
                                    <?= $model->batch_number?>
                                </td>
                                <td style="text-align: center">
                                    <?= $model->product->product_name?>
                                </td>
                                <td style="text-align: center">
                                    <?= $model->manufacture_date?>
                                </td>
                                <td style="text-align: center">
                                    <?= $model->recalled_date?>
                                </td>
                            </tr>
                        </table>

                        <table width="100%">
                            <tr style="width: 100%">
                                <th colspan="5" style="background-color: #000000; color: #ffffff">
                                    Batch Ingredients
                                </th>
                            </tr>
                            <tr>
                                <th>
                                    Ingradient

                                </th>
                                <th>
                                    Percentage

                                </th>
                                <th>
                                    CoO

                                </th>
                                <th>
                                    Supplier

                                </th>
                                <th>
                                    Manufacturing Date

                                </th>
                            </tr>

                            <?php
//                            print_r($ingredients);die;
                            foreach($model->ingredients as $ingredient) {?>
                            <tr>
                                <td style="text-align: center">
                                    <?= $ingredient['Ingredient'] ; ?>
                                </td>
                                <td style="text-align: center">
                                    <?= ($ingredient['Percentage']> 0 ? $ingredient['Percentage'] : 0 );?>
                                </td>
                                <td style="text-align: center">
                                    <?= (isset($ingredient['Coo']['CooName']) ? $ingredient['Coo']['CooName'] : '' );?>

                                </td>
                                <td style="text-align: center">
                                    <?= $ingredient['SupplierDetails']['SupplierName'] ; ?>
                                </td>
                                <td style="text-align: center">
                                    <?= $ingredient['ManufactureDate']; ?>
                                </td>
                            </tr>
                        <?php }?>

                        </table>

                        <table width="100%">
                            <tr style="width: 100%">
                                <th colspan="5" style="background-color: #000000; color: #ffffff">
                                    Supplier Details
                                </th>
                            </tr>
                            <?php
                                $batch_ingredients = BatchIngredient::find()->where(['batch_id' => $model->id ])->groupBy('supplier_id')->all();

                            $counter = 0;

                                for ($x=0 ; $x<  count($batch_ingredients) ; $x++) {

                                    if((($x % 3) == 0)){

                                        echo '<tr>';
                                    }

                                    if(isset($batch_ingredients[$x]->supplier) ){
                                        $counter ++;
                                        echo '<td>';
                                            echo '<table>';
                                                echo '<tr>';
                                                    echo '<td>';
                                                        echo "<img src=" . $batch_ingredients[$x]->supplier->getSupplierImageUrl() . " width='150px'>";
                                                    echo '</td>';
                                                echo '</tr>';

                                                echo '<tr>';
                                                    echo '<td>';
                                                        echo '<b> Name : </b><br>' . $batch_ingredients[$x]->supplier->supplier_name;
                                                    echo '</td>';
                                                echo '</tr>';

                                                echo '<tr>';
                                                    echo '<td>';
                                                        echo '<b> Address : </b><br>' . $batch_ingredients[$x]->supplier->getSupplierAddress();
                                                    echo '</td>';
                                                echo '</tr>';
                                            echo '</table>';
                                        echo '</td>';

                                    }










                                    if((($x % 3) == 0) && ($x!=0)){

                                        echo '</tr>';
                                    }

                                }
                            ?>

                                </td>
                            </tr>
                        </table>

                    </td>
                    </td>
                </tr>
            </table>
        </td>
        <td width="20%" style="border: solid 1px #808080; text-align: center; vertical-align: top">
            <table width="100%" style="padding-top: 20px ; padding-bottom: 20px">
                <tr>
                    <th style="background-color: #000000; color: #ffffff ; text-align: center">
                        Product Image
                    </th>
                </tr>
                <tr>
                    <td>
                        <img style=" padding-top: 5px ; max-height: 200px"
                             src="<?= $model->product->getProductImageUrl() ?>" alt="Beston Market Place">
                    </td>
                </tr>
                <tr style="padding-top: 20px">
                    <th style="background-color: #000000; color: #ffffff ; text-align: center">
                        CoOL
                    </th>
                </tr>
                <tr>
                    <td>

                        <div>
                            <?php IF(isset($model->cool_image)): ?>
                                <img src="<?= yii\helpers\Url::to('@web/uploads/cool/').$model->cool_image ?>" alt="Country of Origin Label" style="width: 100px ; height: 200px" >
                            <?php endif ?>
                        </div>
                    </td>
                </tr>
                <tr>
                    <th style="background-color: #000000; color: #ffffff ; text-align: center">
                        Product QR Code
                    </th>
                </tr>
                <tr>
                    <td>
                        <img style=" padding-top: 5px ; max-width: 100px"
                             src="<?= $model->getQrCodeImageUrl() ?>" alt="Beston Market Place">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


