<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\ProductTypeForm $model
 */

$this->title = Yii::t('app', 'Product Type') . ' ' . $model->getProductType()->type_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <?= $this->render('@app/views/layouts/_menu', ['model'=> $model->getProductType()]); ?>
    <div class="col-lg-1"></div>
    <div class="col-lg-10">

        <hr class="hr">
        <div class="row">
            <div class="col-lg-2 text-center no-padding-right">
                <div class="row">
                    <img class="img-responsive img-thumbnail" src="<?= yii\helpers\Url::to('@web/uploads/type').'/'.$model->getProductType()->icon?>" alt="<?=$model->getProductType()->type_name?>">
                </div>
            </div>
            <div class="col-lg-10">
                <div class="batch-view">
                    <?= DetailView::widget([
                        'model' => $model->getProductType(),
                        'attributes' => [
                            'type_name',
                            [
                                'attribute' => 'show_on_menu',
                                'format' => 'raw',
                                'value' => $model->getProductType()->show_on_menu ? '<span class="label label-success">Yes</span>' : '<span class="label label-danger">No</span>',
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                        'onText' => 'Yes',
                                        'offText' => 'No',
                                    ]
                                ],
                                'labelColOptions' => ['style' => 'width:15.8%'],
                            ],
                            [
                                'attribute' => 'created_at',
                                'format' =>  ['datetime', 'php:d/m/yy h:m:s '],
                                'options' => ['width' => '100']
                            ],
                            [
                                'attribute' => 'updated_at',
                                'format' =>  ['datetime', 'php:d/m/yy h:m:s '],
                                'options' => ['width' => '100']
                            ],
                        ],
                    ]); ?>

                    <?= DetailView::widget([
                        'model' => $model->producttypetranslation,
                        'attributes' => [
                            'type_name',
                        ],
                    ]);
                    ?>
                </div>
            </div>
        </div>



    </div>
    <div class="col-lg-1"></div>
</div>

<?php

//    echo $this->render('@bedezign/yii2/audit/views/_audit_trails', [

echo $this->render('../audit-trail/AuditGrid', [
    'params' => [
        'search_string' => $_GET,
        'query' => $model->getProductType()->getAuditTrails(),
        'filter' => [
            'icon', 'type_name'
        ]
    ],
//
    // which columns to show
    'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
    // set to false to hide filter
])



?>
