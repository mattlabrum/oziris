<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\ProductTypeForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Product Type') . ' ' . $model->getProductType()->type_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Types'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getProductType()->id, 'url' => ['view', 'id' => $model->getProductType()->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="product-type-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->getProductType(),]); ?>
    <?php echo $this->render('_form', ['model' => $model,]); ?>

</div>
