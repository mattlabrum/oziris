<?php

use app\models\ManufacturerTranslation;
use dmstr\bootstrap\Tabs;
use kartik\detail\DetailView;

/**
 * @var yii\web\View $this
 * @var app\models\form\ManufacturerForm $model
 */

$this->title = Yii::t('app', 'Manufacturer') . ' - ' . $model->manufacturer->manufacturer_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manufacturers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturer-view">
    <?= $this->render('@app/views/layouts/_menu', ['model'=> $model->manufacturer]); ?>
    <hr class="hr">

    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-2 text-center no-padding-right">
                <div style="padding: 10px">
                    <div class="row">
                        <label class="control-label">Manufacturer Image</label>
                    </div>
                    <div class="row">
                        <img class="img-responsive img-thumbnail" src="<?= yii\helpers\Url::to('@web/uploads/manufacturer').'/'.$model->getManufacturer()->manufacturer_logo?>" alt="<?= $model->getManufacturer()->manufacturer_logo?>">
                    </div>
                </div>

            </div>
            <div class="col-lg-8">
                <?php
                echo DetailView::widget([
                    'model' => $model->getManufacturer(),
                    'attributes' => [
                        [
                            'group' => true,
                            'label' => 'Manufacturer Information',
                            'rowOptions' => ['class' => 'product-info-header']
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'format' => 'raw',
                                    'value' => '<kbd>' . $model->getManufacturer()->id . '</kbd>',
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => true,
                                ],
                                [
                                    'attribute' => 'manufacturer_name',
                                    'format' => 'raw',
                                    'value' => $model->getManufacturer()->manufacturer_name,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'manufacturer_location_latitude',
                                    'format' => 'raw',
                                    'value' => $model->manufacturer->manufacturer_location_latitude,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => false
                                ],
                                [
                                    'attribute' => 'manufacturer_location_longitude',
                                    'format' => 'raw',
                                    'value' => $model->manufacturer->manufacturer_location_longitude,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],

                        [
                            'attribute' => 'manufacturer_description',
                            'format' => 'raw',
                            'value' => '<span class="text-left"><em>' . $model->manufacturer->manufacturer_description . '</em></span>',
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'location',
                                    'format' => 'raw',
                                    'value' => $model->manufacturer->location,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => false
                                ],
                                [
                                    'attribute' => 'manufacturer_video',
                                    'format' => 'raw',
                                    'value' => $model->manufacturer->manufacturer_video,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                    ],
                    'mode' => 'view',
                    'bordered' => false,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'fadeDelay' => 200,
                    'class'=> 'text-left',
                ]);
                ?>
            </div>
            <div class="col-lg-1"></div>

        </div>

        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-2 text-center no-padding-right">
            </div>
            <div class="col-lg-8">
                <?php
                $translations = ManufacturerTranslation::findAll(['manufacturer_id' => $model->manufacturer->id]);
                $attributes = [];

                foreach($translations as $translation){

                    $row = [
                        'group' => true,
                        'label' => 'Manufacturer Translation '.$translation->language->name,
                        'rowOptions' => ['class' => 'product-info-header']
                    ];
                    array_push($attributes,$row);


                    $column= [
                        'attribute' => 'manufacturer_name',
                        'format' => 'raw',
                        'value' => $translation->manufacturer_name,
                        'labelColOptions' => ['style' => 'width:20%'],
                        'valueColOptions' => ['style' => 'width:80%'],
                        'displayOnly' => false
                    ];
                    array_push($attributes,$column);
                    $column= [
                        'attribute' => 'manufacturer_description',
                        'format' => 'raw',
                        'value' => $translation->manufacturer_description,
                        'labelColOptions' => ['style' => 'width:15%'],
                        'valueColOptions' => ['style' => 'width:20%'],
                        'displayOnly' => false
                    ];
                    array_push($attributes,$column);
                }

                if(count($translations)>0){
                    echo DetailView::widget([
                        'model' => $model->getManufacturerTranslation(),
                        'attributes' => $attributes,
                        'mode' => 'view',
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hAlign' => 'left',
                        'vAlign' => 'middle',
                        'fadeDelay' => 200,
                        'container' => ['id' => 'translations'],
                    ]);

                }
                ?>

            </div>
            <div class="col-lg-1"></div>

        </div>
    </div>
</div>

<?php
echo $this->render('../audit-trail/AuditGrid', [
    'params' => [
        'search_string' => $_GET,
        'query' => $model->manufacturer->getAuditTrails(),
        'filter' => ['manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description', 'location']
    ],
//
    // which columns to show
    'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
    // set to false to hide filter
]);
?>