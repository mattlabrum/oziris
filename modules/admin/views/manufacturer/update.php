<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\ManufacturerForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Manufacturer') . ' ' . $model->manufacturer->manufacturer_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manufacturers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->manufacturer->manufacturer_name, 'url' => ['view', 'id' => $model->manufacturer->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="manufacturer-update">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->manufacturer]); ?>
    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
