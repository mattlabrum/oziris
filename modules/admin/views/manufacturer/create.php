<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\ManufacturerForm $model
 */

$this->title = Yii::t('app', 'Create Manufacturer');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Manufacturers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="manufacturer-create">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->manufacturer]) ?>
    <?= $this->render('_form', ['model' => $model,]); ?>

</div>
