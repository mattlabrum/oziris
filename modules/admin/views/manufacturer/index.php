<?php

use app\widgets\JavaScript;
use kartik\grid\DataColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\Manufacturer $model
 * @var app\models\search\ManufacturerSearch $searchModel
 */

$this->title = Yii::t('app', 'Manufacturers');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="manufacturer-index">


    <div class="clearfix">

        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>
                    <div class="pull-right">
                        <?= Html::a('<span class="fa fa-plus"></span> ' . Yii::t('app', 'Create') . ' ' . Yii::t('app', 'Manufacturer'), ['create', 'ru' => ReturnUrl::getToken()], ['class' => 'btn btn-oziris']) ?>
                        <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Import') . ' ' . Yii::t('app', $this->title), ['class' => 'btn btn-oziris', 'data-toggle' => 'modal', 'data-target' => '#activity-modal']) ?>
                        <?= Html::button('<span class="fa fa-search"></span> ' . Yii::t('app', 'Export') . ' ' . Yii::t('app', 'To CSV'), ['class' => 'btn btn-oziris', 'id' => 'btnExport']) ?>
                        <a class='btn btn-oziris hidden ' id="btnDownload">Download CSV</a>
                    </div>
                </td>
            </tr>
        </table>
        <hr class="hr">
    </div>

    <div class="table-responsive text-center">
        <?= \kartik\grid\GridView::widget([
            'layout' => '{summary}{pager}{items}{pager}',
            'dataProvider' => $dataProvider,
            'pager' => [
                'class' => yii\widgets\LinkPager::className(),
                'firstPageLabel' => Yii::t('app', 'First'),
                'lastPageLabel' => Yii::t('app', 'Last'),
            ],
            'filterModel' => $searchModel,
            'export' => false,
            'responsive' => true,
            'hover'=>true,
            'columns' => [

                [
                    'class' => yii\grid\DataColumn::className(),
                    'attribute' => 'id',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->id, ['manufacturer/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'manufacturer_logo',
                    'value' => function ($model) {
                        return Html::a(Html::img(yii\helpers\Url::to('@web/uploads/manufacturer').'/'.$model->manufacturer_logo,['class' => 'product-image']),['manufacturer/view', 'id' => $model->id,]) ;
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'manufacturer_name',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->manufacturer_name, ['manufacturer/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'manufacturer_description',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->manufacturer_description, ['manufacturer/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'manufacturer_location_latitude',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->manufacturer_location_latitude, ['manufacturer/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'manufacturer_location_longitude',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->manufacturer_location_longitude, ['manufacturer/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => kartik\grid\DataColumn::className(),
                    'attribute' => 'manufacturer_video',
                    'value' => function ($model) {
                        if ($rel = $model->getId()->one()) {
                            return Html::a($rel->manufacturer_video, ['manufacturer/view', 'id' => $rel->id,], ['data-pjax' => 0]);
                        } else {
                            return '';
                        }
                    },
                    'format' => 'raw',
                ],
                [
                    'class' => DataColumn::className(),
                    'attribute' => 'deleted_at',
                    'label' => 'Status',
                    'value' => function ($model) {
                        if ( $model->deleted_at == null) {
                            return Html::a('<span class="glyphicon glyphicon-ok translated"></span>', ['manufacturer/change-status', 'id' => $model->id,],['title'=>Yii::t('app', 'Disable Manufacturer'), 'data-confirm' => Yii::t('kvgrid', 'Are you sure to disable this Manufacturer ?')]);
                        } else {
                            return Html::a('<span class="glyphicon glyphicon-remove not-translated"></span>', ['manufacturer/change-status', 'id' => $model->id,],['title'=>Yii::t('app', 'Enable Manufacturer'), 'data-confirm' => Yii::t('kvgrid', 'Are you sure to enable this Manufacturer ?')]);
                        }
                    },
                    'format' => 'raw',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{update} &nbsp;&nbsp; {duplicate}',
                    'buttons' => [
                        'duplicate' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-duplicate"></span> ', ['duplicate', 'id' => $model->id], ['title' => Yii::t('app', 'Duplicate'),]);
                        }


                    ],
                    'contentOptions' => ['nowrap' => 'nowrap']
                ],
            ],
        ]);
        ?>
    </div>

</div>


<?php echo $this->render('../import-modal/modal'); ?>

<?php JavaScript::begin(); ?>
<script>
    $('#import-file').on('change', function (event) {
        var file_data = $('#import-file').prop('files')[0];
        $('#txtFileInput').html($('#import-file')[0].files[0].name);
        var form_data = new FormData();
        form_data.append('file', file_data);
        $.ajax({
            url: '<?= Url::to(['csv-import/upload-file']); ?>', // point to server-side PHP script
            dataType: 'json',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,
            type: 'post',
            success: function (php_script_response) {
                viewImportData();
                console.log('Success : ' + php_script_response); // display response from the PHP script, if any
            },
            error: function (php_script_response) {
                console.log('Error : ' + php_script_response);  // display response from the PHP script, if any
            }
        });
    });

    function viewImportData() {
        $.ajax({
            url: '<?= Url::to(['csv-import/import-data']); ?>', // point to server-side PHP script
            data: {fileName: $('#import-file')[0].files[0].name},
            dataType: 'json',
            type: 'POST',
            success: function (php_script_response) {
                htmlString = '';
                htmlString = "<div class='table-responsive text-center'>";
                htmlString += "<table class='table table-striped table-bordered table-condensed table-hover'>";
                htmlString += "<tr>";
                htmlString += "<th class='table-header text-center'>ID</th>";
                htmlString += "<th class='table-header text-center'>Manufacturer Name</th>";
                htmlString += "<th class='table-header text-center'>Manufacturer Description</th>";
                htmlString += "<th class='table-header text-center'>Manufacturer Logo</th>";
                htmlString += "<th class='table-header text-center'>Manufacturer Location Longitude</th>";
                htmlString += "<th class='table-header text-center'>Manufacturer Location Latitude</th>";
                htmlString += "</tr>";

                for (i = 0; i < php_script_response.length; i++) {


                    htmlString += "<tr class='table-row'>";
                    htmlString += "<td class='text-center'>" + php_script_response[i].ID + "</td>";
                    htmlString += "<td class='text-center'>" + php_script_response[i].ManufacturerName + "</td>";
                    htmlString += "<td class='text-center'>" + php_script_response[i].ManufacturerDescription + "</td>";
                    htmlString += "<td class='text-center'>" + php_script_response[i].ManufacturerLogo + "</td>";
                    htmlString += "<td class='text-center'>" + php_script_response[i].ManufacturerLocationLongitude + "</td>";
                    htmlString += "<td class='text-center'>" + php_script_response[i].ManufacturerLocationLatitude + "</td>";
                    htmlString += "</tr>";
                }

                htmlString += "</table>";
                htmlString += "</div>";
                $('#display-data-table').html(htmlString);

                console.log("Success"); // display response from the PHP script, if any
                console.log(php_script_response); // display response from the PHP script, if any
            },
            error: function (php_script_response) {
                htmlString = "Error Loading Data Please Try Again!";

                $('#display-data-table').html(htmlString);
                console.log("error"); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            }
        });

    }

    $('#btnExport').on('click', function (event) {
        searchField = {};
        length = $('thead tr td').children().length;
        for (i = 1; i <= length; i++) {
            element = $('thead tr td:nth-child(' + i + ') input').attr('name');
            key = element.substring((element.indexOf('[') + 1), element.indexOf(']'));
            searchField[key] = $('thead tr td:nth-child(' + i + ') input').val()
        }

        $.ajax({
            url: '<?= Url::to(['manufacturer/generate-csv']); ?>', // point to server-side PHP script
            data: searchField,
            type: 'post',
            success: function (php_script_response) {
                if (php_script_response) {
                    $('#btnDownload').removeClass('hidden');
                }
                console.log(php_script_response); // display response from the PHP script, if any
            },
            error: function (php_script_response) {
                console.log('error'); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            }
        });
    });
    $('#btnDownload').on('click', function (event) {

        $('#btnDownload').attr({target: '_blank', href: '<?= Url::to('@web/csv/manufacturers.csv'); ?>'});
    });

    $('#btnSaveData').on('click', function (event) {
        console.log("save data");
        $.ajax({
            url: '<?= Url::to(['manufacturer/save-data']); ?>', // point to server-side PHP script
            dataType: 'html',  // what to expect back from the PHP script, if anything
            data: {fileName: $('#import-file')[0].files[0].name},
            type: 'POST',
            success: function (php_script_response) {
                alert(php_script_response);

                $( '#activity-modal' ).modal( 'hide' ).data( 'bs.modal', null );
                $('#btnSaveData').removeAttr('disabled');
                htmlString = '';
                $('#display-data-table').html(htmlString);

                $('#import-file').val('');
                $('#txtFileInput').text('');

                location.reload(true);
                console.log("success"); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            },
            error: function (php_script_response) {
                console.log("error"); // display response from the PHP script, if any
                console.log(php_script_response);  // display response from the PHP script, if any
            }
        });
    });
    $('#btnClose').on('click', function (event) {
        htmlString = '';
        $( '#activity-modal' ).modal( 'hide' ).data( 'bs.modal', null );
        $('#display-data-table').html(htmlString);
        $('#import-file').val('');
        $('#txtFileInput').text('');
    });
</script>
<?php JavaScript::end(); ?>