<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use \dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\SupplierForm $model
 * @var yii\bootstrap\ActiveForm $form
 */

?>

<div class="supplier-form">

    <?php $form = ActiveForm::begin([
        'id' => 'Supplier',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-lg-4',
                'wrapper' => 'col-lg-8',
                'error' => '',
                'hint' => '',
            ],
        ],
        'enableClientValidation' => false,
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?= Html::hiddenInput('ru', ReturnUrl::getRequestToken()); ?>


    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <hr class="hr">
            <h1>Supplier</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row" style="padding-top: 20px">
                <div class="col-lg-5">
                    <?= $form->field($model->supplier, 'supplier_name')->textInput(['maxlength' => true]) ?>
                    <?= $form->field($model->supplier, 'supplier_location_latitude')->textInput() ?>
                    <?= $form->field($model->supplier, 'supplier_location_longitude')->textInput() ?>
                    <?= $form->field($model->supplier, 'city')->textInput() ?>
                    <?= $form->field($model->supplier, 'country')->textInput() ?>
                </div>
                <div class="col-lg-7">
                    <?= $form->field($model->supplier, 'supplier_description')->textarea(array('rows' => 7)); ?>
                    <?= $form->field($model->supplier, 'supplier_logo')->fileInput(['accept' => 'image/*']); ?>
                </div>

            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <h1>Supplier Translation</h1>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10 oziris-block">
            <div class="row" style="padding-top: 20px">
                <div class="col-lg-5">
                    <?=
                    $form->field($model->suppliertranslation, 'language_id')->widget(\kartik\select2\Select2::classname(), [
                        'name' => 'class_name',
                        'model' => $model,
                        'attribute' => 'language_id',
                        'data' => \yii\helpers\ArrayHelper::map(app\models\Language::find()->all(), 'id', 'name'),
                        'options' => [
                            'placeholder' => Yii::t('app', 'Type to autocomplete'),
                            'multiple' => false,
                        ]
                    ]); ?>
                    <?= $form->field($model->suppliertranslation, 'supplier_name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-lg-7">
                    <?= $form->field($model->suppliertranslation, 'supplier_description')->textarea(array('rows' => 6)); ?>
                </div>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <div class="pull-right" style="padding-top: 20px">
                <?= Html::submitButton('<span class="fa fa-check"></span> ' . ($model->supplier->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Save')), [
                    'id' => 'save-' . $model->formName(),
                    'class' => 'btn btn-oziris-save'
                ]); ?>
                <?php if ($model->supplier->isNewRecord) echo Html::a('<span class="fa fa-times"></span> ' . Yii::t('app', 'Cancel'), ReturnUrl::getUrl(['index']), ['class' => 'btn btn-oziris-normal']) ?>
            </div>
        </div>
        <div class="col-lg-1"></div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
