<?php

use app\models\SupplierTranslation;
use kartik\detail\DetailView;

/**
 * @var View $this
 * @var app\models\form\SupplierForm $model
 */

$this->title = Yii::t('app', 'Supplier') . ' ' . $model->supplier->supplier_name;
?>
<div class="supplier-view">

    <?= $this->render('@app/views/layouts/_menu', ['model' => $model->supplier]) ?>
    <hr class="hr">
    <div>
        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-2 text-center no-padding-right">
                <div style="padding: 10px">
                    <div class="row">
                        <label class="control-label">Supplier Image</label>
                    </div>
                    <div class="row">
                        <img class="img-responsive img-thumbnail" src="<?= yii\helpers\Url::to('@web/uploads/supplier').'/'.$model->supplier->supplier_logo?>" alt="<?= $model->supplier->supplier_logo?>">
                    </div>
                </div>

            </div>
            <div class="col-lg-8">
                <?php
                echo DetailView::widget([
                    'model' => $model->supplier,
                    'attributes' => [
                        [
                            'group' => true,
                            'label' => 'Supplier Information',
                            'rowOptions' => ['class' => 'product-info-header']
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'id',
                                    'format' => 'raw',
                                    'value' => '<kbd>' . $model->supplier->id . '</kbd>',
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => true,
                                ],
                                [
                                    'attribute' => 'supplier_name',
                                    'format' => 'raw',
                                    'value' => $model->supplier->supplier_name,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'supplier_location_latitude',
                                    'format' => 'raw',
                                    'value' => $model->supplier->supplier_location_latitude,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => false
                                ],
                                [
                                    'attribute' => 'supplier_location_longitude',
                                    'format' => 'raw',
                                    'value' => $model->supplier->supplier_location_longitude,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],
                        [
                            'columns' => [
                                [
                                    'attribute' => 'city',
                                    'format' => 'raw',
                                    'value' => $model->supplier->city,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:10%'],
                                    'displayOnly' => false
                                ],
                                [
                                    'attribute' => 'country',
                                    'format' => 'raw',
                                    'value' => $model->supplier->country,
                                    'labelColOptions' => ['style' => 'width:15%'],
                                    'valueColOptions' => ['style' => 'width:20%'],
                                    'displayOnly' => false
                                ],
                            ],
                        ],

                        [
                            'attribute' => 'supplier_description',
                            'format' => 'raw',
                            'value' => '<span class="text-left"><em>' . $model->supplier->supplier_description . '</em></span>',
                        ],
                    ],
                    'mode' => 'view',
                    'bordered' => false,
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'hover' => true,
                    'hAlign' => 'left',
                    'vAlign' => 'middle',
                    'fadeDelay' => 200,
                    'class'=> 'text-left',
                ]);
                ?>
            </div>
            <div class="col-lg-1"></div>

        </div>

        <div class="row">
            <div class="col-lg-1 no-padding-right">

            </div>
            <div class="col-lg-2 text-center no-padding-right">
            </div>
            <div class="col-lg-8">
                <?php
                $translations = SupplierTranslation::find()->where(['supplier_id' => $model->supplier->id])->all();

//                print_r($model->getSupplier()->id);
//                print_r($translations);
//                die;

                $attributes = [];

                foreach($translations as $translation){

                    $row = [
                        'group' => true,
                        'label' => 'Supplier Translation '.$translation->language->name,
                        'rowOptions' => ['class' => 'product-info-header']
                    ];
                    array_push($attributes,$row);


                    $column= [
                        'attribute' => 'supplier_name',
                        'format' => 'raw',
                        'value' => $translation->supplier_name,
                        'labelColOptions' => ['style' => 'width:20%'],
                        'valueColOptions' => ['style' => 'width:80%'],
                        'displayOnly' => false
                    ];
                    array_push($attributes,$column);
                    $column= [
                        'attribute' => 'supplier_description',
                        'format' => 'raw',
                        'value' => $translation->supplier_description,
                        'labelColOptions' => ['style' => 'width:15%'],
                        'valueColOptions' => ['style' => 'width:20%'],
                        'displayOnly' => false
                    ];
                    array_push($attributes,$column);
                }

                if(count($translations)>0){
                    echo DetailView::widget([
                        'model' => $model->getSupplierTranslation(),
                        'attributes' => $attributes,
                        'mode' => 'view',
                        'bordered' => true,
                        'striped' => true,
                        'condensed' => true,
                        'responsive' => true,
                        'hAlign' => 'left',
                        'vAlign' => 'middle',
                        'fadeDelay' => 200,
                        'container' => ['id' => 'translations'],
                    ]);

                }
                ?>

            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
</div>


<?php
//echo $this->render('../audit-trail/AuditGrid', [
//    'params' => [
//        'search_string' => $_GET,
//        'query' => $model->brand->getAuditTrails(),
//        'filter' => ['brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_description', 'manufacturer_id', 'brand_logo', 'brand_video']
//    ],
////
//    // which columns to show
//    'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
//    // set to false to hide filter
//]);
//?>
