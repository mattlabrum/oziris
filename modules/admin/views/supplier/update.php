<?php

use yii\helpers\Html;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\form\SupplierForm $model
 */

$this->title = Yii::t('app', 'Update') . ' ' . Yii::t('app', 'Supplier') . ' ' . $model->getSupplier()->supplier_name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Suppliers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->supplier->id, 'url' => ['view', 'id' => $model->getSupplier()->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="supplier-update">

    <?= $this->render('@app/views/layouts/_menu', [
        'model' => $model->supplier,
    ]); ?>

    <?php echo $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
