<?php
/**
 * Created by PhpStorm.
 * User: Uzair
 * Date: 11-Feb-16
 * Time: 4:22 PM
 */

use app\models\search\AuditTrailSearch;
use bedezign\yii2\audit\Audit;
use bedezign\yii2\audit\web\AuditAsset;
use dmstr\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

$this->registerAssetBundle(AuditAsset::className());
$auditTrailSearch = new AuditTrailSearch();
$auditTrailDataProvider = $auditTrailSearch->search($params);
$auditTrailDataProvider->pagination = ['pageSize' => 50, 'pageParam' => 'page-auditTrails'];
$auditTrailDataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];

$_columns = [];

if (empty($columns) || in_array('user_id', $columns)) {
    $_columns[] = [
        'attribute' => 'user_id',
        'value' => function ($data) {
            return Audit::getInstance()->getUserIdentifier($data->user_id);
        },
        'format' => 'raw',
    ];
}

if (empty($columns) || in_array('action', $columns)) {
    $_columns[] = 'action';
}
if (empty($columns) || in_array('model', $columns)) {
    $_columns[] = 'model';
}
if (empty($columns) || in_array('model_id', $columns)) {
    $_columns[] = 'model_id';
}
if (empty($columns) || in_array('field', $columns)) {
    $_columns[] = 'field';
}
if (in_array('old_value', $columns)) {
    $_columns[] = 'old_value';
}
if (in_array('new_value', $columns)) {
    $_columns[] = 'new_value';
}
if (empty($columns) || in_array('created', $columns)) {
    $_columns[] = 'created';
}
if (empty($columns) || in_array('diff', $columns)) {
    $_columns[] = [
        'label' => Yii::t('audit', 'Diff'),
        'contentOptions' => [
            'class' => 'skip-export-csv',
        ],
        'value' => function ($model) {
            /** @var AuditTrail $model */
            return $model->getDiffHtml();
        },
        'format' => 'raw',
    ];
}

Pjax::begin([
    'id' => 'pjax-AuditTrails',
    'enableReplaceState' => false,
    'linkSelector' => '#pjax-AuditTrails ul.pagination a, th a',
]);

echo '<div class="row text-center" style="padding-left: 10px; padding-right: 10px">';
echo '<table width="100%">';
echo '<tr>';
echo '<td>';
echo '</td>';
echo '<td>';
echo '<div class="pull-right">';


//echo Html::a('<span class="fa fa-search"></span> ' . Yii::t('app', 'Export') . ' ' . Yii::t('app', 'To CSV'),
//    ['generate-audit-csv', 'data' => json_encode($params)] ,['class' => 'btn btn-oziris', 'id' => 'btnExport']);
echo '</div>';
echo '</td>';
echo '</tr>';
echo '</table>';






$defaultExportConfig = [
    GridView::CSV => [
        'label' => Yii::t('kvgrid', 'CSV'),
        'icon' => 'file-code-o' ,
        'iconOptions' => ['class' => 'text-primary'],
        'showHeader' => true,
        'showPageSummary' => true,
        'showFooter' => true,
        'showCaption' => true,
        'filename' => Yii::t('kvgrid', 'grid-export'),
        'alertMsg' => Yii::t('kvgrid', 'The CSV export file will be generated for download.'),
        'options' => ['title' => Yii::t('kvgrid', 'Comma Separated Values')],
        'mime' => 'application/csv',
        'config' => [
            'colDelimiter' => ",",
            'rowDelimiter' => "\r\n",
        ]
    ],
];


echo GridView::widget([
    'id'=>'kv-grid-demo',
    'dataProvider'=>$auditTrailDataProvider,
    'filterModel'=>$auditTrailSearch,
    'columns'=>$_columns,
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
        '{export}',
    ],
    // set export properties
    'export'=>[
    'fontAwesome'=>true
],
    // parameters from the demo form
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    'responsive'=>true,
    'hover'=>true,
    'showPageSummary'=>true,
    'panel'=>[
    'type'=>GridView::TYPE_PRIMARY,
    'heading'=>'Audit Data',
],
    'persistResize'=>false,
    'exportConfig'=>$defaultExportConfig,
]);

//echo GridView::widget([
//    'layout' => '{summary} {pager} {items} {pager}',
//    'export'=>[
//        'fontAwesome'=>true
//    ],
//    'exportConfig' => $defaultExportConfig,
//    'toolbar'=>[
//        '{export}',
//    ],
//    'dataProvider' => $auditTrailDataProvider,
//    'filterModel' => $auditTrailSearch,
//    'pager' => [
//        'class' => yii\widgets\LinkPager::className(),
//        'firstPageLabel' => Yii::t('app', 'First'),
//        'lastPageLabel' => Yii::t('app', 'Last'),
//    ],
//    'responsive' => true,
//    'hover'=>false,
//    'columns' => $_columns,
//]);

echo '</div>';
Pjax::end();