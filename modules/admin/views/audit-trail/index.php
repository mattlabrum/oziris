<?php

use bedezign\yii2\audit\Audit;
use bedezign\yii2\audit\components\Access;
use bedezign\yii2\audit\models\AuditTrail;
use bedezign\yii2\audit\models\AuditTrailSearch;
use bedezign\yii2\audit\web\AuditAsset;
use cornernote\returnurl\ReturnUrl;
use dmstr\helpers\Html;
use kartik\grid\GridView;
use yii\bootstrap\Tabs;
use yii\db\ActiveQuery;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var bool $filter
 * @var array $params
 * @var ActiveQuery $query
 * @var array $columns
 */

$this->title = Yii::t('app', 'Audit Trails');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="audit-trail-index">
    <div class="clearfix">
        <table width="100%">
            <tr>
                <td>
                    <h1 class="pull-left"><?= $this->title ?></h1>
                </td>
                <td>

                </td>
            </tr>
        </table>
        <hr class="hr">

        <div class="row">
            <?= Tabs::widget([
                'id' => 'relation-tabs',
                'encodeLabels' => false,
                'items' => [
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Product',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\Product',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\Product']),
                                'filter' => [
                                    'is_collection', 'product_number', 'barcode', 'product_group_id', 'product_name',
                                    'product_description', 'product_type_id', 'brand_id', 'manufacturer_id',
                                    'service_type_id', 'unit_size', 'unit_size_kg', 'unit_per_box', 'shelf_life',
                                    'shelf_life_unit', 'product_nature_id', 'sample_availability', 'annual_supply',
                                    'minimum_order', 'available_since', 'product_image', 'nutrition_image', 'nutrition_text',
                                ]
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => true,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Product Type',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\ProductType',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\ProductType']),
                                'filter' => [
                                    'icon', 'type_name'
                                ]
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Product Group',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\ProductGroup',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\ProductGroup']),
                                'filter' => [
                                    'product_group_name'
                                ]
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Brand',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\Brand',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\Brand']),
                                'filter' => ['brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_description', 'manufacturer_id', 'brand_logo', 'brand_video']
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Manufacturer',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\Manufacturer',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\Manufacturer']),
                                'filter' => ['manufacturer_name', 'manufacturer_location_latitude', 'manufacturer_location_longitude', 'manufacturer_logo', 'manufacturer_video', 'manufacturer_description']
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Batch',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\Batch',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\Batch']),
                                'filter' => ['manufacture_date', 'recalled_date', 'qr_code', 'product_id', 'batch_number']
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> App User',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\AppUser',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\AppUser']),
                                'filter' => ['first_name', 'last_name', 'email_address', 'mobile', 'country', 'is_active', 'gender', 'city', 'dob', 'state', 'address', 'zipcode']
                            ],
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Review & Rating',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\ReviewRating',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\ReviewRating']),
                                'filter' => ['user_id', 'brand_id', 'product_id', 'manufacturer_id', 'rating', 'review']
                            ],
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> Reported Review',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => 'app\models\ReportedReview',
                                'query' => AuditTrail::find()->where(['model' => 'app\models\ReportedReview']),
                                'filter' => ['user_id', 'rating_review_id']
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ],
                    [
                        'label' => '<span class="fa fa-asterisk"></span> All',
                        'content' => $this->render('../audit-trail/AuditGrid', [
                            'params' => [
                                'search_string' => $_GET,
                                'type' => '',
                                'query' => AuditTrail::find(),
                                'filter' => [
                                    'is_collection', 'product_number', 'barcode', 'product_group_id', 'product_name', 'product_description', 'product_type_id', 'brand_id', 'manufacturer_id',
                                    'service_type_id', 'unit_size', 'unit_size_kg', 'unit_per_box', 'shelf_life', 'shelf_life_unit', 'product_nature_id', 'sample_availability', 'annual_supply',
                                    'minimum_order', 'available_since', 'product_image', 'nutrition_image', 'nutrition_text', 'manufacture_date', 'recalled_date', 'qr_code', 'product_id', 'batch_number',
                                    'icon', 'type_name', 'product_group_name', 'brand_name', 'brand_location_latitude', 'brand_location_longitude', 'brand_description', 'manufacturer_id', 'brand_logo',
                                    'brand_video', 'manufacture_date', 'recalled_date', 'qr_code', 'product_id', 'batch_number', 'user_id', 'brand_id', 'product_id', 'rating', 'review', 'rating_review_id'

                                ]
                            ],
//
                            // which columns to show
                            'columns' => ['user_id', 'action', 'model', 'model_id', 'field', 'old_value', 'new_value', 'diff', 'created'],
                            // set to false to hide filter
                        ]),
                        'active' => false,
                    ]
                ]
            ]);

            ?>

        </div>
    </div>
</div>








