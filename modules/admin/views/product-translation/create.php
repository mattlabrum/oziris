<?php

use yii\helpers\Html;
use yii\helpers\Url;
use cornernote\returnurl\ReturnUrl;

/**
 * @var yii\web\View $this
 * @var app\models\ProductTranslation $model
 */

$this->title = Yii::t('app', 'Create Product Translation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Product Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-translation-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>

</div>
