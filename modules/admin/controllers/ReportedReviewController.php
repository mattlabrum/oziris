<?php

namespace app\modules\admin\controllers;
use app\models\ReportedReview;
use Yii;
use cornernote\returnurl\ReturnUrl;
use yii\httpclient\Client;

/**
 * This is the class for controller "app\modules\admin\controllers\ReportedReviewController".
 */
class ReportedReviewController extends \app\modules\admin\controllers\base\ReportedReviewController
{
    /**
     * Deletes an existing ReportedReview model.
     * Deletes the ReportedItem.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {


        $reportedReview = $this->findModel($id);
        $client = new Client();

        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(getenv('SERVER_TYPE').'/delete_comment')
            ->setData(
                [
                    'comment_id' => $reportedReview->ratingReview->comment_id,
                ])->send();

        ReportedReview::deleteAll(['rating_review_id' => $reportedReview->ratingReview->id ]);
        $reportedReview->ratingReview->delete();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Reported Review has been deleted.'));

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

}
