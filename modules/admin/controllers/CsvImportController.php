<?php

namespace app\modules\admin\controllers;

use app\utils\csvToArray;
use yii\web\Controller;
use Yii;
use yii\filters\AccessControl;



/**
 * TrackerController implements the CRUD actions for Package model.
 */
class CsvImportController extends Controller
{

    public $layout = '@app/views/layouts/main';
    private $array;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['upload-file','import-data']
                    ]
                ]
            ]
        ];
    }

    public function actionUploadFile()
    {
        $sourcePath = $_FILES['file']['tmp_name'];       // Storing source path of the file in a variable
        $targetPath = "dataimport/" . $_FILES['file']['name']; // Target path where file is to be stored
        return move_uploaded_file($sourcePath, $targetPath);
    }

    public function actionImportData()
    {
        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);
        return json_encode($csvArray);
    }

}
