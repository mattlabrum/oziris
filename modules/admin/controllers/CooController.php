<?php
/**
 *
 * @package default
 */
namespace app\modules\admin\controllers;
use yii\filters\AccessControl;

/**
 * This is the class for controller "CooController".
 */
class CooController extends \app\modules\admin\controllers\base\CooController
{
    public $layout = '@app/views/layouts/main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

}
