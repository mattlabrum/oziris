<?php

namespace app\modules\admin\controllers;
use yii\filters\AccessControl;

/**
 * This is the class for controller "app\modules\admin\controllers\BrandTranslationController".
 */
class BrandTranslationController extends \app\modules\admin\controllers\base\BrandTranslationController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete','get-translation-label'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionGetTranslationLabel(){
        $brandTranslation = $this->findModel($_POST);
        return $brandTranslation->brand_name;
    }


}
