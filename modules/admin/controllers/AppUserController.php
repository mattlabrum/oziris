<?php

namespace app\modules\admin\controllers;
use yii\filters\AccessControl;
use Yii;
use cornernote\returnurl\ReturnUrl;
use app\models\search\AppUserSearch;
use dmstr\bootstrap\Tabs;
use app\models\search\ScannedProductSearch;

/**
 * This is the class for controller "app\controllers\AppUserController".
 */
class AppUserController extends \app\modules\admin\controllers\base\AppUserController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','view','list-user-scans'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'update', 'change-user-status', 'list-user-scans', 'generate-csv'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * Enables/Disables an existing AppUser model.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeUserStatus($id)
    {
        if($this->findModel($id)->changeUserStatus()){
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'App User Status Changed Successfully.'));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'App User Status Change was Unsuccessful.'));
        }


       return $this->redirect('index');
    }

    /**
     * List Scanned Products of an AppUser model.
     * @param integer $id
     * @return mixed
     */
    public function actionListUserScans()
    {

        $params = $_GET;
        $searchModel = new ScannedProductSearch;
        $dataProvider = $searchModel->search($params);

        Tabs::clearLocalStorage();

        return $this->render('../scanned-product/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => null,
        ]);
    }
    public function actionGenerateCsv()
    {
        $searchModel = new AppUserSearch;

        $searchModel->first_name = (isset($_POST['first_name']) ? $_POST['first_name'] : '');
        $searchModel->last_name = (isset($_POST['last_name']) ? $_POST['last_name'] : '');
        $searchModel->dob = (isset($_POST['dob']) ? $_POST['dob'] : '');
        $searchModel->gender = (isset($_POST['gender']) ? $_POST['gender'] : '');
        $searchModel->email_address = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
        $searchModel->mobile = (isset($_POST['mobile']) ? $_POST['mobile'] : '');
        $searchModel->country = (isset($_POST['country']) ? $_POST['country'] : '');



        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);



        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            $arr = [];

            $arr['id'] = '"'.$object->id.'"';
            $arr['first_name'] = '"'.$object->first_name.'"';
            $arr['last_name'] = '"'.$object->last_name.'"';
            $arr['dob'] = '"'.$object->dob.'"';
            $arr['gender'] = '"'.$object->gender.'"';
            $arr['email_address'] = '"'.$object->email_address.'"';
            $arr['mobile'] = '"'.$object->mobile.'"';
            $arr['country'] = '"'.$object->country.'"';

            array_push($toCSVArray, $arr);
        }

        $fh = fopen('csv/appusers.csv', 'w') or die('Cannot open the file');
        $strHeader = 'ID,FirstName,LastName,DOB,Gender,EmailAddress,Mobile,Country';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);

        return $writeSuccessful;

    }

}
