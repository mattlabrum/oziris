<?php

namespace app\modules\admin\controllers;
use app\models\BatchIngredient;
use app\models\form\IngredientForm;
use app\models\IngredientGroup;
use app\models\IngredientTranslation;
use cornernote\returnurl\ReturnUrl;
use Yii;
use yii\filters\AccessControl;
use app\models\Ingredient;
use app\models\ProductIngredient;

/**
 * This is the class for controller "app\controllers\IngredientController".
 */
class IngredientController extends \app\modules\admin\controllers\base\IngredientController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'update-percentage' ,  'populate-ingredients-dropdown', 'add-product-ingredient', 'get-product-ingredients', 'delete-product-ingredient','get-ingredients-for-batch','populate-ingredient-groups-dropdown'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    public $ingredientTranslation;

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = IngredientTranslation::find()->where(['ingredient_id' => $id])->one();
        $ingredientModel = new IngredientForm();
        $ingredientModel->setIngredient($model);
        $ingredientModel->setIngredientTranslation($modelTranslation);
        return $this->render('view', ['model' => $ingredientModel]);
    }

    /**
     * Updates an existing Ingredient model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $ingredientModel = new IngredientForm();
        $ingredientModel->scenario = 'update';
        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['Ingredient'];
            if($model->save(true)){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Ingredient has been '.$ingredientModel->scenario.'d .'));
                if(Yii::$app->request->post()['IngredientTranslation']['language_id']!=''){
                    $this->ingredientTranslation = IngredientTranslation::find()->where(['ingredient_id' => $model->id, 'language_id' => $_POST['IngredientTranslation']['language_id'] ])->one();
                    if (isset($this->ingredientTranslation)) {
                        $this->ingredientTranslation->attributes = $_POST['IngredientTranslation'];
                        $this->ingredientTranslation->ingredient_id = $model->id;
                    } else {
                        $this->ingredientTranslation = new IngredientTranslation();
                        $this->ingredientTranslation->attributes = $_POST['IngredientTranslation'];
                        $this->ingredientTranslation->ingredient_id = $model->id;
                    }
                    if($this->ingredientTranslation->save(false)) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Ingredient has been '.$ingredientModel->scenario.'d and translated.'));
                    }
                }
                $ingredientModel->setIngredient($model);
                $ingredientModel->setIngredientTranslation($this->ingredientTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        $ingredientModel->setIngredient($model);
        $ingredientModel->setIngredientTranslation($this->ingredientTranslation);
        return $this->render('update', ['model' => $ingredientModel]);
    }

    /**
     * Creates a new Ingredient model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Ingredient;
        $ingredientModel = new IngredientForm();
        $ingredientModel->scenario = 'create';
        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['Ingredient'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Ingredient has been '.$ingredientModel->scenario.'d .'));
                if(Yii::$app->request->post()['IngredientTranslation']['language_id']!=''){
                    $this->ingredientTranslation = new IngredientTranslation();
                    $this->ingredientTranslation->attributes = $_POST['IngredientTranslation'];
                    $this->ingredientTranslation->ingredient_id = $model->id;
                    if($this->ingredientTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Ingredient has been '.$ingredientModel->scenario.'d and translated.'));
                    }
                }
                $ingredientModel->setIngredient($model);
                $ingredientModel->setIngredientTranslation($this->ingredientTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        return $this->render('create', ['model' => $ingredientModel]);
    }

    public function actionPopulateIngredientGroupsDropdown()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $groups = IngredientGroup::find()->all();
        $results = [];
        $results[] = ['id' => '', 'text' => ''];
        foreach ($groups as $group) {
            $results[] = ['id' => $group->id, 'text' => $group->name];
        }
        return ['results' => $results];
    }

    public function actionPopulateIngredientsDropdown()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ingredients = Ingredient::find()->where(['group_id' => $_POST['group_id']])->all();

        $results = [];
        $results[] = ['id' => '', 'text' => ''];
        foreach ($ingredients as $ingredient) {
            $results[] = ['id' => $ingredient->id, 'text' => $ingredient->name];
        }
        return ['results' => $results];
    }

    /**
     * Adds a new ProductIngredient  model.
     * @return mixed $productIngredient;
     */
    public function actionAddProductIngredient()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $ingredient_id = Ingredient::find()->where(['name' => $_POST['ingredient'] ])->one();

        if(!isset($ingredient_id)){
            $ingredient_id = $_POST['ingredient'];
        }

        $model = ProductIngredient::find()->where(['product_id' => $_POST['product_id'], 'ingredient_id' => $ingredient_id])->one();
        if(!isset($model)){
            $model = new ProductIngredient();
            $model->scenario = 'create';

            if(!is_numeric($_POST['group']) && $_POST['group']!=''){
                $ingredientGroup =IngredientGroup::find()->where(['name' => $_POST['group']])->one();
                if(!isset($ingredientGroup)){
                    $ingredientGroup = new IngredientGroup();
                    $ingredientGroup->name = $_POST['group'];
                    $ingredientGroup->save(false);
                }

            }

            if(!is_numeric($_POST['ingredient']) && $_POST['ingredient']!=''){
                $ingredient =Ingredient::find()->where(['name' => $_POST['ingredient']])->one();
                if(!isset($ingredient)){
                    $ingredient = new Ingredient();
                    $ingredient->name = $_POST['ingredient'];
                    $ingredient->group_id = (is_numeric($_POST['group']!='')) ? $_POST['group'] : $ingredientGroup->id;
                    $ingredient->save(false);
                }

            }
            if(isset($ingredientGroup) && isset($ingredient)){
                $model->product_id = $_POST['product_id'];
                $model->percentage = $_POST['percentage'];
                $model->ingredient_id = $ingredient->id;
                $model->save(false);
            }

            $productIngredient = [
                'id'=> $model->id,
                'ingredient' => $model->ingredient->name,
                'percentage' => $model->percentage,
                'ingredient-group' => $model->ingredient->group->name,
            ];
            return $productIngredient;
        }else{
            return false;
        }


    }

    /**
     * Deletes an existing OrderItem model.
     * @return boolean
     */
    public function actionDeleteProductIngredient()
    {
        return ProductIngredient::find()->where(['id'=>$_POST])->one()->delete();
    }


    /**
     * Edit Percentage Value .
     * @return boolean
     */
    public function actionUpdatePercentage()
    {
        $productIngredient =  ProductIngredient::find()->where(['id'=>$_POST['id']])->one();
        $productIngredient->percentage = $_POST['percentage'];

        return $productIngredient->save(false);
    }



    /**
     * Returns all existing ProductIngredient models.
     * @return ProductIngredient[]
     */
    public function actionGetProductIngredients()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $ingredients = ProductIngredient::findAll(['product_id' => $_POST['product_id']]);
        $productIngredients = [];
        foreach ($ingredients as $ingredient) {
            $productIngredients[] = [
                'id'=> $ingredient->id,
                'ingredient' => $ingredient->ingredient->name,
                'percentage' => $ingredient->percentage,
                'ingredient-group' => $ingredient->ingredient->group->name,
            ];
        }
        return ['ingredients' => $productIngredients];
    }


    public function actionError()
    {
        echo 'display this on error';
    }

    /**
     * Returns all existing ProductIngredient models.
     * @return ProductIngredient[]
     */
    public function actionGetIngredientsForBatch()
    {
        if($_POST['batch_id']>0){
            $ingredients = BatchIngredient::findAll(['batch_id' => $_POST['batch_id']]);
        }else{
            $ingredients = ProductIngredient::findAll(['product_id' => $_POST['product_id']]);
        }
        $productIngredients = [];
        foreach ($ingredients as $ingredient) {
            if($_POST['batch_id']>0){
                $batchIngredient = BatchIngredient::find()->where(['batch_id'=>$_POST['batch_id'], 'ingredient_id' =>$ingredient->ingredient_id])->one();
                    if(isset($batchIngredient)){
                        array_push($productIngredients,$batchIngredient);
                    }else{
                        $batchIngredient = new BatchIngredient;
                        $batchIngredient->scenario = 'create';
                        $batchIngredient->ingredient_id = $ingredient->ingredient_id;
                        $batchIngredient->percentage = $ingredient->percentage;
                        $batchIngredient->batch_id = $_POST['batch_id'];
                        array_push($productIngredients,$batchIngredient);
                    }
            }else{
                $batchIngredient = new BatchIngredient;
                $batchIngredient->scenario = 'create';
                $batchIngredient->ingredient_id = $ingredient->ingredient_id;
                $batchIngredient->percentage = $ingredient->percentage;
                $batchIngredient->batch_id = $_POST['batch_id'];
                array_push($productIngredients,$batchIngredient);
            }
        }
        if(count($productIngredients)){
            return $this->renderAjax('batch-ingredient', ['model' => $productIngredients]);
        }
    }


}
