<?php

namespace app\modules\admin\controllers;
use app\models\base\OrderStatusTime;
use app\models\Order;
use unyii2\imap\Mailbox;
use yii\httpclient\Client;
use yii\web\Controller;
use Yii;



/**
 * AddressController implements the CRUD actions for Address model.
 */
class CronController extends Controller
{

    public $layout = '@app/views/layouts/main';

    /**
     * Lists all Address models.
     * @return mixed
     */
    public function actionUpdateDeliveryStatus() {




        $mailbox = new Mailbox(yii::$app->imap->connection);
        $mailbox->readMailParts = false;
        $mailIds = $mailbox->searchMailBox('ALL');//  UNSEEN FROM uzair@digitalnoir.com.au  Prints all Mail ids.

        foreach($mailIds as $mail){
            if(isset(explode(',', $mailbox->getMail($mail)->subject)[1])){
                $ref = explode(',', $mailbox->getMail($mail,true)->subject)[1];
                $ref_str = (substr(trim($ref),0,3));
                if($ref_str == 'Ref'){
                    $order_id= (substr($ref,5,strlen($ref)));
                    $order = Order::find()->where(['woo_order_id' => $order_id])->one();
                    if(isset($order) && $order->status_id !=4 ){
                        $order->status_id = 4;
                        $order->updated_at = time();
                        if($order->save()){
                            $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $order->woo_order_id, 'status_id' => $order->status_id])->one();
                            $orderStatusTime->time = $order->updated_at;
                            $orderStatusTime->save();
                            print_r("saved -> </br>");
                            $client = new Client();
                            $client->createRequest()
                                ->setMethod('post')
                                ->setUrl(getenv('SERVER_TYPE').'/update_order_details')
                                ->setData(
                                    [
                                        'order_id' => $order->woo_order_id,
                                        'order_note' => $order->order_note,
                                        'order_status' => $order->status->name,
                                    ])->send();
                        }
                    }
                }
            }
        }

        $countnum = imap_num_msg($mailbox->getImapStream());
        if($countnum > 0) {
            $imapresult=imap_mail_move($mailbox->getImapStream(),'1:'.$countnum,'INBOX.processed');
            imap_expunge($mailbox->getImapStream());
        }


    }

}