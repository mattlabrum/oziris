<?php

namespace app\modules\admin\controllers;
use app\models\Brand;
use app\models\BrandTranslation;
use app\models\form\BrandForm;
use app\models\search\BrandSearch;
use app\models\upload\UploadFile;
use app\utils\csvToArray;
use cornernote\returnurl\ReturnUrl;
use dmstr\bootstrap\Tabs;
use Exception;
use Yii;
use yii\filters\AccessControl;
use yii\web\UploadedFile;


/**
 * This is the class for controller "app\modules\admin\controllers\BrandController".
 */
class BrandController extends \app\modules\admin\controllers\base\BrandController
{

    public $brandTranslation;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete','generate-csv', 'save-data', 'duplicate', 'change-status'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * Updates an existing Brand model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $brand_logo = $model->brand_logo;

        $brandModel = new BrandForm();
        $brandModel->scenario = 'update';

        if (Yii::$app->request->isPost) {
            $model->attributes = $_POST['Brand'];
            if ((isset($_FILES['Brand']['name']['brand_logo']) && ($_FILES['Brand']['name']['brand_logo'] != ''))) {
                $brand_logo_name = tempnam('uploads/brand', 'brand-');
                $brand_logo_name = substr($brand_logo_name, (strrpos($brand_logo_name, '/') + 1), strlen($brand_logo_name));
                if($brand_logo_name != 'default.jpg'){
                    unlink('uploads/brand/' . $brand_logo_name);
                }


                $image_upload_status = $this->uploadFile($model, $brand_logo_name);

                if (isset($image_upload_status['brand_logo_status'])) {
                    if (!$image_upload_status['brand_logo_status']) {
                        $model->brand_logo = $brand_logo;
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Brand Image Upload unsuccessful!'));
                    } else {
                        try {
                            if($brand_logo != 'default.jpg') {
                                unlink('uploads/brand/' . $brand_logo);
                            }
                        } catch (Exception $e) {

                        }
                        $model->brand_logo = $brand_logo_name . '.' . $image_upload_status['brand_logo_extension'];
                    }
                }
            }else{
                $model->brand_logo = $brand_logo;
            }

            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Brand has been ' . $brandModel->scenario . 'd .'));

                if ($_POST['BrandTranslation']['language_id'] != '') {
                    $this->brandTranslation = BrandTranslation::find()->where(['brand_id' => $model->id])->andWhere(['language_id' => $_POST['BrandTranslation']['language_id']])->one();

                    if (isset($this->brandTranslation)) {
                        $this->brandTranslation->attributes = $_POST['BrandTranslation'];
                        $this->brandTranslation->brand_id = $model->id;
                    } else {
                        $this->brandTranslation = new BrandTranslation();
                        $this->brandTranslation->attributes = $_POST['BrandTranslation'];
                        $this->brandTranslation->brand_id = $model->id;
                    }


                    if ($this->brandTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Brand has been updated and translated.'));
                    }
                }

                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }

       }
        $brandModel->setBrand($model);
        $brandModel->setBrandTranslation($this->brandTranslation);
        return $this->render('update', ['model' => $brandModel]);
    }

    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = BrandTranslation::findOne(['id' => $id]);
        if(!isset($modelTranslation)){
            $modelTranslation = new BrandTranslation();
        }
        $brandModel = new BrandForm();
        $brandModel->scenario = 'duplicate';

        if (Yii::$app->request->post()) {
            $model = new Brand();
            $model->attributes = $_POST['Brand'];
            $model->brand_logo = ((!isset($_FILES['Brand']['name']['brand_logo'])|| ($_FILES['Brand']['name']['brand_logo'] == '')) ? 'default.jpg' : $_FILES['Brand']['name']['brand_logo']);

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Brand has been '.$brandModel->scenario.'d .'));
                if(Yii::$app->request->post()['BrandTranslation']['language_id']!=''){
                    $modelTranslation->attributes = $_POST['BrandTranslation'];
                    $modelTranslation->brand_id= $model->id;
                    if($modelTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Brand  has been '.$brandModel->scenario.'d and translated.'));
                    }
                }

                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }

        }
        $brandModel->setBrand($model);
        $brandModel->setBrandTranslation($modelTranslation);

        return $this->render('duplicate', ['model' => $brandModel]);
    }

    private function uploadFile($mainModel, $brand_logo_name)
    {
        $brand_logo = new UploadFile();
        $brand_logo->upload_file = UploadedFile::getInstance($mainModel, 'brand_logo');
        try {
            $image_upload_status = [
                'brand_logo_status' => $brand_logo->upload('brand',$brand_logo_name),
                'brand_logo_extension' => $brand_logo->upload_file->extension,
            ];
            return $image_upload_status;
        } catch (Exception $e) {

        }
    }


    /**
     * Displays a single Brand model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        Tabs::rememberActiveState();

        $model = $this->findModel($id);
        $modelTranslation = BrandTranslation::findOne(['id' => $id]);
        $brandModel = new BrandForm();//$this->findModel($id);
        $brandModel->setBrand($model);
        $brandModel->setBrandTranslation($modelTranslation);
        return $this->render('view', ['model' => $brandModel]);
    }

    public function actionCreate()
    {
        $brand = new Brand();
        $brandTranslation = new BrandTranslation();
        if (Yii::$app->request->post()) {
            $brand->attributes = $_POST['Brand'];

            $brand_logo_name = tempnam( 'uploads/brand' , 'brand-' );
            $brand_logo_name = substr($brand_logo_name,(strrpos($brand_logo_name,'/')+1),strlen($brand_logo_name));
            if($brand_logo_name != 'default.jpg') {
                unlink('uploads/brand/' . $brand_logo_name);
            }


            $image_upload_status = $this->uploadFile($brand,$brand_logo_name);

            if(isset($image_upload_status['brand_logo_status'])) {
                if (!$image_upload_status['brand_logo_status']) {
                    if((isset($_FILES['Brand']['name']['brand_logo']) && ($_FILES['Brand']['name']['brand_logo'] != ''))){
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Brand Image Upload unsuccessful!'));
                    }
                }else{

                    $brand->brand_logo = $brand_logo_name.'.'.$image_upload_status['brand_logo_extension'];
                }
            }

            if(($_FILES['Brand']['name']['brand_logo'] == '')){
                $brand->brand_logo = 'default.jpg';
            }

            if($brand->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Brand has been created.'));
                if($_POST['BrandTranslation']['language_id']!=''){
                    $brandTranslation->attributes = $_POST['BrandTranslation'];
                    $brandTranslation->brand_id= $brand->id;

                    if($brandTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Brand  has been Created and translated.'));
                    }
                }
                return $this->redirect(['view', 'id' => $brand->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }

        $brandModel = new BrandForm();
        $brandModel->scenario = 'create';
        $brandModel->setBrand($brand);
        $brandModel->setBrandTranslation($brandTranslation);

        return $this->render('create', ['model' => $brandModel]);
    }

    /**
     * Searches all products according to search criteria and generates CSV.
     * @return CSV Download link
     */
    public function actionGenerateCsv()
    {
        $searchModel = new BrandSearch();

        $searchModel->brand_name = (isset($_POST['brand_name']) ? $_POST['brand_name'] : '');
        $searchModel->brand_logo = (isset($_POST['brand_logo']) ? $_POST['brand_logo'] : '');
        $searchModel->brand_description = (isset($_POST['brand_description']) ? $_POST['brand_description'] : '');
        $searchModel->brand_location_latitude = (isset($_POST['brand_location_latitude']) ? $_POST['brand_location_latitude'] : '');
        $searchModel->brand_location_longitude = (isset($_POST['brand_location_longitude']) ? $_POST['brand_location_longitude'] : '');
        $searchModel->manufacturer_id = (isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);



        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            $arr = [];

            $arr['id'] = '"'.$object->id.'"';
            $arr['brand_name'] = '"'.$object->brand_name.'"';
            $arr['brand_description'] = '"'.$object->brand_description.'"';
            $arr['brand_location_latitude'] = '"'.$object->brand_location_latitude.'"';
            $arr['brand_location_longitude'] = '"'.$object->brand_location_longitude.'"';
            $arr['brand_logo'] = '"'.$object->brand_logo.'"';
            $arr['manufacturer_id'] = '"'.$object->manufacturer_id.'"';

            array_push($toCSVArray, $arr);
        }

        $fh = fopen('csv/brands.csv', 'w') or die('Cannot open the file');
        $strHeader = 'ID,BrandName,BrandDescription,BrandLocationLatitude,BrandLocationLongitude,BrandLogo,BrandManufacturer';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);

        return $writeSuccessful;

    }

    public function actionSaveData()
    {

        $brandsToSave = [];
        $errorString = 'Brands Were not Imported! Please check the CSV';
        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);
//        $csvArray = csvToArray::csvToArray('dataimport/batches.csv');
        foreach ($csvArray as $brand) {

            if (isset($brand['ID']) && $brand['ID'] > 0 && isset(Brand::findOne($brand['ID'])->id)) {
                $brandToSave = Brand::findOne($brand['ID']);
                $this->populateBrandData($brand,$brandToSave);
            } else {
                $brandToSave = new Brand();
                $this->populateBrandData($brand,$brandToSave);
            }
            array_push($brandsToSave, $brandToSave);
        }


        if (count($brandsToSave) == count($csvArray)) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($brandsToSave as $brandToSave) {
                    if(!$brandToSave->save()){
                        throw new Exception('Unable to save record.');
                    }
                }
                $transaction->commit();
                $errorString = count($brandsToSave) . " Brands Imported Successfully!";
                return $errorString;
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
                $transaction->rollback();
                return $this->redirect('index');
            }
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
            return $this->redirect('index');
        }
    }

    function populateBrandData($brand,$brandToSave){
        $brandToSave->brand_name= $brand['BrandName'];
        $brandToSave->brand_logo=$brand['BrandLogo'];
        $brandToSave->manufacturer_id=$brand['BrandManufacturer'];
        $brandToSave->brand_description=$brand['BrandDescription'];
        $brandToSave->brand_location_latitude=$brand['BrandLocationLatitude'];
        $brandToSave->brand_location_longitude=$brand['BrandLocationLongitude'];
        return $brandToSave;
    }

    /**
     * Enables/Disables an existing Brand model.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeStatus($id)
    {


        $this->findModel($id)->changeStatus();
        return $this->redirect('index');
    }

}
