<?php

namespace app\modules\admin\controllers\api\services;


use app\models\AppAuthDatum;
use app\models\AppLoginToken;
use app\models\AppSocialMediaAuth;
use app\models\AppUser;
use app\models\AppUserDeviceInfo;
use app\utils\AuthTokenGenerator;
use dmstr\helpers\Html;
use Exception;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;

class UserManagement
{
    public function setBestonMarketPlacePassword($email_address,$password){
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(getenv('SERVER_TYPE').'/update_user_info')
            ->setData(
                [
                    'password' => $password,
                    'email_address' => $email_address,
                ])->send();
        $app_user = AppUser::find()->where(['email_address' => $email_address])->one();
        if(isset($app_user)){
            $app_user_auth = new AppAuthDatum();
            $app_user_auth->deleteAll(['user_id' => $app_user->id]);
            $app_user_auth->user_id = $app_user->id;
            $app_user_auth->password_hash = password_hash($password, PASSWORD_BCRYPT);
            $app_user_auth->save();
        }
        return $response->isOk;
    }

    public function forgetPasswordEmail($email_address)
    {
        $app_user = AppUser::find()->where(['email_address' => $email_address])->one();

        if (isset($app_user)) {

            $app_user_auth = AppAuthDatum::findOne(['user_id' => $app_user->id]);

            if(isset($app_user_auth)){
                $app_user_auth->delete();
            }

            $app_user_auth = new AppAuthDatum();

            $password = AuthTokenGenerator::generateAuthToken(6);

            $app_user_auth->user_id = $app_user->id;
            $app_user_auth->password_hash = password_hash($password, PASSWORD_BCRYPT);

            if($app_user_auth->save()==true && $this->setBestonMarketPlacePassword($email_address,$password)==true){

                $message = "<!DOCTYPE html>
<html>
<body>

<table style='width:564px'>
  <tr>
    <td><img src='https://gallery.mailchimp.com/856292cc8c597f4afb5cb6186/images/55eb0813-603c-4908-9421-4f75f5a80d21.png' alt='Oziris, Intelligent Secure' style='width:560px;height:180px;'></td>
  </tr>
  <tr>
    <td ><h2><font face='arial'>Oziris & Beston Market Place Password Reset</font></h2></td>
  </tr>
    <tr>
    <td><font face='arial'>Please use the password below. We recommend changing this once you log back in.
    <br><br>
    <b>$password</b>
    <br>Thanks,
    <br><br>
                Beston Technologies Team.
    <br><br>
    <hr>
    <br><br>
                Copyright © Beston Technologies All rights reserved.
    <br><br>
                Contact us at:
	<br>
	<a href='mailto:wecare@oziris.com.au'>wecare@oziris.com.au</a>

	<br>
	<a href='http://www.oziris.com.au/'>www.oziris.com.au</a></font>
    </td>
  </tr>
</table>

</body>
</html>";

                try{
                    \Yii::$app->mailer->compose(null)
                        ->setFrom(['noreply@bestonmarketplace.com.au' => 'Beston Technologies'])
                        ->setTo($email_address)
                        ->setSubject('Oziris & Beston Market Place Password Reset!' )
                        ->setHtmlBody($message)
                        ->send();

                }catch( InvalidConfigException $ex){
                    return true;
                }
                return true;
            }
        }else{
            return false;
        }
    }


    public function registerUserDeviceToApp(){
        $device_info = AppUserDeviceInfo::find()->where(['device_token' => $_POST['device_token']])->one();
        if(!isset($device_info)){
            $device_info = new AppUserDeviceInfo();
        }
        $device_info->attributes = $_POST;
        return $device_info->save();
    }

    public function getUser($user_id)
    {
        $app_user = AppUser::find()->where(['id' => $user_id])->one();
        return (isset($app_user) ? $app_user : false);
    }

    public function checkValidUserEmail($email_address)
    {
        $app_user = AppUser::find()->where(['email_address' => $email_address])->one();
        return (isset($app_user) ? false : true);
    }

    public function insertNewUser($user)
    {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        try {
            $app_user = new AppUser();
            $app_user->attributes = $user;

            if (!$app_user->save()) {
                throw new Exception('Unable to save record.');
            }
            $app_user_auth = new AppAuthDatum();
            $app_user_auth->user_id = $app_user->id;
            $app_user_auth->password_hash = password_hash($user['password'], PASSWORD_BCRYPT);


            if (!$app_user_auth->save()) {
                throw new Exception('Unable to save record.');
            }
            $transaction->commit();
            return $this->createUserOnBestonMarketplace($user);
            //return true;
        } catch (Exception $e) {
            $transaction->rollback();
            return false;
        }
    }

    public function createUserOnBestonMarketplace($user){

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(getenv('SERVER_TYPE').'/save_user_info')
            ->setData(
                [
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'mobile' => $user['mobile'],
                    'password' => $user['password'],
                    'billing_country' => $user['country'],
                    'email_address' => $user['email_address'],
                ])->send();
        return $response->isOk;
    }

    public function insertSocialUserAccount($user)
    {

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $app_user = new AppUser();
            $app_user->attributes = $user;

            if (!$app_user->save()) {

                throw new Exception('Unable to save record.');
            }
            $app_social_auth = new AppSocialMediaAuth();
            $app_social_auth->user_id = $app_user->id;


            $app_social_auth->provider = (isset($user['provider']) ? $user['provider'] : '');;
            $app_social_auth->access_id = (isset($user['access_id']) ? $user['access_id'] : '');
            $app_social_auth->access_token = (isset($user['access_token']) ? $user['access_token'] : '');
            $app_social_auth->expiration_date = (isset($user['expiration_date']) ? $user['expiration_date'] : '');;


            if (!$app_social_auth->save()) {
                throw new Exception('Unable to save record.');
            }
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollback();
        }
        return false;
    }

    public function activateUser($user_id)
    {
        $user = AppUser::findOne(['id'=>$user_id]);

        if (isset($user)){
            $user->is_active = 1;
            if($user->save()){
                return true;
            }else{
                if($user->hasErrors()){
                    return Html::errorSummary($user,['encode' => false]);
                }
            }



        }else{
            return 'User Not Found!';
        }



    }

    public function updateUser($user)
    {
        $app_user = AppUser::findOne(['id' => $user['user_id']]);
        if (isset($app_user)) {
            if (($app_user->email_address == $user['email_address'])) {
                $app_user->attributes = $user;
                if ($app_user->save()) {
                    if ($user['password'] != '') {
                        $app_user_auth = new AppAuthDatum();
                        $app_user_auth->deleteAll(['user_id' => $app_user->id]);
                        $app_user_auth->user_id = $app_user->id;
                        $app_user_auth->password_hash = password_hash($user['password'], PASSWORD_BCRYPT);
                        $this->setBestonMarketPlacePassword($user['email_address'],$user['password']);
                        return $app_user_auth->save();
                    }
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return "User Not Found!";
        }
    }


    public function validateSocialMediaAuthData($user_auth_data)
    {
        $user_auth = AppSocialMediaAuth::findOne(['access_id' => $user_auth_data['access_id']]);
        if (isset($user_auth)) {

            if($user_auth->access_token != $user_auth_data['access_token']) {
                $user_auth->access_token = $user_auth_data['access_token'];
                $user_auth->expiration_date = $user_auth_data['expiration_date'];
                $user_auth->save();
            }
                $login_token = new AppLoginToken();
                // delete all old tokens
                $login_token->deleteAll(['user_id' => $user_auth->user_id]);

                // Generate and save new token
                $login_token->token = AuthTokenGenerator::generateAuthToken();
                $login_token->user_id = $user_auth->user_id;
                if ($login_token->save()) {
                    return $login_token;
                }
        } else {
            return false;
        }
    }

    public function validateUserAppAuthData($user_auth_data)
    {
        $user = AppUser::find()->where(['email_address' => $user_auth_data['email_address']])->andWhere('deleted_at = 0')->one();
        if (isset($user)) {
            $user_auth = AppAuthDatum::findOne(['user_id' => $user->id]);
            if (isset($user_auth)) {
                if (password_verify($user_auth_data['password'], $user_auth->password_hash)) {
                    $login_token = new AppLoginToken();
                    // delete all old tokens
                    $login_token->deleteAll(['user_id' => $user_auth->user_id]);

                    // Generate and save new token
                    $login_token->token = AuthTokenGenerator::generateAuthToken();
                    $login_token->user_id = $user_auth->user_id;
                    if ($login_token->save()) {
                        return $login_token;
                    }

                } else {
                    return false;
                }
            } else {
                return false;
            }
        }else{
            return false;
        }
    }


}