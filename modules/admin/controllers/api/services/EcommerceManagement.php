<?php

namespace app\modules\admin\controllers\api\services;

use app\models\AppUser;
use app\models\base\Address;
use app\models\base\AppUserDeviceInfo;
use app\models\base\ProductLocationAttribute;
use app\models\OrderStatus;
use app\models\OrderStatusTime;
use app\models\ProductType;
use app\models\Currency;
use app\models\Order;
use app\models\OrderCoupon;
use app\models\OrderItem;
use app\models\Payment;
use app\models\Product;
use app\models\Response;
use app\models\Shipping;
use app\models\Tax;
use app\utils\BarcodeGenerator;
use app\utils\FetchContinentFromCountryCode;
use app\utils\PushNotificationSender;
use DateTime;
use Tinify\Tinify;
use WC_API_Client;
use WC_API_Client_Exception;
use WC_API_Client_HTTP_Exception;
use yii\base\InvalidConfigException;
use yii\helpers\Url;

class EcommerceManagement
{
    public function logObject($ob){
        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $ob,
        ];
        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }
    public function getAllProducts()
    {
        $products = Product::findAll(['deleted_at'=>null]);
        $response = new Response();
        foreach($products as $product){
            $translated_products[] = [
                'id' => $product->id,
                'ProductGroup' => $product->productGroup->product_group_name,
                'ProductName' => $product->product_name,
                'ProductDescription' => $product->product_description,
                'ProductType' => $product->productType->type_name,
                'Manufacturer' => $product->manufacturer->manufacturer_name,
                'UnitSize' => $product->unit_size,
                'UnitSizeKg' => $product->unit_size_kg,
                'ProductImage' => 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->product_image,
                'NutritionImage' => 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->nutrition_image,
                'NutritionText' => $product->nutrition_text,
            ];
         }
        $response->setResponse($translated_products);
        return $response->getFullResponse() ;
    }
    public function updateBmpQuantities()
    {
        $products = Product::findAll(['deleted_at'=>null,'ecommerce_enabled'=>1]);
        foreach($products as $product){
            $quantities[] = [
                'id' => $product->id,
                'quantity' => $product->productQuantity
            ];
        }
        return $quantities;
    }
    public function updateBmpProductDetails()
    {
        $response = new Response();
        $product = Product::find()->where(['id'=> $_POST['product_id']])->one();
            $translated_product[] = [
                'id' => $product->id,
                'ecommerce_enabled' => $product->getEcommerceEnabled(2),
                'is_beston_pure_product' => $product->is_beston_pure_product,
                'product_group_show_on_menu' => $product->productGroup->show_on_menu,
                'product_type_show_on_menu' => $product->productType->show_on_menu,
                'preorder_enabled' => $product->preorder_enabled,
                'is_top_seller' => $product->is_top_seller,
                'is_featured' => $product->is_featured,
                'ProductTypeIcon' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/type').'/'.$product->productType->icon,
                'ProductThumbnailImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product/thumbnail').'/'.$product->product_image,
                'UnitSize' => $product->unit_size,
                'UnitSizeKg' => $product->unit_size_kg,
                'ProductImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->product_image,
                'ProductRelatedImage' => $product->getProductRelatedImages(),
                'ProductQrCode' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/qrcode').'/'.$product->getDefaultBatch()->qr_code_image,
                'NutritionImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->nutrition_image,
                'ServesPerPack' => $product->serves_per_pack,
                'ServeSize' => $product->serve_size,
                'Price' => $product->getProductPrice(2),
                'Quantity' => $product->getBestonQuantity(2),
                'Taxable' => $product->getProductTaxable(2),
                'Ingredients' => $product->getIngredients(),
                'NutritionTags' => $product->getNutritionTags(),
                'en_CollectionItems' => $product->getCollectionItems(2),
                'ch_CollectionItems' => $product->getCollectionItems(1),
                'Dimentions' => [
                    'length' => $product->length,
                    'width' => $product->width,
                    'height' => $product->height,
                ],
                'Nutrition' => $product->getNutrition(),
                'en_ProductType' => $product->productType->type_name,
                'en_ProductGroup' => ($product->productGroup->getGroupTranslation(2) != false ? trim($product->productGroup->getGroupTranslation(2)->product_group_name) : trim($product->productGroup->product_group_name)),
                'en_ProductName' => ($product->getProductTranslation(2) != false ? trim($product->getProductTranslation(2)->product_name) : trim($product->product_name)),
                'en_ProductDescription' => ($product->getProductTranslation(2) != false ? trim($product->getProductTranslation(2)->product_description) : trim($product->product_description)),
                'en_Manufacturer' => trim($product->manufacturer->getManufacturerTranslation(2)->manufacturer_name),
                'en_ManufacturerDescription' => trim($product->manufacturer->getManufacturerTranslation(2)->manufacturer_description),
                'en_Brand' => trim($product->brand->getBrandTranslation(2)->brand_name),
                'en_BrandDescription' => trim($product->brand->getBrandTranslation(2)->brand_description),
                'BrandImage' => $product->brand->getBrandLogo(),
                'BrandLatitude' => $product->brand->brand_location_latitude,
                'BrandLongitude' => $product->brand->brand_location_longitude,
                'en_NutritionText' => ($product->getProductTranslation(2) != false ? trim($product->getProductTranslation(2)->nutrition_text) : trim($product->nutrition_text)),
                'ch_ProductType' => $product->productType->type_name,
                'ch_ProductGroup' => ($product->productGroup->getGroupTranslation(1) != false ? trim($product->productGroup->getGroupTranslation(1)->product_group_name) : trim($product->productGroup->product_group_name)),
                'ch_ProductName' => ($product->getProductTranslation(1) != false ? trim($product->getProductTranslation(1)->product_name) : trim($product->product_name)),
                'ch_ProductDescription' => ($product->getProductTranslation(1) != false ? trim($product->getProductTranslation(1)->product_description) : trim($product->product_description)),
                'ch_Brand' => trim($product->brand->getBrandTranslation(1)->brand_name),
                'ch_BrandDescription' => trim($product->brand->getBrandTranslation(1)->brand_description),
                'ch_Manufacturer' => trim($product->manufacturer->getManufacturerTranslation(1)->manufacturer_name),
                'ch_ManufacturerDescription' => trim($product->manufacturer->getManufacturerTranslation(1)->manufacturer_description),
                'ch_NutritionText' => ($product->getProductTranslation(1) != false ? trim($product->getProductTranslation(1)->nutrition_text) : trim($product->nutrition_text)),
                'reviews' => $product->getProductReviews(),
            ];
        if(isset($translated_product)){
            $response->setResponse($translated_product);
        }else{
            $response->setResponse(false);
            $response->setMessage('Something went wrong, please try again');
        }
        return $response->getFullResponse();
    }

    public function getEcommerceEnabledProducts()
    {
        $products = Product::findAll(['deleted_at'=>null,'ecommerce_enabled'=>1]);
        $response  = new Response();
        foreach($products as $product){
            $translated_products[] = [
                'id' => $product->id,
                'ecommerce_enabled' => $product->ecommerce_enabled,
                'preorder_enabled' => $product->preorder_enabled,
                'is_beston_pure_product' => $product->is_beston_pure_product,
                'is_top_seller' => $product->is_top_seller,
                'is_featured' => $product->is_featured,
                'UnitSize' => $product->unit_size,
                'UnitSizeKg' => $product->unit_size_kg,
                'ProductImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->product_image,
                'ProductThumbnailImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product/thumbnail').'/'.$product->product_image,
                'ProductRelatedImage' => $product->getProductRelatedImages(),
                'ProductQrCode' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/qrcode').'/'.$product->getDefaultBatch()->qr_code_image,
                'NutritionImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->nutrition_image,
                'ServesPerPack' => $product->serves_per_pack,
                'ServeSize' => $product->serve_size,
                'Price' => $product->getProductPrice(1),
                'Quantity' => $product->getBestonQuantity(1),
                'Ingredients' => $product->getIngredients(),
                'NutritionTags' => $product->getNutritionTags(),
                'en_CollectionItems' => $product->getCollectionItems(0),
                'ch_CollectionItems' => $product->getCollectionItems(1),
                'en_ProductName' => ($product->getProductTranslation(0) != false ? trim($product->getProductTranslation(0)->product_name) : trim($product->product_name)),
                'en_ProductDescription' => ($product->getProductTranslation(0) != false ? trim($product->getProductTranslation(0)->product_description) : trim($product->product_description)),
                'ch_ProductName' => ($product->getProductTranslation(1) != false ? trim($product->getProductTranslation(1)->product_name) : trim($product->product_name)),
                'ch_ProductDescription' => ($product->getProductTranslation(1) != false ? trim($product->getProductTranslation(1)->product_description) : trim($product->product_description)),
                'en_NutritionText' => ($product->getProductTranslation(0) != false ? trim($product->getProductTranslation(0)->nutrition_text) : trim($product->nutrition_text)),
                'ch_NutritionText' => ($product->getProductTranslation(1) != false ? trim($product->getProductTranslation(1)->nutrition_text) : trim($product->nutrition_text)),
                'ProductType' => [
                    'product_type_id' => $product->productType->id,
                    'product_type_icon' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/type').'/'.$product->productType->icon,
                    'en_ProductType' => $product->productType->getProductTypeTranslation(0)->type_name,
                    'ch_ProductType' => $product->productType->getProductTypeTranslation(1)->type_name,
                    'product_type_show_on_menu' => $product->productType->show_on_menu,
                ],
                'ProductGroup' => [
                    'product_group_id' => $product->productGroup->id,
                    'product_group_show_on_menu' => $product->productGroup->show_on_menu,
                    'en_ProductGroup' => ($product->productGroup->getGroupTranslation(0) != false ? trim($product->productGroup->getGroupTranslation(0)->product_group_name) : trim($product->productGroup->product_group_name)),
                    'ch_ProductGroup' => ($product->productGroup->getGroupTranslation(1) != false ? trim($product->productGroup->getGroupTranslation(1)->product_group_name) : trim($product->productGroup->product_group_name)),
                ],
                'Manufacturer' => [
                    'manufacturer_id' => trim($product->manufacturer->id),
                    'en_Manufacturer' => trim($product->manufacturer->getManufacturerTranslation(0)->manufacturer_name),
                    'en_ManufacturerDescription' => trim($product->manufacturer->getManufacturerTranslation(0)->manufacturer_description),
                    'ch_Manufacturer' => trim($product->manufacturer->getManufacturerTranslation(1)->manufacturer_name),
                    'ch_ManufacturerDescription' => trim($product->manufacturer->getManufacturerTranslation(1)->manufacturer_description),
                ],
                'Brand' => [
                    'brand_id' => trim($product->brand->id),
                    'en_Brand' => trim($product->brand->getBrandTranslation(0)->brand_name),
                    'en_BrandDescription' => trim($product->brand->getBrandTranslation(0)->brand_description),
                    'ch_Brand' => trim($product->brand->getBrandTranslation(1)->brand_name),
                    'ch_BrandDescription' => trim($product->brand->getBrandTranslation(1)->brand_description),
                    'BrandImage' => $product->brand->getBrandLogo(),
                    'BrandLatitude' => $product->brand->brand_location_latitude,
                    'BrandLongitude' => $product->brand->brand_location_longitude,
                ],
                'Dimentions' => [
                    'length' => $product->length,
                    'width' => $product->width,
                    'height' => $product->height,
                ],
                'Nutrition' => $product->getNutrition(),
            ];
        }
        if(isset($translated_products)){
            $response->setResponse($translated_products);
//            return $translated_products;
        }else{
            $response->setResponse(false);
            $response->setMessage('Something went wrong, please try again');
        }
        return $response->getFullResponse();
    }
    public function setAddresses()
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->where(['shipping_address_id'=> null, 'billing_address_id'=> null ])->all();
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id,['filter[meta]' => 'true'])->order;
//return $order;
                $shipping_address = new Address();
                $shipping_address->address_1 = $order->shipping_address->address_1;
                $shipping_address->address_2 = $order->shipping_address->address_2;
                $shipping_address->type = 'shipping';
                $shipping_address->suburb = $order->shipping_address->city;
                $shipping_address->state = $order->shipping_address->state;
                $shipping_address->postcode = $order->shipping_address->postcode;
                $shipping_address->country = $order->shipping_address->country;
                $shipping_address->first_name = $order->shipping_address->first_name;
                $shipping_address->last_name = $order->shipping_address->last_name;
                $shipping_address->save(false);

                $billing_address = new Address();
                $billing_address->address_1 = $order->billing_address->address_1;
                $billing_address->address_2 = $order->billing_address->address_2;
                $billing_address->type = 'billing';
                $billing_address->first_name = $order->billing_address->first_name;
                $billing_address->contact_no = $order->billing_address->phone;
                $billing_address->email = $order->billing_address->email;
                $billing_address->last_name = $order->billing_address->last_name;
                $billing_address->suburb = $order->billing_address->city;
                $billing_address->state = $order->customer->billing_address->state;
                $billing_address->postcode = $order->customer->billing_address->postcode;
                $billing_address->country = $order->customer->billing_address->country;
                $billing_address->save(false);
                $live_order->shipping_address_id = $shipping_address->id;
                $live_order->billing_address_id = $billing_address->id;
                $live_order->save();
            }
        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function setOrderCreateDate()
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->all();
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id,['filter[meta]' => 'true'])->order;
                $order_date = new DateTime($order->created_at);
                $live_order->created_at = ($order_date->getTimestamp());
                $live_order->save();
            }
        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function setCouponData()
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->all();
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id)->order;
                if(count($order->coupon_lines) > 0) {
                    foreach($order->coupon_lines as $line_coupon){
                        $coupon = new OrderCoupon();
                        $coupon->code = $line_coupon->code;
                        $coupon->discount_amount = $line_coupon->amount;
                        $coupon->order_id = $order->order_number;
                        $coupon->save(false) ;
                    }
                }
            }
        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function importOrderFromWooCommerce($order_number)
    {
            $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
            $order = $client->orders->get($order_number,['filter[meta]' => 'true'])->order;
//            return $order;
            $old_order = Order::find()->where(['woo_order_id' =>$order->order_number])->one();
            if(isset($old_order)){
                return;
            }
            $tes_order = new Order();
            $tes_order->woo_order_id = $order->order_number;
            $tes_order->status_id = OrderStatus::find()->where(['like', 'name', $order->status])->one()->id;
            $tes_order->shipping_id = Shipping::find()->where(['name' => $order->shipping_lines[0]->method_title])->one()->id;

            $allStatuses = OrderStatus::find()->all();
            foreach ($allStatuses as $status) {
                $statusTime = new OrderStatusTime();
                $statusTime->order_id = $order->order_number;
                $statusTime->status_id = $status->id;
                $statusTime->sort = $status->sort;
                $statusTime->save();
            }

            $order_date = new DateTime($order->created_at);
            $tes_order->created_at = ($order_date->getTimestamp());

            $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $order->order_number, 'status_id' => $tes_order->status_id])->one();
            $orderStatusTime->time = $tes_order->created_at;
            $orderStatusTime->save();

            if(count($order->coupon_lines) > 0) {
                foreach($order->coupon_lines as $line_coupon){
                    $coupon = new OrderCoupon();
                    $coupon->code = $line_coupon->code;
                    $coupon->discount_amount = $line_coupon->amount;
                    $coupon->order_id = $order->order_number;
                    $coupon->save(false) ;
                }
            }

            $order_number_length = strlen($order->order_number);
            $l = 6-$order_number_length;

            $str_ord_number = '';
            for($i=0; $i<$l; $i++  ){
                $str_ord_number.= '0';
            }
            $tes_order->barcode = '1 9350884'.$str_ord_number.$order->order_number;
            BarcodeGenerator::barcode(preg_replace('/\s+/', '', 'order/barcode/'.$tes_order->barcode).'.png', $tes_order->barcode, "200", "horizontal" ,$code_type="code128" , false, 4 );
            $tes_order->currency_id = Currency::find()->where(['like', 'name', $order->currency])->one()->id;
            $tes_order->total_amount = $order->total;
            $tes_order->subtotal = $order->subtotal;
            $payment = Payment::find()->where(['name' => $order->payment_details->method_title])->one();
            $tes_order->payment_id = ((isset($payment)) ? $payment->id : null);
            $tes_order->order_note = $order->note;
            $tes_order->customer_ip = $order->customer_ip;
            $tes_order->customer_user_agent = $order->customer_user_agent;
            $user = AppUser::find()->where(['email_address' => $order->customer->email])->one();
            if(isset($user)){
                $tes_order->customer_id = $user->id;
            }else{
                $user = AppUser::find()->where(['email_address' => 'default'])->one();
                $tes_order->customer_id = $user->id;
            }
            $shipping_address = new Address();
            $shipping_address->address_1 = $order->shipping_address->address_1;
            $shipping_address->address_2 = $order->shipping_address->address_2;
            $shipping_address->type = 'shipping';
            $shipping_address->suburb = $order->shipping_address->city;
            $shipping_address->state = $order->shipping_address->state;
            $shipping_address->postcode = $order->shipping_address->postcode;
            $shipping_address->country = $order->shipping_address->country;
            $shipping_address->continent = FetchContinentFromCountryCode::fetchContinentFromCountryCode($order->shipping_address->country);
            $shipping_address->first_name = $order->shipping_address->first_name;
            $shipping_address->last_name = $order->shipping_address->last_name;
            $shipping_address->save(false);

            $billing_address = new Address();
            $billing_address->address_1 = $order->billing_address->address_1;
            $billing_address->address_2 = $order->billing_address->address_2;
            $billing_address->type = 'billing';
            $billing_address->first_name = $order->billing_address->first_name;
            $billing_address->contact_no = $order->billing_address->phone;
            $billing_address->email = $order->billing_address->email;
            $billing_address->last_name = $order->billing_address->last_name;
            $billing_address->suburb = $order->billing_address->city;
            $billing_address->state = $order->billing_address->state;
            $billing_address->postcode = $order->billing_address->postcode;
            $billing_address->country = $order->billing_address->country;
            $billing_address->save(false);

            $tes_order->billing_address_id = $billing_address->id;
            $tes_order->shipping_address_id = $shipping_address->id;

            $tes_order->tax_id = 1;
            $tes_order->tax_amount = $order-> total_tax;

            foreach ($order->line_items as $item){
                $order_item = new OrderItem();
                $product_quantity = ProductLocationAttribute::find()->where(['product_id' => $item->sku])->one();

                if(isset($product_quantity)){
                    $product_quantity->beston_quantity -= $item->quantity;
                    $product_quantity->save();
                    if ($product_quantity->beston_quantity < $product_quantity->reorder_threshold){
                        $this->sendReorderNotification(Product::find(['id' =>$item->sku])->one(),$product_quantity->beston_quantity ,$product_quantity->reorder_threshold );
                    }
                }
                $order_item->product_id = $item->sku;
                $order_item->order_id = $tes_order->woo_order_id;
                $order_item->quantity = $item->quantity;
                $order_item->save(false);
            }
            $tes_order->generateQrCode();
            $tes_order->leave_at_door = $order->order_meta->leave_at_door;
            $tes_order->boxes = $order->order_meta->enviro_box_count;
//            return $tes_order;
            return $tes_order->save(false);
        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;

            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function sendReorderNotification($product,$qty,$reorder_qty){
        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $from = "admin@bestonmarketplace.com.au";
        $to = "uzair@digitalnoir.com.au";
//        $to = "uzair@digitalnoir.com.au ; lruizperez@bestonglobalfoods.com.au ; jchristensen@bestonglobalfoods.com.au";
        $subject = "Product : ".$product->product_name." Brand : ".$product->brand->brand_name." Low Stock Notification";

        $message = "<!DOCTYPE html>
            <html>
            <body>
            <table style='width:564px'>
              <tr>
                <td ><h2><font face='arial'>Beston Marketplace Low Stock Notification :</font></h2></td>
              </tr>

                <tr>
                <td>
                Hi,<br><br>

                Please organise stock reorder for ".$product->product_name."<br><br>
<div style='text-align: center;'>
<table border='1'>
  <tr>
    <th width='75px'>OZIRIS ID</th>
    <th width='75px'>Product #</th>
    <th width='75px'>Name</th>
    <th width='75px'>Brand</th>
    <th width='75px'>Manufacturer</th>
    <th width='75px'>Remaining Quantity</th>
    <th width='75px'>Re-Order Threshold</th>
  </tr>
  <tr class='text-center'>
    <td>".$product->id."</td>
    <td>".$product->product_number."</td>
    <td>".$product->product_name."</td>
    <td>".$product->brand->brand_name."</td>
    <td>".$product->manufacturer->manufacturer_name."</td>
    <td>".$qty."</td>
    <td>".$reorder_qty."</td>
  </tr>
</table>
<br>
<a href='https://www.oziris.com.au/OzirisBackend/web/admin/product/update?id=".$product->id."' target='_blank'>Click Here to Manage Product Quantity on TES</a>
<br><br>
</div>


                <br>Thanks,
                <br><br>
                The Beston Marketplace team.
                <br><br>
                <hr>
                <br><br>
                            Copyright © Beston Technologies All rights reserved.
                <br><br>

                </td>
              </tr>
            </table>

            </body>
            </html>";

        try{
            \Yii::$app->mailer->compose(null)
                ->setFrom(['noreply@bestonmarketplace.com.au' => 'Beston Technologies'])
                ->setTo($to)
                ->setSubject($subject)
                ->setHtmlBody($message)
                ->send();
            return true;
        }catch( InvalidConfigException $ex){
            return false;
        }
    }

    public function generateOrderBarCode()
    {
        $orders = Order::find()->where(['barcode' => null])->all();
        foreach($orders as $order){
            $order_number_length = strlen($order->woo_order_id);
            $l = 6-$order_number_length;
            $str_ord_number = '';
            for($i=0; $i<$l; $i++  ){
                $str_ord_number.= '0';
            }
            $order->barcode = '1 9350884'.$str_ord_number.$order->woo_order_id;
            BarcodeGenerator::barcode(preg_replace('/\s+/', '', 'order/barcode/'.$order->barcode).'.png', $order->barcode, "200", "horizontal" ,$code_type="code128" , false, 4 );
            print_r($order->save());
        }
    }

    public function updateOrderStatus()
    {
        $order = Order::find()->where(['woo_order_id' => $_POST['order_id']])->one();
        $order->status_id = OrderStatus::find()->where(['name' => $_POST['status']])->one()->id;
        $order->save(false);

        $devices_ios = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'iOS' ])->all();
        if( count($devices_ios) > 0 ){
            $notification = "Order Status has been changed to : ".$order->status->description;
            PushNotificationSender::sendIOSNotification($devices_ios,$notification,$order->woo_order_id);
        }
        $devices_android = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'android' ])->all();
        if( count($devices_android) > 0 ){
            PushNotificationSender::sendAndroidNotification($devices_android,$notification);
        }
    }

    public function updateCompressImages()
    {
        $products = Product::find()->all();
            $allImages = [];
        foreach($products as $product){
            foreach($product->getProductImages() as $image){
                array_push($allImages,$image);
            }
        }

        foreach($allImages as $image){
            $sourcePath = 'https://'.$_SERVER['HTTP_HOST'].$image;
            $targetPath = "uploads/product/".basename($image); // Target path where file is to be stored
            $thumbnailPath = "uploads/product/thumbnail/".basename($image);
            $tinify = new Tinify();
            $tinify->setKey(getenv('TinyPNG'));
            $source = $tinify->fromFile($sourcePath);
            $resized = $source->resize(array(
                "method" => "scale",
                "height" => 200
            ));
//            $source->toFile($targetPath);
            $resized->toFile($thumbnailPath);
        }

        return count($allImages);

    }


}

/**
 ******Custom Queries ***********
 *
 * *******Get Order CSV****************
SELECT woo_order_id as 'Order #', FROM_UNIXTIME(order.created_at, '%D %M %Y %h:%i') as 'Order Date', order_status.name as 'Order Status', FROM_UNIXTIME(order.updated_at, '%D %M %Y %h:%i') as 'Update Date', app_user.email_address as 'Customer Email', address.contact_no as 'Contact Number' , address.state as 'State' , address.postcode as 'State'  , order.total_amount as 'Total'
FROM  `order` ,  `order_status` ,  `app_user` , `address`
WHERE order.status_id = order_status.id
AND order.customer_id = app_user.id
AND order.billing_address_id = address.id
ORDER BY  `order`.`woo_order_id`DESC
 **/