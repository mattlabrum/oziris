<?php

namespace app\modules\admin\controllers\api\services;

use app\models\AppUserDeviceInfo;
use app\models\Batch;
use app\models\BatchItem;
use app\models\Order;
use app\models\OrderStatus;
use app\models\OrderStatusTime;
use app\models\Product;
use app\utils\PushNotificationSender;
use dektrium\user\models\User;
use yii\httpclient\Client;

class BestonInventoryManager
{

    public function validateUserInformation($data){
        $user = User::find()->where(['email' => $data['email_address']])->one();
        return password_verify($data['password'], $user->password_hash);
    }

    public function getProductBatches($user,$product_code){
        if ($this->validateUserInformation($user)){
            $product = Product::find()->where(['product_number'=> $product_code])->orWhere(['barcode'=> $product_code])->one();
        }
        if(isset($product)){
            $batches = Batch::find()->where('product_id != batch_number')->andWhere('manufacture_date != "" ')->andWhere(['product_id' =>$product->id])->all();
            foreach ($batches as $batch){
                $batches_to_return[] = [
                    'id' => $batch->id,
                    'manufacturer_date' => $batch->manufacture_date,
                    'batch_number' => $batch->batch_number,
                    'product_name' => $batch->product->product_name,
                ];
            }
            return $batches_to_return;
        }else{
            return false;
        }

    }

    public function attachProductToBatch($user,$data){
        if ($this->validateUserInformation($user)){
            $batch_item = BatchItem::find()->where(['qr_code_text'=> $data['qr_code']])->one();
        }
        if(isset($batch_item)){
            $batch_item->delete();
        }
        $new_batch_item = new BatchItem();
        $new_batch_item->batch_id = $data['batch_id'];
        $new_batch_item->qr_code_text = $data['qr_code'];
        $new_batch_item->item_status = '';
        return $new_batch_item->save();
    }


    public function getOrderItems($raw_items) {

        foreach($raw_items as $item){
            if($item->product->is_collection){
                foreach($item->product->collectionItems as $collection_item){
                    $items[] = [
                        'quantity' => $collection_item->quantity,
                        'product_id' => $collection_item->product->id,
                        'product_name' => $collection_item->product->product_name,
                    ];
                }
            }else{
                $items[] = [
                    'quantity' => $item->quantity,
                    'product_id' => $item->product->id,
                    'product_name' => $item->product->product_name,
                ];
            }
        }

        $groups = array();
        foreach ($items as $item) {
            $key = $item['product_id'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'product_id' => $item['product_id'],
                    'product_name' => $item['product_name'],
                    'quantity' => $item['quantity'],
                );
            } else {
                $groups[$key]['quantity'] += $item['quantity'];
            }
        }
        return $groups;
    }
    public function pickAndPack($qr_code,$product_id,$order_id)
    {
        $item = BatchItem::find()->where(['item_status' => '', 'qr_code_text' => $qr_code ])->one();
        if(isset($item) && $item->batch->product_id == $product_id ){
            $item->item_status = 'sold';
            $item->order_id = $order_id;
            if($item->save(false)){
                return true;
            }else{
                return false;
            }
        }else {
            return false;
        }
    }
    public function returnOrderItems($qr_code)
    {
        $order = Order::find()->where(['qr_code_text' => $qr_code])->one();
        if(isset($order)){
            $returnInfo = [
                'order_id' => $order->woo_order_id,
                'order_items' => $this->getOrderItems($order->orderItems),
            ];
            return $returnInfo;
        }else{
            return 'Order Not Found';
        }
    }
    public function assignOrderSeal($qr_code, $order_id)
    {
        $order = Order::find()->where(['woo_order_id' => $order_id])->one();
        if(isset($order)){
            $order->order_qr_seal = $qr_code;
            return $order->save(false);
        }else{
            return 'Order Not Found';
        }
    }

    public function finalizeOrder($data, $order_id)
    {
        $order = Order::find()->where(['woo_order_id' => $order_id])->one();
        if(isset($order)){
            $uploaddir = 'order/picture/';
            $file = basename($data['order_image']['name']);
            $uploadfile = $uploaddir . 'order-'.$order_id.'.'.pathinfo($file, PATHINFO_EXTENSION);;
            if (move_uploaded_file($data['order_image']['tmp_name'], $uploadfile)) {

                $order->status_id = OrderStatus::find()->where(['name' => 'ready-to-dispatch'])->one()->id;
                $order->order_image = $uploadfile;

                if($order->customer->email_address != 'default' ){
                    $devices_ios = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'iOS' ])->all();
                    if( count($devices_ios) > 0 ){
                        $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $order->status_id])->one()->description;
                        PushNotificationSender::sendIOSNotification($devices_ios,$notification,$order->woo_order_id);
                    }
                    $devices_android = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'android' ])->all();
                    if( count($devices_android) > 0 ){
                        $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $order->status_id])->one()->description;
                        PushNotificationSender::sendAndroidNotification($devices_android,$notification);
                    }
                }

                $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $order->woo_order_id, 'status_id' => $order->status_id])->one();
                $orderStatusTime->time = $order->updated_at;
                $orderStatusTime->save();

                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('post')
                    ->setUrl(getenv('SERVER_TYPE').'/update_order_details')
                    ->setData(
                        [
                            'order_id' => $order->woo_order_id,
                            'order_note' => $order->order_note,
                            'order_status' => $order->status->name,
                        ])->send();

                return $order->save(false);
            } else {
                return false;
            }
        }else{
            return 'Order Not Found';
        }
    }
}