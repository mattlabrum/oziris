<?php

namespace app\modules\admin\controllers\api\services;


use app\models\AppAuthDatum;
use app\models\AppLoginToken;
use app\models\AppUser;
use app\models\ReportedReview;
use app\models\ReviewRating;
use app\utils\AuthTokenGenerator;
use Exception;
use yii\httpclient\Client;

class ReviewRatingManagement
{

public function saveReview($review){

    $client = new Client();
//    return AppUser::find()->where(['id' => $review->user_id])->one()->email_address;

    $response = $client->createRequest()
        ->setMethod('post')
        ->setUrl(getenv('SERVER_TYPE').'/update_comment')
        ->setData(
            [
                'product_id' => $review->product_id,
                'review' => $review->review,
                'rating' => $review->rating,
                'comment_id' => $review->comment_id,
                'user_id' => AppUser::find()->where(['id' => $review->user_id])->one()->email_address,
            ])->send();

    $return = json_decode($response->content);
    if($return->comment_id != '-1'){
        $review->comment_id = $return->comment_id;
    }
    return $review->save();
}

    public function reportReviewBeston($comment_id,$user_id){
//return $user_id;

        $review = ReviewRating::find()->where(['comment_id' => $comment_id])->one();

        if(isset($review)){
            $reported_review = ReportedReview::find()->where(['rating_review_id'=>$review->id])->andWhere(['user_id'=>$user_id])->one();
            if(!isset($reported_review)){
                $reported_review = new ReportedReview();
                $reported_review->user_id= $user_id;
                $reported_review->rating_review_id= $review->id;
                return $reported_review->save(false);
            }
        }else{
            return "Review Does Not Exist";
        }

}

public function reportReview($review_id,$user_id){

    $reported_review = ReportedReview::find()->where(['rating_review_id'=>$review_id])->andWhere(['user_id'=>$user_id])->one();
    if(!isset($reported_review)){
        $reported_review = new ReportedReview();
        $reported_review->user_id= $user_id;
        $reported_review->rating_review_id= $review_id;

        return $reported_review->save();
    }
    return false;
}

    public function getAllReviews($type,$id){

        $raw_reviews = ReviewRating::find()->where([$type.'_id'=>$id,'deleted_at' => null])->all();
        $reported_reviews = [];
        foreach($raw_reviews as $raw_review){
            $reported_reviews[] = [
                'id'=>$raw_review->id,
                'user_name'=>$raw_review->app_user->getUserName(),
                'user_id'=>$raw_review->user_id,
                'brand_id'=>$raw_review->brand_id,
                'product_id'=>$raw_review->product_id,
                'manufacturer_id'=>$raw_review->manufacturer_id,
                'rating'=>$raw_review->rating,
                'review'=>$raw_review->review,
                "review_date_time" => date('d-m-Y h:i:s', $raw_review->created_at),
            ];
        }

        return $reported_reviews;



    }

}