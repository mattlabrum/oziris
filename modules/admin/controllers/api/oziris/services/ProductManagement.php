<?php
/**
 * @property \app\models\Product $product
 */

namespace app\modules\admin\controllers\api\oziris\services;


use app\models\AppSetting;
use app\models\base\BatchItem;
use app\models\base\ProductLocationAttribute;
use app\models\Batch;
use app\models\Brand;
use app\models\Language;
use app\models\Manufacturer;
use app\models\Order;
use app\models\OrderStatusTime;
use app\models\Product;
use app\models\ProductGroup;
use app\models\ProductType;
use app\models\Response;
use app\models\ScannedProduct;
use app\models\ShippingBox;
use app\utils\FetchContinentFromCountryCode;
use DateTime;
use Exception;
use yii\helpers\Url;

class ProductManagement
{

    public function getAppSettings()
    {
        $appSettings = AppSetting::find(['id'=>1])->one();
        $response = new Response();

        $response->setResponse(
                                [
                                'promo_image' => 'http://' . $_SERVER['HTTP_HOST'] . Url::to('@web/images') . '/' . $appSettings->promo_image,
                                'promo_url' => $appSettings->promo_url,
                                'promo_product_id' => $appSettings->promo_product_id,
                                'promo_search_string' => $appSettings->promo_search_string,
                            ]
                        );
        return $response->getFullResponse();


    }

    public function getAllProducts($language_code)
    {
        $products = ProductLocationAttribute::findAll(['ecommerce_enabled'=>1]);
        foreach($products as $product){
            if($product->product->deleted_at == null){
                $p = $product->product->getLimitedTranslatedData($language_code);
                $translated_products[] = $p;
            }
        }
        return $translated_products;
    }


    public function getProductDefaultBatchDetails($language_code, $product_id)
    {
        $response = new Response();
        $default_batch = Batch::find()->where(['product_id' => $product_id , 'batch_number' => $product_id , 'manufacture_date' => null])->one();

        if(isset($default_batch)){
            $response->setResponse($default_batch->getTranslatedData($language_code));
        }
        return $response->getFullResponse();
    }


    public function getProductDetails($language_code, $product_id)
    {
        $product = Product::find()->where(['id'=> $product_id])->one()->getTranslatedData($language_code);
        return $product;
    }

    public function listAllProducts($language_code)
    {
        $products = ProductLocationAttribute::findAll(['ecommerce_enabled'=>1]);
        foreach($products as $product){
            if($product->product->deleted_at == null){
                $p = [
                    "id" => $product->product->id,
                    "product_group" => $product->product->productGroup->product_group_name,
                    "product_name" => ($product->product->getProductTranslation($language_code) != false ? trim($product->product->getProductTranslation($language_code)->product_name) : trim($product->product->product_name)),
                    "product_type" => $product->product->productType->type_name,
                    "brand" => $product->product->brand->brand_name,
                    "manufacturer" => $product->product->manufacturer->manufacturer_name,
                    "manufacturer_location" => $product->product->manufacturer->location,
                    "product_image" => $product->product->getProductImageUrl(),
                    "type_icon" => $product->product->productType->getTypeIconUrl(),
                ];

                $translated_products[] = $p;
            }
        }
        return $translated_products;
    }

    public function checkOrderQR($qr_code,$language_code){

        $box = ShippingBox::find()->where(['box_qr' => $qr_code])->one();
        $box_items = array();
        if(isset($box)){
            $order = Order::find()->where(['woo_order_id' => $box->order_id])->one();
            $box_items = BatchItem::find()->where(['box_id' => $box->id])->all();
        }else{
            $order = Order::find()->where(['qr_code_text' => $qr_code])->one();
        }
        if(isset($order)){
            $brand = new Brand();
            $brand->brand_name = 'Beston Market Place';
            $brand->brand_logo = 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/logo.png');


            $returnInfo = [
                'id' => $order->id,
                'manufacture_date' => $order->created_at,
                'recalled_date' => '',
                'qr_code_image' =>$order->qr_code,
                'qr_code' => $order->qr_code,
                'product' => [
                    "id" => 204,
                    "is_collection" => 1,
                    "collection_items" => (count($box_items) > 0 ? $this->getBoxItems($box_items, $language_code) : $this->getOrderItems($order->orderItems,$language_code))  ,// (if(count()))$this->getOrderItems($order->orderItems,$language_code),
                    "product_number" => '',
                    "barcode" => '',
                    "product_group" => 'Order Date : '.date('d-M-y', $order->created_at),
                    "product_name" => 'Order Number : '.$order->woo_order_id,
                    "product_description" => '',
                    "product_type" => new ProductType(),
                    "brand" => $brand,
                    "manufacturer" => new Manufacturer(),
                    "shelf_life" => 10,
                    "shelf_life_unit" => 2,
                    "UnitSize" => '',
                    "product_image" => 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/images/beston-logo-app.png'),
                    "nutrition_image" => '',
                    "nutrition_text" => '',
                ],
            ];
            return $returnInfo;
        }else{
            return 'Order Not Found';
        }

    }

    public function getBoxItems($box_items, $language_code)
    {
        $items = array();
        foreach ($box_items as $item) {
            $items[] = [
                'quantity' => 1,
                'product_id' => $item->batch->product->id,
                'product' => $item->batch->product->getTranslatedData($language_code),
            ];
        }

        $groups = array();
        foreach ($items as $item) {
            $key = $item['product_id'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
//                    'product_id' => $item['product_id'],
                    'product' => $item['product'],
                    'quantity' => $item['quantity'],
                );
            } else {
                $groups[$key]['quantity'] += $item['quantity'];
            }
        }

        $products = array();
        foreach ($groups as $group) {

            $products[] = [
                'quantity' => $group['quantity'],
                'product' => $group['product'],
            ];

        }
        return $products;

    }

    public function getOrderItems($raw_items,$language_code) {
        $items = [];
        foreach($raw_items as $item){
            $order_item_batch = Batch::find()->where(['product_id' => $item->product_id, 'batch_number' => $item->product_id])->andWhere(['deleted_at' =>null])->one();
            $items[] = [
                'quantity' => $item->quantity,
                'product' => $order_item_batch->product->getTranslatedData($language_code),
            ];
        }
        return $items;
    }

    public function checkCounterfeitProduct($product_code,$qr_code,$batch_number,$language_code,$user_id)
    {
        $scanned_product = [
            'product_code'=>$product_code,
            'user_id'=>$user_id,
            'qr_code'=>$qr_code,
            'batch_number'=>$batch_number,
            'batch_id'=>'',
        ];

        if ($qr_code!='') {

            if(substr($qr_code,0,5) == 'order') {
                return $this->checkOrderQR($qr_code,$language_code);
            }

            $batch = Batch::findOne(['qr_code' => $qr_code, 'deleted_at' => null]);
            if (isset($batch)) {
                $batch_info = $batch->getTranslatedData($language_code);

                if($batch_info != false){
                    $scanned_product['batch_id'] = $batch->id;
                }
            }else {
                $batch_item = BatchItem::findOne(['qr_code_text' => $qr_code]);
                if (isset($batch_item)) {
                    $batch_info = $batch_item->batch->getTranslatedData($language_code);

                    $statuses_raw = OrderStatusTime::find()->where(['order_id' => $batch_item->order_id])->andWhere('sort != 0')->orderBy('sort')->all();
                    $statuses= [];

                    foreach($statuses_raw as $status_raw) {
                        $statuses[] = [
                            'status' => $status_raw->getStatusName(),
                            'time' => $status_raw->time,
                        ];
                    }

                    $batch_info['order_trace'] = $statuses;
                    if($batch_info != false){
                        $scanned_product['batch_id'] = $batch_item->batch->id;
                    }
                }
            }

            if($scanned_product['batch_id'] == '' || $scanned_product['batch_id'] == null){
                $product = Product::find()->where(['barcode' => $qr_code])->one();

                if(isset($product)){
                    $batch = Batch::find()->where(['batch_number' => $product->id , 'product_id' => $product->id])->one();
                    if(isset($batch)){
                        $batch_info = $batch->getTranslatedData($language_code);

                        if($batch_info != false){
                            $scanned_product['batch_id'] = $batch->id;
                        }
                    }

                }
            }
        }
        if ($batch_number!='' && $product_code!='') {
            $batches = Batch::findAll(['batch_number' => $batch_number,'deleted_at'=> null]);
            foreach ($batches as $batch) {
                if ($batch->product->barcode == $product_code || $batch->product->product_number == $product_code) {
                    $batch_info = $batch->getTranslatedData($language_code);
                    if($batch_info != false){
                        $scanned_product['batch_id'] = $batch->id;
                    }
                }
            }
        }else if($batch_number=='' && $product_code!=''){
            $query = Batch::find();
            $query->joinWith(['product']);
            $query->where(['product.barcode' => $product_code]);
            $query->orWhere(['product.product_number' => $product_code]);
            $query->andWhere(['batch.manufacture_date' => null]);
            $query->andWhere(['batch.deleted_at' => null]);
            $query->andWhere(['product.deleted_at' => null]);
//            $query->andWhere(['brand.deleted_at' => null]);
//            $query->andWhere(['manufacturer.deleted_at' => null]);
            $default_batch = $query->one();

            if(isset($default_batch)){
                $batch_info = $default_batch->getTranslatedData($language_code);
                if($batch_info != false){
                    $scanned_product['batch_id'] = $default_batch->id;
                }
            }
//            return $batch_info;
        }
        $this->saveScannedProduct($scanned_product);
        if(isset($batch_info)){
            $batch_info['product']['product_description']  = strip_tags(html_entity_decode(html_entity_decode($batch_info['product']['product_description'], ENT_QUOTES | ENT_XML1, 'UTF-8')));

            return $batch_info;
        }else{
            return false;
        }
    }

    public function saveScannedProduct($data)
    {
        $ip_address = $this->getUserIP();
        $scanned_product = new ScannedProduct();
        $scanned_product->batch_id = $data['batch_id'];
        $scanned_product->batch_number = $data['batch_number'];
        $scanned_product->product_code = $data['product_code'];
        $scanned_product->qr_code = $data['qr_code'];
        $scanned_product->user_id = $data['user_id'];
        try {
            $geoIP  = json_decode(file_get_contents('http://freegeoip.net/json/'.$ip_address.''), true);
            //            $geoIP  = json_decode(file_get_contents('https://oziris.com.au:8080/json/'.$ip_address.''), true);
            $scanned_product->scanned_latitude = $geoIP['latitude'];
            $scanned_product->scanned_longitude = $geoIP['longitude'];
        } catch (Exception $e) {
            $scanned_product->scanned_latitude = 0;
            $scanned_product->scanned_longitude = 0;
        }
        try {
            $location = $this->getGeoCity($scanned_product->scanned_latitude, $scanned_product->scanned_longitude);
            $scanned_product->city = $location->address_components[2]->long_name;
            $scanned_product->continent =  FetchContinentFromCountryCode::fetchContinentFromCountryCode($location->address_components[5]->short_name);

        } catch (Exception $e) {
            $scanned_product->city = 'Un-disclosed';
            $scanned_product->continent = 'Un-disclosed';
        }
        if(!isset($scanned_product->city)){
            $scanned_product->city = 'Un-disclosed';
            $scanned_product->continent = 'Un-disclosed';
        }
        $scanned_product->save();
    }

    public function getScannedProductsForUser($data)
    {
        $scanned_products = ScannedProduct::find()->where(['user_id' => $data['user_id']])->andWhere('batch_id IS NOT NULL')->all();
        $scanned_translated_products = [];
        foreach($scanned_products as $scanned_product){
            $translated_product = $scanned_product->batch->getTranslatedData($data['language_code']);




            $translated_product['product']['product_description']  = strip_tags(html_entity_decode(html_entity_decode($translated_product['product']['product_description'], ENT_QUOTES | ENT_XML1, 'UTF-8')));
            if($translated_product!=false){
                $scanned_translated_products[] = [
                    "batch" => $translated_product,
                    "scanned_date_time" => date('d-m-Y h:i:s', $scanned_product->created_at),
                    "scanned_longitude" => $scanned_product->scanned_longitude,
                    "scanned_latitude" => $scanned_product->scanned_latitude,
                ];
            }
        }
        return $scanned_translated_products;
    }

    public function getShowBatchStatus($barcode)
    {
        $product = Product::find()->where(['barcode' => $barcode ])->one();
        $response = new Response();
        if(isset($product)){

            if($product->show_batch == null){
                $response->setResponse(['show_batch' => 0]);
            }else{
                $response->setResponse(['show_batch' => $product->show_batch]);
            }


        }else{
            $response->setResponse(false);
            $response->setMessage("Product not Found!");
        }
        return $response->getFullResponse();
    }

    function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        return $ip;
    }

    function getGeoCity($lat,$lng) {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
        $get     = file_get_contents($url);
        $geoData = json_decode($get);
//        print_r($geoData);
        if(isset($geoData->results[0])) {
            return $geoData->results[0];
        }
        return null;
    }
}