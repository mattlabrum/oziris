<?php

namespace app\modules\admin\controllers\api\oziris\services;




use app\models\Manufacturer;

class ManufacturerManagement
{

    public function getAllManufacturers($language_code){

        $manufacturers = Manufacturer::findAll(['deleted_at' => null]);
        $translated_manufacturers = [];
        foreach($manufacturers as $manufacturer){
            $translated_manufacturers = $manufacturer->getManufacturerTranslation($language_code);
        }
        return $translated_manufacturers;
    }

}