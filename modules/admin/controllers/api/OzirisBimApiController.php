<?php

namespace app\modules\admin\controllers\api;
use app\modules\admin\controllers\api\services\BestonOrderManager;
use app\modules\admin\controllers\api\services\BestonInventoryManager;

/**
 * This is the class for REST controller "app\modules\admin\controllers\ProductController".
 */
class OzirisBimApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject(){
        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];
        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }
    /**
     * validate app login details
     * @return Boolean
     */
    public function actionValidateLoginInfo()
    {
        $data = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $bim = new BestonInventoryManager();
        return $bim->validateUserInformation($data);
    }
    /**
     * batches against product code
     * @return Boolean
     */
    public function actionGetProductBatches()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $bim = new BestonInventoryManager();
        return $bim->getProductBatches($user,$_POST['product_code']);
    }
    /**
     * creates a batch item
     * @return Boolean
     */
    public function actionCreateBatchItem()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $batch_item = [
            'batch_id' => $_POST['batch_id'],
            'qr_code' => $_POST['qr_code']
        ];
        $bim = new BestonInventoryManager();
        return $bim->attachProductToBatch($user,$batch_item);
    }
    /**
     * Return List of items in an order
     * @return Boolean
     */
    public function actionReturnOrderItems()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
            $qr_code = $_POST['qr_code'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->returnOrderItems($qr_code);
        }else{
            return "Invalid User Name / Password";
        }
    }
    /**
     * validate item QR code, assign order ID to batch_item and mark item as sold
     * @return Boolean
     */
    public function actionPickAndPack()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['qr_code'];
        $product_id = $_POST['product_id'];
        $order_id = $_POST['order_id'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->pickAndPack($qr_code,$product_id, $order_id);
        }else{
            return "Invalid User Name / Password";
        }
    }
    /**
     * attach order_qr_seal to order
     * @return Boolean
     */
    public function actionAssignOrderSeal()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['seal_qr_code'];
        $order_id = $_POST['order_id'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->assignOrderSeal($qr_code, $order_id);
        }else{
            return "Invalid User Name / Password";
        }
    }
    /**
     * finalize order, change order status to 'ReadyToDispatch' and upload image
     * @return Boolean
     */
    public function actionFinalizeOrder()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $order_id = $_POST['order_id'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->finalizeOrder($_FILES, $order_id);
        }else{
            return "Invalid User Name / Password";
        }
    }
}