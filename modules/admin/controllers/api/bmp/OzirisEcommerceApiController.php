<?php

namespace app\modules\admin\controllers\api\bmp;

use app\modules\admin\controllers\api\bmp\services\EcommerceManagement;

class OzirisEcommerceApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject(){

        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];

        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }

    /*************************************Ecommerce Web Services Start********************************************/

    public function actionGetEcommerceEnabledProducts()
    {
        $language_code = (isset($_POST['language_code']) ? $_POST['language_code'] : '');
        $return = new EcommerceManagement();
        return $return->getEcommerceEnabledProducts($language_code);
    }

    public function actionImportOrderFromWooCommerce()
    {
        $this->logPostObject();
        $order_number = (isset($_POST['order_number']) ? $_POST['order_number'] : '');
        $return = new EcommerceManagement();
        return $return->importOrderFromWooCommerce($order_number);

    }

    public function actionUpdateBmpProductDetails()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->updateBmpProductDetails();
    }

    public function actionUpdateOrderStatus()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->updateOrderStatus();
    }

    public function actionUpdateSubscriptionOrder()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->updateSubscriptionOrder($_POST['subscription_id']);
    }

    public function actionImportShippingAmount()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->importShippingAmount();
    }



    /*************************************Ecommerce Web Services Start********************************************/


}