<?php

namespace app\modules\admin\controllers\api\bmp;

use app\models\AppLoginToken;
use app\models\AppUser;
use app\models\base\Manufacturer;
use app\models\Batch;
use app\models\Brand;
use app\models\ReviewRating;
use app\modules\admin\controllers\api\bmp\services\OrderManagement;
use app\modules\admin\controllers\api\bmp\services\ReviewRatingManagement;
use app\modules\admin\controllers\api\bmp\services\UserManagement;
use yii\httpclient\Client;


/**
 * This is the class for REST controller "app\modules\admin\controllers\ProductController".
 */
class OzirisWebApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';


    public function logPostObject(){

        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];

        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }

    /*************************************User Web Services Start********************************************/


    public function actionValidateUserPhoneSession()
    {
        $session_key = (isset($_POST['session_key']) ? $_POST['session_key'] : '');
        $user_id = (isset($_POST['user_id']) ? $_POST['user_id'] : '');
        $user = new UserManagement();
        return $user->checkValidPhoneSession($user_id,$session_key);

    }

    /**
     * Searches for the provided user email in app_user.
     *
     * @return boolean
     *
     */
    public function actionValidateUserEmail()
    {
        $email_address = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
        $user = new UserManagement();
        return $user->checkValidUserEmail($email_address);
    }
    /*************************************User Web Services Start********************************************/
    /**
     * Searches for the provided user email in app_user.
     *
     * @return boolean
     *
     */
    public function actionForgetPasswordEmail()
    {
        $this->logPostObject();
        $user = new UserManagement();
        return $user->forgetPasswordEmail($_POST['email_address']);
    }

    /**
     * Searches for the provided user email in app_user.
     * if email does not exist then signup user with the given email address
     *
     * @return boolean
     *
     */
    public function actionSignUpWithUser()
    {
        $user = new UserManagement();
        $this->logPostObject();
        return $user->insertNewUser($_POST);
    }

    /**
     * Registers User On Beston Market place
     * @return boolean
     *
     */
    public function actionRegisterUserWithBestonMarketPlace()
    {
        $user = new UserManagement();
        $ozirisUsers = AppUser::find()->select(['first_name','last_name','mobile','country','email_address'])->all();
        foreach($ozirisUsers as $ozirisUser){
            print_r($user->createUserOnBestonMarketplace($ozirisUser));
        }

    }

    /**
     * Updates BestonMarket User Password
     * @return boolean
     */
    public function actionUpdateBestonUserPassword()
    {
        $this->logPostObject();
        $user = new UserManagement();
        return $user->setBestonMarketPlacePassword($_POST['email_address'],$_POST['password']);
    }

    /**
     * Updates User Information
     *
     *
     * @return boolean
     *
     */
    public function actionUpdateUser()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $user = new UserManagement();
            return $user->updateUser($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }


    /**
     * Get User Information
     *
     *
     * @return AppUser
     *
     */
    public function actionGetUser()
    {

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $user = new UserManagement();
            return $user->getUser($_POST['user_id']);
        } else {
            return "User Not Authenticated!";
        }
    }


    /**
     * Validates user credentials
     * Generates login token which will authenticate user for all web services
     *
     * @return boolean
     *
     */
    public function actionValidateUserAppAuthData()
    {
        $user = new UserManagement();
        return $user->validateUserAppAuthData($_POST);

    }

    /**
     * Registers user's device info to app
     * @return boolean
     *
     */
    public function actionRegisterUserDeviceToApp()
    {
        $user = new UserManagement();
        return $user->registerUserDeviceToApp();
    }

    /**
     * Validates user social media credentials
     * Generates login token which will authenticate user for all web services
     *
     * @return boolean
     *
     */
    public function actionValidateSocialMediaAuthData()
    {
        $user = new UserManagement();
        return $user->validateSocialMediaAuthData($_POST);

    }


    /*************************************Review Rating Web Services Start********************************************/

    /**
     * Post Review Information
     *
        *
     * @return Boolean
     *
     */

    public function actionPostReview()
    {

        $this->logPostObject();

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            if ($_POST['product_id'] != '') {
                $review = ReviewRating::find()->where(['product_id' => $_POST['product_id'], 'user_id' => $_POST['user_id'],'deleted_at' =>null])->one();
            } else if ($_POST['brand_id'] != '') {
                $review = ReviewRating::find()->where(['brand_id' => $_POST['brand_id'], 'user_id' => $_POST['user_id'],'deleted_at' =>null])->one();
            } else if ($_POST['manufacturer_id'] != '') {
                $review = ReviewRating::find()->where(['manufacturer_id' => $_POST['manufacturer_id'], 'user_id' => $_POST['user_id'],'deleted_at' =>null])->one();
            }

            if(!isset($review)){
                $review = new ReviewRating();
            }

            $review->brand_id = (isset($_POST['brand_id']) ? $_POST['brand_id'] : '');
            $review->manufacturer_id = (isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '');
            $review->product_id= (isset($_POST['product_id']) ? $_POST['product_id'] : '');
            $review->review= $_POST['review'];
            $review->rating= $_POST['rating'];
            $review->user_id= $_POST['user_id'];
            return $review_rating->saveReview($review);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Report a Review
     * @return Boolean
     */

    public function actionReportReview()
    {

        $this->logPostObject();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->reportReview($_POST['review_id'],$_POST['user_id']);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Returns all Reviews for a product || manufacturer || Brand
     * @return Boolean
     */

    public function actionGetAllReviewsForId()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->getAllReviews($_POST['type'],$_POST['id']);
        } else {
            return "User Not Authenticated!";
        }
    }



    /*************************************Review Rating Web Services End********************************************/


    public function actionGenerateDefaultBatchQrCodes()
    {
        ini_set('max_execution_time', 300);
        $batches = Batch::find()->where(['qr_code' => 'default'])->all();
        $count=0;
        foreach($batches as $batch){
            $batch->qr_code_image = $batch->generateQrCode();

            $batch->save(false);
            $count++;
        }

        return $count;
    }

    /*************************************Review Rating Web Services for BestonMarketplace Start********************************************/

    /**
     * Post Review Information
     *
     *
     * @return Boolean
     *
     */

    public function actionPostReviewBeston()
    {
        $this->logPostObject();
        $user = AppUser::find()->where(['email_address' => $_POST['user_id']])->one();
        if (isset($user)) {
            if ($_POST['old_comment_id'] != '') {
                $review = ReviewRating::find()->where(['comment_id' => $_POST['old_comment_id'], 'deleted_at' => null])->one();
            }
            if (!isset($review)) {
                $review = new ReviewRating();
            }

            $review->brand_id = '';
            $review->manufacturer_id = '';
            $review->product_id = (isset($_POST['product_id']) ? $_POST['product_id'] : '');
            $review->review = $_POST['review'];
            $review->rating = $_POST['rating'];
            $review->user_id = $user->id;
            $review->comment_id = $_POST['comment_id'];;

            return $review->save();
        } else {
            return false;
        }

    }


    /**
     * Report a Review
     *
     *
     * @return Boolean
     *
     */

    public function actionReportReviewBeston()
    {
        $this->logPostObject();
        $user = AppUser::find()->where(['email_address' => $_POST['user_id']])->one();
        if (isset($user)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->reportReviewBeston($_POST['comment_id'], $user->id);
        } else {
            return "User Does Not Exist";
        }
    }

    /**
     * Returns all Reviews for a product || manufacturer || Brand
     *
     *
     * @return Boolean
     *
     */

    public function actionImportAllReviewsToBeston()
    {

        $reviews = ReviewRating::find()->where(['comment_id'=>null])->all();

        $client = new Client();

        foreach( $reviews as $review){

            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(getenv('SERVER_TYPE').'/update_comment')
                ->setData(
                    [
                        'product_id' => $review->product_id,
                        'review' => $review->review,
                        'rating' => $review->rating,
                        'comment_id' => $review->comment_id,
                        'user_id' => AppUser::find()->where(['id' => $review->user_id])->one()->email_address,
                    ])->send();

            $return = json_decode($response->content);
            if($return->comment_id != '-1'){
                $review->comment_id = $return->comment_id;
            }
            print_r($review->save());
        }
    }


    public function actionGetStatusTimesForBestonOrder(){
        $orderManagement = new OrderManagement();
        return $orderManagement->getStatusTimesForOrder($_POST['order_id']);

    }



}