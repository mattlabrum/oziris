<?php

namespace app\modules\admin\controllers\api\bmp\services;


use app\models\AppAuthDatum;
use app\models\AppLoginToken;
use app\models\AppUser;
use app\models\Order;
use app\models\OrderStatus;
use app\models\OrderStatusTime;
use app\models\ReportedReview;
use app\models\ReviewRating;
use app\utils\AuthTokenGenerator;
use Exception;
use yii\helpers\Url;
use yii\httpclient\Client;

class OrderManagement
{

    public function getStatusTimesForOrder($order_id)
    {
        $order_info= [];
        $order = Order::find()->where(['woo_order_id'=>$order_id])->one();

        $statuses_raw = OrderStatusTime::find()->where(['order_id' => $order_id])->andWhere('sort != 0')->orderBy('sort')->all();
        $statuses= [];

        foreach($statuses_raw as $status_raw){
            if($status_raw->status_id == '9'){
                if($status_raw->time=='null'){
                    $statuses[] = [
                        'status' => $status_raw->getStatusName(),
                        'time' => $status_raw->time,
                    ];
                }else{
                    $statuses[] = [
                        'status' => $status_raw->getStatusName(),
                        'time' => $status_raw->time,
                        'image' => ($order->order_image=='') ? '' : 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/'.$order->order_image),
                    ];
                }
            }else if($status_raw->status_id == '11'){

                $del_days = '';
                if($order->getShippingAddress()->one()->state == "SA"){
                    $del_days = '24 Hours';
                }else if($order->getShippingAddress()->one()->state == "NSW"){
                    $del_days = '3 - 4 Business Days';
                }else if($order->getShippingAddress()->one()->state == "VIC"){
                    $del_days = '4 - 5 Business Days';
                }else if($order->getShippingAddress()->one()->state == "ACT"){
                    $del_days = '4 - 5 Business Days';
                }else if($order->getShippingAddress()->one()->state == "QLD"){
                    $del_days = '5 - 6 Business Days';
                }else if($order->getShippingAddress()->one()->state == "NT"){
                    $del_days = '';
                }else if($order->getShippingAddress()->one()->state == "TAS"){
                    $del_days = '';
                }
                $statuses[] = [
                    'status' => $status_raw->getStatusName(),
                    'time' => $status_raw->time,
                    'approx_del_days' => $del_days,
                    'shipping_address' => $order->getShippingAddress()->one()->getAddress(),
                ];
            }else{
                $statuses[] = [
                    'status' => $status_raw->getStatusName(),
                    'time' => $status_raw->time,
                ];
            }
        }
        if(isset($order)){
            $order_info= [
                'order_no' => $order_id,
                'qr_codes' => $order->qr_code_text,
                'statuses' => $statuses,
            ];
        }
        return $order_info;

    }
}