<?php

namespace app\modules\admin\controllers\api;

use app\models\AppLoginToken;
use app\models\AppUser;
use app\models\base\Manufacturer;
use app\models\Batch;
use app\models\Brand;
use app\models\Language;
use app\models\Product;
use app\models\ReviewRating;
use app\modules\admin\controllers\api\services\BrandManagement;
use app\modules\admin\controllers\api\services\ManufacturerManagement;
use app\modules\admin\controllers\api\services\ProductManagement;
use app\modules\admin\controllers\api\services\ReviewRatingManagement;
use app\modules\admin\controllers\api\services\UserManagement;
use Exception;


/**
 * This is the class for REST controller "app\modules\admin\controllers\ProductController".
 */
class OzirisBomApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject(){
        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];
        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }
    /**
     * Returns all Reviews for a product || manufacturer || Brand
     *
     *
     * @return Boolean
     *
     */
    public function actionGetAllReviewsForId()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->getAllReviews($_POST['type'],$_POST['id']);
        } else {
            return "User Not Authenticated!";
        }
    }
    /*************************************Review Rating Web Services End********************************************/
//    public function actionGenerateDefaultBatchQrCodes()
//    {
//        ini_set('max_execution_time', 300);
//        $batches = Batch::find()->where(['qr_code' => 'default'])->all();
//        $count=0;
//        foreach($batches as $batch){
//            $batch->qr_code_image = $batch->generateQrCode();
//
//            $batch->save(false);
//            $count++;
//        }
//
//        return $count;
//    }



}