<?php
namespace app\modules\admin\controllers\api\cortex\services;

use app\models\base\AppUser;
use app\models\Response;
use Stripe\Error\Card;
use Stripe\Error\InvalidRequest;
use Stripe\Stripe;
use unyii2\imap\Exception;
use WC_API_Client;

class EcommerceManagement
{
    public function getDeliveryInformation($user_id)
    {
        $response = new Response();

        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);

            $user_email = AppUser::find()->where(['id' => $user_id])->one()->email_address;
            $customer = $client->customers->get_by_email($user_email);

            if(isset($customer)){

                $address = [
                    'shipping_address' => $customer->customer->shipping_address,
                    'billing_address' => $customer->customer->billing_address,
                ];

                $response->setMessage('');

                $response->setResponse($address);
            }else{
                $response->setMessage('Customer not found!');
                $response->setResponse(false);
            }
            return $response->getFullResponse();

        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;

            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }



    public function processStripePayments($payment)
    {
        $response = new Response();

        Stripe::setApiKey("sk_test_w9ORwQE8ItqgydEj0B3gVJMH");
        $token = $payment['stripe_token'];
        try {

            $charge = \Stripe\Charge::create(array(
                "amount" => $payment['amount'], // amount in cents, again
                "currency" => "aud",
                "source" => $token,
                "description" => $payment['desc']
            ));

            $response->setResponse(true);
            $response->setMessage('Transaction Successful!');

            return $response->getFullResponse();

        } catch(InvalidRequest $e) {

            $response->setResponse(false);
            $response->setMessage($e->getMessage());

            return $response->getFullResponse();

        }
        catch(Card $e) {

            $response->setResponse(false);
            $response->setMessage($e->getMessage());

            return $response->getFullResponse();

        }
    }


    public function createOrderOnTes($order)
        {
            $response = new Response();
            $options = array(
                'debug' => true,
                'return_as_array' => false,
                'validate_url' => false,
                'timeout' => 90,
                'ssl_verify' => false,
            );
            try {
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);

                $user_email = AppUser::find()->where(['id' => $order['user_id']])->one()->email_address;
                $order['customer_id'] = $client->customers->get_by_email($user_email);

                $newOrder = $client->orders->create($order);

                $return = new \app\modules\admin\controllers\api\bmp\services\EcommerceManagement();
                if($return->importOrderFromWooCommerce($newOrder->order->id)){
                    $response->setMessage('Order created successfully on TES');
                    $response->setResponse($newOrder->order->id);
                }else{
                    $response->setMessage('Order creation unsuccessful');
                    $response->setResponse(false);
                }
                return $response->getFullResponse();

            } catch (WC_API_Client_Exception $e) {
                $this->logObject($e);
                echo $e->getMessage() . PHP_EOL;
                echo $e->getCode() . PHP_EOL;

                if ($e instanceof WC_API_Client_HTTP_Exception) {
                    print_r($e->get_request());
                    print_r($e->get_response());
                }
            }
        }
}

