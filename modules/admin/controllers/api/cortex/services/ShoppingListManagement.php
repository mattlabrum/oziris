<?php
namespace app\modules\admin\controllers\api\cortex\services;



use app\models\base\ProductLocationAttribute;
use app\models\base\ShoppingListItem;
use app\models\Response;
use app\models\ShoppingList;

class ShoppingListManagement
{
    public function createShoppingList($user_id, $name)
    {
        $response = new Response();
        $nameExists = false;
        $userShoppingLists = ShoppingList::find(['user_id' => $user_id])->all();
        foreach ($userShoppingLists as $list) {
            if ($list->name == $name) {
                $nameExists = true;
                break;
            }
        }
        if (!$nameExists) {
            $newList = new ShoppingList();
            $newList->name = $name;
            $newList->user_id = $user_id;
            if ($newList->save()) {
                $response->setMessage("Shopping List Saved Successfully!");
                $response->setResponse(['id' => $newList->id]);
            } else {
                $response->setResponse(false);
                $response->setMessage("Shopping List Not Saved!");
            }
        } else {
            $response->setResponse(false);
            $response->setMessage("Shopping List Already Exists!");
        }
        return $response->getFullResponse();
    }

    public function actionUpdateShoppingListItem($item)
    {
        $response = new Response();
        $shoppingList = ShoppingListItem::find()->where(['id' => $item['item_id']]);

        if(isset($shoppingList)){
            $shoppingList->product_id  = $item['product_id'];
            $shoppingList->quantity = $item['quantity'];
            $shoppingList->shopping_list_id = $item['shopping_list_id'];

            if($shoppingList->save(false)){
                $response->setResponse($shoppingList);
                $response->setMessage("Product updated successfully!");
            }else{
                $response->setResponse(false);
                $response->setMessage("Product was not updated!");
            }

        }else{
            $response->setResponse(false);
            $response->setMessage("Item Not Found!");
         }
        return $response->getFullResponse();

    }



    public function deleteShoppingListItem($item)
    {
        $response = new Response();
        $shoppingList = ShoppingListItem::deleteAll(['id' => $item['item_id']]);
        $response->setResponse($shoppingList);
        $response->setMessage("List Item deleted successfully!");
        return $response->getFullResponse();
    }




    public function addProductsToShoppingList($item)
    {
        $response = new Response();
        $shoppingList = new ShoppingListItem();
        $shoppingList->product_id  = $item['product_id'];
        $shoppingList->quantity = $item['quantity'];
        $shoppingList->shopping_list_id = $item['shopping_list_id'];

        if($shoppingList->save(false)){
            $response->setResponse($shoppingList);
            $response->setMessage("Product added successfully!");
        }else{
            $response->setResponse(false);
            $response->setMessage("Product was not added!");
        }
        return $response->getFullResponse();
    }

    public function returnAllShoppingLists($user_id)
    {
        $response = new Response();
        $userShoppingLists = ShoppingList::find(['user_id' => $user_id])->all();
        foreach($userShoppingLists as $list){
            $lists[] = [
                'id' => $list->id,
                'name' => $list->name,
            ];
        }
        if(count($userShoppingLists)>0){
            $response->setResponse(['lists'=> $lists] );
        }else{
            $response->setResponse(false);
            $response->setMessage("No Shopping Lists Found!");
        }
        return $response->getFullResponse();
    }

    public function deleteShoppingList($user_id,$list_id)
    {
        $response = new Response();

        if($list_id == -1){
            $status = ShoppingList::deleteAll(['user_id' => $user_id]);
            if($status){
                $response->setResponse(true);
            }else{
                $response->setResponse(false);
                $response->setMessage("Shopping List Delete Unsuccessful!");
            }
        }else{
            $status = ShoppingList::find()->where(['user_id' => $user_id, 'list_id' => $list_id])->one()->delete();
            if($status){
                $response->setResponse(true);
            }else{
                $response->setResponse(false);
                $response->setMessage("Shopping List Delete Unsuccessful!");
            }
        }
        return $response->getFullResponse();
    }

    public function updateShoppingList($user_id,$list_id,$name)
    {
        $response = new Response();
        $list = ShoppingList::find()->where(['user_id' => $user_id, 'id' => $list_id])->one();
        if(isset($list)){
            $list->name = $name;
            if($list->save()){
                $response->setResponse(true);
            }else{
                $response->setResponse(false);
                $response->setMessage("Shopping List Update Unsuccessful!");
            }
        }else{
            $response->setResponse(false);
            $response->setMessage("Shopping List Not Found!");
        }

        return $response->getFullResponse();
    }

}