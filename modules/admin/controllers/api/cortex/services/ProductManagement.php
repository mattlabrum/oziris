<?php
namespace app\modules\admin\controllers\api\cortex\services;


use app\models\base\ShoppingListItem;
use app\models\Order;
use app\models\Product;
use app\models\base\ProductLocationAttribute;
use app\models\Batch;
use app\models\ProductGroup;
use app\models\ProductType;
use app\models\Response;
use app\models\search\ProductSearch;
use app\utils\csvToArray;

class ProductManagement
{

    public function getAllProductsForCategories()
    {
        $response = new Response();

        $products = [];
        $type_id = '';
        $group_id = '';


        $type = ProductType::find()->where(['deleted_at' => NULL, 'deleted_at' => null])->andWhere(['type_name' => $_POST['category']])->one();

        ((isset($type)) ? $type_id = $type->id : '');

        $group = ProductGroup::find()->where(['deleted_at' => NULL, 'deleted_at' => null])->andWhere(['product_group_name' => $_POST['category']])->one();
        ((isset($group)) ? $group_id = $group->id : '');
//        $products = Product::find()->where(['product_group_id' => $group_id])->orWhere(['product_type_id' => $type_id])->all();

        if ($_POST['category'] == 'All') {

            $raw_products = Product::find()->joinWith(['productLocationAttributes'])->where(['product_location_attributes.language_id' => 2, 'product_location_attributes.ecommerce_enabled' => 1])->all();
        } else {
            $raw_products = Product::find()->joinWith(['productLocationAttributes'])->where(['product_location_attributes.language_id' => 2, 'product_location_attributes.ecommerce_enabled' => 1])->andWhere(['product.product_type_id' => $type_id])->orWhere(['product.product_group_id' => $group_id])->all();
        }


        foreach ($raw_products as $product) {
            if ($product->deleted_at == NULL) {
                $products[] = $this->populateProduct($product);
            }
        }

        $response->setResponse($products);
        return $response->getFullResponse();
    }

    private function populateProduct($product)
    {
        return [
            'product_id' => $product->id,
            'product_name' => $product->product_name,
            'product_image' => $product->getProductImageUrl(),
            'number_of_reviews' => $product->getNoOfReviewsforProduct(),
            'average_product_rating' => $product->getAverageProductRating(),
            'product_price' => $product->getProductPrice(2),
            'product_sale_price' => $product->getProductSalePrice(2),
            'product_type' => $product->productType->type_name,
            'product_group' => $product->productGroup->product_group_name,
            'height' => $product->height,
            'width' => $product->width,
            'length' => $product->length,
            'unit_size' => $product->unit_size,
            'unit_size_per_box' => $product->unit_per_box,

        ];
    }

    public function searchProducts($keywords)
    {
        $response = new Response();
        $product_search = new ProductSearch();

        $dataProvider = $product_search->appKeyWordSearch($keywords);
        $dataProvider->setPagination(false);
        $raw_products = $dataProvider->getModels();
        foreach ($raw_products as $raw_product) {
            $products[] = $this->populateProduct($raw_product);
        }
        $response->setResponse($products);

        return $response->getFullResponse();

    }

    public function getAllCategories()
    {
        $response = new Response();

        $categories = [];

        $types = ProductType::find()->where(['deleted_at' => NULL, 'deleted_at' => null, 'show_on_menu' => 1])->all();

        foreach ($types as $type) {
            $products = Product::find()->joinWith(['productGroup', 'productLocationAttributes'])->where(['product.product_type_id' => $type->id, 'product_group.show_on_menu' => 1, 'product_location_attributes.language_id' => 2, 'product_location_attributes.ecommerce_enabled' => 1])->groupBy(['product_group_id'])->all();
            $groups = [];
            foreach ($products as $product) {
                $groups[] = [
                    'group_id' => $product->productGroup->id,
                    'group_name' => $product->productGroup->product_group_name,
                ];
            }
            $categories[] = [
                'type' => [
                    'type_id' => $type->id,
                    'type_name' => $type->type_name,
                    'type_icon' => $type->getTypeIconUrl(),
                    'groups' => $groups
                ]
            ];
        }

        $categories[] = [
            'type' => [
                'type_id' => -1,
                'type_name' => 'All',
                'type_icon' => '',
                'groups' => []
            ]
        ];

        $response->setResponse($categories);
        return $response->getFullResponse();
    }


    public function getAllProducts($language_id)
    {
        $locations = ProductLocationAttribute::find()->where(['language_id' => $language_id, 'ecommerce_enabled' => 1])->all();
        foreach ($locations as $location) {
            if (!$location->product->deleted_at) {
                $products[] = $this->populateProduct($location->product);
            }
        }
        return $products;
    }

    public function getAllShoppingListProducts($data)
    {
        $response = new Response();
        $items = ShoppingListItem::find()->where(['shopping_list_id' => $data['list_id']])->all();
        $products = [];
        foreach ($items as $item) {
            $product = $this->populateProduct($item->product);
            $product['quantity'] = $item->quantity;
            $product['item_id'] = $item->id;
            array_push($products, $product);
        }
        $response->setResponse($products);
        return $response->getFullResponse();
    }

    public function getAllPantryProducts($user)
    {
        $response = new Response();

        $orders = Order::find()->where(['customer_id' => $user['user_id']])->all();

        $products = [];

        foreach ($orders as $order) {
            foreach ($order->getOrderItems() as $item) {
                $key = csvToArray::in_array_field($item->product->id, 'product_id', $products);
                if ($key == -1) {
                    $product = $this->populateProduct($item->product);
                    $product['quantity'] = $item->quantity;
                    array_push($products, $product);
                } else {
                    $products[$key]['quantity'] += $item->quantity;
                }
            }
        }

        $response->setResponse($products);
        return $response->getFullResponse();
    }

    public function getProductDetails($product_id, $language_id)
    {

        $response = new Response();
        $default_batch = Batch::find()->where(['product_id' => $product_id, 'batch_number' => $product_id, 'manufacture_date' => null])->one();

        if (isset($default_batch)) {
            $response->setResponse($default_batch->getTranslatedData($language_id));
        }
        return $response->getFullResponse();

    }


}