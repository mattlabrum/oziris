<?php

namespace app\modules\admin\controllers\api\cortex;

use app\models\AppLoginToken;
use app\models\Product;
use app\models\Response;
use app\models\search\ProductSearch;
use app\models\ShoppingList;
use app\modules\admin\controllers\api\cortex\services\EcommerceManagement;
use app\modules\admin\controllers\api\cortex\services\ProductManagement;
use app\modules\admin\controllers\api\cortex\services\ShoppingListManagement;
use app\modules\admin\controllers\api\cortex\services\UserManagement;
use yii\rest\ActiveController;


class BestonMarketPlaceVrApiController extends ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject(){
        $req = [
            'API Name' => $this->className(),
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];
        $req_dump =  print_r($req,true);
        $fp = fopen('BestonMarketPlaceVrApiController.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }
    /*************************************Product Web Services Start********************************************/
    /**
     * Returns all e-commerce enabled products according to the language_id
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * <br> param - 3 :
     * <br> language_id {for Cortex : always set '2'};
     * <br>
     * @return Product[]
     */
    public function actionGetAllProducts()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
        $product = new ProductManagement();
        return $product->getAllProducts($_POST['language_id']);
        } else {
            return "User Not Authenticated!";
        }
    }
    /**
     * Return details of a Single Product and their translations according to the language_id and product_id
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * <br> param - 3 :
     * <br> language_id;
     * <br>
     * <br> param - 4 :
     * <br> product_id;
     * <br>
     * @return Product
     */
    public function actionGetProductDetails()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $product = new ProductManagement();
            return $product->getProductDetails($_POST['product_id'],$_POST['language_id']);
        } else {
            return "User Not Authenticated!";
        }
    }
    /*************************************Product Web Services End********************************************/

    /*************************************User Web Services Start********************************************/
    /**
     * Searches for the provided user email in app_user.
     *
     * @return boolean
     *
     */
    public function actionForgetPasswordEmail()
    {
        $this->logPostObject();
        $user = new UserManagement();
        return $user->forgetPasswordEmail($_POST['email_address']);
    }

    /**
     * Searches for the provided user email in app_user.
     *
     * @return boolean
     *
     */
    public function actionValidateUserEmail()
    {
        $email_address = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
        $user = new UserManagement();
        return $user->checkValidUserEmail($email_address);
    }

    /**
     * Validates user social media credentials
     * Generates login token which will authenticate user for all web services
     *
     * @return boolean
     *
     */
    public function actionValidateSocialMediaAuthData()
    {
        $user = new UserManagement();
        return $user->validateSocialMediaAuthData($_POST);
    }
    /**
     * Validates user credentials
     * Generates login token which will authenticate user for all web services
     *
     * @return boolean
     *
     */
    public function actionValidateUserAppAuthData()
    {
        $user = new UserManagement();
        return $user->validateUserAppAuthData($_POST);
    }


    /**
     * Registers user's device info to app
     * @return boolean
     *
     */
    public function actionRegisterUserDeviceToApp()
    {
        $user = new UserManagement();
        return $user->registerUserDeviceToApp();
    }
    /*************************************User Web Services End********************************************/

    /*************************************Shopping List Web Services Start************************************/

    /**
     * This web service would check if the given Shopping list already exists for the user, If not it will create a new shopping list
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * <br> param - 3 :
     * <br> name {shopping list name};
     * <br>
     * @return Integer id
     */

    public function actionCreateShoppingList()
    {

        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $list = new ShoppingListManagement();
            return $list->createShoppingList($_POST['user_id'],$_POST['name']);
        } else {

            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }

    /**
     * This web service would return a users all shopping lists
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * @return Integer id
     */

    public function actionReturnAllShoppingLists()
    {
        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $list = new ShoppingListManagement();
            return $list->returnAllShoppingLists($_POST['user_id']);
        } else {
            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();

        }
    }

    /**
     * This web service would delete a user's shopping list
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * <br> param - 3 :
     * <br> list_id; {send -1 if deleteAll()}
     * <br>
     * @return Integer id
     */

    public function actionDeleteShoppingList()
    {

        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $list = new ShoppingListManagement();
            return $list->deleteShoppingList($_POST['user_id'],$_POST['list_id']);
        } else {

            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }


    /**
     * This web service would update the name of the shopping list
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * <br> param - 3 :
     * <br> list_id; {send -1 if deleteAll()}
     * <br>
     * <br> param - 4 :
     * <br> name;
     * <br>
     * @return Integer id
     */

    public function actionUpdateShoppingList()
    {
        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $list = new ShoppingListManagement();
            return $list->updateShoppingList($_POST['user_id'],$_POST['list_id'],$_POST['name']);
        } else {

            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }

    /**
     * This web service would update the name of the shopping list
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * @return Integer id
     */

    public function actionGetAllCategories()
    {
        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $categories = new ProductManagement();
            return $categories->getAllCategories();
        } else {
            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }

    /**
     * This web service would update the name of the shopping list
     * <br>
     * <br> param - 1 :
     * <br> user_id;
     * <br>
     * <br> param - 2 :
     * <br> token;
     * <br>
     * @return Integer id
     */

    public function actionAllProductsForCategory()
    {
        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $productManager = new ProductManagement();
            return $productManager->getAllProductsForCategories();

        } else {
            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }


    public function actionCreateOrderOnTes()
    {
        $response = new Response();
        $this->logPostObject();
        $data = json_decode(file_get_contents('php://input'), true);

        $auth = AppLoginToken::findOne(['user_id' => $data['user_id'], 'token' => $data['token']]);
        if (isset($auth)) {
            $ecommerceManager = new EcommerceManagement();
            return $ecommerceManager->createOrderOnTes($data);
        } else {
            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }

    public function actionSearchProduct()
    {
        $response = new Response();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $productManager = new ProductManagement();
            return $productManager->searchProducts($_POST['search_string']);

        } else {
            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }

    public function actionGetDeliveryInformation()
    {
        $response = new Response();
        $this->logPostObject();

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $ecommerceManager = new EcommerceManagement();
            return $ecommerceManager->getDeliveryInformation($_POST['user_id']);
        } else {
            $response->setResponse(false);
            $response->setMessage("User Not Authenticated!");
            return $response->getFullResponse();
        }
    }

    /**
     * Processes stripe payment
     * @return boolean
     */
    public function actionProcessStripePayment()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $ecommerceManager = new EcommerceManagement();

            $payment = [
                'stripe_token' => $_POST['stripe_token'],
                'amount' => $_POST['amount'],
                'desc' => $_POST['desc'],
            ];

            return $ecommerceManager->processStripePayments($payment);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Searches for the provided user email in app_user.
     * if email does not exist then signup user with the given email address
     * @return boolean
     */
    public function actionSignUpWithUser()
    {
        $user = new UserManagement();
        $this->logPostObject();
        return $user->insertNewUser($_POST);
    }

    /**
     * Updates User Information
     * @return boolean
     */
    public function actionUpdateUser()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $user = new UserManagement();
            return $user->updateUser($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Updates User Information
     * @return boolean
     */
    public function actionAddProductsToShoppingList()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $shoppingListManagement = new ShoppingListManagement();
            return $shoppingListManagement->addProductsToShoppingList($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Updates User Information
     * @return boolean
     */
    public function actionUpdateShoppingListItem()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $shoppingListManagement = new ShoppingListManagement();
            return $shoppingListManagement->actionUpdateShoppingListItem($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Updates User Information
     * @return boolean
     */
    public function actionDeleteShoppingListItem()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $shoppingListManagement = new ShoppingListManagement();
            return $shoppingListManagement->deleteShoppingListItem($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Updates User Information
     * @return boolean
     */
    public function actionGetAllShoppingListProducts()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $shoppingListManagement = new ProductManagement();
            return $shoppingListManagement->getAllShoppingListProducts($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Updates User Information
     * @return boolean
     */
    public function actionGetAllPantryProducts()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $shoppingListManagement = new ProductManagement();
            return $shoppingListManagement->getAllPantryProducts($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

}