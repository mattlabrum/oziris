<?php

namespace app\modules\admin\controllers\api\jfc;
use app\modules\admin\controllers\api\jfc\services\BestonInventoryManager;

/**
 * This is the class for REST controller "app\modules\admin\controllers\ProductController".
 */
class OzirisBimApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject(){
        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];
        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }



    /*------------------------------------------------------------------------------------------------------------------------------------------------*/
    /**
     * Return List of items in an order
     * @return Boolean
     */
    public function actionReturnOrderItems()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['qr_code'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->returnOrderItems($qr_code);
        }else{
            return "Invalid User Name / Password";
        }
    }


    /**
     * validate item QR code, assign order ID to batch_item and mark item as sold
     * @return Boolean
     */
    public function actionPickAndPack()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['qr_code'];
        $order_id = $_POST['order_id'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->pickAndPack($qr_code, $order_id);
        }else{
            return "Invalid User Name / Password";
        }
    }

    /**
     * attach box_qr_code to pallet_qr_code
     * @return Boolean
     */
    public function actionAssignBoxToPallet()
    {
        $this->logPostObject();

        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $box_qr_codes =  explode(",",$_POST['box_qr_code']);
        $pallet_qr_code = $_POST['pallet_qr_code'];

//        return $box_qr_codes;

        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->assignBoxToPallet($box_qr_codes, $pallet_qr_code);
        }else{
            return "Invalid User Name / Password";
        }
    }

    /**
     * enable the contents of the box to be marked as sold again and discard the box seal
     * @return Boolean
     */
    public function actionDiscardBoxSeal()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['qr_code'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->discardBoxSeal($qr_code);
        }else{
            return "Invalid User Name / Password";
        }
    }

    public function actionDiscardOrderItems()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['qr_code'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->discardOrderItems($qr_code);
        }else{
            return "Invalid User Name / Password";
        }
    }

//    /**
//     * attach box_qr_code to pallet_qr_code
//     * @return Boolean
//     */
//    public function actionReturnBoxItems()
//    {
//        $user = [
//            'password' => $_POST['password'],
//            'email_address' => $_POST['email_address'],
//        ];
//        $box_qr_code = $_POST['box_qr_code'];
//        $pallet_qr_code = $_POST['pallet_qr_code'];
//        $bim = new BestonInventoryManager();
//        if($bim->validateUserInformation($user)){
//            return $bim->assignBoxToPallet($box_qr_code, $pallet_qr_code);
//        }else{
//            return "Invalid User Name / Password";
//        }
//    }


    /*------------------------------------------------------------------------------------------------------------------------------------------------*/



    /**
     * validate app login details
     * @return Boolean
     */
    public function actionValidateLoginInfo()
    {
        $data = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $bim = new BestonInventoryManager();
        return $bim->validateUserInformation($data);
    }
    /**
     * batches against product code
     * @return Boolean
     */
    public function actionGetProductBatches()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $bim = new BestonInventoryManager();
        return $bim->getProductBatches($user,$_POST['product_code']);
    }
    /**
     * creates a batch item
     * @return Boolean
     */
    public function actionCreateBatchItem()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $batch_item = [
            'batch_id' => $_POST['batch_id'],
            'qr_code' => $_POST['qr_code']
        ];
        $bim = new BestonInventoryManager();
        return $bim->attachProductToBatch($user,$batch_item);
    }

    /**
     * attach order_qr_seal to order
     * @return Boolean
     */
    public function actionAssignBoxSeal()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $qr_code = $_POST['seal_qr_code'];
        $order_id = $_POST['order_id'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->assignBoxSeal($qr_code, $order_id);
        }else{
            return "Invalid User Name / Password";
        }
    }

    /**
     * finalize order, change order status to 'ReadyToDispatch' and upload image
     * @return Boolean
     */
    public function actionFinalizeOrder()
    {
        $user = [
            'password' => $_POST['password'],
            'email_address' => $_POST['email_address'],
        ];
        $order_id = $_POST['order_id'];
        $bim = new BestonInventoryManager();
        if($bim->validateUserInformation($user)){
            return $bim->finalizeOrder($_FILES, $order_id);
        }else{
            return "Invalid User Name / Password";
        }
    }
}