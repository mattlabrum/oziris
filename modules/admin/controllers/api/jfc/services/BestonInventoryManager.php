<?php

namespace app\modules\admin\controllers\api\jfc\services;

use app\models\AppUserDeviceInfo;
use app\models\base\ShippingPallet;
use app\models\Batch;
use app\models\BatchItem;
use app\models\Order;
use app\models\OrderItem;
use app\models\OrderStatus;
use app\models\OrderStatusTime;
use app\models\Product;
use app\models\Response;
use app\models\ShippingBox;
use app\utils\BestonSMSSender;
use app\utils\PushNotificationSender;
use app\utils\ReturnDeliveryDay;
use dektrium\user\models\User;
use yii\base\Exception;
use yii\httpclient\Client;


class BestonInventoryManager
{

    public function validateUserInformation($data){
        $user = User::find()->where(['email' => $data['email_address']])->one();
        if(isset($user)){
            return password_verify($data['password'], $user->password_hash);
        }

    }

    public function getProductBatches($user,$product_code){
        if ($this->validateUserInformation($user)){
            $product = Product::find()->where(['id'=> $product_code])->orWhere(['barcode'=> $product_code])->one();
        }
        if(isset($product)){
            $batches = Batch::find()->where('product_id != batch_number')->andWhere('manufacture_date != "" ')->andWhere(['product_id' =>$product->id])->all();
            foreach ($batches as $batch){
                $batches_to_return[] = [
                    'id' => $batch->id,
                    'manufacturer_date' => $batch->manufacture_date,
                    'batch_number' => $batch->batch_number,
                    'product_name' => $batch->product->product_name,
                ];
            }
            return $batches_to_return;
        }else{
            return false;
        }
    }

    public function attachProductToBatch($user,$data){
        if ($this->validateUserInformation($user)){
            $batch_item = BatchItem::find()->where(['qr_code_text'=> $data['qr_code']])->one();
        }
        if(isset($batch_item)){
            $batch_item->delete();
        }
        $new_batch_item = new BatchItem();
        $new_batch_item->batch_id = $data['batch_id'];
        $new_batch_item->qr_code_text = $data['qr_code'];
        $new_batch_item->item_status = '';
        return $new_batch_item->save(false);
    }


    public function getOrderItems($raw_items, $order_id) {

//        return $raw_items[0]->quantity;

        foreach($raw_items as $item){
            if($item->product->is_collection){
                foreach($item->product->getCollectionItems(2) as $collection_item){
                    $items[] = [
                        'order_quantity' => $collection_item['quantity'] * $item->quantity,
                        'product_id' => $collection_item['product']['id'],
                        'product_name' => $collection_item['product']['product_name'],
                    ];
                }
            }else{
                $items[] = [
                    'order_quantity' => $item->quantity,
                    'product_id' => $item->product->id,
                    'product_name' => $item->product->product_name,
                ];
            }
        }

        $groups = array();
        foreach ($items as $item) {
            $key = $item['product_id'];
            if (!isset($groups[$key])) {
                $groups[$key] = array(
                    'product_id' => $item['product_id'],
                    'product_name' => $item['product_name'],
                    'order_quantity' => $item['order_quantity'],
                );
            } else {
                $groups[$key]['order_quantity'] += $item['order_quantity'];
            }
        }

        $products = [];
        foreach ($groups as $group) {

            $group['picked_quantity'] = count(BatchItem::find()
                ->joinWith('batch')
                ->where(['batch.product_id' => $group['product_id'] , 'batch_items.order_id' => $order_id ])
                ->all());
            $products[] = $group;

        }
        return $products;
    }


    public function pickAndPack($qr_code, $order_id)
    {
        $response = new Response();
        $batch_item = BatchItem::find()
                                ->where(['item_status' => NULL, 'qr_code_text' => $qr_code])
                                ->orWhere(['item_status' => '', 'qr_code_text' => $qr_code])
                                ->one();
        if (isset($batch_item)) {
            $order = Order::find()->where(['woo_order_id' => $order_id])->one();
            if (isset($order)) {
                $items = $this->getOrderItems($order->orderItems, $order_id);
                foreach ($items as $item) {
                    if ($batch_item->batch->product_id == $item['product_id'] && $item['picked_quantity'] < $item['order_quantity']) {
                        $batch_item->item_status = 'sold';
                        $batch_item->order_id = $order_id;
                        $response->setMessage("");
                        $response->setResponse([
                            'status' =>$batch_item->save(false),
                            'product_id' =>$batch_item->batch->product_id,
                        ]);
                        break;
                    } else {
                        $response->setMessage("Scanned Product is not required in the selected order!");
                        $response->setResponse(false);
                    }
                }
            }
        } else {
            $response->setMessage("Scanned Product is either sold/discarded or it is not scanned in the system");
            $response->setResponse(false);
        }
        return $response->getFullResponse();
    }

    public function returnOrderItems($qr_code)
    {
        $response = new Response();

        $order = Order::find()->where(['qr_code_text' => $qr_code])->one();
        if(isset($order)){

            $old_status_id = $order->status_id;

            if($order->order_image == NULL || $order->order_image == ''){
                $returnInfo = [
                    'order_id' => $order->woo_order_id,
                    'order_items' => $this->getOrderItems($order->orderItems,$order->woo_order_id ),
                ];

//          **********ORDER STATUS NUMBER LIST************
//          1	    processing	        Order Confirmed
//        	2	    pending	            Pending Payment
//        	3	    on-hold	            On Hold
//        	4	    delivered	        Delivered
//        	5	    cancelled	        Cancelled
//        	6	    refunded	        Refunded
//        	7	    failed	            Failed
//        	8	    completed	        Dispatched
//        	9	    ready-to-dispatch	Ready To Dispatch
//        	10	    awaiting-picking	Preparing Order
//        	11	    with-driver	        On Transit
//        	12	    test	            Test Order

                if($old_status_id == 1 || $old_status_id == 3 || $old_status_id == 2 || $old_status_id == 7 || $old_status_id == 12  ) {
                    $order->status_id = 10;
                }
                $order->updated_at = time();
                if($order->save(false)){
                    $client = new Client();
                    $client->createRequest()
                        ->setMethod('post')
                        ->setUrl(getenv('SERVER_TYPE').'/update_order_details')
                        ->setData(
                            [
                                'order_id' => $order->woo_order_id,
                                'order_note' => $order->order_note,
                                'order_status' => $order->status->name,
                            ])->send();

                    $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $order->woo_order_id, 'status_id' => 10])->one();
                    $orderStatusTime->time = $order->updated_at;
                    $orderStatusTime->save();

                    if($old_status_id == 1 ){
                        if($order->customer->email_address != 'default' ){
                            $devices_ios = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'iOS' ])->all();
                            if( count($devices_ios) > 0 ){
                                $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $order->status_id])->one()->description;
                                PushNotificationSender::sendIOSNotification($devices_ios,$notification,$order->woo_order_id);
                            }
                            $devices_android = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'android' ])->all();
                            if( count($devices_android) > 0 ){
                                $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $order->status_id])->one()->description;
                                PushNotificationSender::sendAndroidNotification($devices_android,$notification);
                            }
                        }
//                        $sms_notification = "Order Status for Order # ".$order->woo_order_id." has been changed to : ".OrderStatus::find()->where(['name' => 'awaiting-picking'])->one()->description;
//                        BestonSMSSender::sendSMSNotification($order->billingAddress->getContactNo(),$sms_notification) ;
                    }

                }






                $response->setResponse($returnInfo);
            }else{
                $response->setResponse(false);
                $response->setMessage('The Order is already fulfilled');
            }
        }else{
            $response->setResponse(false);
            $response->setMessage('Order Not Found');
        }
        return $response->getFullResponse();
    }


    public function assignBoxToPallet($box_qr_codes, $pallet_qr_code)
    {
        $response = new Response();

        $pallet = ShippingPallet::find()->where(['pallet_qr' => $pallet_qr_code])->one();
        $errorString = "";
        foreach( $box_qr_codes as $box_qr_code  ){
            $box = ShippingBox::find()->where(['box_qr' => $box_qr_code])->one();
            if(isset($pallet)){
                if(isset($box)){
                    $box->pallet_id = $pallet->id;
                    if($box->save(false)){
                        $response->setResponse(true);
                        $response->setMessage('Box linked Successfully!');
                    }
                }else{
                    $errorString = $errorString." ".$box_qr_code;
                }
            }else{
                if(isset($box)){
                    $pallet = new ShippingPallet();
                    $pallet->pallet_qr = $pallet_qr_code;
                    $pallet->save(false);

                    $box->pallet_id = $pallet->id;
                    if($box->save(false)){
                        $response->setResponse(true);
                        $response->setMessage('Box linked Successfully!');
                    }
                }else{
                    $errorString = $errorString." ".$box_qr_code;
                }
            }
        }
        if($errorString != ""){
            $errorString = "The following Box QR Codes were not linked to Pallet because they do not exist in the system : " . $errorString;
        }else {
            $errorString = "All Boxes linked to the Pallet";
        }
        $response->setMessage($errorString);
        return $response->getFullResponse();
    }

    public function assignBoxSeal($qr_code, $order_id)
    {
        $response = new Response();
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $box = ShippingBox::find()->where(['box_qr' => $qr_code])->one();
        try {
            if (!isset($box)) {
                $box = new ShippingBox();
                $box->box_qr = $qr_code;
                $box->order_id = $order_id;
                if (!$box->save()) {
                    $response->setResponse(false);
                    $response->setMessage('Unable to save record.');
                    throw new Exception('Unable to save record.');
                } else {
                    $response->setResponse(true);
                }
                $result = BatchItem::updateAll(['box_id' => $box->id], ['order_id' => $order_id, 'box_id' => null]);
            } else {
                $response->setResponse(false);
                $response->setMessage('Box already exists!');
                throw new Exception('Box already exists!');
            }
            $transaction->commit();
            return $response->getFullResponse();
        } catch (Exception $e) {
            $transaction->rollback();
            return $response->getFullResponse();
        }
    }

    public function discardOrderItems($qr_code)
    {
        $order = Order::find()->where(['qr_code_text' => $qr_code])->one();
        $response = new Response();

        if(isset($order)){

            $rows_updated = BatchItem::updateAll(['order_id' => null , 'box_id' => null , 'item_status' => '' ] , ['order_id' => $order->woo_order_id ]);

                if($rows_updated>0){
                    $response->setMessage($rows_updated." Items Updated !");
                    $response->setResponse(true);
                }else{
                    $response->setMessage("No Items found to be Updated !");
                    $response->setResponse(false);
                }

            $order->order_image = NULL;
            $order->save(false);

        }else{
            $response->setMessage("Order Not Found");
            $response->setResponse(false);

        }

        return $response->getFullResponse();
    }


    public function discardBoxSeal($qr_code)
    {
        $response = new Response();
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $box = ShippingBox::find()->where(['box_qr' => $qr_code])->one();
        try {
            if (isset($box)) {
                $result = BatchItem::updateAll(['box_id' => null], ['box_id' => $box->id]);
                if (!$box->delete()) {
                    $response->setResponse(false);
                    $response->setMessage('Unable to save record.');
                } else {
                    $response->setResponse(true);
                }
            } else {
                $response->setResponse(false);
                $response->setMessage('box not found!');
            }
            $transaction->commit();
            return $response->getFullResponse();
        } catch (Exception $e) {
            $transaction->rollback();
            return $response->getFullResponse();
        }
    }

    public function finalizeOrder($data, $order_id)
    {

        $order = Order::find()->where(['woo_order_id' => $order_id])->one();
        if(isset($order)){
            $uploaddir = 'order/picture/';
            $file = basename($data['order_image']['name']);
            $uploadfile = $uploaddir . 'order-'.$order_id.'.'.pathinfo($file, PATHINFO_EXTENSION);;
            if (move_uploaded_file($data['order_image']['tmp_name'], $uploadfile)) {

                $readyToOrder = OrderStatus::find()->where(['name' => 'ready-to-dispatch'])->one()->id;


                if($order->status_id != $readyToOrder){

                }
                $order->status_id = $readyToOrder;
                $order->order_image = $uploadfile;

                if($order->customer->email_address != 'default' ){
                    $devices_ios = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'iOS' ])->all();
                    if( count($devices_ios) > 0 ){
                        $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $order->status_id])->one()->description;
                        PushNotificationSender::sendIOSNotification($devices_ios,$notification,$order->woo_order_id);
                    }
                    $devices_android = AppUserDeviceInfo::find()->where(['user_id' => $order->customer_id, 'device_type' => 'android' ])->all();
                    if( count($devices_android) > 0 ){
                        $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $order->status_id])->one()->description;
                        PushNotificationSender::sendAndroidNotification($devices_android,$notification);
                    }
                }
                $sms_notification = "Your order number # ".$order->woo_order_id." is now ready and it will be delivered on ".ReturnDeliveryDay::getDeliveryDate($order->getShippingAddress()->one()->postcode);
                BestonSMSSender::sendSMSNotification($order->getBillingAddress()->one()->getContactNo(),$sms_notification) ;

                $order->save(false);

                $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $order->woo_order_id, 'status_id' => $order->status_id])->one();
                $orderStatusTime->time = $order->updated_at;
                $orderStatusTime->save();

                $client = new Client();
                $response = $client->createRequest()
                    ->setMethod('post')
                    ->setUrl(getenv('SERVER_TYPE').'/update_order_details')
                    ->setData(
                        [
                            'order_id' => $order->woo_order_id,
                            'order_note' => $order->order_note,
                            'order_status' => $order->status->name,
                        ])->send();

                return true;
            } else {
                return false;
            }
        }else{
            return 'Order Not Found';
        }
    }

    /* remove after updating live and staging */

    public function insertOldBoxCodes(){
        $orders = Order::find()->where(['not', ['order_qr_seal' => null]])->all();

        foreach($orders as $order){

            $boxes = explode(" ", $order->order_qr_seal);

            foreach($boxes as $box){
                $b = new ShippingBox();
                $b->box_qr = $box;
                $b->order_id = $order->woo_order_id;
                $b->save(true);
            }
        }

//        return $orders;
    }

}