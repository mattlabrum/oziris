<?php

namespace app\modules\admin\controllers\api\reports;
use app\modules\admin\controllers\api\reports\services\EcommerceReportsManagement;
use app\modules\admin\controllers\api\reports\services\ReportsManagement;


/**
 * This is the class for REST controller "app\modules\admin\controllers\ProductController".
 */
class OzirisReportsApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    /*************************************Reports Web Services Start********************************************/

    public function actionGetCountriesReportData()
    {
        $return = new ReportsManagement();
        return $return->getCountriesReportData();
    }

    public function actionGetProductGroupData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getEcommerceProductGroupDropdownData();
    }

    public function actionGetProductTypeData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getEcommerceProductTypeDropdownData();
    }

    public function actionGetProductNatureData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getEcommerceProductNatureDropdownData();
    }

    public function actionGetCitiesReportData()
    {
        $return = new ReportsManagement();
        return $return->getCitiesReportData();
    }

    public function actionGetAgeRangeReportData()
    {
        $return = new ReportsManagement();
        return $return->getAgeRangeReportData();
    }

    public function actionGetGenderReportData()
    {
        $return = new ReportsManagement();
        return $return->getGenderReportData();
    }

    public function actionPopulateCityDropDown()
    {
        $return = new ReportsManagement();
        return $return->populateCityDropDown();
    }

    public function actionPopulateCountryDropDown()
    {
        $return = new ReportsManagement();
        return $return->populateCountryDropDown();
    }

    public function actionPopulateDataTable()
    {
        $return = new ReportsManagement();
        return $return->populateDataTable();
    }

    public function actionPopulateAgeDropDown()
    {
        $return = new ReportsManagement();
        return $return->populateAgeDropDown();
    }

    public function actionPopulateGenderDropDown()
    {
        $return = new ReportsManagement();
        return $return->populateGenderDropDown();
    }

    public function actionUpdateTableDataReport()
    {
        $city = $_POST['city'];
        $country = $_POST['country'];
        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $age = $_POST['age'];
        $gender = $_POST['gender'];

        $return = new ReportsManagement();
        return $return->updateTableDataReport($city, $country, $date_from, $date_to, $age, $gender);
    }


    public function actionGetMostPopularScannedProducts()
    {
        $city = $_POST['city'];
        $country = $_POST['country'];
        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $age = $_POST['age'];
        $gender = $_POST['gender'];
        $sort = $_POST['sort'];
        $limit = $_POST['limit'];
        $return = new ReportsManagement();
        return $return->getMostPopularScannedProducts($city, $country, $date_from, $date_to, $age, $gender, $sort, $limit);
    }

    public function actionGetScannedProductsByDuration()
    {
        $city = $_POST['city'];
        $country = $_POST['country'];
        $date_from = $_POST['date_from'];
        $date_to = $_POST['date_to'];
        $age = $_POST['age'];
        $gender = $_POST['gender'];
        $sort = $_POST['duration'];
        $return = new ReportsManagement();
        return $return->getScannedProductsByDuration($city, $country, $date_from, $date_to, $age, $gender, $sort);
    }

    public function actionGetEcommerceSalesData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getEcommerceSalesData($_GET);
    }

    public function actionGetEcommerceContinentDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populateContinentDropDown();
    }

    public function actionGetEcommerceCountryDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populateCountryDropDown();
    }

    public function actionGetEcommerceStateDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populateStateDropDown();
    }

    public function actionGetEcommercePostcodeDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populatePostCodeDropDown();
    }

    public function actionGetEcommerceAgeDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populateAgeDropDown();
    }

    public function actionGetEcommerceGenderDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populateGenderDropDown();
    }

    public function actionGetEcommerceTradingChannelDropdownData()
    {
        $return = new EcommerceReportsManagement();
        return $return->populateTradingChannelDropDown();
    }

    public function actionSetOrderContinents()
    {
        $return = new EcommerceReportsManagement();
        return $return->setOrderContinents();
    }

    public function actionGetSalesComparison()
    {
        $return = new EcommerceReportsManagement();
        return $return->getSalesComparison($_GET);
    }

    public function actionGetOrdersComparison()
    {
        $return = new EcommerceReportsManagement();
        return $return->getOrdersComparison($_GET);
    }

    public function actionGetTotalSalesByStatePieChartData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getTotalSalesByStatePieChartData($_GET);
    }

    public function actionGetTotalSalesByChannelPieChartData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getTotalSalesByChannelPieChartData($_GET);
    }

    public function actionGetTotalSalesByProductTypePieChartData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getTotalSalesByProductTypePieChartData($_GET);
    }

    public function actionGetTotalSalesByProductGroupPieChartData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getTotalSalesByProductGroupPieChartData($_GET);
    }

    public function actionGetTotalSalesByProductNaturePieChartData()
    {
        $return = new EcommerceReportsManagement();
        return $return->getTotalSalesByProductNaturePieChartData($_GET);
    }

    public function actionImportOrderItemPrices()
    {
        $return = new EcommerceReportsManagement();
        return $return->importOrderItemPrices($_POST['from'],$_POST['to']);
    }

    public function actionImportActualOrderItemPrices()
    {
        $return = new EcommerceReportsManagement();
        return $return->importActualOrderItemPrices($_POST['from'],$_POST['to']);
    }

    /*************************************Reports Web Services End********************************************/

}