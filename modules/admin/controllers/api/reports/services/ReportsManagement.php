<?php

namespace app\modules\admin\controllers\api\reports\services;


use app\models\base\AppUser;
use app\models\Product;
use app\models\ProductGroup;
use app\models\ReviewRating;
use app\models\ScannedProduct;
use yii\db\Query;

class ReportsManagement
{

    public function getCountriesReportData()
    {
        $conn = \Yii::$app->db;
        $command = $conn->createCommand('
          SELECT app_user.country ,COUNT(app_user.id) AS users
          FROM app_user
          GROUP BY country
          ORDER BY users DESC'
        );
        $total_users = $this->getTotalUsers();
        $data = [];
        foreach ($command->queryAll() as $row) {
            $data[] = [
                'country' => $row['country'],
                'users' => $row['users'],
                'total_users' => $total_users,
            ];
        }
        return $data;
    }




    public function getCitiesReportData()
    {
        $conn = \Yii::$app->db;
        $command = $conn->createCommand('
          SELECT scanned_product.city ,COUNT(scanned_product.id) AS users
          FROM scanned_product
          GROUP BY city
          ORDER BY users DESC'
        );
        $total_users = $this->getTotalScans();
        $data = [];
        foreach ($command->queryAll() as $row) {
            $data[] = [
                'city' => $row['city'],
                'users' => $row['users'],
                'total_users' => $total_users,
            ];
        }
        return $data;
    }

    public function getTotalUsers()
    {
        return count(AppUser::find()->all());
    }

    public function getTotalProducts()
    {
        return count(Product::find()->all());
    }

    public function getTotalCounterfeitScans()
    {
        return count(ScannedProduct::find()->where(['batch_id' => null])->all());
    }

    public function getTotalScans()
    {

        return count(ScannedProduct::find()->select('id')->all());
//        return count(ScannedProduct::find()->where(['batch_id is not null'])->all());
    }

    public function getTotalReviewRatings()
    {
        return count(ReviewRating::find()->all());
    }

    public function getFilteredUsers($city, $country, $date_from, $date_to, $age, $gender)
    {
        if ($age == '') {
            $query = AppUser::find();
            if ($gender != '') {
                $query->where("gender = '$gender'");
            }
//            if ($city != '') {
//                $query->andWhere("city = '$city'");
//            }
            if ($country != '') {
                $query->andWhere("country = '$country' ");
            }
            if ($date_from != '' && $date_to != '') {
                $query->andWhere('created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to));
            }
            return ['users' => count($query->all())];
        } else {
            $conn = \Yii::$app->db;
            $sql = "SELECT ";
            if ($age == '18 - 30') {
                $sql = $sql . "SUM(IF(age BETWEEN 18 and 30,1,0)) as '18 - 30'";
            } elseif ($age == '31 - 45') {
                $sql = $sql . "SUM(IF(age BETWEEN 31 and 45,1,0)) as '31 - 45'";
            } elseif ($age == '46 - 60') {
                $sql = $sql . "SUM(IF(age BETWEEN 46 and 60,1,0)) as '46 - 60'";
            } elseif ($age == 'Over 60') {
                $sql = $sql . "SUM(IF(age >=61, 1, 0)) as 'Over 60'";
            } elseif ($age == 'Un-disclosed') {
                $sql = $sql . "SUM(IF(age IS NULL, 1, 0)) as 'Un-disclosed'";
            }
            $sql = $sql . " FROM (SELECT TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age FROM app_user ";
            if ($gender != '' || $city != '' || $country != '' || ($date_from != '' && $date_to != '')) {
                $sql = $sql . " where (";
            }
            if ($gender != '') {
                $sql = $sql . "gender = '$gender'";
            }
            if ($city != '') {
                if ($gender != '') {
                    $sql = $sql . " and ";
                }
                $sql = $sql . "city = '$city'";
            }
            if ($country != '') {
                if ($gender != '' || $city != '') {
                    $sql = $sql . " and ";
                }
                $sql = $sql . "country = '$country' ";

            }
            if ($date_from != '' && $date_to != '') {
                if ($gender != '' || $city != '' || $country != '') {
                    $sql = $sql . " and ";
                }

                $sql = $sql . 'created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to);
            }
            if ($gender != '' || $city != '' || $country != '' || ($date_from != '' && $date_to != '')) {
                $sql = $sql . ')) as derived';
            } else {
                $sql = $sql . ') as derived';
            }


            $command = $conn->createCommand($sql);
            $response = $command->queryAll();

            return ['users' => (isset($response[0][$age]) ? $response[0][$age] : '0')];
        }

    }

    public function getFilteredReviewRatings($city, $country, $date_from, $date_to, $age, $gender)
    {

        if ($age == '') {
            $query = ReviewRating::find();
            $query->joinWith('app_user');
            if ($gender != '') {
                $query->where("app_user.gender = '$gender'");
            }
//            if ($city != '') {
//                $query->andWhere("app_user.city = '$city'");
//            }
            if ($country != '') {
                $query->andWhere("app_user.country = '$country' ");
            }
            if ($date_from != '' && $date_to != '') {
                $query->andWhere('review_rating.created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to));
            }
            return ['review_ratings' => count($query->all())];
        } else {
            $conn = \Yii::$app->db;
            $sql = "SELECT ";
            if ($age == '18 - 30') {
                $sql = $sql . "SUM(IF(age BETWEEN 18 and 30,1,0)) as '18 - 30'";
            } elseif ($age == '31 - 45') {
                $sql = $sql . "SUM(IF(age BETWEEN 31 and 45,1,0)) as '31 - 45'";
            } elseif ($age == '46 - 60') {
                $sql = $sql . "SUM(IF(age BETWEEN 46 and 60,1,0)) as '46 - 60'";
            } elseif ($age == 'Over 60') {
                $sql = $sql . "SUM(IF(age >=61, 1, 0)) as 'Over 60'";
            } elseif ($age == 'Un-disclosed') {
                $sql = $sql . "SUM(IF(age IS NULL, 1, 0)) as 'Un-disclosed'";
            }
            $sql = $sql . " FROM (SELECT TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age FROM app_user join review_rating where ";
            $sql = $sql . "(";
            $sql = $sql . "app_user.id = review_rating.user_id";
            if ($gender != '') {
                $sql = $sql . " and ";
                $sql = $sql . "app_user.gender = '$gender'";
            }
            if ($city != '') {
                $sql = $sql . " and ";
                $sql = $sql . "app_user.city = '$city'";
            }
            if ($country != '') {
                $sql = $sql . " and ";
                $sql = $sql . "app_user.country = '$country' ";

            }
            if ($date_from != '' && $date_to != '') {
                $sql = $sql . " and ";
                $sql = $sql . 'review_rating.created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to);
            }

            $sql = $sql . ')) as derived';

            $command = $conn->createCommand($sql);
            $response = $command->queryAll();

            return ['review_ratings' => (isset($response[0][$age]) ? $response[0][$age] : '0')];
        }
    }

    public function updateTableDataReport($city, $country, $date_from, $date_to, $age, $gender)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $result = [];

        if ($age == '') {
            $query = ScannedProduct::find();
            $query->joinWith('app_user');
            if ($gender != '') {
                $query->where("app_user.gender = '$gender'");
            }
//            if ($city != '') {
//                $query->andWhere("app_user.city = '$city'");
//            }
            if ($country != '') {
                $query->andWhere("app_user.country = '$country' ");
            }
            if ($date_from != '' && $date_to != '') {
                $query->andWhere('scanned_product.created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to));
            }
            $result[] = [
                'scanned_product' => count($query->all())
            ];

            $query->andWhere(['scanned_product.batch_id' => null]);

            $result[] = [
                'counterfeit_scans' => count($query->all())
            ];
        } else {
            $conn = \Yii::$app->db;
            $sql = "SELECT ";
            if ($age == '18 - 30') {
                $sql = $sql . "SUM(IF(age BETWEEN 18 and 30,1,0)) as '18 - 30'";
            } elseif ($age == '31 - 45') {
                $sql = $sql . "SUM(IF(age BETWEEN 31 and 45,1,0)) as '31 - 45'";
            } elseif ($age == '46 - 60') {
                $sql = $sql . "SUM(IF(age BETWEEN 46 and 60,1,0)) as '46 - 60'";
            } elseif ($age == 'Over 60') {
                $sql = $sql . "SUM(IF(age >=61, 1, 0)) as 'Over 60'";
            } elseif ($age == 'Un-disclosed') {
                $sql = $sql . "SUM(IF(age IS NULL, 1, 0)) as 'Un-disclosed'";
            }
            $sql = $sql . " FROM (SELECT TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age FROM app_user join scanned_product where ";
            $sql = $sql . "(";
            $sql = $sql . "app_user.id = scanned_product.user_id";
            if ($gender != '') {
                $sql = $sql . " and ";
                $sql = $sql . "app_user.gender = '$gender'";
            }
//            if ($city != '') {
//                $sql = $sql . " and ";
//                $sql = $sql . "app_user.city = '$city'";
//            }
            if ($country != '') {
                $sql = $sql . " and ";
                $sql = $sql . "app_user.country = '$country' ";

            }
            if ($date_from != '' && $date_to != '') {
                $sql = $sql . " and ";
                $sql = $sql . 'scanned_product.created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to);
            }

            $sql_counterfeit = $sql . " and scanned_product.batch_id IS NULL " . ")) as derived";
            $sql_normal = $sql . ')) as derived';

            $command = $conn->createCommand($sql_normal);
            $response = $command->queryAll();

            $result[] = [
                'scanned_product' => (isset($response[0][$age]) ? $response[0][$age] : '0')
            ];

            $command = $conn->createCommand($sql_counterfeit);
            $response = $command->queryAll();

            $result[] = [
                'counterfeit_scans' => (isset($response[0][$age]) ? $response[0][$age] : '0')
            ];

        }
        $result[] = $this->getFilteredReviewRatings($city, $country, $date_from, $date_to, $age, $gender);
        $result[] = $this->getFilteredUsers($city, $country, $date_from, $date_to, $age, $gender);
        return $result;
    }



    function getMostPopularScannedProducts($city, $country, $date_from, $date_to, $age, $gender, $sort, $limit){
        $conn = \Yii::$app->db;

        $sql = "SELECT count(scanned_product.batch_id) as scans , batch.product_id as product_id , product.product_name as product_name FROM app_user join batch, scanned_product , product where ( app_user.id = scanned_product.user_id and scanned_product.batch_id = batch.id and batch.product_id = product.id";

        if ($gender != '') {
            $sql = $sql . " and ";
            $sql = $sql . "app_user.gender = '$gender'";
        }
//        if ($city != '') {
//            $sql = $sql . " and ";
//            $sql = $sql . "app_user.city = '$city'";
//        }
        if ($country != '') {
            $sql = $sql . " and ";
            $sql = $sql . "app_user.country = '$country' ";

        }
        if ($date_from != '' && $date_to != '') {
            $sql = $sql . " and ";
            $sql = $sql . 'scanned_product.created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to);
        }
        if($age !=''){
            $sql = $sql." and (TIMESTAMPDIFF(YEAR, dob, CURDATE())";
            if ($age == '18 - 30') {
                $sql = $sql."BETWEEN 18 and 30 )";
            } elseif ($age == '31 - 45') {
                $sql = $sql."BETWEEN 31 and 45 )";
            } elseif ($age == '46 - 60') {
                $sql = $sql."BETWEEN 46 and 60 )";
            } elseif ($age == 'Over 60') {
                $sql = $sql." >60 )";
            } elseif ($age == 'Un-disclosed') {
                $sql = $sql." IS NULL )";
            }

        }
        $sql = $sql.") group by batch.product_id ORDER BY scans ".$sort;

        if($limit > 0){
            $sql = $sql." LIMIT ".$limit;
        }



        $command = $conn->createCommand($sql);
        $response = $command->queryAll();
        $formated_data = [];
        $labels = [];
        $data = [];
        foreach($response as $row){
            $labels[] = $row['product_name'];
            $data[] = $row['scans'];

        }
        $datasets[] = [
            'fillColor' => '#248556',
            'data' => $data
        ];

        $formated_data = [
            'labels' => $labels,
            'datasets' => $datasets
        ];
        return $formated_data;

    }

    function getScannedProductsByDuration($city, $country, $date_from, $date_to, $age, $gender, $duration){
        $conn = \Yii::$app->db;

        $sql = "select FROM_UNIXTIME(scanned_product.created_at,'%d') as day , FROM_UNIXTIME(scanned_product.created_at,'%m') as month , FROM_UNIXTIME(scanned_product.created_at,'%y') as year , count(*) as scans , product.id  from scanned_product join batch, product where (scanned_product.batch_id = batch.id and batch.product_id = product.id ";

        if ($gender != '') {
            $sql = $sql . " and ";
            $sql = $sql . "app_user.gender = '$gender'";
        }
//        if ($city != '') {
//            $sql = $sql . " and ";
//            $sql = $sql . "app_user.city = '$city'";
//        }
        if ($country != '') {
            $sql = $sql . " and ";
            $sql = $sql . "app_user.country = '$country' ";

        }
        if ($date_from != '' && $date_to != '') {
            $sql = $sql . " and ";
            $sql = $sql . 'scanned_product.created_at BETWEEN ' . strtotime($date_from) . ' AND ' . strtotime($date_to);
        }
        if($age !=''){
            $sql = $sql." and (TIMESTAMPDIFF(YEAR, dob, CURDATE())";
            if ($age == '18 - 30') {
                $sql = $sql."BETWEEN 18 and 30 )";
            } elseif ($age == '31 - 45') {
                $sql = $sql."BETWEEN 31 and 45 )";
            } elseif ($age == '46 - 60') {
                $sql = $sql."BETWEEN 46 and 60 )";
            } elseif ($age == 'Over 60') {
                $sql = $sql." >60 )";
            } elseif ($age == 'Un-disclosed') {
                $sql = $sql." IS NULL )";
            }

        }
        $sql = $sql.") group by ".$duration." order by year ASC, month ASC, day ASC";

        $command = $conn->createCommand($sql);
        $response = $command->queryAll();
        $formated_data = [];
        $labels = [];
        $data = [];
        foreach($response as $row){
            if($duration == 'day'){
                $labels[] = $row['day'].'-'.$row['month'].'-'.$row['year'];
            }else if($duration == 'month'){
                $labels[] = $row['month'].'-'.$row['year'];
            } else if($duration == 'year'){
                $labels[] = $row['year'];
            }
            $data[] = $row['scans'];
        }
        $datasets[] = [
            'fillColor' => '#BBD9CA',
            'strokeColor' => 'rgba(220,220,220,1)',
            'pointColor' => '#248556',
            'pointStrokeColor' => '#fff',
            'pointHighlightFill' => '#fff',
            'pointHighlightStroke' => 'rgba(220,220,220,1)',
            'data' => $data
        ];

        $formated_data = [
            'labels' => $labels,
            'datasets' => $datasets
        ];
        return $formated_data;
    }

    public function getAgeRangeReportData()
    {
        $conn = \Yii::$app->db;
        $command = $conn->createCommand("SELECT
            SUM(IF(age BETWEEN 18 and 30,1,0)) as '18 - 30',
            SUM(IF(age BETWEEN 31 and 45,1,0)) as '31 - 45',
            SUM(IF(age BETWEEN 46 and 60,1,0)) as '45 - 60',
            SUM(IF(age >=61, 1, 0)) as 'Over 60',
            SUM(IF(age IS NULL, 1, 0)) as 'Un-disclosed'

            FROM (SELECT TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age FROM app_user) as derived
            "
        );

        $total_users = $this->getTotalUsers();

        $rows = $command->queryAll();
        $data = [
            [
                'value' => $rows[0]['18 - 30'],
                'color' => '#248556',
                'total_users' => $total_users,
                'label' => '18 - 30'
            ],
            [
                'value' => $rows[0]['31 - 45'],
                'color' => '#ffc12d',
                'total_users' => $total_users,
                'label' => '31 - 45'
            ],
            [
                'value' => $rows[0]['45 - 60'],
                'color' => '000000',
                'total_users' => $total_users,
                'label' => '45 - 60'
            ],
            [
                'value' => $rows[0]['Over 60'],
                'color' => '##f35a5a',
                'total_users' => $total_users,
                'label' => 'Over 60'
            ],
            [
                'value' => $rows[0]['Un-disclosed'],
                'color' => '#848484',
                'total_users' => $total_users,
                'label' => 'Un-disclosed'
            ]
        ];
        return $data;
    }

    public function getGenderReportData()
    {
        $conn = \Yii::$app->db;
        $command = $conn->createCommand('
          SELECT app_user.gender ,COUNT(app_user.id) AS users
          FROM app_user
          GROUP BY gender
          ORDER BY users DESC'
        );

        $total_users = $this->getTotalUsers();
        $rows = $command->queryAll();
        $data = [];
        foreach ($rows as $row) {
            if (!strcasecmp($row['gender'], 'male')) {
                $data[] =
                    [
                        'value' => $row['users'],
                        'color' => '#248556',
                        'total_users' => $total_users,
                        'label' => 'Male'
                    ];
            } elseif (!strcasecmp($row['gender'], 'female')) {
                $data[] =
                    [
                        'value' => $row['users'],
                        'color' => '#ffc12d',
                        'total_users' => $total_users,
                        'label' => 'Female'
                    ];
            } elseif (!isset($row['gender'])) {
                $data[] =
                    [
                        'value' => $row['users'],
                        'color' => '#848484',
                        'total_users' => $total_users,
                        'label' => 'Un-Disclosed'
                    ];

            }
        }
        return $data;
    }


    public function populateAgeDropDown()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = [
            [
                'id' => '',
                'text' => ''
            ],
            [
                'id' => '1',
                'text' => '18 - 30'
            ],
            [
                'id' => '2',
                'text' => '31 - 45'
            ],
            [
                'id' => '3',
                'text' => '46 - 60'
            ],
            [
                'id' => '4',
                'text' => 'Over 60'
            ],
            [
                'id' => '5',
                'text' => 'Un-disclosed'
            ]
        ];
        return ['results' => $results];
    }

    public function populateGenderDropDown()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $results = [
            [
                'id' => '',
                'text' => ''
            ],
            [
                'id' => '1',
                'text' => 'Male'
            ],
            [
                'id' => '2',
                'text' => 'Female'
            ],
            [
                'id' => '3',
                'text' => 'Un-disclosed'
            ]
        ];
        return ['results' => $results];
    }


    public function populateCityDropDown()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $users = AppUser::find()->where('id>0')
            ->groupBy('city')
            ->all();

        $results = [];
        $results[] = ['id' => '', 'text' => ''];
        foreach ($users as $user) {
            if (isset($user->city)) {
                $results[] = ['id' => $user->id, 'text' => $user->city];
            }

        }
        return ['results' => $results];
    }


    public function populateDataTable()
    {
        $result = [];


        $results[] = [
            'total_users' => $this->getTotalUsers(),
            'total_products' => $this->getTotalProducts(),
            'total_counterfeit_scans' => $this->getTotalCounterfeitScans(),
            'total_scans' => $this->getTotalScans(),
            'review_ratings' => $this->getTotalReviewRatings(),
        ];


        return $results;
    }

    public function populateCountryDropDown()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $users = AppUser::find()->where('id>0')
            ->groupBy('country')
            ->all();

        $results = [];
        $results[] = ['id' => '', 'text' => ''];
        foreach ($users as $user) {
            if (isset($user->country)) {
                $results[] = ['id' => $user->id, 'text' => $user->country];
            }

        }
        return ['results' => $results];
    }


}