<?php

namespace app\modules\admin\controllers\api\reports\services;


use app\models\Address;
use app\models\base\Budget;
use app\models\base\ProductNature;
use app\models\base\ProductType;
use app\models\Order;
use app\models\OrderCoupon;
use app\models\OrderItem;
use app\models\ProductGroup;
use app\utils\FetchContinentFromCountryCode;
use app\utils\ReturnRequiredDatesAndMonths;
use DateTime;
use WC_API_Client;
use WC_API_Client_Exception;
use WC_API_Client_HTTP_Exception;
use yii\web\Response;

class EcommerceReportsManagement
{
    public function getEcommerceSalesData($data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;

//        return $data;

        if ($data['date_from'] != '' || $data['date_to'] != '') {
            $start_date = new DateTime($data['date_from']);
            $data['date_from'] = $start_date->getTimestamp() + 30600;

            $end_date = new DateTime($data['date_to']);
            $data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        }

        if ($data['date_from'] == '') {
            $days = round((time() - 1466672262) / 86400);
        } else {
            $days = round((time() - $data['date_from']) / 86400);
        }


//        return $data['date_to'];

        $conn = \Yii::$app->db;
        $sql = "SELECT sum(total_amount) as sales , sum(tax_amount) as total_tax ";
        $sql = $sql . "FROM `order` , `address` ";
        $sql = $sql . "WHERE order.shipping_address_id=address.id ";


        if ($data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $data['continent'] . "' ";
        }
        if ($data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $data['country'] . "' ";
        }
        if ($data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $data['state'] . "' ";
        }
        if ($data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $data['postcode'] . "' ";
        }
        if ($data['date_from'] != '' && $data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $data['date_from'] . " AND " . $data['date_to'] . " ";
        }
        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";

        $command = $conn->createCommand($sql);

        $grossSales = $command->queryAll();

        $sales = $grossSales[0]['sales'];
        $totalTax = $grossSales[0]['total_tax'];
        $totalCoupons = 0;
        $avgDailySales = 0;
        $avgSalesPerOrder = 0;
        $orders = $this->getEcommerceOrdersData($data);

//        if($data['date_from'] == ''){
//            $days = round((time() - 1466672262)/86400);
//        }else{
//            $days = round((time() - $data['date_from'])/86400);
//        }


        $avgDailySales = round($sales / $days, 2);
        $netSales = $sales - $totalTax;
        if ($orders['totalOrders'] != 0) {
            $avgSalesPerOrder = round($sales / $orders['totalOrders'], 2);
        }


        $sale = [
            'grossSale' => number_format($sales, 2,'.',','),//round($sales, 2),
            'avgDailySales' => number_format($avgDailySales, 2,'.',','),//round($avgDailySales, 2),
            'totalTax' => number_format($totalTax, 2,'.',','),//round($totalTax, 2),
            'netSales' => number_format($netSales, 2,'.',','),//round($netSales, 2),
            'avgSalesPerOrder' => number_format($avgSalesPerOrder, 2,'.',','),//round($avgSalesPerOrder, 2),
        ];

        $data = [
            'sale' => $sale,
            'orders' => $orders,
            'coupons' => $this->getTotalCouponsRedeemed($data)
        ];

        return $data;
    }

    public function getEcommerceProductTypeDropdownData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $types = ProductType::find()->all();
        $results[] = [
            'id' => '0',
            'text' => 'Product Type : All'
        ];
        foreach($types as $type){
            $results[] = [
                'id' => $type->id,
                'text' => $type->type_name,
            ];
        }
        return ['results' => $results];
    }

    public function getEcommerceProductNatureDropdownData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $natures = ProductNature::find()->all();
        $results[] = [
            'id' => '0',
            'text' => 'Product Nature : All'
        ];
        foreach($natures as $nature){
            $results[] = [
                'id' => $nature->id,
                'text' => $nature->name,
            ];
        }
        return ['results' => $results];
    }

    public function getEcommerceProductGroupDropdownData()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $groups = ProductGroup::find()->all();
        $results[] = [
            'id' => '0',
            'text' => 'Product Group : All'
        ];
        foreach($groups as $group){
            $results[] = [
                    'id' => $group->id,
                    'text' => $group->product_group_name
            ];
        }


        return ['results' => $results];
    }

    public function getTotalCouponsRedeemed($data)
    {

        $conn = \Yii::$app->db;

        $sql = "SELECT sum(discount_amount) as discount_amount ";
        $sql = $sql . "FROM `order` , `address` , order_coupons ";
        $sql = $sql . "WHERE order_coupons.order_id=order.woo_order_id ";
        $sql = $sql . "and order.shipping_address_id=address.id ";

        if ($data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $data['continent'] . "' ";
        }
        if ($data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $data['country'] . "' ";
        }
        if ($data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $data['state'] . "' ";
        }
        if ($data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $data['postcode'] . "' ";
        }
        if ($data['date_from'] != '' && $data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $data['date_from'] . " AND " . $data['date_to'] . " ";
        }
        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";

//        return $sql;

        $command = $conn->createCommand($sql);
        $coupons = $command->queryAll();

        $totalCoupons = round($coupons[0]['discount_amount'], 2);


        return $totalCoupons;
    }

    public function getEcommerceOrdersData($data)
    {

        $conn = \Yii::$app->db;

        $sql = "SELECT COUNT(*) AS orders , order_status.name , status_id ";
        $sql = $sql . "FROM `order` , `order_status` , `address` ";
        $sql = $sql . "WHERE order.status_id=order_status.id ";
        $sql = $sql . "and order.shipping_address_id=address.id ";

        if ($data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $data['continent'] . "' ";
        }
        if ($data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $data['country'] . "' ";
        }
        if ($data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $data['state'] . "' ";
        }
        if ($data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $data['postcode'] . "' ";
        }
        if ($data['date_from'] != '' && $data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $data['date_from'] . " AND " . $data['date_to'] . " ";
        }
        $sql = $sql . "group by status_id";

        $command = $conn->createCommand($sql);

        $orderNumbers = $command->queryAll();

        $totalOrdersProcessing = 0;
        $totalOrdersReadyToDispatch = 0;
        $totalOrdersDispatched = 0;
        $totalOrdersCompleted = 0;
        $totalOrdersCancelled = 0;
        $totalOrders = 0;

        foreach ($orderNumbers as $count) {
            if (in_array('processing', $count)) {
                $totalOrdersProcessing = $count['orders'];
            }
            if (in_array('ready-to-dispatch', $count)) {
                $totalOrdersReadyToDispatch = $count['orders'];
            }
            if (in_array('completed', $count)) {
                $totalOrdersDispatched = $count['orders'];
            }
            if (in_array('delivered', $count)) {
                $totalOrdersCompleted = $count['orders'];
            }
            if (in_array('cancelled', $count)) {
                $totalOrdersCancelled = $count['orders'];
            }

            $totalOrders = ($totalOrdersProcessing + $totalOrdersReadyToDispatch + $totalOrdersDispatched + $totalOrdersCompleted + $totalOrdersCancelled);
        }
        $orders = [
            'totalOrdersProcessing' => $totalOrdersProcessing,
            'totalOrdersReadyToDispatch' => $totalOrdersReadyToDispatch,
            'totalOrdersDispatched' => $totalOrdersDispatched,
            'totalOrdersCompleted' => $totalOrdersCompleted,
            'totalOrdersCancelled' => $totalOrdersCancelled,
            'totalOrders' => $totalOrders,

        ];

        return $orders;
    }

    public function populateAgeDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $results = [
            [
                'id' => '0',
                'text' => 'Age : All'
            ],
            [
                'id' => '1',
                'text' => '18 - 30'
            ],
            [
                'id' => '2',
                'text' => '31 - 45'
            ],
            [
                'id' => '3',
                'text' => '46 - 60'
            ],
            [
                'id' => '4',
                'text' => 'Over 60'
            ],
            [
                'id' => '5',
                'text' => 'Un-disclosed'
            ]
        ];
        return ['results' => $results];
    }

    public function populateGenderDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $results = [
            [
                'id' => '0',
                'text' => 'Gender : All'
            ],
            [
                'id' => '1',
                'text' => 'Male'
            ],
            [
                'id' => '2',
                'text' => 'Female'
            ],
            [
                'id' => '3',
                'text' => 'Un-disclosed'
            ]
        ];
        return ['results' => $results];
    }

    public function populateTradingChannelDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $results = [
            [
                'id' => '0',
                'text' => 'Trading Channel : All'
            ],
            [
                'id' => '1',
                'text' => 'B2B'
            ],
            [
                'id' => '2',
                'text' => 'B2C'
            ],
        ];
        return ['results' => $results];
    }

    public function setOrderContinents()
    {
        $addresses = Address::find()->all();

        foreach ($addresses as $address) {
            if ($address->type == 'shipping') {
                $address->continent = FetchContinentFromCountryCode::fetchContinentFromCountryCode($address->country);
            } else {
                $address->continent = null;
            }
            $address->save(false);
        }

    }

    public function populateStateDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $addresses = Address::find()
            ->groupBy('state')
            ->all();

        $results = [];
        $results[] = ['id' => '0', 'text' => 'State : All'];
        foreach ($addresses as $address) {
            if (isset($address->country)) {
                $results[] = ['id' => $address->id, 'text' => $address->state];
            }

        }
        return ['results' => $results];
    }

    public function populatePostCodeDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $addresses = Address::find()
            ->groupBy('postcode')
            ->all();

        $results = [];
        $results[] = ['id' => '0', 'text' => 'Post Code : All'];
        foreach ($addresses as $address) {
            if (isset($address->postcode)) {
                $results[] = ['id' => $address->id, 'text' => $address->postcode];
            }

        }
        return ['results' => $results];
    }


    public function populateCountryDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $addresses = Address::find()
            ->groupBy('country')
            ->all();

        $results = [];
        $results[] = ['id' => '0', 'text' => 'Country : All'];
        foreach ($addresses as $address) {
            if (isset($address->country)) {
                $results[] = ['id' => $address->id, 'text' => $address->country];
            }

        }
        return ['results' => $results];
    }

    public function populateContinentDropDown()
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $addresses = Address::find()
            ->groupBy('continent')
            ->all();

        $results = [];
        $results[] = ['id' => '0', 'text' => 'Continent : All'];
        foreach ($addresses as $address) {
            if (isset($address->continent)) {
                $results[] = ['id' => $address->id, 'text' => $address->continent];
            }

        }
        return ['results' => $results];
    }

    public function getSalesComparison($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

        $current_line_data=[];
        $previous_line_data=[];
        $budget_line_data=[];
        $labels = [];

        if (!isset ($raw_data['saleduration'])){
            $raw_data['saleduration'] = 'year';
        }
        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
        $start_date = new DateTime($raw_data['date_from']);
        $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
        $end_date = new DateTime($raw_data['date_to']);
        $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        }else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }
        $current = "SELECT SUM(order.total_amount) as Sales , DAY(FROM_UNIXTIME(created_at)) as Day , MONTH(FROM_UNIXTIME(created_at)) as Month , YEAR(FROM_UNIXTIME(created_at)) as Year ";
        $current = $current . "FROM `order` , `order_status` , `address` ";
        $current = $current . "WHERE order.status_id=order_status.id ";
        $current = $current . "and order.shipping_address_id=address.id ";
        if ($raw_data['continent'] != 'Continent : All') {
            $current = $current . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $current = $current . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $current = $current . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $current = $current . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $current = $current . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $current = $current . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        $current = $current . "AND status_id IN (1,2,3,4,8,9) ";

        if($raw_data['saleduration'] == 'day'){
            $current = $current . "GROUP BY Day, Month, Year ";
            $current = $current . "ORDER BY Year ASC, Month ASC , Day ASC ";
        }else if($raw_data['saleduration'] == 'month'){
            $current = $current . "GROUP BY Month, Year ";
            $current = $current . "ORDER BY Year ASC, Month ASC ";
        }else if($raw_data['saleduration'] == 'year'){
            $current = $current . "GROUP BY Year ";
            $current = $current . "ORDER BY Year ASC ";
        }

        $command = $conn->createCommand($current);
        $data_current = $command->queryAll();

        $duration_current =  ReturnRequiredDatesAndMonths::getDateRange($raw_data['date_from'], $raw_data['date_to'], $raw_data['saleduration'], 'd/M/y');

        foreach($data_current as $i => $row ){
            foreach($duration_current as $d => $dur ){
                if( $raw_data['saleduration'] == 'day' ){
                    if(($data_current[$i]['Day'] == $duration_current[$d]['Day']) && ($data_current[$i]['Month'] == $duration_current[$d]['Month']) && ($data_current[$i]['Year'] == $duration_current[$d]['Year']) ){
                        $duration_current[$d]['Sales'] = $data_current[$i]['Sales'];
                    }
                }else if( $raw_data['saleduration'] == 'month' ){
                    if(($data_current[$i]['Month'] == $duration_current[$d]['Month']) && ($data_current[$i]['Year'] == $duration_current[$d]['Year']) ){
                        $duration_current[$d]['Sales'] = $data_current[$i]['Sales'];
                    }
                }else if( $raw_data['saleduration'] == 'year' ){
                    if(($data_current[$i]['Year'] == $duration_current[$d]['Year']) ){
                        $duration_current[$d]['Sales'] = $data_current[$i]['Sales'];
                    }
                }
            }
        }

        $previous_date_from = $raw_data['date_from']-31536000;
        $previous_date_to = $raw_data['date_to']-31536000;

        $previous = "SELECT SUM(order.total_amount) as Sales , DAY(FROM_UNIXTIME(created_at)) as Day , MONTH(FROM_UNIXTIME(created_at)) as Month , YEAR(FROM_UNIXTIME(created_at)) as Year ";
        $previous = $previous . "FROM `order` , `order_status` , `address` ";
        $previous = $previous . "WHERE order.status_id=order_status.id ";
        $previous = $previous . "and order.shipping_address_id=address.id ";
        if ($raw_data['continent'] != 'Continent : All') {
            $previous = $previous . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $previous = $previous . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $previous = $previous . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $previous = $previous . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $previous = $previous . "and order.created_at BETWEEN " . $previous_date_from . " AND " . $previous_date_to . " ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $previous = $previous . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        $previous = $previous . "AND status_id IN (1,2,3,4,8,9) ";
        if($raw_data['saleduration'] == 'day'){
            $previous = $previous . "GROUP BY Day, Month, Year ";
            $previous = $previous . "ORDER BY Year ASC, Month ASC , Day ASC ";
        }else if($raw_data['saleduration'] == 'month'){
            $previous = $previous . "GROUP BY Month, Year ";
            $previous = $previous . "ORDER BY Year ASC, Month ASC ";
        }else if($raw_data['saleduration'] == 'year'){
            $previous = $previous . "GROUP BY Year ";
            $previous = $previous . "ORDER BY Year ASC ";
        }
        $command = $conn->createCommand($previous);
        $data_previous = $command->queryAll();

        $duration_previous =  ReturnRequiredDatesAndMonths::getDateRange($previous_date_from, $previous_date_to, $raw_data['saleduration'], 'd/M/y');

        foreach($data_previous as $i => $row ){
            foreach($duration_previous as $d => $dur ){
                if( $raw_data['saleduration'] == 'day' ){
                    if(($data_previous[$i]['Day'] == $duration_previous[$d]['Day']) && ($data_previous[$i]['Month'] == $duration_previous[$d]['Month']) && ($data_previous[$i]['Year'] == $duration_previous[$d]['Year']) ){
                        $duration_previous[$d]['Sales'] = $data_previous[$i]['Sales'];
                    }
                }else if( $raw_data['saleduration'] == 'month' ){
                    if(($data_previous[$i]['Month'] == $duration_previous[$d]['Month']) && ($data_previous[$i]['Year'] == $duration_previous[$d]['Year']) ){
                        $duration_previous[$d]['Sales'] = $data_previous[$i]['Sales'];
                    }
                }else if( $raw_data['saleduration'] == 'year' ){
                    if(($data_previous[$i]['Year'] == $duration_previous[$d]['Year']) ){
                        $duration_previous[$d]['Sales'] = $data_previous[$i]['Sales'];
                    }
                }
            }
        }


        foreach($duration_current as $row){
            if( $raw_data['saleduration'] == 'day' ){
                $labels[] = $row['Day'].'-'.ReturnRequiredDatesAndMonths::fetchMonthNameFromMonthNumber($row['Month']).'-'.$row['Year'];
            }else if( $raw_data['saleduration'] == 'month' ){
                $labels[] = ReturnRequiredDatesAndMonths::fetchMonthNameFromMonthNumber($row['Month']).'-'.$row['Year'];
            }else if( $raw_data['saleduration'] == 'year' ){
                $labels[] = $row['Year'];
            }
            $current_line_data[] = $row['Sales'];
        }

        foreach($duration_previous as $row){
            $previous_line_data[] = $row['Sales'];
        }



//        $budget = Budget::find()
//            ->where(['between','month', date('n', $raw_data['date_from'] ) ,date('n', $raw_data['date_to'] )])
////            ->andWhere(['between','year', date('Y', $raw_data['date_from'] ) ,date('Y', $raw_data['date_to'] )])
//            ->all();
//return $budget;
//        ;
//        $raw_data['date_to']


        $line1 = [
            'fillColor' => 'rgba(36, 133, 86, 0)',
            'strokeColor' => 'rgba(36, 133, 86, 1)',
            'pointColor' => 'rgba(36, 133, 86, 1)',
            'label' => 'Current Period Sales',
            'data' => $current_line_data
        ];
//        $line2 = [
//            'fillColor' => 'rgba(151,187,205,0)',
//            'strokeColor' => 'rgba(151,187,205,1)',
//            'pointColor' => 'rgba(151,187,205,1)',
//            'label' => 'Budgeted Sales',
//            'data' => $budget
//            ];
        $line3 = [

            'fillColor' => 'rgba(255, 65, 75, 0)',
            'strokeColor' => 'rgba(255, 65, 75, 1)',
            'pointColor' => 'rgba(255, 65, 75, 1)',
            'label' => 'Last Year Sales',
            'data' => $previous_line_data
        ];
        $datasets = [];

        array_push($datasets, $line1);
//        array_push($datasets, $line2);
        array_push($datasets, $line3);

        $formated_data = [
            'labels' => $labels,
            'datasets' => $datasets
        ];
        return $formated_data;
    }

    public function getOrdersComparison($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

        $current_line_data=[];
        $previous_line_data=[];
        $budget_line_data=[];
        $labels = [];

        if (!isset ($raw_data['orderduration'])){
            $raw_data['orderduration'] = 'year';
        }
        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
            $start_date = new DateTime($raw_data['date_from']);
            $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
            $end_date = new DateTime($raw_data['date_to']);
            $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        }else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }
        $current = "SELECT DAY(FROM_UNIXTIME(created_at)) as Day , MONTH(FROM_UNIXTIME(created_at)) as Month , YEAR(FROM_UNIXTIME(created_at)) as Year , COALESCE(count(*),0) as cnt ";
        $current = $current . "FROM `order` , `order_status` , `address` ";
        $current = $current . "WHERE order.status_id=order_status.id ";
        $current = $current . "and order.shipping_address_id=address.id ";
        if ($raw_data['continent'] != 'Continent : All') {
            $current = $current . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $current = $current . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $current = $current . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $current = $current . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $current = $current . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $current = $current . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        $current = $current . "AND status_id IN (1,2,3,4,8,9) ";

        if($raw_data['orderduration'] == 'day'){
            $current = $current . "GROUP BY Day, Month, Year ";
            $current = $current . "ORDER BY Year ASC, Month ASC , Day ASC ";
        }else if($raw_data['orderduration'] == 'month'){
            $current = $current . "GROUP BY Month, Year ";
            $current = $current . "ORDER BY Year ASC, Month ASC ";
        }else if($raw_data['orderduration'] == 'year'){
            $current = $current . "GROUP BY Year ";
            $current = $current . "ORDER BY Year ASC ";
        }

        $command = $conn->createCommand($current);
        $data_current = $command->queryAll();

        $duration_current =  ReturnRequiredDatesAndMonths::getDateRange($raw_data['date_from'], $raw_data['date_to'], $raw_data['orderduration'], 'd/M/y');

        foreach($data_current as $i => $row ){
            foreach($duration_current as $d => $dur ){
                if( $raw_data['orderduration'] == 'day' ){
                    if(($data_current[$i]['Day'] == $duration_current[$d]['Day']) && ($data_current[$i]['Month'] == $duration_current[$d]['Month']) && ($data_current[$i]['Year'] == $duration_current[$d]['Year']) ){
                        $duration_current[$d]['cnt'] = $data_current[$i]['cnt'];
                    }
                }else if( $raw_data['orderduration'] == 'month' ){
                    if(($data_current[$i]['Month'] == $duration_current[$d]['Month']) && ($data_current[$i]['Year'] == $duration_current[$d]['Year']) ){
                        $duration_current[$d]['cnt'] = $data_current[$i]['cnt'];
                    }
                }else if( $raw_data['orderduration'] == 'year' ){
                    if(($data_current[$i]['Year'] == $duration_current[$d]['Year']) ){
                        $duration_current[$d]['cnt'] = $data_current[$i]['cnt'];
                    }
                }
            }
        }

        $previous_date_from = $raw_data['date_from']-31536000;
        $previous_date_to = $raw_data['date_to']-31536000;

        $previous = "SELECT DAY(FROM_UNIXTIME(created_at)) as Day , MONTH(FROM_UNIXTIME(created_at)) as Month , YEAR(FROM_UNIXTIME(created_at)) as Year , COALESCE(count(*),0) as cnt ";
        $previous = $previous . "FROM `order` , `order_status` , `address` ";
        $previous = $previous . "WHERE order.status_id=order_status.id ";
        $previous = $previous . "and order.shipping_address_id=address.id ";
        if ($raw_data['continent'] != 'Continent : All') {
            $previous = $previous . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $previous = $previous . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $previous = $previous . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $previous = $previous . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $previous = $previous . "and order.created_at BETWEEN " . $previous_date_from . " AND " . $previous_date_to . " ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $previous = $previous . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        $previous = $previous . "AND status_id IN (1,2,3,4,8,9) ";
        if($raw_data['orderduration'] == 'day'){
            $previous = $previous . "GROUP BY Day, Month, Year ";
            $previous = $previous . "ORDER BY Year ASC, Month ASC , Day ASC ";
        }else if($raw_data['orderduration'] == 'month'){
            $previous = $previous . "GROUP BY Month, Year ";
            $previous = $previous . "ORDER BY Year ASC, Month ASC ";
        }else if($raw_data['orderduration'] == 'year'){
            $previous = $previous . "GROUP BY Year ";
            $previous = $previous . "ORDER BY Year ASC ";
        }
        $command = $conn->createCommand($previous);
        $data_previous = $command->queryAll();

        $duration_previous =  ReturnRequiredDatesAndMonths::getDateRange($previous_date_from, $previous_date_to, $raw_data['orderduration'], 'd/M/y');

        foreach($data_previous as $i => $row ){
            foreach($duration_previous as $d => $dur ){
                if( $raw_data['orderduration'] == 'day' ){
                    if(($data_previous[$i]['Day'] == $duration_previous[$d]['Day']) && ($data_previous[$i]['Month'] == $duration_previous[$d]['Month']) && ($data_previous[$i]['Year'] == $duration_previous[$d]['Year']) ){
                        $duration_previous[$d]['cnt'] = $data_previous[$i]['cnt'];
                    }
                }else if( $raw_data['orderduration'] == 'month' ){
                    if(($data_previous[$i]['Month'] == $duration_previous[$d]['Month']) && ($data_previous[$i]['Year'] == $duration_previous[$d]['Year']) ){
                        $duration_previous[$d]['cnt'] = $data_previous[$i]['cnt'];
                    }
                }else if( $raw_data['orderduration'] == 'year' ){
                    if(($data_previous[$i]['Year'] == $duration_previous[$d]['Year']) ){
                        $duration_previous[$d]['cnt'] = $data_previous[$i]['cnt'];
                    }
                }
            }
        }

        foreach($duration_current as $row){
            if( $raw_data['orderduration'] == 'day' ){
                $labels[] = $row['Day'].'-'.ReturnRequiredDatesAndMonths::fetchMonthNameFromMonthNumber($row['Month']).'-'.$row['Year'];
            }else if( $raw_data['orderduration'] == 'month' ){
                $labels[] = ReturnRequiredDatesAndMonths::fetchMonthNameFromMonthNumber($row['Month']).'-'.$row['Year'];
            }else if( $raw_data['orderduration'] == 'year' ){
                $labels[] = $row['Year'];
            }
            $current_line_data[] = $row['cnt'];
            $budget_line_data[] = ($row['cnt']+10);
        }

        foreach($duration_previous as $row){
            $previous_line_data[] = $row['cnt'];
        }

        $line1 = [
            'fillColor' => 'rgba(36, 133, 86, 0)',
            'strokeColor' => 'rgba(36, 133, 86, 1)',
            'pointColor' => 'rgba(36, 133, 86, 1)',
            'label' => 'Current Period Orders',
            'data' => $current_line_data
        ];
        $line3 = [

            'fillColor' => 'rgba(255, 65, 75, 0)',
            'strokeColor' => 'rgba(255, 65, 75, 1)',
            'pointColor' => 'rgba(255, 65, 75, 1)',
            'label' => 'Last Year Orders',
            'data' => $previous_line_data
        ];
        $datasets = [];

        array_push($datasets, $line1);
        array_push($datasets, $line3);

        $formated_data = [
            'labels' => $labels,
            'datasets' => $datasets
        ];
        return $formated_data;
    }

    public function getTotalSalesByStatePieChartData($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

        $formatted_state_order_data =[];
        $formatted_state_sale_data = [];

        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
            $start_date = new DateTime($raw_data['date_from']);
            $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
            $end_date = new DateTime($raw_data['date_to']);
            $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        }else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }

        if($raw_data['chartfilter'] == 'continent'){
            $group_filter = 'country';
        }else if($raw_data['chartfilter'] == 'country'){
            $group_filter = 'state';
        }else if($raw_data['chartfilter'] == 'state'){
            $group_filter = 'postcode';
        }else if($raw_data['chartfilter'] == 'postcode'){
            $group_filter = 'continent';
        }

        $sql = "SELECT address.".$group_filter." as state , COALESCE(count(*),0) as Orders, sum(order.total_amount) as Sales ";
        $sql = $sql . "FROM `order` , `order_status` , `address` ";
        $sql = $sql . "WHERE order.status_id=order_status.id ";
        $sql = $sql . "and order.shipping_address_id=address.id ";

        if($raw_data['chartfilter'] != 'postcode'){
            $sql = $sql . "and address.".$raw_data['chartfilter']." = '".$raw_data['chartfilterdata']."' ";
        }

        if ($raw_data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $sql = $sql . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }




        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }
        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";

        $sql = $sql . "GROUP BY address.".$group_filter." ";
//        return $sql;
        $command = $conn->createCommand($sql);
        $states_data = $command->queryAll();

        foreach ($states_data as $row) {
            $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
                $formatted_state_sale_data[] =
                [
                    'value' => round($row['Sales'],2),
                    'color' => $color ,
                    'label' => $row['state']
                ];

                $formatted_state_order_data[] =
                [
                    'value' => $row['Orders'],
                    'color' => $color ,
                    'label' => $row['state']
                ];
        }
        return [
            'filter' => $raw_data['chartfilter'],
            'sales' => $formatted_state_sale_data,
            'orders' =>$formatted_state_order_data
        ];
    }


    public function getTotalSalesByChannelPieChartData($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

        $formatted_channel_order_data =[];
        $formatted_channel_sale_data = [];

        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
            $start_date = new DateTime($raw_data['date_from']);
            $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
            $end_date = new DateTime($raw_data['date_to']);
            $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        } else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }

//        if($raw_data['chartfilter'] == 'channel'){
//            $group_filter = 'channel';
//        }elseif($raw_data['chartfilter'] == 'continent'){
//            $group_filter = 'continent';
//        }else if($raw_data['chartfilter'] == 'country'){
//            $group_filter = 'state';
//        }else if($raw_data['chartfilter'] == 'state'){
//            $group_filter = 'postcode';
//        }else if($raw_data['chartfilter'] == 'postcode'){
//            $group_filter = 'continent';
//        }

//        if($raw_data['chartfilter'] == 'channel'){
//
//        }else{
//            $sql = "SELECT address.".$raw_data['chartfilter']." as channel , COALESCE(count(*),0) as Orders, sum(order.total_amount) as Sales ";
//        }

        $sql = "SELECT order.trading_channel as channel , COALESCE(count(*),0) as Orders, sum(order.total_amount) as Sales ";
        $sql = $sql . "FROM `order` , `order_status` , `address` ";
        $sql = $sql . "WHERE order.status_id=order_status.id ";
        $sql = $sql . "and order.shipping_address_id=address.id ";

//        if($raw_data['chartfilter'] != 'channel'){
//            $sql = $sql . "and order.trading_channel = '".$raw_data['chartfilter']."' ";
//        }

        if ($raw_data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $sql = $sql . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }
        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";
        $sql = $sql . "GROUP BY order.trading_channel ";

        $command = $conn->createCommand($sql);
        $channel_data = $command->queryAll();

        foreach ($channel_data as $row) {
            if (!strcasecmp($row['channel'], 'B2B')) {
                $formatted_channel_sale_data[] =
                    [
                        'value' => round($row['Sales'], 2),
                        'color' => '#4876CA',
                        'label' => $row['channel']
                    ];

                $formatted_channel_order_data[] =
                    [
                        'value' => $row['Orders'],
                        'color' => '#4876CA',
                        'label' => $row['channel']
                    ];

            } elseif (!strcasecmp($row['channel'], 'B2C')) {
                $formatted_channel_sale_data[] =
                    [
                        'value' => round($row['Sales'], 2),
                        'color' => '#FCC400',
                        'label' => $row['channel']
                    ];
                $formatted_channel_order_data[] =
                    [
                        'value' => $row['Orders'],
                        'color' => '#FCC400',
                        'label' => $row['channel']
                    ];
            }
        }
        return [
            'sales' => $formatted_channel_sale_data,
            'orders' => $formatted_channel_order_data
        ];
    }


    public function getTotalSalesByProductTypePieChartData($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

//        $formatted_channel_order_data =[];
//        $formatted_channel_sale_data = [];

        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
            $start_date = new DateTime($raw_data['date_from']);
            $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
            $end_date = new DateTime($raw_data['date_to']);
            $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        } else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }


        $sql = "SELECT product_type.type_name, COUNT( batch_items.id ) AS Qty, SUM( product_location_attributes.price ) AS Sale ";
        $sql = $sql . "FROM  `batch_items` ,  `product` ,  `batch` ,  `order` ,  `product_type` , `address`, `product_location_attributes` ";
        $sql = $sql . "WHERE batch.id = batch_items.batch_id ";
        $sql = $sql . "AND product.product_type_id = product_type.id ";
        $sql = $sql . "AND batch.product_id = product.id ";
        $sql = $sql . "AND order.woo_order_id = batch_items.order_id ";
        $sql = $sql . "AND order.shipping_address_id=address.id ";
        $sql = $sql . "AND batch_items.item_status =  'sold' ";
        $sql = $sql . "AND product.id = product_location_attributes.product_id ";
        $sql = $sql . "AND product_location_attributes.language_id = 2 ";

        if ($raw_data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $sql = $sql . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }

        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";
        $sql = $sql . "GROUP BY product.product_type_id ";


        $command = $conn->createCommand($sql);
        $channel_data = $command->queryAll();

        $formatted_type_sale_data = [];
        $formatted_type_order_data = [];

        foreach ($channel_data as $row) {
            $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $formatted_type_sale_data[] =
                [
                    'value' => round($row['Sale'], 2),
                    'color' =>  $color,
                    'label' => $row['type_name']
                ];

            $formatted_type_order_data[] =
                [
                    'value' => $row['Qty'],
                    'color' => $color,
                    'label' => $row['type_name']
                ];
        }
        return [
            'sales' => $formatted_type_sale_data,
            'orders' => $formatted_type_order_data
        ];
    }

    public function getTotalSalesByProductGroupPieChartData($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
            $start_date = new DateTime($raw_data['date_from']);
            $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
            $end_date = new DateTime($raw_data['date_to']);
            $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        } else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }


        $sql = "SELECT product_group.product_group_name, COUNT( batch_items.id ) AS Qty, SUM( product_location_attributes.price ) AS Sale ";
        $sql = $sql . "FROM  `batch_items` ,  `product` ,  `batch` ,  `order` ,  `product_group` , `address`, `product_location_attributes` ";
        $sql = $sql . "WHERE batch.id = batch_items.batch_id ";
        $sql = $sql . "AND product.product_group_id = product_group.id ";
        $sql = $sql . "AND batch.product_id = product.id ";
        $sql = $sql . "AND order.woo_order_id = batch_items.order_id ";
        $sql = $sql . "AND order.shipping_address_id=address.id ";
        $sql = $sql . "AND batch_items.item_status =  'sold' ";
        $sql = $sql . "AND product.id = product_location_attributes.product_id ";
        $sql = $sql . "AND product_location_attributes.language_id = 2 ";

        if ($raw_data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $sql = $sql . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }

        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";
        $sql = $sql . "GROUP BY product.product_group_id ";


        $command = $conn->createCommand($sql);
        $group_data = $command->queryAll();

        $formatted_group_sale_data = [];
        $formatted_group_order_data = [];

        foreach ($group_data as $row) {
            $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $formatted_group_sale_data[] =
                [
                    'value' => round($row['Sale'], 2),
                    'color' =>  $color,
                    'label' => $row['product_group_name']
                ];

            $formatted_group_order_data[] =
                [
                    'value' => $row['Qty'],
                    'color' => $color,
                    'label' => $row['product_group_name']
                ];
        }
        return [
            'sales' => $formatted_group_sale_data,
            'orders' => $formatted_group_order_data
        ];
    }

    public function getTotalSalesByProductNaturePieChartData($raw_data)
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $conn = \Yii::$app->db;

        if ($raw_data['date_from'] != '' || $raw_data['date_to'] != '') {
            $start_date = new DateTime($raw_data['date_from']);
            $raw_data['date_from'] = $start_date->getTimestamp() + 30600;
            $end_date = new DateTime($raw_data['date_to']);
            $raw_data['date_to'] = $end_date->getTimestamp() + 86400 + 30599;
        } else {
            $raw_data['date_from'] = 1466672262;
            $raw_data['date_to'] = time();
        }


        $sql = "SELECT product_nature.name, COUNT( batch_items.id ) AS Qty, SUM( product_location_attributes.price ) AS Sale ";
        $sql = $sql . "FROM  `batch_items` ,  `product` ,  `batch` ,  `order` ,  `product_nature` , `address`, `product_location_attributes` ";
        $sql = $sql . "WHERE batch.id = batch_items.batch_id ";
        $sql = $sql . "AND product.product_nature_id = product_nature.id ";
        $sql = $sql . "AND batch.product_id = product.id ";
        $sql = $sql . "AND order.woo_order_id = batch_items.order_id ";
        $sql = $sql . "AND order.shipping_address_id=address.id ";
        $sql = $sql . "AND batch_items.item_status =  'sold' ";
        $sql = $sql . "AND product.id = product_location_attributes.product_id ";
        $sql = $sql . "AND product_location_attributes.language_id = 2 ";

        if ($raw_data['continent'] != 'Continent : All') {
            $sql = $sql . "and address.continent = '" . $raw_data['continent'] . "' ";
        }
        if ($raw_data['country'] != 'Country : All') {
            $sql = $sql . "and address.country = '" . $raw_data['country'] . "' ";
        }
        if ($raw_data['state'] != 'State : All') {
            $sql = $sql . "and address.state = '" . $raw_data['state'] . "' ";
        }
        if ($raw_data['postcode'] != 'Post Code : All') {
            $sql = $sql . "and address.postcode = '" . $raw_data['postcode'] . "' ";
        }
        if ($raw_data['channel'] != 'Trading Channel : All') {
            $sql = $sql . "and order.trading_channel = '" . $raw_data['channel'] . "' ";
        }
        if ($raw_data['date_from'] != '' && $raw_data['date_to'] != '') {
            $sql = $sql . "and order.created_at BETWEEN " . $raw_data['date_from'] . " AND " . $raw_data['date_to'] . " ";
        }

        $sql = $sql . "AND status_id IN (1,2,3,4,8,9) ";
        $sql = $sql . "GROUP BY product.product_nature_id ";


        $command = $conn->createCommand($sql);
        $group_data = $command->queryAll();


        $formatted_nature_sale_data = [];
        $formatted_nature_order_data = [];

        foreach ($group_data as $row) {
            $color = '#' . str_pad(dechex(mt_rand(0, 0xFFFFFF)), 6, '0', STR_PAD_LEFT);
            $formatted_nature_sale_data[] =
                [
                    'value' => round($row['Sale'], 2),
                    'color' =>  $color,
                    'label' => $row['name']
                ];

            $formatted_nature_order_data[] =
                [
                    'value' => $row['Qty'],
                    'color' => $color,
                    'label' => $row['name']
                ];
        }
        return [
            'sales' => $formatted_nature_sale_data,
            'orders' => $formatted_nature_order_data
        ];
    }

    public function importActualOrderItemPrices($from, $to)
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 300,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->where(['between', 'woo_order_id', $from, $to ])->all();
            echo 'Count : '.count($orders);
            $c = 0;
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id,['filter[meta]' => 'true'])->order;
//return $order;
                foreach($order->line_items as $item){
                    $orderItem = OrderItem::find()->where(['product_id' => $item->sku , 'order_id' => $live_order->woo_order_id])->one();
                    if(isset($orderItem)){
                        $orderItem->actual_price = $item->subtotal;
                        $orderItem->actual_tax = $item->subtotal_tax;
                        $orderItem->save(false);
                        $c++;
                    }
                }
            }
            echo 'Edits : '.$c;

        } catch (WC_API_Client_Exception $e) {
//            echo $live_order->woo_order_id;
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }


    public function importOrderItemPrices($from, $to)
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 300,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->where(['between', 'woo_order_id', $from, $to ])->all();
//            $orders = Order::find()->where(['woo_order_id' => '7872'])->all();

            echo 'Count : '.count($orders);
            $c = 0;
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id,['filter[meta]' => 'true'])->order;
//return $order;
                foreach($order->line_items as $item){
                   $orderItem = OrderItem::find()->where(['product_id' => $item->sku , 'order_id' => $live_order->woo_order_id])->one();
                    if(isset($orderItem)){
                        $orderItem->price = $item->total;
                        $orderItem->tax = $item->total_tax;
                        $orderItem->save(false);
                        $c++;
                    }
                }
            }
            echo 'Edits : '.$c;

        } catch (WC_API_Client_Exception $e) {
//            echo $live_order->woo_order_id;
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }


}