<?php

namespace app\modules\admin\controllers\api;


use app\models\Order;
use app\modules\admin\controllers\api\services\EcommerceManagement;
use yii\helpers\Url;

class OzirisEcommerceApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject(){

        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];

        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }

    /*************************************Ecommerce Web Services Start********************************************/

    public function actionGetAllProducts()
    {
        $return = new EcommerceManagement();
        return $return->getAllProducts();
    }

    public function actionGetEcommerceEnabledProducts()
    {
        $language_code = (isset($_POST['language_code']) ? $_POST['language_code'] : '');
        $return = new EcommerceManagement();
        return $return->getEcommerceEnabledProducts($language_code);
    }

    public function actionImportOrderFromWooCommerce()
    {
        $this->logPostObject();
        $order_number = (isset($_POST['order_number']) ? $_POST['order_number'] : '');
        $return = new EcommerceManagement();
        return $return->importOrderFromWooCommerce($order_number);

    }

    public function actionSetAddresses()
    {
            $return = new EcommerceManagement();
            return $return->setAddresses();
    }

    public function actionSetOrderCreateDate()
    {
        $return = new EcommerceManagement();
        return $return->setOrderCreateDate();
    }

    public function actionGenerateOrderBarCode()
    {
        $return = new EcommerceManagement();
        return $return->generateOrderBarCode();
    }

    public function actionUpdateBmpProductDetails()
    {

        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->updateBmpProductDetails();
    }

    public function actionUpdateBmpQuantities()
    {
        $return = new EcommerceManagement();
        return $return->updateBmpQuantities();
    }

    public function actionUpdateOrderStatus()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->updateOrderStatus();
    }

    public function actionCompressImages()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->updateCompressImages();
    }

    public function actionSetCouponData()
    {
        $this->logPostObject();
        $return = new EcommerceManagement();
        return $return->setCouponData();
    }


    /*************************************Ecommerce Web Services Start********************************************/


}