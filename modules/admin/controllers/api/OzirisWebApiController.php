<?php

namespace app\modules\admin\controllers\api;

use app\models\AppLoginToken;
use app\models\AppUser;
use app\models\AppUserDeviceInfo;
use app\models\base\Manufacturer;
use app\models\Batch;
use app\models\Brand;
use app\models\Language;
use app\models\Product;
use app\models\ReviewRating;
use app\modules\admin\controllers\api\oziris\services\OrderManagement;
use app\modules\admin\controllers\api\services\BrandManagement;
use app\modules\admin\controllers\api\services\ManufacturerManagement;
use app\modules\admin\controllers\api\services\ProductManagement;
use app\modules\admin\controllers\api\services\ReviewRatingManagement;
use app\modules\admin\controllers\api\services\UserManagement;
use Exception;
use yii\httpclient\Client;


/**
 * This is the class for REST controller "app\modules\admin\controllers\ProductController".
 */
class OzirisWebApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';


    public function logPostObject(){

        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];

        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }

    /*************************************Product Web Services Start********************************************/

    /**
     * Searches for a product based on $qr_code || $product_code || $batch_number || $language_code.
     *
     * @return Product||null
     *
     */
    public function actionCheckCounterfeitProduct()
    {
        $this->logPostObject();
        $user_id = (isset($_POST['user_id']) ? $_POST['user_id'] : '');

        $auth = AppLoginToken::findOne(['user_id' => $user_id , 'token' => $_POST['token']]);
        $user_id = (isset($auth) ? $auth->user_id : AppUser::findOne(['email_address' => 'default'])->id);
        $product_code = (isset($_POST['product_code']) ? $_POST['product_code'] : '');
        $qr_code = (isset($_POST['qr_code']) ? $_POST['qr_code'] : '');
        $batch_number = (isset($_POST['batch_number']) ? $_POST['batch_number'] : '');
        $language_code = (isset($_POST['language_code']) ? $_POST['language_code'] : '');

        $return = new ProductManagement();
        try{
            return $return->checkCounterfeitProduct($product_code, $qr_code, $batch_number, $language_code, $user_id);
        }catch (Exception $ex){
            return false;
        }

    }

    /**
     * Returns all products and their translations according to the language_id Information
     * @param $language_id Language;
     *
     * @return Products[]
     *
     */
    public function actionGetAllProductsForLanguage()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
        $product = new ProductManagement();
        return $product->getAllProducts($_POST['language_code']);
        } else {
            return "User Not Authenticated!";
        }

    }

    /**
     * Returns all products and their translations according to the language_id Information
     * @param $language_id Language;
     *
     * @return Products[]
     *
     */
    public function actionGetScannedProductsForUser()
    {
        $auth = AppLoginToken::find()->where(['user_id' => $_POST['user_id'], 'token' => $_POST['token']])->one();

        if (isset($auth)) {
            $product = new ProductManagement();
            return $product->getScannedProductsForUser($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }


    /*************************************Product Web Services End********************************************/

    /*************************************User Web Services Start********************************************/

    /**
     * Searches for the provided user email in app_user.
     *
     * @return boolean
     *
     */
    public function actionValidateUserEmail()
    {
        $email_address = (isset($_POST['email_address']) ? $_POST['email_address'] : '');
        $user = new UserManagement();
        return $user->checkValidUserEmail($email_address);
    }

    /*************************************User Web Services Start********************************************/

    /**
     * Searches for the provided user email in app_user.
     *
     * @return boolean
     *
     */
    public function actionForgetPasswordEmail()
    {
        $this->logPostObject();

        $user = new UserManagement();
        return $user->forgetPasswordEmail($_POST['email_address']);
    }


    /**
     * creates a user account and also creates the social media auth entry
     *
     * @return boolean
     *
     */
    public function actionSignUpWithSocialAccount()
    {
        $this->logPostObject();
        $user = new UserManagement();
        return $user->insertSocialUserAccount($_POST);
    }

    /**
     * Searches for the provided user email in app_user.
     * if email does not exist then signup user with the given email address
     *
     * @return boolean
     *
     */
    public function actionSignUpWithUser()
    {
        $user = new UserManagement();
        $this->logPostObject();
        return $user->insertNewUser($_POST);
    }

    /**
     * Registers User On Beston Market place
     * @return boolean
     *
     */
    public function actionRegisterUserWithBestonMarketPlace()
    {
        $user = new UserManagement();
        $ozirisUsers = AppUser::find()->select(['first_name','last_name','mobile','country','email_address'])->all();
        foreach($ozirisUsers as $ozirisUser){
            print_r($user->createUserOnBestonMarketplace($ozirisUser));
        }

    }

    /**
     * Searches for the provided user ID in app_user.
     * if found then sets the is_active field to true.
     *
     * @return boolean
     *
     */
    public function actionActivateUser()
    {
        $user = new UserManagement();
        return $user->activateUser($_POST);
    }

    /**
     * Updates BestonMarket User Password
     * @return boolean
     */
    public function actionUpdateBestonUserPassword()
    {
        $this->logPostObject();
        $user = new UserManagement();
        return $user->setBestonMarketPlacePassword($_POST['email_address'],$_POST['password']);
    }

    /**
     * Updates User Information
     *
     *
     * @return boolean
     *
     */
    public function actionUpdateUser()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $user = new UserManagement();
            return $user->updateUser($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }


    /**
     * Get User Information
     *
     *
     * @return AppUser
     *
     */
    public function actionGetUser()
    {

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $user = new UserManagement();
            return $user->getUser($_POST['user_id']);
        } else {
            return "User Not Authenticated!";
        }
    }


    /**
     * Validates user credentials
     * Generates login token which will authenticate user for all web services
     *
     * @return boolean
     *
     */
    public function actionValidateUserAppAuthData()
    {
        $user = new UserManagement();
        return $user->validateUserAppAuthData($_POST);

    }

    /**
     * Registers user's device info to app
     * @return boolean
     *
     */
    public function actionRegisterUserDeviceToApp()
    {
        $user = new UserManagement();
        return $user->registerUserDeviceToApp();
    }

    /**
     * Validates user social media credentials
     * Generates login token which will authenticate user for all web services
     *
     * @return boolean
     *
     */
    public function actionValidateSocialMediaAuthData()
    {
        $user = new UserManagement();
        return $user->validateSocialMediaAuthData($_POST);

    }


    /*************************************User Web Services End********************************************/


    /*************************************Brand Web Services Start********************************************/

    /**
     * Returns all Brands for a specific Manufacturer
     *
     *
     * @return Brand[]
     *
     */
    public function actionGetAllBrandsForManufacturer()
    {

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $brand = new BrandManagement();
            return $brand->getAllBrandsForManufacturer($_POST);
        } else {
            return "User Not Authenticated!";
        }
    }

    /*************************************Brand Web Services End********************************************/

    /*************************************Manufacturer Web Services Start********************************************/

    /**
     * Get All Manufacturers Information
     *
     *
     * @return Manufacturer[]
     *
     */
    public function actionGetAllManufacturer()
    {

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $manufacturer = new ManufacturerManagement();
            return $manufacturer->getAllManufacturers($_POST['language_code']);
        } else {
            return "User Not Authenticated!";
        }
    }

    /*************************************Brand Web Services End********************************************/


    /*************************************Review Rating Web Services Start********************************************/

    /**
     * Post Review Information
     *
        *
     * @return Boolean
     *
     */

    public function actionPostReview()
    {

        $this->logPostObject();

        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            if ($_POST['product_id'] != '') {
                $review = ReviewRating::find()->where(['product_id' => $_POST['product_id'], 'user_id' => $_POST['user_id'],'deleted_at' =>null])->one();
            } else if ($_POST['brand_id'] != '') {
                $review = ReviewRating::find()->where(['brand_id' => $_POST['brand_id'], 'user_id' => $_POST['user_id'],'deleted_at' =>null])->one();
            } else if ($_POST['manufacturer_id'] != '') {
                $review = ReviewRating::find()->where(['manufacturer_id' => $_POST['manufacturer_id'], 'user_id' => $_POST['user_id'],'deleted_at' =>null])->one();
            }

            if(!isset($review)){
                $review = new ReviewRating();
            }

            $review->brand_id = (isset($_POST['brand_id']) ? $_POST['brand_id'] : '');
            $review->manufacturer_id = (isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '');
            $review->product_id= (isset($_POST['product_id']) ? $_POST['product_id'] : '');
            $review->review= $_POST['review'];
            $review->rating= $_POST['rating'];
            $review->user_id= $_POST['user_id'];
            return $review_rating->saveReview($review);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Report a Review
     * @return Boolean
     */

    public function actionReportReview()
    {

        $this->logPostObject();
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->reportReview($_POST['review_id'],$_POST['user_id']);
        } else {
            return "User Not Authenticated!";
        }
    }

    /**
     * Returns all Reviews for a product || manufacturer || Brand
     * @return Boolean
     */

    public function actionGetAllReviewsForId()
    {
        $auth = AppLoginToken::findOne(['user_id' => $_POST['user_id'], 'token' => $_POST['token']]);
        if (isset($auth)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->getAllReviews($_POST['type'],$_POST['id']);
        } else {
            return "User Not Authenticated!";
        }
    }



    /*************************************Review Rating Web Services End********************************************/


    public function actionGenerateDefaultBatchQrCodes()
    {
        ini_set('max_execution_time', 300);
        $batches = Batch::find()->where(['qr_code' => 'default'])->all();
        $count=0;
        foreach($batches as $batch){
            $batch->qr_code_image = $batch->generateQrCode();

            $batch->save(false);
            $count++;
        }

        return $count;
    }

    /*************************************Review Rating Web Services for BestonMarketplace Start********************************************/

    /**
     * Post Review Information
     *
     *
     * @return Boolean
     *
     */

    public function actionPostReviewBeston()
    {
        $this->logPostObject();
        $user = AppUser::find()->where(['email_address' => $_POST['user_id']])->one();
        if (isset($user)) {
            if ($_POST['old_comment_id'] != '') {
                $review = ReviewRating::find()->where(['comment_id' => $_POST['old_comment_id'], 'deleted_at' => null])->one();
            }
            if (!isset($review)) {
                $review = new ReviewRating();
            }

            $review->brand_id = '';
            $review->manufacturer_id = '';
            $review->product_id = (isset($_POST['product_id']) ? $_POST['product_id'] : '');
            $review->review = $_POST['review'];
            $review->rating = $_POST['rating'];
            $review->user_id = $user->id;
            $review->comment_id = $_POST['comment_id'];;

            return $review->save();
        } else {
            return false;
        }

    }


    /**
     * Report a Review
     *
     *
     * @return Boolean
     *
     */

    public function actionReportReviewBeston()
    {
        $this->logPostObject();
        $user = AppUser::find()->where(['email_address' => $_POST['user_id']])->one();
        if (isset($user)) {
            $review_rating = new ReviewRatingManagement();
            return $review_rating->reportReviewBeston($_POST['comment_id'], $user->id);
        } else {
            return "User Does Not Exist";
        }
    }

    /**
     * Returns all Reviews for a product || manufacturer || Brand
     *
     *
     * @return Boolean
     *
     */

    public function actionImportAllReviewsToBeston()
    {

        $reviews = ReviewRating::find()->where(['comment_id'=>null])->all();

        $client = new Client();

        foreach( $reviews as $review){

            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(getenv('SERVER_TYPE').'/update_comment')
                ->setData(
                    [
                        'product_id' => $review->product_id,
                        'review' => $review->review,
                        'rating' => $review->rating,
                        'comment_id' => $review->comment_id,
                        'user_id' => AppUser::find()->where(['id' => $review->user_id])->one()->email_address,
                    ])->send();

            $return = json_decode($response->content);
            if($return->comment_id != '-1'){
                $review->comment_id = $return->comment_id;
            }
            print_r($review->save());
        }
    }


    public function actionPostReviewsFromPostman()
    {
        $this->logPostObject();
        $user_id = AppUser::find()->where(['email_address' => $_POST['user_id']])->one()->id;

        $review = ReviewRating::find()->where(['user_id' => $user_id, 'product_id' => $_POST['product_id']] )->one();

        if(!isset($review)){
            $review = new ReviewRating();
        }
        $client = new Client();

        $review->product_id = $_POST['product_id'];
        $review->review = $_POST['review'];
        $review->rating = $_POST['rating'];
        $review->user_id = $user_id;



        $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(getenv('SERVER_TYPE').'/update_comment')
                ->setData(
                    [
                        'product_id' => $review->product_id,
                        'review' => $review->review,
                        'rating' => $review->rating,
                        'comment_id' => $review->comment_id,
                        'user_id' => $_POST['user_id'],
                    ])->send();

            $return = json_decode($response->content);
            if($return->comment_id != '-1'){
                $review->comment_id = $return->comment_id;
            }
            if($review->save() ==1){
                return "Review Posted!";
            }else{
                return "Review Not Posted!";

            }

        }



    /*************************************Review Rating Web Services End********************************************/

    public function actionDeleteDeviceId(){
        $devices = explode(',',$_POST['devices']);
print_r($devices);
        foreach($devices as $device){
            $device_to_delete = AppUserDeviceInfo::find()->where(['device_token' => $device])->one();
            if (isset($device_to_delete)) {
                print_r($device_to_delete->delete());
            }
        }


    }


    public function actionGetStatusTimesForBestonOrder(){
        $orderManagement = new OrderManagement();
        return $orderManagement->getStatusTimesForOrder($_POST['order_id']);

    }



}