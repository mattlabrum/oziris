<?php

namespace app\modules\admin\controllers\api\pixelforce;


use app\models\Order;
use app\modules\admin\controllers\api\pixelforce\services\EcommerceManagement;
use yii\helpers\Url;

class OzirisEcommerceApiController extends \yii\rest\ActiveController
{
    public $modelClass = 'app\models\AppUser';

    public function logPostObject($service_name){

        $req = [
            'date : ' => date("Y-m-d H:i:s"),
            'Service Name : ' => $service_name,
            'request : ' => $_REQUEST
        ];

        $req_dump =  print_r($req,true);
        $fp = fopen('request-v2.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }

    /*************************************Ecommerce Web Services Start********************************************/

    public function actionGetAllProducts()
    {
        $return = new EcommerceManagement();
        return $return->getAllProducts();
    }

    public function actionGetAllProductTypes()
    {
        $return = new EcommerceManagement();
        return $return->getAllProductTypes();
    }

    public function actionGetAllProductGroups()
    {
        $return = new EcommerceManagement();
        return $return->getAllProductGroups();
    }

    public function actionGetAllNutrition()
    {
        $return = new EcommerceManagement();
        return $return->getAllNutritions();
    }

    public function actionGetAllNutritionTags()
    {
        $return = new EcommerceManagement();
        return $return->getAllNutritionTags();
    }

    public function actionGetAllIngredients()
    {
        $return = new EcommerceManagement();
        return $return->getAllIngredients();
    }

    public function actionGetAllBrands()
    {
        $return = new EcommerceManagement();
        return $return->getAllBrands();
    }

    public function actionGetAllManufacturers()
    {
        $return = new EcommerceManagement();
        return $return->getAllManufacturers();
    }

    public function actionGetBoxQrCodes()
    {
        $return = new EcommerceManagement();
        return $return->getBoxQrCodes($_POST['order_id']);
    }


    public function actionGetOrderItemsQrCodes()
    {
        $return = new EcommerceManagement();
        return $return->getOrderItemsQrCodes($_POST['order_id']);
    }

    public function actionGetEcommerceEnabledProducts()
    {
        $language_code = (isset($_POST['language_code']) ? $_POST['language_code'] : '');
        $this->logPostObject('Get Ecommerce Enabled Products');
        $return = new EcommerceManagement();
        return $return->getEcommerceEnabledProducts($language_code);
    }

    public function actionImportAllOrders()
    {
        $this->logPostObject('Import Order From Pixel Force');
        $return = new EcommerceManagement();
        return $return->importAllOrders();
    }

    public function actionImportAllUsers()
    {
        $this->logPostObject('Import Order From Pixel Force');
        $return = new EcommerceManagement();
        return $return->importAllOrders();
    }

    public function actionImportOrder()
    {
        $this->logPostObject('Import Order From Pixel Force');
        $order_number = (isset($_POST['order_number']) ? $_POST['order_number'] : '');
        $return = new EcommerceManagement();
        return $return->importOrder($order_number);
    }

    public function actionGetAllBatches()
    {
        $this->logPostObject('Get All Batches');
        $return = new EcommerceManagement();
        return $return->getAllBatches();
    }

    public function actionReturnAllCoupons()
    {
        $this->logPostObject('Get All Batches');
        $return = new EcommerceManagement();
        return $return->returnAllCoupons();
    }


    /*************************************Ecommerce Web Services Start********************************************/

}