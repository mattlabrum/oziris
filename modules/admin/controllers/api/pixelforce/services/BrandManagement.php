<?php

namespace app\modules\admin\controllers\api\pixelforce\services;



use app\models\base\Manufacturer;
use yii\console\Exception;

class BrandManagement
{

    public function getAllBrandsForManufacturer($data){

        try{

            $manufacturer = Manufacturer::find()->where(['id' => $data['manufacturer_id']])->andWhere(['deleted_at' =>null])->one();
            $translated_brands = [];
            foreach($manufacturer->getBrands() as $brand){
                $translated_brands[] = $brand->getBrandTranslation($data['language_code']);
            }
            return $translated_brands;


        }catch (Exception $e){
            return false;
        }


    }

}