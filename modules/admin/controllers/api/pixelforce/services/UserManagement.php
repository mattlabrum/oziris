<?php

namespace app\modules\admin\controllers\api\pixelforce\services;


use app\models\AppAuthDatum;
use app\models\AppLoginToken;
use app\models\AppSocialMediaAuth;
use app\models\AppUser;
use app\models\AppUserDeviceInfo;
use app\models\Response;
use app\utils\AuthTokenGenerator;
use dmstr\helpers\Html;
use Exception;
use yii\httpclient\Client;

class UserManagement
{
    public function setBestonMarketPlacePassword($email_address,$password){
        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(getenv('SERVER_TYPE').'/update_user_info')
            ->setData(
                [
                    'password' => $password,
                    'email_address' => $email_address,
                ])->send();
        $app_user = AppUser::find()->where(['email_address' => $email_address])->one();
        if(isset($app_user)){
            $app_user_auth = new AppAuthDatum();
            $app_user_auth->deleteAll(['user_id' => $app_user->id]);
            $app_user_auth->user_id = $app_user->id;
            $app_user_auth->password_hash = password_hash($password, PASSWORD_BCRYPT);
            $app_user_auth->save();
        }
        return $response->isOk;
    }

    public function forgetPasswordEmail($email_address)
    {
        $app_user = AppUser::find()->where(['email_address' => $email_address])->one();
        $response = new Response();
        if (isset($app_user)) {

            $app_user_auth = AppAuthDatum::findOne(['user_id' => $app_user->id]);

            if(isset($app_user_auth)){
                $app_user_auth->delete();
            }

            $app_user_auth = new AppAuthDatum();

            $password = AuthTokenGenerator::generateAuthToken(6);

//            return $this->setBestonMarketPlacePassword($email_address,$password);

            $app_user_auth->user_id = $app_user->id;
            $app_user_auth->password_hash = password_hash($password, PASSWORD_BCRYPT);
            //return $app_user_auth->save();


            if($app_user_auth->save()==true && $this->setBestonMarketPlacePassword($email_address,$password)==true){

                ini_set('display_errors', 1);
                error_reporting(E_ALL);
                $from = "no-reply@oziris.com.au";
                $to = $email_address;
                $subject = "Oziris Password Reset";

                $message = "<!DOCTYPE html>
<html>
<body>

<table style='width:564px'>
  <tr>
    <td><img src='https://gallery.mailchimp.com/856292cc8c597f4afb5cb6186/images/55eb0813-603c-4908-9421-4f75f5a80d21.png' alt='Oziris, Intelligent Secure' style='width:560px;height:180px;'></td>
  </tr>
  <tr>
    <td ><h2><font face='arial'>Oziris & Beston Market Place Password Reset</font></h2></td>
  </tr>
    <tr>
    <td><font face='arial'>Please use the password below. We recommend changing this once you log back in.
    <br><br>
    <b>$password</b>
    <br>Thanks,
    <br><br>
                The Oziris team.
    <br><br>
    <hr>
    <br><br>
                Copyright © Beston Technologies All rights reserved.
    <br><br>
                Contact us at:
	<br>
	<a href='mailto:wecare@oziris.com.au'>wecare@oziris.com.au</a>

	<br>
	<a href='http://www.oziris.com.au/'>www.oziris.com.au</a></font>
    </td>
  </tr>
</table>

</body>
</html>";

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                // More headers
                $headers .= 'From: ' . $from . "\r\n";
                if(mail($to, $subject, $message, $headers)){
                    $response->setResponse(true);
                }else{
                    $response->setResponse(false);
                    $response->setMessage('Sending Forget Password email was not Successful!');
                }
            }
        }else{
            $response->setResponse(false);
            $response->setMessage('User not Found!');
        }
            return $response->getFullResponse();
    }


    public function registerUserDeviceToApp(){
        $device_info = AppUserDeviceInfo::find()->where(['device_token' => $_POST['device_token']])->one();
        if(!isset($device_info)){
            $device_info = new AppUserDeviceInfo();
        }
        $device_info->attributes = $_POST;
        return $device_info->save();
    }

    public function getUser($user_id)
    {
        $app_user = AppUser::find()->where(['id' => $user_id])->one();
        return (isset($app_user) ? $app_user : false);
    }

    public function checkValidUserEmail($email_address)
    {
        $response = new Response();
        try{
            $app_user = AppUser::find()->where(['email_address' => $email_address])->one();
            $response->setResponse(isset($app_user) ? false : true);
        }catch(Exception $ex){
            $errorString = "Something went wrong Please try again later!"."<br><br> Error Details! <br><br>".$ex;
            $response->setMessage($errorString);
        }
        return $response->getFullResponse();
    }

    public function insertAllNewUsers($page)
    {

        $url = "http://8ston.com/backend/users?page=".$page;
        $options = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"AUTH-ID: df1e5dbb17973d0c0c6\r\n" .
                    "AUTH-TOKEN:b58eec226f17c5c7d3a386edaa53e1c6\r\n"
            )
        );
        $context = stream_context_create($options);
        $file = file_get_contents($url, false, $context);
        $users = json_decode($file)->response;
//        return $users;
        foreach($users as $user){
            print_r($this->insertNewUser([
                'email_address' => $user->email,
                'first_name' => $user->fullname ,
                'last_name' => '',
                'country' => $user->country,
                'mobile' => $user->phone,
                'password' => '',
                '8ston_user_id' => $user->id
            ]));
        }
    }


    public function insertNewUser($user)
    {
//        $connection = \Yii::$app->db;
//        $transaction = $connection->beginTransaction();
        $response = new Response();
//        try {
            $app_user = AppUser::find()->where(['8ston_user_id' => $user['8ston_user_id']])->one();

            if(isset($app_user)){
                $app_user->attributes = $user;
                if (!$app_user->save(false)) {
                    throw new Exception();
                }
                $response->setMessage('User Updated successfully!');
                $response->setResponse(['user_id' => $app_user->id]);

            }else{
                $app_user = new AppUser();
                $app_user->attributes = $user;
                if (!$app_user->save(false)) {
                    throw new Exception();
                }
                $response->setMessage('User created successfully!');
                $response->setResponse(['user_id' => $app_user->id]);
//                $transaction->commit();
            }
//        } catch (Exception $e) {
//            $transaction->rollback();
//            $errorString = "Something went wrong Please try again later!"."<br><br> Error Details! <br><br>".$e;
//            $response->setMessage($errorString);
//        }
        return $response->getFullResponse();
    }

    public function createUserOnBestonMarketplace($user){

        $client = new Client();
        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(getenv('SERVER_TYPE').'/save_user_info')
            ->setData(
                [
                    'first_name' => $user['first_name'],
                    'last_name' => $user['last_name'],
                    'mobile' => $user['mobile'],
                    'password' => $user['password'],
                    'billing_country' => $user['country'],
                    'email_address' => $user['email_address'],
                ])->send();
        return $response->isOk;
    }

    public function insertSocialUserAccount($user)
    {
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        $response = new Response();

        try {
            $app_user = new AppUser();
            $app_user->attributes = $user;
            if (!$app_user->save()) {
                throw new Exception('Unable to save record.');
            }
            $app_social_auth = new AppSocialMediaAuth();
            $app_social_auth->user_id = $app_user->id;

            $app_social_auth->provider = (isset($user['provider']) ? $user['provider'] : '');;
            $app_social_auth->access_id = (isset($user['access_id']) ? $user['access_id'] : '');
            $app_social_auth->access_token = (isset($user['access_token']) ? $user['access_token'] : '');
            $app_social_auth->expiration_date = (isset($user['expiration_date']) ? $user['expiration_date'] : '');;

            if (!$app_social_auth->save()) {
                throw new Exception('Unable to save record.');
            }
            $transaction->commit();
            $response->setResponse(true);

        } catch (Exception $e) {
            $transaction->rollback();
            $errorString = "Something required fields are missing, make sure first_name, last_name, mobile, country, provider, access_id, access_token, expiration_date, email_address are entered. ";
            $response->setMessage($errorString);
        }
        return $response->getFullResponse();
    }

    public function activateUser($user_id)
    {
        $user = AppUser::findOne(['id'=>$user_id]);
        $response = new Response();
        if (isset($user)){
            $user->is_active = 1;
            $response->setResponse($user->save());
        }else{
            $errorString = "User Not Found!";
            $response->setMessage($errorString);
        }
        return $response->getFullResponse();
    }

    public function updateUser($user)
    {
        $app_user = AppUser::findOne(['id' => $user['user_id']]);
        if (isset($app_user)) {
            if (($app_user->email_address == $user['email_address'])) {
                $app_user->attributes = $user;
                if ($app_user->save()) {
                    if ($user['password'] != '') {
                        $app_user_auth = new AppAuthDatum();
                        $app_user_auth->deleteAll(['user_id' => $app_user->id]);
                        $app_user_auth->user_id = $app_user->id;
                        $app_user_auth->password_hash = password_hash($user['password'], PASSWORD_BCRYPT);
                        $this->setBestonMarketPlacePassword($user['email_address'],$user['password']);
                        return $app_user_auth->save();
                    }
                    return true;
                } else {
                    return false;
                }
            } else {
                if ($this->checkValidUserEmail($user['email_address'])) {
                    $app_user->attributes = $user;
                    if ($app_user->save()) {
                        if ($user['password'] != '') {
                            $app_user_auth = new AppAuthDatum();
                            $app_user_auth->deleteAll(['user_id' => $app_user->id]);
                            $app_user_auth->user_id = $app_user->id;
                            $app_user_auth->password_hash = password_hash($user['password'], PASSWORD_BCRYPT);
                            $this->setBestonMarketPlacePassword($user['email_address'],$user['password']);
                            return $app_user_auth->save();
                        }
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    return "Email Address Already Exits!";
                }
            }
        } else {
            return "User Not Found!";
        }
    }


    public function validateSocialMediaAuthData($user_auth_data)
    {
        $response = new Response();
        $user_auth = AppSocialMediaAuth::findOne(['access_id' => $user_auth_data['access_id']]);
        if (isset($user_auth)) {
            if($user_auth->access_token != $user_auth_data['access_token']) {
                $user_auth->access_token = $user_auth_data['access_token'];
                $user_auth->expiration_date = $user_auth_data['expiration_date'];
                $user_auth->save();
            }
                $login_token = new AppLoginToken();
                // delete all old tokens
                $login_token->deleteAll(['user_id' => $user_auth->user_id]);
                // Generate and save new token
                $login_token->token = AuthTokenGenerator::generateAuthToken();
                $login_token->user_id = $user_auth->user_id;
                if ($login_token->save()) {
                    $response->setResponse($login_token);
                }
        } else {
            $response->setResponse(false);
            $response->setMessage('Social Media account not linked');
        }

        return $response->getFullResponse();
    }

    public function validateUserAppAuthData($user_auth_data)
    {
        $response = new Response();
        $user = AppUser::find()->where(['email_address' => $user_auth_data['email_address']])->andWhere('deleted_at = 0')->one();
        if (isset($user)) {
            $user_auth = AppAuthDatum::findOne(['user_id' => $user->id]);
            if (isset($user_auth)) {
                if (password_verify($user_auth_data['password'], $user_auth->password_hash)) {
                    $login_token = new AppLoginToken();
                    // delete all old tokens
                    $login_token->deleteAll(['user_id' => $user_auth->user_id]);
                    // Generate and save new token
                    $login_token->token = AuthTokenGenerator::generateAuthToken();
                    $login_token->user_id = $user_auth->user_id;
                    if ($login_token->save()) {
                        $response->setResponse($login_token);
                    }
                } else {
                    $response->setResponse(false);
                    $response->setMessage('Invalid User Name and Password');
                }
            } else {
                $response->setResponse(false);
                $response->setMessage('Social Media Account Password does not exist for this User!');
            }
        }else{
            $response->setResponse(false);
            $response->setMessage('User Does Not exist!');
        }
        return $response->getFullResponse();
    }

    public function sendPushNotification($data){

        $android_device = $data['android'];
        $ios_device = $data['ios'];

        $notification = $data['notification'];

        $status = false;
        $status = $this->sendNotificationToIOSDevice([$ios_device], $notification);
        $status = $this->sendNotificationToAndroidDevice([$android_device], $notification);

        return $status;

    }

    public function sendNotificationToAndroidDevice($devices, $notification)
    {
        $registration_ids = [];
        foreach($devices as $device)
            array_push($registration_ids,$device);

        // API access key from Google API's Console
        define('API_ACCESS_KEY', 'AIzaSyDZznaCTsjEkoWKP37KaNED7AbZQx8kQug');


        $msg = array
        (
            'message' => $notification,
            'vibrate' => 1,
            'sound' => 1,

        );
        $fields = array
        (
            'registration_ids' => $registration_ids,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        $result =  json_decode($result);
        if ($result->success > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function sendNotificationToIOSDevice($devices, $notification)
    {

        $passphrase = 'Oz1r1s';
        $counter = 0;
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'apple_push_notification_production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
//        stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');

        $message = $notification;

        foreach ($devices as $device) {
            $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err,
                $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp) {
                return false;
            }

            $body['aps'] = array(
                'alert' => $message,
                'sound' => 'default'
            );

            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $device) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            if ($result) {
                $counter++;
                echo $counter;
            }
            fclose($fp);
        }


        if ($counter > 0) {
            return true;
        } else {
            return false;
        }

    }





}