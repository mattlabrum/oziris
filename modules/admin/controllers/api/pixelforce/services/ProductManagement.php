<?php
/**
 * @property \app\models\Product $product
 */

namespace app\modules\admin\controllers\api\pixelforce\services;


use app\models\base\BatchItem;
use app\models\Batch;
use app\models\Brand;
use app\models\Response;
use app\models\Manufacturer;
use app\models\Order;
use app\models\Product;
use app\models\ProductType;
use app\models\ScannedProduct;
use app\utils\FetchContinentFromCountryCode;
use Exception;
use yii\helpers\Url;

class ProductManagement
{
    public function getAllProducts($language_code)
    {

        $products = Product::findAll(['deleted_at'=>null]);
        foreach($products as $product){
            $translated_products[] = $product->getTranslatedData($language_code);
        }
        return $translated_products ;
    }

    public function checkOrderQR($qr_code,$language_code){

        $response = new Response();

        $order = Order::find()->where(['qr_code_text' => $qr_code])->orWhere(['order_qr_seal' => $qr_code])->one();
        $brand = new Brand();
        $brand->brand_name = 'Beston Market Place';
        $brand->brand_logo = 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/logo.png');
        if(isset($order)){
            $returnInfo = [
                'id' => $order->id,
                'manufacture_date' => $order->created_at,
                'recalled_date' => '',
                'qr_code_image' =>$order->qr_code,
                'qr_code' => $order->qr_code,
                'product' => [
                    "id" => 204,
                    "is_collection" => 1,
                    "collection_items" => $this->getOrderItems($order->orderItems,$language_code),
                    "product_number" => '',
                    "barcode" => '',
                    "product_group" => 'Order Date : '.date('d-M-y', $order->created_at),
                    "product_name" => 'Order Number : '.$order->woo_order_id,
                    "product_description" => '',
                    "product_type" => new ProductType(),
                    "brand" => $brand,
                    "manufacturer" => new Manufacturer(),
                    "shelf_life" => 10,
                    "shelf_life_unit" => 2,
                    "UnitSize" => '',
                    "product_image" => 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/images/beston-logo-app.png'),
                    "nutrition_image" => '',
                    "nutrition_text" => '',
                ],
            ];
            $response->setResponse($returnInfo);
        }else{
            $response->setMessage('Order Not Found');

        }
        return $response->getFullResponse();
    }

    public function getOrderItems($raw_items,$language_code) {
        $items = [];
        foreach($raw_items as $item){
            $order_item_batch = Batch::find()->where(['product_id' => $item->product_id, 'batch_number' => $item->product_id])->andWhere(['deleted_at' =>null])->one();
            $items[] = [
                'quantity' => $item->quantity,
                'product' => $order_item_batch->product->getTranslatedData($language_code),
            ];
        }
        return $items;
    }

    public function checkCounterfeitProduct($product_code,$qr_code,$batch_number,$language_code,$user_id)
    {
        $scanned_product = [
            'product_code'=>$product_code,
            'user_id'=>$user_id,
            'qr_code'=>$qr_code,
            'batch_number'=>$batch_number,
            'batch_id'=>'',
        ];
        if ($qr_code!='') {
            if(substr($qr_code,0,5) == 'order') {
                return $this->checkOrderQR($qr_code,$language_code);
            }
            $batch = Batch::findOne(['qr_code' => $qr_code, 'deleted_at' => null]);
            if (isset($batch)) {
                $batch_info = $batch->getTranslatedData($language_code);
                if($batch_info != false){
                    $scanned_product['batch_id'] = $batch->id;
                }
            }else {
                $batch_item = BatchItem::findOne(['qr_code_text' => $qr_code]);
                if (isset($batch_item)) {
                    $batch_info = $batch_item->batch->getTranslatedData($language_code);
                    if($batch_info != false){
                        $scanned_product['batch_id'] = $batch_item->batch->id;
                    }
                }
            }
            if($scanned_product['batch_id'] == '' || $scanned_product['batch_id'] == null){
                return $this->checkOrderQR($qr_code,$language_code);
            }
        }
        if ($batch_number!='' && $product_code!='') {
            $batches = Batch::findAll(['batch_number' => $batch_number,'deleted_at'=> null]);
            foreach ($batches as $batch) {
                if ($batch->product->barcode == $product_code || $batch->product->product_number == $product_code) {
                    $batch_info = $batch->getTranslatedData($language_code);
                    if($batch_info != false){
                        $scanned_product['batch_id'] = $batch->id;
                    }
                }
            }
        }else if($batch_number=='' && $product_code!=''){
            $query = Batch::find();
            $query->joinWith(['product']);
            $query->where(['product.barcode' => $product_code]);
            $query->orWhere(['product.product_number' => $product_code]);
            $query->andWhere(['batch.manufacture_date' => null]);
            $query->andWhere(['batch.deleted_at' => null]);
            $query->andWhere(['product.deleted_at' => null]);
//            $query->andWhere(['brand.deleted_at' => null]);
//            $query->andWhere(['manufacturer.deleted_at' => null]);
            $default_batch = $query->one();

            if(isset($default_batch)){
                $batch_info = $default_batch->getTranslatedData($language_code);
                if($batch_info != false){
                    $scanned_product['batch_id'] = $default_batch->id;
                }
            }
        }
        $this->saveScannedProduct($scanned_product);
        $response = new Response();
        if(isset($batch_info)){
            $response->setResponse($batch_info);
        }else{
            $response->setMessage('Non Traceable Item !');
        }
        return $response->getFullResponse();
    }

    public function saveScannedProduct($data)
    {

        $ip_address = $this->getUserIP();

        $scanned_product = new ScannedProduct();
        $scanned_product->batch_id = $data['batch_id'];
        $scanned_product->batch_number = $data['batch_number'];
        $scanned_product->product_code = $data['product_code'];
        $scanned_product->qr_code = $data['qr_code'];
        $scanned_product->user_id = $data['user_id'];

        try {
            $geoIP  = json_decode(file_get_contents('http://localhost:8080/json/'.$ip_address.''), true);
//          $geoIP  = json_decode(file_get_contents('https://oziris.com.au:8080/json/'.$ip_address.''), true);
            $scanned_product->scanned_latitude = $geoIP['latitude'];
            $scanned_product->scanned_longitude = $geoIP['longitude'];
        } catch (Exception $e) {
            $scanned_product->scanned_latitude = 0;
            $scanned_product->scanned_longitude = 0;
        }
        try {
            $location = $this->getGeoCity($scanned_product->scanned_latitude, $scanned_product->scanned_longitude);
            $scanned_product->city = $location->address_components[2]->long_name;
            $scanned_product->continent =  FetchContinentFromCountryCode::fetchContinentFromCountryCode($location->address_components[5]->short_name);
        } catch (Exception $e) {
            $scanned_product->city = 'Un-disclosed';
            $scanned_product->continent = 'Un-disclosed';
        }
        if(!isset($scanned_product->city)){
            $scanned_product->city = 'Un-disclosed';
            $scanned_product->continent = 'Un-disclosed';
        }
        $scanned_product->save();
    }

    public function getScannedProductsForUser($data)
    {
        $scanned_products = ScannedProduct::find()->where(['user_id' => $data['user_id']])->andWhere('batch_id IS NOT NULL')->all();
        $scanned_translated_products = [];
        foreach($scanned_products as $scanned_product){
            $translated_product = $scanned_product->batch->getTranslatedData($data['language_code']);
            if($translated_product!=false){
                $scanned_translated_products[] = [
                    "batch" => $translated_product,
                    "scanned_date_time" => date('d-m-Y h:i:s', $scanned_product->created_at),
                    "scanned_longitude" => $scanned_product->scanned_longitude,
                    "scanned_latitude" => $scanned_product->scanned_latitude,
                ];
            }
        }
        return $scanned_translated_products;
    }

    function getUserIP()
    {
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }
        return $ip;
    }

    function getGeoCity($lat,$lng) {
        $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
        $get     = file_get_contents($url);
        $geoData = json_decode($get);
//        print_r($geoData);

        if(isset($geoData->results[0])) {
            return $geoData->results[0];
        }
        return null;
    }
}