<?php

namespace app\modules\admin\controllers\api\pixelforce\services;


use app\models\AppUser;
use app\models\base\Address;
use app\models\base\AppUserDeviceInfo;
use app\models\Batch;
use app\models\BatchItem;
use app\models\Brand;
use app\models\Coupon;
use app\models\Ingredient;
use app\models\Manufacturer;
use app\models\Nutrition;
use app\models\NutritionTag;
use app\models\OrderItem;
use app\models\OrderStatusTime;
use app\models\Payment;
use app\models\ProductGroup;
use app\models\ProductLocationAttribute;
use app\models\ProductTranslation;
use app\models\ProductType;
use app\models\Order;
use app\models\OrderCoupon;
use app\models\OrderStatus;
use app\models\Product;
use app\models\Response;
use app\models\ShippingBox;
use app\utils\BarcodeGenerator;
use app\utils\FetchContinentFromCountryCode;
use app\utils\PushNotificationSender;
use DateTime;
use Exception;
use Tinify\Tinify;
use WC_API_Client;
use WC_API_Client_Exception;
use WC_API_Client_HTTP_Exception;
use yii\helpers\Url;
use yii\httpclient\Client;

class EcommerceManagement
{
    public function logObject($ob){
        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $ob,
        ];
        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }

    public function getBoxQrCodes($order_id)
    {
        $raw_shipping_boxes = ShippingBox::findAll(['order_id' => $order_id]);
        $shipping_boxes = [];
        $response = new Response();
        foreach($raw_shipping_boxes as $box){
            $shipping_boxes[] = $box->box_qr;
        }
        if(count($shipping_boxes) > 0 ){
            $response->setMessage(['order_id' => $order_id]);
            $response->setResponse(['box_qr_codes' => $shipping_boxes ]);
        }else{
            $response->setMessage('Order has not been fullfilled!');
            $response->setResponse(false);
        }
        return $response->getFullResponse();
    }


    public function getOrderItemsQrCodes($order_id)
    {
        $raw_batch_items = BatchItem::findAll(['order_id' => $order_id]);
        $batch_items = [];
        $response = new Response();
        foreach($raw_batch_items as $item){
            $batch_items[] = [
                'qr_code_text' => $item->qr_code_text,
                'item_status' => $item->item_status,
                'batch_id' => $item->batch_id,
            ];
        }
        if(count($batch_items) > 0 ){
            $response->setMessage(['order_id' => $order_id]);
            $response->setResponse($batch_items);
        }else{
            $response->setMessage('Order has not been fullfilled!');
            $response->setResponse(false);
        }

        return $response->getFullResponse();


    }


    public function getAllProductTypes()
    {
        $productTypes = ProductType::findAll(['deleted_at'=>null]);
        $response = new Response();
        foreach($productTypes as $type){
            $types[] = [
                'id'=> $type->id,
                'ch_ProductType'=> $type->getProductTypeTranslation(1)->type_name,
                'en_ProductType'=> $type->type_name,
                'icon'=> $type->getProductType()->icon,
                'product_type_show_on_menu'=> $type->show_on_menu,
            ];
        }
        $response->setResponse($types);
        return $response->getFullResponse() ;
    }

    public function getAllProductGroups()
    {
        $productGroups = ProductGroup::findAll(['deleted_at'=>null]);
        $response = new Response();
        foreach($productGroups as $group){
            $groups[] = [
                'id'=> $group->id,
                'ch_ProductGroup'=> $group->getGroupTranslation(1)->product_group_name,
                'en_ProductGroup'=> $group->product_group_name,
                'product_group_show_on_menu'=> $group->show_on_menu,
            ];
        }
        $response->setResponse($groups);
        return $response->getFullResponse() ;
    }

    public function getAllNutritions()
    {
        $nutritions = Nutrition::find()->all();
        $response = new Response();
        foreach($nutritions as $nutrition){
            $allNutritions[] = [
                'id'=> $nutrition->id,
                'ch_Nutrition'=> $nutrition->getNutritionTranslation(1),
                'en_Nutrition'=> $nutrition->name,
            ];
        }
        $response->setResponse($allNutritions);
        return $response->getFullResponse() ;
    }

    public function getAllNutritionTags()
    {
        $nutritionTags = NutritionTag::find()->all();
        $response = new Response();
        foreach($nutritionTags as $nutritionTag){
            $allNutritionTags[] = [
                'id'=> $nutritionTag->id,
                'ch_NutritionTag'=> $nutritionTag->getNutritionTagTranslation(1),
                'en_NutritionTag'=> $nutritionTag->name,
            ];
        }
        $response->setResponse($allNutritionTags);
        return $response->getFullResponse() ;
    }

    public function getAllIngredients()
    {
        $ingredients = Ingredient::find()->all();
        $response = new Response();
        foreach($ingredients as $ingredient){
            $allIngredients[] = [
                'id'=> $ingredient->id,
                'ch_Ingredient'=> $ingredient->getIngredientTranslation(1),
                'en_Ingredient'=> $ingredient->name,
            ];
        }
        $response->setResponse($allIngredients);
        return $response->getFullResponse() ;
    }

    public function getAllBrands()
    {
        $brands = Brand::findAll(['deleted_at'=>null]);
        $response = new Response();
        foreach($brands as $brand){
            $allBrands[] = [

            'id'=> $brand->id,
            'ch_BrandName'=> $brand->getBrandTranslation(1)->brand_name,
            'en_BrandName'=> $brand->brand_name,
            'ch_BrandDescription'=> $brand->getBrandTranslation(1)->brand_description,
            'en_BrandDescription'=> $brand->brand_description,
            'brand_location_latitude'=> $brand->brand_location_latitude,
            'brand_location_longitude'=> $brand->brand_location_longitude,
            'brand_logo'=> $brand->getBrandLogo(),
            'brand_video'=> $brand->brand_video,
            ];
        }
        $response->setResponse($allBrands);
        return $response->getFullResponse() ;
    }

    public function getAllManufacturers()
    {
        $manufacturers = Manufacturer::findAll(['deleted_at'=>null]);
        $response = new Response();
        foreach($manufacturers as $manufacturer){
            $allManufacturers[] = [

                'id'=> $manufacturer->id,
                'ch_ManufactureName'=> $manufacturer->getManufacturerTranslation(1)->manufacturer_name,
                'en_ManufactureName'=> $manufacturer->manufacturer_name,
                'ch_ManufactureDescription'=> $manufacturer->getManufacturerTranslation(1)->manufacturer_description,
                'en_ManufactureDescription'=> $manufacturer->manufacturer_description,
                'manufacture_location_latitude'=> $manufacturer->manufacturer_location_latitude,
                'manufacture_location_longitude'=> $manufacturer->manufacturer_location_longitude,
                'manufacture_logo'=> $manufacturer->getManufacturerLogo(),
                'manufacture_video'=> $manufacturer->manufacturer_video,
            ];
        }
        $response->setResponse($allManufacturers);
        return $response->getFullResponse() ;
    }





    public function getAllProducts()
    {
        $products = Product::findAll(['deleted_at'=>null]);
        $response = new Response();
        foreach($products as $product){
            $translated_products[] = [
                'id' => $product->id,
                'ProductGroup' => $product->productGroup->product_group_name,
                'ProductName' => $product->product_name,
                'ProductDescription' => $product->product_description,
                'ProductType' => $product->productType->type_name,
                'Manufacturer' => $product->manufacturer->manufacturer_name,
                'UnitSize' => $product->unit_size,
                'UnitSizeKg' => $product->unit_size_kg,
                'ProductImage' => 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->product_image,
                'NutritionImage' => 'http://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->nutrition_image,
                'NutritionText' => $product->nutrition_text,
            ];
         }
        $response->setResponse($translated_products);
        return $response->getFullResponse() ;
    }

    public function getEcommerceEnabledProducts()
    {
        $products = ProductLocationAttribute::findAll(['language_id'=> 1,'ecommerce_enabled'=>1]);
        $response  = new Response();
        foreach($products as $product){
//            $product = $product->product;
            $translated_products[] = [
                'id' => $product->product->id,
                'ecommerce_enabled' => $product->product->getEcommerceEnabled(1),
                'preorder_enabled' => $product->product->preorder_enabled,
                'is_beston_pure_product' => $product->product->is_beston_pure_product,
                'is_top_seller' => $product->product->is_top_seller,
                'is_featured' => $product->product->is_featured,
                'UnitSize' => $product->product->unit_size,
                'UnitSizeKg' => $product->product->unit_size_kg,
                'ProductImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->product->product_image,
                'ProductThumbnailImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product/thumbnail').'/'.$product->product->product_image,
                'ProductRelatedImage' => $product->product->getProductRelatedImages(),
                'ProductQrCode' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/upload/qrcode').'/'.$product->product->getDefaultBatch()->qr_code_image,
                'NutritionImage' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/product').'/'.$product->product->nutrition_image,
                'ServesPerPack' => $product->product->serves_per_pack,
                'ServeSize' => $product->product->serve_size,
                'PriceAustralia' => $product->product->getProductPrice('2'),
                'PriceChina' => $product->product->getProductPrice('1'),
                'Quantity' => $product->product->getBestonQuantity(1),
                'Ingredients' => $product->product->getIngredients(),
                'NutritionTags' => $product->product->getNutritionTags(),
                'en_CollectionItems' => $product->product->getCollectionItems(0),
                'ch_CollectionItems' => $product->product->getCollectionItems(1),
                'en_ProductName' => ($product->product->getProductTranslation(0) != false ? trim($product->product->getProductTranslation(0)->product_name) : trim($product->product->product_name)),
                'en_ProductDescription' => ($product->product->getProductTranslation(0) != false ? trim($product->product->getProductTranslation(0)->product_description) : trim($product->product->product_description)),
                'ch_ProductName' => ($product->product->getProductTranslation(1) != false ? trim($product->product->getProductTranslation(1)->product_name) : trim($product->product->product_name)),
                'ch_ProductDescription' => ($product->product->getProductTranslation(1) != false ? trim($product->product->getProductTranslation(1)->product_description) : trim($product->product->product_description)),
                'en_NutritionText' => ($product->product->getProductTranslation(0) != false ? trim($product->product->getProductTranslation(0)->nutrition_text) : trim($product->product->nutrition_text)),
                'ch_NutritionText' => ($product->product->getProductTranslation(1) != false ? trim($product->product->getProductTranslation(1)->nutrition_text) : trim($product->product->nutrition_text)),

                'ProductType' => [
                    'product_type_id' => $product->product->productType->id,
                    'product_type_icon' => 'https://'.$_SERVER['HTTP_HOST'].Url::to('@web/uploads/type').'/'.$product->product->productType->icon,
                    'en_ProductType' => $product->product->productType->getProductTypeTranslation(0)->type_name,
                    'ch_ProductType' => $product->product->productType->getProductTypeTranslation(1)->type_name,
                    'product_type_show_on_menu' => $product->product->productType->show_on_menu,
                ],
                'ProductGroup' => [
                    'product_group_id' => $product->product->productGroup->id,
                    'product_group_show_on_menu' => $product->product->productGroup->show_on_menu,
                    'en_ProductGroup' => ($product->product->productGroup->getGroupTranslation(0) != false ? trim($product->product->productGroup->getGroupTranslation(0)->product_group_name) : trim($product->product->productGroup->product_group_name)),
                    'ch_ProductGroup' => ($product->product->productGroup->getGroupTranslation(1) != false ? trim($product->product->productGroup->getGroupTranslation(1)->product_group_name) : trim($product->product->productGroup->product_group_name)),
                ],
                'Manufacturer' => [
                    'manufacturer_id' => trim($product->product->manufacturer->id),
                    'en_Manufacturer' => trim($product->product->manufacturer->getManufacturerTranslation(0)->manufacturer_name),
                    'en_ManufacturerDescription' => trim($product->product->manufacturer->getManufacturerTranslation(0)->manufacturer_description),
                    'ch_Manufacturer' => trim($product->product->manufacturer->getManufacturerTranslation(1)->manufacturer_name),
                    'ch_ManufacturerDescription' => trim($product->product->manufacturer->getManufacturerTranslation(1)->manufacturer_description),
                ],
                'Brand' => [
                    'brand_id' => trim($product->product->brand->id),
                    'en_Brand' => trim($product->product->brand->getBrandTranslation(0)->brand_name),
                    'en_BrandDescription' => trim($product->product->brand->getBrandTranslation(0)->brand_description),
                    'ch_Brand' => trim($product->product->brand->getBrandTranslation(1)->brand_name),
                    'ch_BrandDescription' => trim($product->product->brand->getBrandTranslation(1)->brand_description),
                    'BrandImage' => $product->product->brand->getBrandLogo(),
                    'BrandLatitude' => $product->product->brand->brand_location_latitude,
                    'BrandLongitude' => $product->product->brand->brand_location_longitude,
                ],
                'Dimentions' => [
                    'length' => $product->product->length,
                    'width' => $product->product->width,
                    'height' => $product->product->height,
                ],
                'Nutrition' => $product->product->getNutrition(),
                'Batches' => $this->getBatchIds($product->product_id)
            ];
        }
        if(isset($translated_products)){
            $response->setResponse($translated_products);
//            return $translated_products;
        }else{
            $response->setResponse(false);
            $response->setMessage('Something went wrong, please try again');
        }

        return $response->getFullResponse();
    }

    public function getBatchIds($product_id){
        $batches = Batch::find()->select(['id'])->where(['product_id' => $product_id])->all();
        return $batches;
    }

    public function setAddresses()
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {

            $orders = Order::find()->where(['shipping_address_id'=> null, 'billing_address_id'=> null ])->all();

            foreach($orders as $live_order){


                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id,['filter[meta]' => 'true'])->order;
                $shipping_address = new Address();
                $shipping_address->address_1 = $order->shipping_address->address_1;
                $shipping_address->address_2 = $order->shipping_address->address_2;
                $shipping_address->type = 'shipping';
                $shipping_address->suburb = $order->shipping_address->city;
                $shipping_address->state = $order->shipping_address->state;
                $shipping_address->postcode = $order->shipping_address->postcode;
                $shipping_address->country = $order->shipping_address->country;
                $shipping_address->first_name = $order->shipping_address->first_name;
                $shipping_address->last_name = $order->shipping_address->last_name;
                $shipping_address->save(false);

                $billing_address = new Address();
                $billing_address->address_1 = $order->billing_address->address_1;
                $billing_address->address_2 = $order->billing_address->address_2;
                $billing_address->type = 'billing';
                $billing_address->first_name = $order->billing_address->first_name;
                $billing_address->contact_no = $order->billing_address->phone;
                $billing_address->email = $order->billing_address->email;
                $billing_address->last_name = $order->billing_address->last_name;
                $billing_address->suburb = $order->billing_address->city;
                $billing_address->state = $order->customer->billing_address->state;
                $billing_address->postcode = $order->customer->billing_address->postcode;
                $billing_address->country = $order->customer->billing_address->country;
                $billing_address->save(false);


                $live_order->shipping_address_id = $shipping_address->id;
                $live_order->billing_address_id = $billing_address->id;
                $live_order->save();


            }

        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function setOrderCreateDate()
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->all();
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id,['filter[meta]' => 'true'])->order;
                $order_date = new DateTime($order->created_at);
                $live_order->created_at = ($order_date->getTimestamp());
                $live_order->save();
            }
        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function setCouponData()
    {
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );
        try {
            $orders = Order::find()->all();
            foreach($orders as $live_order){
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $order = $client->orders->get($live_order->woo_order_id)->order;
                if(count($order->coupon_lines) > 0) {
                    foreach($order->coupon_lines as $line_coupon){
                        $coupon = new OrderCoupon();
                        $coupon->code = $line_coupon->code;
                        $coupon->discount_amount = $line_coupon->amount;
                        $coupon->order_id = $order->order_number;
                        $coupon->save(false) ;
                    }
                }
            }
        } catch (WC_API_Client_Exception $e) {
            $this->logObject($e);
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }



    public function importAllOrders()
    {
        $url = "http://8ston.com/backend/orders";
        $options = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"AUTH-ID: df1e5dbb17973d0c0c6\r\n" .
                    "AUTH-TOKEN:b58eec226f17c5c7d3a386edaa53e1c6\r\n" // check function.stream-context-create on php.net
            )
        );
        $context = stream_context_create($options);
        $file = file_get_contents($url, false, $context);
        $orders = json_decode($file)->response;

        foreach($orders as $order){
            $this->importOrder($order->id);
        }
    }

    /**
     * @param $order_id
     * @return bool
     */
    public function importOrder($order_id)
    {
        $url = "http://8ston.com/backend/order?order_id=".$order_id;
        $options = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"AUTH-ID: df1e5dbb17973d0c0c6\r\n" .
                "AUTH-TOKEN:b58eec226f17c5c7d3a386edaa53e1c6\r\n" // check function.stream-context-create on php.net
            )
        );
        $context = stream_context_create($options);
        $file = file_get_contents($url, false, $context);
        $order = json_decode($file)->response->order;

        $response = new Response();

        try {
            $old_order = Order::find()->where(['woo_order_id' => ($order_id+10000000)])->one();
            if(isset($old_order)){
                $response->setMessage('Order already exists!');
                $response->setResponse(false);
                return $response->getFullResponse();
            }
            $tes_order = new Order();
            $tes_order->woo_order_id = ($order_id+10000000);

            $tes_order->status_id = 1;//OrderStatus::find()->where(['like', 'name', $order->status])->one()->id;
            $tes_order->shipping_id = 1;//Shipping::find()->where(['name' => $order->shipping_lines[0]->method_title])->one()->id;
            $order_date = new DateTime($order->purchased_at);
            $tes_order->created_at = ($order_date->getTimestamp());
//            if(count($order->coupon_lines) > 0) {
//                foreach($order->coupon_lines as $line_coupon){
//                    $coupon = new OrderCoupon();
//                    $coupon->code = $line_coupon->code;
//                    $coupon->discount_amount = $line_coupon->amount;
//                    $coupon->order_id = $order->order_number;
//                    $coupon->save(false) ;
//                }
//            }

            $order_number_length = strlen($order->id);
            $l = 6-$order_number_length;
            $str_ord_number = '';
            for($i=0; $i<$l; $i++  ){
                $str_ord_number.= '0';
            }
            $tes_order->barcode = '1 9350884'.$str_ord_number.$order->id;
            BarcodeGenerator::barcode(preg_replace('/\s+/', '', 'order/barcode/'.$tes_order->barcode).'.png', $tes_order->barcode, "200", "horizontal" ,$code_type="code128" , false, 4 );
            $tes_order->currency_id = 2;//Currency::find()->where(['like', 'name', $order->currency])->one()->id;
            $tes_order->total_amount = $order->amount;
            $tes_order->subtotal = 0;//$order->subtotal;
            $payment = Payment::find()->where(['name' => $order->status])->one();
            $tes_order->payment_id = ((isset($payment)) ? $payment->id : null);
            $tes_order->order_note = '';
            $tes_order->customer_ip = '';
            $tes_order->customer_user_agent = '';
            $user = AppUser::find()->where(['8ston_user_id' => $order->user_id])->one();
            if(isset($user)){
                $tes_order->customer_id = $user->id;
            }else{
                $user = AppUser::find()->where(['email_address' => 'default'])->one();
                $tes_order->customer_id = $user->id;
            }
            $shipping_address = new Address();
            $shipping_address->address_1 = $order->address;
            $shipping_address->address_2 = '';
            $shipping_address->type = 'shipping';
            $shipping_address->suburb = $order->suburb;
            $shipping_address->state = $order->state;
            $shipping_address->postcode = $order->postcode;
            $shipping_address->country = 'CN';
            $shipping_address->continent = FetchContinentFromCountryCode::fetchContinentFromCountryCode($shipping_address->country);
            $shipping_address->first_name = $order->receiver_name;
            $shipping_address->last_name = '';
            $shipping_address->save(false);

            $billing_address = new Address();
            $billing_address->address_1 = $order->address;
            $billing_address->address_2 = '';
            $billing_address->type = 'billing';
            $billing_address->first_name = $order->receiver_name;
            $billing_address->contact_no = $order->phone;
            $billing_address->email = '';
            $billing_address->last_name = '';
            $billing_address->suburb = $order->suburb;
            $billing_address->state = $order->state;
            $billing_address->postcode = $order->postcode;
            $billing_address->country = 'CN';
            $billing_address->save(false);

            $tes_order->billing_address_id = $billing_address->id;
            $tes_order->shipping_address_id = $shipping_address->id;
            $tes_order->tax_id = 1;
            $tes_order->tax_amount = 0;

            foreach ($order->line_items as $item){
                $order_item = new OrderItem();
                $product = ProductTranslation::find()->where(['product_name' => $item->product_name])->one();
                if(isset($product)){
                    $product_location = ProductLocationAttribute::find()->where(['product_id' => $product->product_id , 'language_id' => 1 ])->one();
                    if (isset($product_location)) {
                        $product_location->beston_quantity -= $item->quantity;
                        $product_location->save();
                        if ($product_location->beston_quantity < $product_location->reorder_threshold) {
                            $this->sendReorderNotification($product_location);
                        }
                    }
                }

                $order_item->price = $item->unit_price;
                $order_item->tax = 0;
                $order_item->actual_price = $item->unit_price;
                $order_item->actual_tax = 0;
                $order_item->product_id = $product->product->id;
                $order_item->order_id = $tes_order->woo_order_id;
                $order_item->quantity = $item->quantity;
                $order_item->save(false);
            }
            $tes_order->generateQrCode();
            $tes_order->leave_at_door = '';
            $tes_order->boxes = '';

            $allStatuses = OrderStatus::find()->all();
            foreach ($allStatuses as $status) {
                $statusTime = new OrderStatusTime();
                $statusTime->order_id = $tes_order->woo_order_id;
                $statusTime->status_id = $status->id;
                $statusTime->sort = $status->sort;
                $statusTime->save();
            }

            $order_date = new DateTime($order->created_at);
            $tes_order->created_at = ($order_date->getTimestamp());

            $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $tes_order->woo_order_id, 'status_id' => $tes_order->status_id])->one();
            $orderStatusTime->time = $tes_order->created_at;
            $orderStatusTime->save();

            if($tes_order->save(false)){
                $response->setMessage($tes_order->woo_order_id);
                $response->setResponse(true);
            }else{
                $response->setMessage('failed');
                $response->setResponse(false);
            }
            return $response->getFullResponse();



        } catch (Exception $e) {
            $response->setMessage($e->getMessage() . PHP_EOL);
            $response->setResponse(false);
return $response->getFullResponse();
            echo $e->getMessage() . PHP_EOL;
            echo $e->getCode() . PHP_EOL;
            if ($e instanceof WC_API_Client_HTTP_Exception) {
                print_r($e->get_request());
                print_r($e->get_response());
            }
        }
    }

    public function sendReorderNotification($product){

        ini_set('display_errors', 1);
        error_reporting(E_ALL);
        $from = "admin@bestonmarketplace.com.au";
        $to = "uzair@digitalnoir.com.au ; lruizperez@bestonglobalfoods.com.au ; jchristensen@bestonglobalfoods.com.au";
        $subject = "Product : ".$product->product->product_name." Brand : ".$product->product->brand->brand_name." Low Stock Notification";

        $message = "<!DOCTYPE html>
            <html>
            <body>

            <table style='width:564px'>
              <tr>
                <td ><h2><font face='arial'>Beston Marketplace Low Stock Notification :</font></h2></td>
              </tr>

                <tr>
                <td>
                Hi,<br><br>

                Please organise stock reorder for ".$product->product->product_name."<br><br>
<div style='text-align: center;'>
<table border='1'>
  <tr>
    <th width='75px'>OZIRIS ID</th>
    <th width='75px'>Product #</th>
    <th width='75px'>Name</th>
    <th width='75px'>Brand</th>
    <th width='75px'>Manufacturer</th>
    <th width='75px'>Remaining Quantity</th>
    <th width='75px'>Re-Order Threshold</th>
  </tr>
  <tr class='text-center'>
    <td>".$product->product->id."</td>
    <td>".$product->product->product_number."</td>
    <td>".$product->product->product_name."</td>
    <td>".$product->product->brand->brand_name."</td>
    <td>".$product->product->manufacturer->manufacturer_name."</td>
    <td>".$product->beston_quantity."</td>
    <td>".$product->reorder_threshold."</td>
  </tr>
</table>
<br>
<a href='https://www.oziris.com.au/OzirisBackend/web/admin/product/update?id=".$product->product->id."' target='_blank'>Click Here to Manage Product Quantity on TES</a>
<br><br>
</div>


                <br>Thanks,
                <br><br>
                The Beston Marketplace team.
                <br><br>
                <hr>
                <br><br>
                            Copyright © Beston Technologies All rights reserved.
                <br><br>

                </td>
              </tr>
            </table>

            </body>
            </html>";

        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        // More headers
        $headers .= 'From: ' . $from . "\r\n";

        return mail($to, $subject, $message, $headers);


    }

    public function generateOrderBarCode()
    {
        $orders = Order::find()->where(['barcode' => null])->all();

        foreach($orders as $order){

            $order_number_length = strlen($order->woo_order_id);
            $l = 6-$order_number_length;

            $str_ord_number = '';
            for($i=0; $i<$l; $i++  ){
                $str_ord_number.= '0';
            }

            $order->barcode = '1 9350884'.$str_ord_number.$order->woo_order_id;
            BarcodeGenerator::barcode(preg_replace('/\s+/', '', 'order/barcode/'.$order->barcode).'.png', $order->barcode, "200", "horizontal" ,$code_type="code128" , false, 4 );

            print_r($order->save());
        }
    }

    public function returnAllCoupons(){
        $raw_coupon = Coupon::find()->all();

        foreach ($raw_coupon as $coupon){
            $coupons[] = [
                "code"=>$coupon->code,
                "discount_type"=>$coupon->discount_type,
                "description"=>$coupon->description,
                "amount"=>$coupon->amount,
                "expiry_date"=>$coupon->expiry_date,
                "individual_use"=>$coupon->individual_use,
                "usage_limit"=>$coupon->usage_limit,
                "usage_limit_per_user"=>$coupon->usage_limit_per_user,
                "limit_usage_to_x_items"=>$coupon->limit_usage_to_x_items,
                "free_shipping"=>$coupon->free_shipping,
                "exclude_sale_items"=>$coupon->exclude_sale_items,
                "minimum_amount"=>$coupon->minimum_amount,
                "maximum_amount"=>$coupon->maximum_amount,
                "email_restrictions"=>$coupon->email_restrictions,
                "coupon_products"=> $coupon->couponProducts
            ]  ;
        }

        return $coupons;
    }

    public function getAllBatches(){
        $products = ProductLocationAttribute::find()->where(['language_id'=>'1'])->all();
        $batches = [];
        foreach($products as $product){
            $raw_batches = Batch::find()->where(['product_id'=>$product->product_id])->all();
            foreach($raw_batches as $raw_batch){
                $batches[] = [
                    'id' => $raw_batch->id,
                    'product_id' => $raw_batch->product_id,
                    'batch_qr' => $raw_batch->qr_code,
                    'batch_qr_url' => $raw_batch->getQrCodeImageUrl(),
                    'batch_number' => $raw_batch->batch_number,
                    'manufacture_date' => $raw_batch->manufacture_date,
                    'recall_date' => $raw_batch->recalled_date,
                    'received_at_fulfilment_center' => date('d/m/Y h:i A ',$raw_batch->created_at),
                    'coo_percentage' => $raw_batch->overall_coo_percentage,
                    'coo_image_url' => $raw_batch->getCoolImageUrl(),
                    'batch_items' => $raw_batch->getBatchItems(),
                    "ingredients" => $raw_batch->getIngredients(),
                ];
            }
        }
        return $batches;
    }
}