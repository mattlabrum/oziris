<?php

namespace app\modules\admin\controllers;
use app\models\base\BatchItem;
use app\models\base\Coo;
use app\models\Batch;
use app\models\BatchIngredient;
use app\models\Product;
use app\models\search\BatchSearch;
use app\models\ShelfLifeUnit;
use app\utils\csvToArray;
use app\widgets\xj\QRcode;
use app\widgets\xj\Text;
use DateTime;
use Exception;
use Imagick;
use kartik\mpdf\Pdf;
use Yii;
use cornernote\returnurl\ReturnUrl;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the class for controller "app\modules\admin\controllers\BatchController".
 */
class BatchController extends \app\modules\admin\controllers\base\BatchController
{

    public $errorProduct = false;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'duplicate', 'discard', 'save-data', 'generate-csv', 'print-batch', 'upload-cool', 'get-manufacture-date-from-expiry'],
                        'roles' => ['admin']
                    ]

                ]
            ]
        ];
    }

    public function actionGetManufactureDateFromExpiry()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $product = Product::find()->where(['id' => $_POST['product_id']])->one();
        $shelf_life = 0;
        if( strpos($product->shelf_life, '.')  ){
            $unit = '';
            if($product->shelf_life_unit == 4){
               $shelf_life = (int) ($product->shelf_life * 365.25);
                $unit = 'days';
            }elseif ($product->shelf_life_unit == 3){
                $shelf_life = (int)($product->shelf_life * 30.42);
                $unit = 'days';
            }elseif ($product->shelf_life_unit == 2){
                $shelf_life = (int)($product->shelf_life * 24);
                $unit = 'hours';
            }elseif ($product->shelf_life_unit == 1){
                $shelf_life = (int)($product->shelf_life * 60);
                $unit = 'minutes';
            }

        }else{
            $shelf_life = $product->shelf_life;
            $unit = ShelfLifeUnit::find()->where(['id' => $product->shelf_life_unit ])->one()->name;
        }

        if(isset($product)){
            $date = DateTime::createFromFormat('m/d/Y H:i', $_POST['expiry_date']);

            $date->modify('-'.$shelf_life.' '.$unit);
            return $date->format('m/d/Y H:i');
        }
    }




    /**
     * Creates a copy of an existing Batch model.
     * If duplication is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);

        if (\Yii::$app->request->isPost) {
            $model = new Batch();
            $model->scenario = 'duplicate';
            $model->load(Yii::$app->request->post());
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $qr_text = 'www.oziris.com.au/home?batch_id=';
            $model->qr_code = $qr_text;
            try {
                if (!$model->save()) {
                    throw new Exception('Unable to save record.');
                }

                $model->qr_code = $qr_text . $model->id;
                $model->qr_code_image = $this->generateQrCode($model->qr_code,$model->batch_number);

                if (!$model->update()) {
                    throw new Exception('Unable to save record.');
                }
                $transaction->commit();
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Batch has been created.'));
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            } catch (Exception $e) {
                $transaction->rollback();
            }

        }
            return $this->render('duplicate', compact('model'));
    }

    public function actionSaveData()
    {

        $batchesToSave = [];
        $errorString = 'Batches Were not Imported! Please check the following entries :'.'</br></br>';
        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);
//        $csvArray = csvToArray::csvToArray('dataimport/batches.csv');
        foreach($csvArray as $batch){

            if (isset($batch['ID']) && $batch['ID'] > 0 && isset(Batch::findOne($batch['ID'])->id)) {
                $batchToSave = Batch::findOne($batch['ID']);
                $this->populateBatchData($batch,$batchToSave);
            }else{
                $batchToSave = new Batch();
                $this->populateBatchData($batch,$batchToSave);
            }

            if (!$this->errorProduct) {
                array_push($batchesToSave, $batchToSave);
            } else{
                $errorString .= $this->handleError($batch['BatchNumber']);
            }

        }
        if (count($batchesToSave) == count($csvArray)) {
            foreach ($batchesToSave as $batchToSave) {
                $batchToSave->save(true);
                $errorString = count($batchesToSave)." Batches Imported Successfully!";
            }
            Yii::$app->getSession()->setFlash('success', Yii::t('app', $errorString));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
        }

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    function populateBatchData($batch,$batchToSave){
        $batchToSave->manufacture_date= $batch['ManufactureDate'];
        $batchToSave->recalled_date=$batch['RecalledDate'];
        $batchToSave->qr_code=$batch['QrCode'];
        $batchToSave->product_id=(isset(Product::findOne(['id' => $batch['ProductID']])->id) ? Product::findOne(['id' => $batch['ProductID']])->id : $this->errorProduct = true);
        $batchToSave->batch_number=$batch['BatchNumber'];
        return $batchToSave;
    }

    function handleError($batchNumber)
    {
        $errorString = 'Incorrect Product Entry for :' . $batchNumber . '</br>';
        $this->errorProduct = false;

        return $errorString;
    }


    /**
     * Searches all products according to search criteria and generates CSV.
     * @return CSV Download link
     */
    public function actionGenerateCsv()
    {
        $searchModel = new BatchSearch();

        $searchModel->manufacture_date = (isset($_POST['manufacture_date']) ? $_POST['manufacture_date'] : '');
        $searchModel->recalled_date = (isset($_POST['recalled_date']) ? $_POST['recalled_date'] : '');
        $searchModel->qr_code = (isset($_POST['qr_code']) ? $_POST['qr_code'] : '');
        $searchModel->product_id = (isset($_POST['product_id']) ? $_POST['product_id'] : '');
        $searchModel->batch_number = (isset($_POST['batch_number']) ? $_POST['batch_number'] : '');

        $dataProvider = $searchModel->filterCSV($searchModel);
        $dataProvider->setPagination(false);



        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){

            $manufacture_date = strtotime($object->manufacture_date);//date_format(date_create_from_format('m/d/Y H:i', $object->manufacture_date), 'd/m/Y H:i');

            if(isset( $object->product )){
                $arr = [];

                $arr['batchid'] = '"'.$object->id.'"';
                $arr['id'] = '"'.$object->product->id.'"';
                $arr['batch_number'] = '"'.$object->batch_number.'"';
                $arr['product_id'] = '"'. $object->product->product_name .'"';
                $arr['manufacture_date'] = '"'.date('d/m/Y H:i',$manufacture_date).'"';
                $arr['expiry_date'] = '"'.$this->calculateBatchExpiryDate($manufacture_date , $object->product->shelf_life , $object->product->shelfLifeUnit->name).'"';
                $arr['recalled_date'] = '"'.$object->recalled_date.'"';

                $arr['BatchLevelQuantity'] = '"'.$object->getProductBatchLevelQuantity().'"';
                $arr['ActualQuantity'] = '"'.$object->getProductQuantity().'"';
                $arr['TheoQuantity'] = '"'.$object->getTheoraticalQuantity().'"';
                $arr['SoldQuantity'] = '"'.$object->getProductSoldQuantity().'"';
                $arr['DiscardedQuantity'] = '"'.$object->getProductDiscardQuantity().'"';
                $arr['Variance'] = '"'.$object->getProductVariance().'"';

                array_push($toCSVArray, $arr);

            }



        }

        $fh = fopen('csv/batches.csv', 'w') or die('Cannot open the file');
        $strHeader = 'OzirisBatchID,OzirisProductID,BatchNumber,ProductName,ManufactureDate,ExpiryDate,RecalledDate,BatchLevelQuantity,ActualQuantity,TheoQuantity,SoldQuantity,DiscardedQuantity,Variance';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);

        return $writeSuccessful;

    }

    public function calculateBatchExpiryDate($manufacture_date , $shelf_life , $shelf_life_unit ){
        $hours = 0;
        if($shelf_life_unit == 'Days'){
            $hours = round($shelf_life*24);
        }else if ($shelf_life_unit == 'Months'){
            $hours = round(($shelf_life * 30.4375)*24);
        }else if($shelf_life_unit == 'Years'){
            $hours = round(($shelf_life * 365.25)*24);
        }

        $expiry_timestamp = strtotime('+'.$hours.' hours',$manufacture_date);
        return date('d/m/Y H:i' , $expiry_timestamp);
    }

    public function calculateCOOPercentage($data)
    {
        $totalCooPercentage = 0;
        if(isset($data['no_of_ingredients'])){
            for($i=0;$i<$data['no_of_ingredients'] ; $i++){
                $coo = Coo::find()->where(['id' =>$data['coo_id-'.$i] ])->one();
                if(isset($coo)){
                    if($coo->country == 'Australia' || $coo->country == 'australia' ){
                        $totalCooPercentage += $data['percentage-'.$i];
                    }
                }

            }
        }

        return $totalCooPercentage;
    }

    /**
     * Creates a new Batch model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Batch;
        $model->scenario = 'create';
        if ($model->load(Yii::$app->request->post())) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            $model->qr_code = 'default';
            try {
                $model->overall_coo_percentage = $this->calculateCOOPercentage($_POST);
                $model->expiry_date = $_POST['Batch']['expiry_date'];
                if (!$model->save(false)) {
                    throw new Exception('Unable to save record.');
                }
                $model->qr_code_image = $model->generateQrCode();
                if (!$model->update(false)) {
                    throw new Exception('Unable to save record.');
                }
                $transaction->commit();
                $this->populateBatchIngredient($_POST,$model->id);
                $this->populateBulkQRCodes(['from' =>$_POST['qr_from'] , 'to' => $_POST['qr_to'], 'batch_id' => $model->id]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Batch has been created.'));
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', Html::errorSummary($model)));
                $transaction->rollback();
            }
        }
        return $this->render('create', compact('model'));
    }
    /**
     * Updates an existing Batch model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ((\Yii::$app->request->isPost)) {

        $model->batch_number = $_POST['Batch']['batch_number'];
        $model->expiry_date = $_POST['Batch']['expiry_date'];
        $model->product_id = $_POST['Batch']['product_id'];
        $model->manufacture_date = $_POST['Batch']['manufacture_date'];
        $model->recalled_date = $_POST['Batch']['recalled_date'];
        $model->quantity = $_POST['Batch']['quantity'];
        $model->label_type_text = $_POST['Batch']['label_type_text'];
        $model->overall_coo_percentage = $this->calculateCOOPercentage($_POST);
            if ($model->save(false)) {

                $this->populateBatchIngredient($_POST, $id);
                $this->populateBulkQRCodes(['from' =>$_POST['qr_from'] , 'to' => $_POST['qr_to'], 'batch_id' => $id]);
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Batch has been updated.'));
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }

        } else {
            $model->load(Yii::$app->request->get());
        }
        return $this->render('update', compact('model'));
    }

    /**
     * Generates a PDF of an existing batch model with its COOL label and Ingredient details.
     * @param integer $id
     * @return pdf
     */
    public function actionPrintBatch($id) {

        $model = $this->findModel($id);

        $content = $this->renderPartial('print-batch',['model' => $model]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
//            'cssFile' => '@app/assets/web/css/app1.css',
            'options' => ['title' => 'Batch <'.$model->batch_number. '> Details'],
            'methods' => [
                'SetHeader'=>['Batch <'.$model->batch_number. '> Details'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
        // return the pdf output as per the destination setting
        return $pdf->render();
    }


    public function actionUploadCool(){
        $filteredData=substr($_POST['img'], strpos($_POST['img'], ",")+1);
        $unencodedData=base64_decode($filteredData);
        file_put_contents(Url::to('uploads/cool/').'cool-'.$_POST['batch_id'].'.png', $unencodedData);

        $batch = Batch::find()->where(['id' => $_POST['batch_id'] ])->one();



        if(isset($batch)){
            $batch->cool_image = 'cool-'.$_POST['batch_id'].'.png';
            $batch->save(false);
        }

    }



    public function actionDiscard($id)
    {
        $items = BatchItem::find()->where(['batch_id' => $id])->andWhere(['item_status' => ''])->all();

        foreach($items as $item){
            $item->item_status = 'discard';
            $item->save(false);
        }

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    public function populateBulkQRCodes($attributes)
    {
        $counter = 0;
        $from = substr(str_replace(' ', '', $attributes['from']), 3, strlen(str_replace(' ', '', $attributes['from'])));
        $to = substr(str_replace(' ', '', $attributes['to']), 3, strlen(str_replace(' ', '', $attributes['to'])));


        if($to - $from > 1000){
            Yii::$app->getSession()->setFlash('error', 'System does not allow creation of more than 1000 batch items in one transaction!');
        }else{
            if ($from != '' && $to != '') {
                if ($from <= $to) {
                    while ($from <= $to) {
                        $qr_code = 'JFC' . $from;
                        $new_batch_item = BatchItem::find()->where(['qr_code_text' => $qr_code])->one();
                        if(!isset($new_batch_item)){
                            $new_batch_item = new BatchItem();
                            $new_batch_item->batch_id = $attributes['batch_id'];
                            $new_batch_item->qr_code_text = $qr_code;
                            $new_batch_item->item_status = '';
                            $new_batch_item->save(false);
                            $counter++;
                        }
                        $from++;
                    }
                    Yii::$app->getSession()->setFlash('warning', $counter.' QR codes attached to the batch!');
                }else{
                    Yii::$app->getSession()->setFlash('error', 'Invalid QR range, No QR codes added!');
                }
            }else{
                Yii::$app->getSession()->setFlash('error', 'Invalid QR range, No QR codes added!');
            }
        }



    }

    public function populateBatchIngredient($data,$batch_id){
        $ingredientsToSave=[];

        if(isset($data['no_of_ingredients'])){
            for($i=0;$i<$data['no_of_ingredients'] ; $i++){
                $batchIngredient = BatchIngredient::find()->where(['batch_id'=>$batch_id, 'ingredient_id' =>$data['ingredient_id-'.$i]])->one();
                if(isset($batchIngredient)){
                    $batchIngredient->manufacture_date = $data['ingredient_date-'.$i];
                    $batchIngredient->arrival_at_manufacturer_date= $data['arrival_at_manufacturer_date-'.$i];
                    $batchIngredient->percentage = $data['percentage-'.$i];
                    $batchIngredient->show_on_oziris = (isset($data['show_on_oziris-'.$i]) ? 1 : 0);
                    $batchIngredient->coo_id = $data['coo_id-'.$i];
                    $batchIngredient->supplier_id = $data['supplier_id-'.$i];
                    $batchIngredient->save(false);
                }else{
                    $batchIngredient = new BatchIngredient;
                    $batchIngredient->scenario = 'create';
                    $batchIngredient->ingredient_id = $data['ingredient_id-'.$i];
                    $batchIngredient->percentage = $data['percentage-'.$i];
                    $batchIngredient->arrival_at_manufacturer_date= $data['arrival_at_manufacturer_date-'.$i];
                    $batchIngredient->show_on_oziris = ((isset($data['show_on_oziris-'.$i])&& $data['show_on_oziris-'.$i] == 'on') ? 1 : 0);
                    $batchIngredient->coo_id = $data['coo_id-'.$i];
                    $batchIngredient->batch_id = $batch_id;
                    $batchIngredient->manufacture_date = $data['ingredient_date-'.$i];
                    $batchIngredient->supplier_id = $data['supplier_id-'.$i];
                    $batchIngredient->save();
                }

            }
        }

//        $this->updateBatchItems($data['Batch']['batchItems'], $batch_id);


    return $ingredientsToSave;
    }


}