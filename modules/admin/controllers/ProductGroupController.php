<?php

namespace app\modules\admin\controllers;
use app\models\Product;
use app\models\ProductGroupTranslation;
use app\models\form\ProductGroupForm;
use Yii;
use cornernote\returnurl\ReturnUrl;
use dmstr\bootstrap\Tabs;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;


/**
 * This is the class for controller "app\modules\admin\controllers\ProductGroupController".
 */
class ProductGroupController extends \app\modules\admin\controllers\base\ProductGroupController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'change-status'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }


    public $productGroupTranslation;
    /**
     * Displays a single ProductGroup model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = ProductGroupTranslation::findOne(['id' => $id]);
        $productGroupModel = new ProductGroupForm();
        $productGroupModel->setProductGroup($model);
        $productGroupModel->setProductGroupTranslation($modelTranslation);
        return $this->render('view', ['model' => $productGroupModel]);
    }
    
    /**
     * Updates an existing ProductGroup and ProductGroup Translation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $productGroupModel = new ProductGroupForm();
        $productGroupModel->scenario = 'update';
        if (Yii::$app->request->post()) {
            $model->product_group_name=Yii::$app->request->post()['ProductGroup']['product_group_name'];
            $model->show_on_menu=Yii::$app->request->post()['ProductGroup']['show_on_menu'];
            if($model->save(true)){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Group has been '.$productGroupModel->scenario.'d .'));
                if(Yii::$app->request->post()['ProductGroupTranslation']['language_id']!=''){
                    $this->productGroupTranslation = ProductGroupTranslation::find()->where(['product_group_id' => $model->id, 'language_id' => $_POST['ProductGroupTranslation']['language_id'] ])->one();
                    if (isset($this->productGroupTranslation)) {
                        $this->productGroupTranslation->attributes = $_POST['ProductGroupTranslation'];
                        $this->productGroupTranslation->product_group_id = $model->id;
                    } else {
                        $this->productGroupTranslation = new ProductGroupTranslation();
                        $this->productGroupTranslation->attributes = $_POST['ProductGroupTranslation'];
                        $this->productGroupTranslation->product_group_id = $model->id;
                    }
                    if($this->productGroupTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Group  has been '.$productGroupModel->scenario.'d and translated.'));
                    }
                 }
                $productGroupModel->setProductGroup($model);
                $productGroupModel->setProductGroupTranslation($this->productGroupTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        $productGroupModel->setProductGroup($model);
        $productGroupModel->setProductGroupTranslation($this->productGroupTranslation);
        return $this->render('update', ['model' => $productGroupModel]);
    }

    /**
     * Enables/Disables an existing ProductGroup model.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeStatus($id)
    {


        $this->findModel($id)->changeStatus();
        return $this->redirect('index');
    }

//        $query = Product::find()->where(['product_group_id' => $id]);
//
//        $dataProvider = new ActiveDataProvider([
//            'query' => $query,
//            //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
//        ]);
//
//        if (count(Product::find()->where(['product_group_id' => $id, 'deleted_at' => null])->all()) > 0) {
//            return $this->render('../dependence/index', [
//                'dataProvider' => $dataProvider,
//                'searchModel' => null,
//            ]);
//
//        } else {
//            if ($this->findModel($id)->changeStatus()) {
//                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Group Status Changed Successfully.'));
//            } else {
//                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Product Group Status Change was Unsuccessful.'));
//            }
//
//            $query = Product::find()->where(['product_group_id' => $id]);
//
//            $dataProvider = new ActiveDataProvider([
//                'query' => $query,
//                //'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
//            ]);
//
//            return $this->render('../dependence/index', [
//                'dataProvider' => $dataProvider,
//                'searchModel' => null,
//            ]);
//        }
//    }

}
