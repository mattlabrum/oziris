<?php

namespace app\modules\admin\controllers;
use app\models\form\ManufacturerForm;
use app\models\ManufacturerTranslation;
use app\models\search\ManufacturerSearch;
use app\models\upload\UploadFile;
use dmstr\bootstrap\Tabs;
use Exception;
use yii\filters\AccessControl;
use app\utils\csvToArray;
use app\models\Manufacturer;
use Yii;
use cornernote\returnurl\ReturnUrl;
use yii\web\UploadedFile;

/**
 * This is the class for controller "app\modules\admin\controllers\ManufacturerController".
 */
class ManufacturerController extends \app\modules\admin\controllers\base\ManufacturerController
{
    public $manufacturerTranslation;
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'generate-csv', 'save-data', 'duplicate', 'change-status'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * Updates an existing Manufacturer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $manufacturer_logo = $model->manufacturer_logo;

        $manufacturerModel = new ManufacturerForm();
        $manufacturerModel->scenario = 'update';

        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['Manufacturer'];
            if ((isset($_FILES['Manufacturer']['name']['manufacturer_logo']) && ($_FILES['Manufacturer']['name']['manufacturer_logo'] != ''))) {
                $manufacturer_logo_name = tempnam('uploads/manufacturer', 'manufacturer-');
                $manufacturer_logo_name = substr($manufacturer_logo_name, (strrpos($manufacturer_logo_name, '/') + 1), strlen($manufacturer_logo_name));
                if($manufacturer_logo_name != 'default.jpg'){
                    unlink('uploads/manufacturer/' . $manufacturer_logo_name);
                }


                $image_upload_status = $this->uploadFile($model, $manufacturer_logo_name);

                if (isset($image_upload_status['manufacturer_logo_status'])) {
                    if (!$image_upload_status['manufacturer_logo_status']) {
                        $model->manufacturer_logo = $manufacturer_logo;
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Manufacturer Image Upload unsuccessful!'));
                    } else {
                        try {
                            if($manufacturer_logo != 'default.jpg') {
                                unlink('uploads/manufacturer/' . $manufacturer_logo);
                            }
                        } catch (Exception $e) {

                        }
                        $model->manufacturer_logo = $manufacturer_logo_name . '.' . $image_upload_status['manufacturer_logo_extension'];
                    }
                }
            }else{
                $model->manufacturer_logo = $manufacturer_logo;
            }
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Manufacturer has been '.$manufacturerModel->scenario.'d .'));

                if ($_POST['ManufacturerTranslation']['language_id'] != '') {
                    $this->manufacturerTranslation = ManufacturerTranslation::find()->where(['manufacturer_id' => $model->id])->andWhere(['language_id' => $_POST['ManufacturerTranslation']['language_id']])->one();

                    if (isset($this->manufacturerTranslation)) {
                        $this->manufacturerTranslation->attributes = $_POST['ManufacturerTranslation'];
                        $this->manufacturerTranslation->manufacturer_id = $model->id;
                    } else {
                        $this->manufacturerTranslation = new ManufacturerTranslation();
                        $this->manufacturerTranslation->attributes = $_POST['ManufacturerTranslation'];
                        $this->manufacturerTranslation->manufacturer_id= $model->id;
                    }


                    if ($this->manufacturerTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Manufacturer has been updated and translated.'));
                    }
                }
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }

        } elseif (!\Yii::$app->request->isPost) {
//            $manufacturerModel->load(Yii::$app->request->get());
        }

        $manufacturerModel->setManufacturer($model);
        $manufacturerModel->setManufacturerTranslation($this->manufacturerTranslation);

        return $this->render('update', ['model' => $manufacturerModel]);
    }

    public function actionCreate()
    {
        $manufacturer = new Manufacturer();
        $manufacturerTranslation = new ManufacturerTranslation();
        if (Yii::$app->request->post()) {
            $manufacturer->attributes = $_POST['Manufacturer'];

            $manufacturer_logo_name = tempnam( 'uploads/manufacturer' , 'manufacturer-' );
            $manufacturer_logo_name = substr($manufacturer_logo_name,(strrpos($manufacturer_logo_name,'/')+1),strlen($manufacturer_logo_name));

            if($manufacturer_logo_name != 'default.jpg') {
                unlink('uploads/manufacturer/' . $manufacturer_logo_name);
            }


            $image_upload_status = $this->uploadFile($manufacturer,$manufacturer_logo_name);

            if(isset($image_upload_status['manufacturer_logo_status'])) {
                if (!$image_upload_status['manufacturer_logo_status']) {
                    if((isset($_FILES['Manufacturer']['name']['manufacturer_logo']) && ($_FILES['Manufacturer']['name']['manufacturer_logo'] != ''))){
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Manufacturer Image Upload unsuccessful!'));
                    }
                }else{

                    $manufacturer->manufacturer_logo = $manufacturer_logo_name.'.'.$image_upload_status['manufacturer_logo_extension'];
                }
            }

            if(($_FILES['Manufacturer']['name']['manufacturer_logo'] == '')){
                $manufacturer->manufacturer_logo= 'default.jpg';
            }

            if($manufacturer->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Manufacturer has been created.'));
                if($_POST['ManufacturerTranslation']['language_id']!=''){
                    $manufacturerTranslation->attributes = $_POST['ManufacturerTranslation'];
                    $manufacturerTranslation->manufacturer_id= $manufacturer->id;
                    if($manufacturerTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Manufacturer  has been Created and translated.'));
                    }
                }
                return $this->redirect(['view', 'id' => $manufacturer->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }

        $manufacturerModel = new ManufacturerForm();
        $manufacturerModel->scenario = 'create';
        $manufacturerModel->setManufacturer($manufacturer);
        $manufacturerModel->setManufacturerTranslation($manufacturerTranslation);
        return $this->render('create', ['model' => $manufacturerModel]);
    }

    private function uploadFile($mainModel, $manufacturer_logo_name)
    {
        $manufacturer_logo = new UploadFile();
        $manufacturer_logo->upload_file = UploadedFile::getInstance($mainModel, 'manufacturer_logo');
        try {
            $image_upload_status = [
                'manufacturer_logo_status' => $manufacturer_logo->upload('manufacturer',$manufacturer_logo_name),
                'manufacturer_logo_extension' => $manufacturer_logo->upload_file->extension,
            ];
            return $image_upload_status;
        } catch (Exception $e) {

        }
    }

    /**
     * Duplicates an existing Manufacturer model.
     * If duplicate is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = ManufacturerTranslation::findOne(['id' => $id]);
        if(!isset($modelTranslation)){
            $modelTranslation = new ManufacturerTranslation();
        }
        $manufacturerModel = new ManufacturerForm();
        $manufacturerModel->scenario = 'duplicate';


        if (Yii::$app->request->post()) {
            $model = new Manufacturer();
            $model->attributes = $_POST['Manufacturer'];
            $model->manufacturer_logo = ((!isset($_FILES['Manufacturer']['name']['manufacturer_logo'])|| ($_FILES['Manufacturer']['name']['manufacturer_logo'] == '')) ? 'default.jpg' : $_FILES['Manufacturer']['name']['manufacturer_logo']);

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Manufacturer has been '.$manufacturerModel->scenario.'d .'));
                if(Yii::$app->request->post()['ManufacturerTranslation']['language_id']!=''){
                    $modelTranslation->attributes = $_POST['ManufacturerTranslation'];
                    $modelTranslation->manufacturer_id= $model->id;
                    if($modelTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Manufacturer  has been '.$manufacturerModel->scenario.'d and translated.'));
                    }
                }
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        $manufacturerModel->setManufacturer($model);
        $manufacturerModel->setManufacturerTranslation($modelTranslation);

        return $this->render('duplicate', ['model' => $manufacturerModel]);
    }

    /**
     * Displays a single Manufacturer model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        Tabs::rememberActiveState();

        $model = $this->findModel($id);
        $modelTranslation = ManufacturerTranslation::findOne(['id' => $id]);
        $manufacturerModel = new ManufacturerForm();//$this->findModel($id);
        $manufacturerModel->setManufacturer($model);
        $manufacturerModel->setManufacturerTranslation($modelTranslation);
        return $this->render('view', ['model' => $manufacturerModel]);
    }

    /**
     * Searches all Manufacturers according to search criteria and generates CSV.
     * @return CSV Download link
     */
    public function actionGenerateCsv()
    {
        $searchModel = new ManufacturerSearch();

        $searchModel->manufacturer_name = (isset($_POST['manufacturer_name']) ? $_POST['manufacturer_name'] : '');
        $searchModel->manufacturer_logo = (isset($_POST['manufacturer_logo']) ? $_POST['manufacturer_logo'] : '');
        $searchModel->manufacturer_description = (isset($_POST['manufacturer_description']) ? $_POST['manufacturer_description'] : '');
        $searchModel->manufacturer_location_latitude = (isset($_POST['manufacturer_location_latitude']) ? $_POST['manufacturer_location_latitude'] : '');
        $searchModel->manufacturer_location_longitude = (isset($_POST['manufacturer_location_longitude']) ? $_POST['manufacturer_location_longitude'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);



        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            $arr = [];

            $arr['id'] = '"'.$object->id.'"';
            $arr['manufacturer_name'] = '"'.$object->manufacturer_name.'"';
            $arr['manufacturer_description'] = '"'.$object->manufacturer_description.'"';
            $arr['manufacturer_logo'] = '"'.$object->manufacturer_logo.'"';
            $arr['manufacturer_location_latitude'] = '"'.$object->manufacturer_location_latitude.'"';
            $arr['manufacturer_location_longitude'] = '"'.$object->manufacturer_location_longitude.'"';

            array_push($toCSVArray, $arr);
        }

        $fh = fopen('csv/manufacturers.csv', 'w') or die('Cannot open the file');
        $strHeader = 'ID,ManufacturerName,ManufacturerDescription,ManufacturerLogo,ManufacturerLocationLatitude,ManufacturerLocationLongitude';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);

        return $writeSuccessful;

    }

    public function actionSaveData()
    {

        $manufacturersToSave = [];
        $errorString = 'Manufacturers Were not Imported! Please check the CSV :';
        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);
        foreach($csvArray as $manufacturer){

            if (isset($manufacturer['ID']) && $manufacturer['ID'] > 0 && isset(Manufacturer::findOne($manufacturer['ID'])->id)) {
                $manufacturerToSave = Manufacturer::findOne($manufacturer['ID']);
                $this->populateManufacturerData($manufacturer,$manufacturerToSave);
            }else{
                $manufacturerToSave = new Manufacturer();
                $this->populateManufacturerData($manufacturer,$manufacturerToSave);
            }
            array_push($manufacturersToSave, $manufacturerToSave);
        }
        if (count($manufacturersToSave) == count($csvArray)) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($manufacturersToSave as $manufacturerToSave) {
                    if(!$manufacturerToSave->save()){
                        throw new Exception('Unable to save record.');
                    }
                }
                $transaction->commit();
                $errorString = count($manufacturersToSave)." Manufacturers Imported Successfully!";
                return $errorString;
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
                $transaction->rollback();
                return $this->redirect('index');
            }
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
            return $this->redirect('index');
        }

    }

    function populateManufacturerData($manufacturer,$manufacturerToSave){

        $manufacturerToSave->manufacturer_name= $manufacturer['ManufacturerName'];
        $manufacturerToSave->manufacturer_logo=$manufacturer['ManufacturerLogo'];;
        $manufacturerToSave->manufacturer_description=$manufacturer['ManufacturerDescription'];
        $manufacturerToSave->manufacturer_location_latitude=$manufacturer['ManufacturerLocationLatitude'];
        $manufacturerToSave->manufacturer_location_longitude=$manufacturer['ManufacturerLocationLongitude'];
        return $manufacturerToSave;
    }

    /**
     * Enables/Disables an existing Manufacturer model.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeStatus($id)
    {
        $this->findModel($id)->changeStatus();
        return $this->redirect('index');
    }


}
