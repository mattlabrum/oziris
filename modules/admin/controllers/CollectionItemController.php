<?php

namespace app\modules\admin\controllers;
use Yii;
use app\models\CollectionItem;
use yii\filters\AccessControl;

/**
 * This is the class for controller "app\modules\admin\controllers\CollectionItemController".
 */
class CollectionItemController extends \app\modules\admin\controllers\base\CollectionItemController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'delete-collection-item', 'get-collection-items'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }


    /**
     * Insert a new CollectionItem model.
     * If creation is successful, true is returned.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CollectionItem;
        $model->scenario = 'create';
        $model->attributes = $_POST;
        if($model->save()){
            return $model->id;
        }
    }

    /**
     * Deletes an existing CollectionItem model.
     * @return boolean
     */
    public function actionDeleteCollectionItem()
    {
        return $this->findModel($_POST)->delete();
    }

    /**
     * Deletes an existing CollectionItem model.
     * @return boolean
     */
    public function actionGetCollectionItems()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $collections = CollectionItem::findAll(['collection_id' => $_POST]);
        $results = [];
        foreach ($collections as $collection) {
            $results[] = ['id' => $collection->id,
                          'product_name' => $collection->product->product_name,
                          'brand_name' => $collection->product->brand->brand_name,
                          'manufacturer_name' => $collection->product->manufacturer->manufacturer_name,
                          'quantity' => $collection->quantity
                        ];
        }
        return ['results' => $results];
    }

}
