<?php
/**
 * /vagrant/digitalnoir/OzirisBackend/src/../runtime/giiant/49eb2de82346bc30092f584268252ed2
 *
 * @package default
 */


namespace app\modules\admin\controllers;

use app\models\CouponProduct;
use app\models\Coupon;
use Yii;
use yii\helpers\Url;
use yii\httpclient\Client;
use WC_API_Client;

/**
 * This is the class for controller "CouponController".
 */
class CouponController extends \app\modules\admin\controllers\base\CouponController
{

    /**
     * Updates an existing Coupon model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );

        CouponProduct::deleteAll(['coupon_id' => $id]);

        try {
            if ($model->load($_POST) && $model->save()) {
                $model->expiry_date = $_POST['Coupon']['expiry_date'];
                if($_POST['Coupon']['couponProducts']!= ''){
                    foreach($_POST['Coupon']['couponProducts'] as $product){
                        $coupon_product = new CouponProduct();
                        $coupon_product->coupon_id = $model->id;
                        $coupon_product->product_id = $product;
                        $coupon_product->save(false);
                    }
                }
                $data = [
                    "code" => $model->code,
                    "type" => $model->discount_type,
                    "description" => $model->description,
                    "amount" => $model->amount,
                    "expiry_date" => ($model->expiry_date != '' ? date('Y-m-d H:i:s',strtotime($model->expiry_date)) : '' ) ,// 2017-06-14T06:18:14Z  ,
                    "product_ids" => $_POST['Coupon']['couponProducts'],
                    "individual_use" => ($model->individual_use == 1 ? true : false),
                    "usage_limit" => $model->usage_limit,
                    "usage_limit_per_user" => $model->usage_limit_per_user,
                    "limit_usage_to_x_items" => $model->limit_usage_to_x_items,
                    "enable_free_shipping" => ($model->free_shipping == 1 ? true : false),
                    "exclude_sale_items" => ($model->exclude_sale_items == 1 ? true : false),
                    "minimum_amount" => $model->minimum_amount,
                    "maximum_amount" => $model->maximum_amount,
                    "customer_emails" => explode(',', $model->email_restrictions),
                ];
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $response = $client->coupons->update($model->woo_coupon_id, $data);
                $model->woo_coupon_id= $response->coupon->id;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('update', ['model' => $model]);
    }

    /**
     * Creates a new Coupon model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate() {
        $model = new Coupon;
        $options = array(
            'debug' => true,
            'return_as_array' => false,
            'validate_url' => false,
            'timeout' => 90,
            'ssl_verify' => false,
        );

        $woo_product_ids = [];

        try {
            if ($model->load($_POST) && $model->save()) {
                $model->expiry_date = $_POST['Coupon']['expiry_date'];
                $client = new Client();
                if($_POST['Coupon']['couponProducts']!= ''){
                    foreach($_POST['Coupon']['couponProducts'] as $product){
                        $coupon_product = new CouponProduct();
                        $coupon_product->coupon_id = $model->id;
                        $coupon_product->product_id = $product;
                        $coupon_product->save(false);

                        $response = $client->createRequest()
                            ->setMethod('post')
                            ->setUrl(getenv('SERVER_TYPE').'/get-product-id-by-sku')
                            ->setData(
                                [
                                    'sku' => $product,
                                ])->send();
                        $woo_product_ids [] = json_decode($response->content)->product_id;

                    }
                }

                $data = [
                    "code" => $model->code,
                    "type" => $model->discount_type,
                    "description" => $model->description,
                    "amount" => $model->amount,
                    "expiry_date" => ($model->expiry_date != '' ? date('Y-m-d H:i:s',strtotime($model->expiry_date)) : '' ) ,// 2017-06-14T06:18:14Z  ,
                    "product_ids" => $woo_product_ids ,
                    "individual_use" => ($model->individual_use == 1 ? true : false),
                    "usage_limit" => $model->usage_limit,
                    "usage_limit_per_user" => $model->usage_limit_per_user,
                    "limit_usage_to_x_items" => $model->limit_usage_to_x_items,
                    "enable_free_shipping" => ($model->free_shipping == 1 ? true : false),
                    "exclude_sale_items" => ($model->exclude_sale_items == 1 ? true : false),
                    "minimum_amount" => $model->minimum_amount,
                    "maximum_amount" => $model->maximum_amount,
                    "customer_emails" => explode(',', $model->email_restrictions),
                ];
                $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
                $response = $client->coupons->create($data);
                $model->woo_coupon_id= $response->coupon->id;
                $model->save(false);
                return $this->redirect(['view', 'id' => $model->id]);
            } elseif (!\Yii::$app->request->isPost) {
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }


    /**
     * Deletes an existing Coupon model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        try {
            $model = $this->findModel($id);
            $model->delete();

            $options = array(
                'debug' => true,
                'return_as_array' => false,
                'validate_url' => false,
                'timeout' => 90,
                'ssl_verify' => false,
            );
            $client = new WC_API_Client(getenv('SERVER_TYPE'), getenv('CK_KEY'), getenv('CS_KEY'), $options);
            $response = $client->coupons->delete($model->woo_coupon_id, ['force' => false]);


        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            \Yii::$app->getSession()->addFlash('error', $msg);
            return $this->redirect(Url::previous());
        }


        // TODO: improve detection
        $isPivot = strstr('$id', ',');
        if ($isPivot == true) {
            return $this->redirect(Url::previous());
        } elseif (isset(\Yii::$app->session['__crudReturnUrl']) && \Yii::$app->session['__crudReturnUrl'] != '/') {
            Url::remember(null);
            $url = \Yii::$app->session['__crudReturnUrl'];
            \Yii::$app->session['__crudReturnUrl'] = null;

            return $this->redirect($url);
        } else {
            return $this->redirect(['index']);
        }
    }



}
