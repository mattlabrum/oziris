<?php

namespace app\modules\admin\controllers;
use app\models\base\Supplier;
use app\models\search\SupplierSearch;
use app\utils\csvToArray;
use Yii;
use app\models\SupplierTranslation;
use cornernote\returnurl\ReturnUrl;
use app\models\form\SupplierForm;
use app\models\upload\UploadFile;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use Exception;

/**
 * This is the class for controller "app\controllers\SupplierController".
 */
class SupplierController extends \app\modules\admin\controllers\base\SupplierController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'duplicate', 'generate-csv', 'save-data'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * Creates a new Supplier model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $supplier = new Supplier();
        $supplier->scenario = 'create';
        return $this->saveSupplier($supplier);
    }

    /**
     * Updates a new Supplier model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $supplier = $this->findModel($id);
        $supplier->scenario = 'update';
        return $this->saveSupplier($supplier);
    }

    private function saveSupplier($supplier){
        $supplierTranslation = new SupplierTranslation();


        if (Yii::$app->request->post()) {
            $supplier_logo = $supplier->supplier_logo;
            $supplier->attributes = $_POST['Supplier'];

            if ((isset($_FILES['Supplier']['name']['supplier_logo']) && ($_FILES['Supplier']['name']['supplier_logo'] != ''))) {
                $supplier_logo_name = tempnam( 'uploads/supplier' , 'supplier-' );
                $supplier_logo_name = substr($supplier_logo_name,(strrpos($supplier_logo_name,'/')+1),strlen($supplier_logo_name));
                if($supplier_logo_name != 'default.jpg') {
                    unlink('uploads/supplier/' . $supplier_logo_name);
                }


                $image_upload_status = $this->uploadFile($supplier,$supplier_logo_name);

                if(isset($image_upload_status['supplier_logo_status'])) {
                    if (!$image_upload_status['supplier_logo_status']) {
                        if((isset($_FILES['Supplier']['name']['supplier_logo']) && ($_FILES['Supplier']['name']['supplier_logo'] != ''))){
                            Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Supplier Image Upload unsuccessful!'));
                        }
                    }else{

                        $supplier->supplier_logo = $supplier_logo_name.'.'.$image_upload_status['supplier_logo_extension'];
                    }
                }
            }else{
                $supplier->supplier_logo = $supplier_logo;
            }


            if(($_FILES['Supplier']['name']['supplier_logo'] == '') && $supplier->scenario == 'create'){
                $supplier->supplier_logo = 'default.jpg';
            }

            if($supplier->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Supplier has been created.'));
                if ($_POST['SupplierTranslation']['language_id'] != '') {
                    $supplierTranslation = SupplierTranslation::find()->where(['supplier_id' => $supplier->id])->andWhere(['language_id' => $_POST['SupplierTranslation']['language_id']])->one();

                    if (isset($supplierTranslation)) {
                        $supplierTranslation->attributes = $_POST['SupplierTranslation'];
                        $supplierTranslation->supplier_id = $supplier->id;
                    } else {
                        $supplierTranslation = new SupplierTranslation();
                        $supplierTranslation->attributes = $_POST['SupplierTranslation'];
                        $supplierTranslation->supplier_id = $supplier->id;
                    }


                    if ($supplierTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Supplier has been updated and translated.'));
                    }
                }
                return $this->redirect(['view', 'id' => $supplier->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }

        $supplierModel = new SupplierForm();
        $supplierModel->setSupplier($supplier);
        $supplierModel->setSupplierTranslation($supplierTranslation);

        return $this->render($supplier->scenario, ['model' => $supplierModel]);

    }

    private function uploadFile($mainModel, $supplier_logo_name)
    {
        $supplier_logo = new UploadFile();
        $supplier_logo->upload_file = UploadedFile::getInstance($mainModel, 'supplier_logo');
        try {
            $image_upload_status = [
                'supplier_logo_status' => $supplier_logo->upload('supplier',$supplier_logo_name),
                'supplier_logo_extension' => $supplier_logo->upload_file->extension,
            ];
            return $image_upload_status;
        } catch (Exception $e) {

        }
    }


    /**
     * Displays a single Supplier model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = SupplierTranslation::find()->where(['supplier_id' => $id]);
        $supplierModel = new SupplierForm();//$this->findModel($id);
        $supplierModel->setSupplier($model);
        $supplierModel->setSupplierTranslation($modelTranslation);
        return $this->render('view', ['model' => $supplierModel]);
    }

    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = SupplierTranslation::findOne(['id' => $id]);
        if(!isset($modelTranslation)){
            $modelTranslation = new SupplierTranslation();
        }
        $supplierModel = new SupplierForm();
        $supplierModel->scenario = 'duplicate';

        if (Yii::$app->request->post()) {
            $model = new Supplier();
            $model->attributes = $_POST['Supplier'];
            $model->supplier_logo = ((!isset($_FILES['Supplier']['name']['supplier_logo'])|| ($_FILES['Supplier']['name']['supplier_logo'] == '')) ? 'default.jpg' : $_FILES['Supplier']['name']['supplier_logo']);

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Supplier has been '.$supplierModel->scenario.'d .'));
                if(Yii::$app->request->post()['SupplierTranslation']['language_id']!=''){
                    $modelTranslation->attributes = $_POST['SupplierTranslation'];
                    $modelTranslation->supplier_id= $model->id;
                    if($modelTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Supplier  has been '.$supplierModel->scenario.'d and translated.'));
                    }
                }

                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }

        }
        $supplierModel->setSupplier($model);
        $supplierModel->setSupplierTranslation($modelTranslation);

        return $this->render('update', ['model' => $supplierModel]);
    }

    public function actionSaveData()
    {

        $suppliersToSave = [];
        $errorString = 'Suppliers Were not Imported! Please check the CSV';
        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);
        foreach ($csvArray as $supplier) {

            if (isset($supplier['ID']) && $supplier['ID'] > 0 && isset(Supplier::findOne($supplier['ID'])->id)) {
                $supplierToSave = Supplier::findOne($supplier['ID']);
                $this->populateSupplierData($supplier,$supplierToSave);
            } else {
                $supplierToSave = new Supplier();
                $this->populateSupplierData($supplier,$supplierToSave);
            }
            array_push($suppliersToSave, $supplierToSave);
        }


        if (count($suppliersToSave) == count($csvArray)) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($suppliersToSave as $supplierToSave) {
                    if(!$supplierToSave->save()){
                        throw new Exception('Unable to save record.');
                    }
                }
                $transaction->commit();
                $errorString = count($suppliersToSave) . " Suppliers Imported Successfully!";
                return $errorString;
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
                $transaction->rollback();
                return $this->redirect('index');
            }
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
            return $this->redirect('index');
        }
    }

    function populateSupplierData($supplier,$supplierToSave){
        $supplierToSave->supplier_name= $supplier['SupplierName'];
        $supplierToSave->supplier_logo=$supplier['SupplierLogo'];
        $supplierToSave->supplier_description=$supplier['SupplierDescription'];
        $supplierToSave->supplier_location_latitude=$supplier['SupplierLocationLatitude'];
        $supplierToSave->supplier_location_longitude=$supplier['SupplierLocationLongitude'];
        return $supplierToSave;
    }

    /**
     * Searches all products according to search criteria and generates CSV.
     * @return CSV Download link
     */
    public function actionGenerateCsv()
    {
        $searchModel = new SupplierSearch();

        $searchModel->supplier_name = (isset($_POST['supplier_name']) ? $_POST['supplier_name'] : '');
        $searchModel->supplier_logo = (isset($_POST['supplier_logo']) ? $_POST['supplier_logo'] : '');
        $searchModel->supplier_description = (isset($_POST['supplier_description']) ? $_POST['supplier_description'] : '');
        $searchModel->supplier_location_latitude = (isset($_POST['supplier_location_latitude']) ? $_POST['supplier_location_latitude'] : '');
        $searchModel->supplier_location_longitude = (isset($_POST['supplier_location_longitude']) ? $_POST['supplier_location_longitude'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);



        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            $arr = [];

            $arr['id'] = '"'.$object->id.'"';
            $arr['supplier_name'] = '"'.$object->supplier_name.'"';
            $arr['supplier_description'] = '"'.$object->supplier_description.'"';
            $arr['supplier_location_latitude'] = '"'.$object->supplier_location_latitude.'"';
            $arr['supplier_location_longitude'] = '"'.$object->supplier_location_longitude.'"';
            $arr['supplier_logo'] = '"'.$object->supplier_logo.'"';

            array_push($toCSVArray, $arr);
        }

        $fh = fopen('csv/suppliers.csv', 'w') or die('Cannot open the file');
        $strHeader = 'ID,SupplierName,SupplierDescription,SupplierLocationLatitude,SupplierLocationLongitude,SupplierLogo';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);

        return $writeSuccessful;

    }

}
