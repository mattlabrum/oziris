<?php

namespace app\modules\admin\controllers;
use app\models\Address;
use app\models\AppUser;
use app\models\base\AppUserDeviceInfo;
use app\models\Order;
use app\models\base\ProductLocationAttribute;
use app\models\Language;
use app\models\OrderItem;
use app\models\OrderStatus;
use app\models\OrderStatusTime;
use app\models\search\OrderSearch;
use app\models\search\ProductSearch;
use app\utils\BestonSMSSender;
use app\utils\csvToArray;
use app\utils\FetchContinentFromCountryCode;
use app\utils\PushNotificationSender;
use DateTime;
use dmstr\bootstrap\Tabs;
use dmstr\db\tests\unit\Product;
use kartik\mpdf\Pdf;
use Yii;
use yii\filters\AccessControl;
use yii\httpclient\Client;

/**
 * This is the class for controller "app\controllers\OrderController".
 */
class OrderController extends \app\modules\admin\controllers\base\OrderController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'report', 'stock-analysis', 'ecommerce-reports','update'],
                        'roles' => ['accounts', 'guest']
                    ],
                    [
                        'allow' => false,
                        'actions' => ['update'],
                        'roles' => ['restricted-order-access']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'report' , 'create', 'update', 'delete', 'create-order-item' , 'delete-order-item' , 'generate-manifest', 'stock-transfer' , 'generate-order-csv', 'generate-stock-csv' ,  'add-stock-transfer-item',  'generate-order-item-csv', 'print-label', 'stock-analysis', 'load-stock-grid', 'ecommerce-reports', 'print-orders'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        Tabs::clearLocalStorage();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Finds the Order model based on woo_order_id.
     * @param integer $woo_order_id
     * @return Order the loaded model
     */
    protected function findOrder($woo_order_id)
    {
        $model = Order::find()->where(['woo_order_id' => $woo_order_id])->one();
        return $model;
    }

    public function actionEcommerceReports() {
        return $this->redirect('../reports/ecommerce');
    }

    public function actionStockTransfer() {

        $shipping_address = new Address();
        $shipping_address->type = 'Shipping';
        $shipping_address->save(false);

        $billing_address = new Address();
        $billing_address->type = 'Billing';
        $billing_address->save(false);

        $order = new Order();
        $order->shipping_address_id = $shipping_address->id;
        $order->billing_address_id = $billing_address->id;
        $order->customer_id = 1;
        $order->status_id = 1;
        $order->trading_channel = 'B2B';
        $order->save(false);





        return $this->render('stock-transfer');
    }

    public function actionStockAnalysis() {
        $searchModel = new ProductSearch;

        $dataProvider = $searchModel->stockSearch(Yii::$app->request->get());
//        $dataProvider->query->andWhere(['is_collection' => 0 ]);
        $dataProvider->setPagination([
                'defaultPageSize' => 50,
                'pageSizeLimit'=>50]
        );
        return $this->render('stock', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);

//        return $this->render('stock');
    }

    public function actionCreateOrderItem(){

        $model = new OrderItem();
        $model->scenario = 'create';
        $model->attributes = $_POST;
        if($model->save()){
            Yii::$app->session->setFlash('success','Order Item Added Successfully', true);
            $this->redirect(['update', 'id'=>$_POST['model_id']]);
        }

    }

    public function actionDeleteOrderItem($data,$id){
        OrderItem::find()->where(['id'=>$data])->one()->delete();
        Yii::$app->session->setFlash('success','Order Item Deleted Successfully', true);
        $this->redirect(['update', 'id'=>$id]);
    }

    public function actionLoadStockGrid(){
        $searchModel = new ProductSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['is_collection' => 0 ]);
        $dataProvider->setPagination([
                'defaultPageSize' => 50,
                'pageSizeLimit'=>50]
        );
        return $this->renderAjax('stock-grid', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    public function actionReport($id) {

        $order = Order::find()->where(['woo_order_id' => $id])->one();

        $content = $this->renderPartial('_reportView',['order' => $order]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'options' => ['title' => 'Hamper '.$id. ' Item Report'],
            'methods' => [
                'SetHeader'=>['Hamper '.$id. ' Item Report'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        $pdf->options = [
            'autoScriptToLang' => true,
            'ignore_invalid_utf8' => true,
            'tabSpaces' => 4,
            'autoLangToFont' => true,
        ];

        // return the pdf output as per the destination setting
        return $pdf->render();
    }


    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        $model->updated_at = time();

        if(Yii::$app->request->isPost){
//            if($model->woo_order_id < 10000000){
//                if($_POST['Order']['status_id'] == 8){
//                    if($_POST['Order']['status_id'] != $model->status_id & $model->customer->email_address != 'default' ){
//                        $devices_ios = AppUserDeviceInfo::find()->where(['user_id' => $model->customer_id, 'device_type' => 'iOS' ])->all();
//                        if( count($devices_ios) > 0 ){
//                            $notification = "Order Status has been changed to : ".OrderStatus::find()->where(['id' => $_POST['Order']['status_id']])->one()->description;
//                            PushNotificationSender::sendIOSNotification($devices_ios,$notification,$model->woo_order_id);
//                        }
//                        $devices_android = AppUserDeviceInfo::find()->where(['user_id' => $model->customer_id, 'device_type' => 'android' ])->all();
//                        if( count($devices_android) > 0 ){
//                            $notification = "Order ID ".$model->woo_order_id." has been changed to : ".OrderStatus::find()->where(['id' => $_POST['Order']['status_id']])->one()->description;
//                            PushNotificationSender::sendAndroidNotification($devices_android,$notification);
//                        }
//                        $sms_notification = "Order Status for Order # ".$model->woo_order_id." has been changed to : ".OrderStatus::find()->where(['id' => $_POST['Order']['status_id']])->one()->description;
//                        BestonSMSSender::sendSMSNotification($model->getBillingAddress()->one()->getContactNo(),$sms_notification) ;
//                    }
//                }
//            }


            if ($model->load(Yii::$app->request->post()) && $model->save()) {

                $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $model->woo_order_id, 'status_id' => $model->status_id])->one();
                if(isset($orderStatusTime)){
                    $orderStatusTime->time = $model->updated_at;
                    $orderStatusTime->save();
                }

                if($model->woo_order_id < 10000000){
                    $client = new Client();
                    $response = $client->createRequest()
                        ->setMethod('post')
                        ->setUrl(getenv('SERVER_TYPE').'/update_order_details')
                        ->setData(
                            [
                                'order_id' => $model->woo_order_id,
                                'order_note' => $model->order_note,
                                'order_status' => $model->status->name,
                            ])->send();


                }
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Order has been updated.'));

                return $this->redirect('index');
            }
        }
        return $this->render('update', ['model' =>$model]);
    }

    /**
     * Searches all Orders according to search criteria and generates Manifest CSV.
     * @return CSV Download link
     */
    public function actionGenerateManifest()
    {
        $searchModel = new OrderSearch();

        $searchModel->contact_no = (isset($_POST['contact_no']) ? $_POST['contact_no'] : '');
        $searchModel->created_at = (isset($_POST['created_at']) ? $_POST['created_at'] : '');
        $searchModel->customer_id = (isset($_POST['customer_id']) ? $_POST['customer_id'] : '');
        $searchModel->order_note = (isset($_POST['order_note']) ? $_POST['order_note'] : '');
        $searchModel->status_id = (isset($_POST['status_id']) ? $_POST['status_id'] : '');
        $searchModel->total_amount = (isset($_POST['total_amount']) ? $_POST['total_amount'] : '');
        $searchModel->updated_at = (isset($_POST['updated_at']) ? $_POST['updated_at'] : '');
        $searchModel->woo_order_id = (isset($_POST['woo_order_id']) ? $_POST['woo_order_id'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);



        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            $arr = [];

            $arr['Name'] = '"'.$object->getShippingAddress()->one()->first_name.' '.$object->getShippingAddress()->one()->last_name.'"';
            $arr['Address'] = '"'.$object->getShippingAddress()->one()->address_1.' '.$object->getShippingAddress()->one()->address_2.'"';
            $arr['Suburb'] = '"'.$object->getShippingAddress()->one()->suburb.'"';
            $arr['State'] = '"'.$object->getShippingAddress()->one()->state.'"';
            $arr['Postcode'] = '"'.$object->getShippingAddress()->one()->postcode.'"';
            $arr['Phone'] = '"'.$object->getContactNo().'"';
            $arr['Boxes'] = '"'.$object->boxes.'"';
            $arr['Time'] = '';
            $arr['Comments'] = '"'.$object->order_note.'"';
            $arr['Barcode'] = '"'.$object->barcode.'"';


            array_push($toCSVArray, $arr);
        }

        $fh = fopen('csv/manifest.csv', 'w') or die('Cannot open the file');
        $strHeader = 'Name,Address,Suburb,State,Postcode,Phone,Boxes,Time,Comments,Barcode';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);

        return $writeSuccessful;

    }

    public function actionGenerateOrderCsv()
    {
        $searchModel = new OrderSearch();

        $searchModel->contact_no = (isset($_POST['contact_no']) ? $_POST['contact_no'] : '');
        $searchModel->created_at = (isset($_POST['created_at']) ? $_POST['created_at'] : '');
        $searchModel->customer_id = (isset($_POST['customer_id']) ? $_POST['customer_id'] : '');
        $searchModel->order_note = (isset($_POST['order_note']) ? $_POST['order_note'] : '');
        $searchModel->status_id = (isset($_POST['status_id']) ? $_POST['status_id'] : '');
        $searchModel->total_amount = (isset($_POST['total_amount']) ? $_POST['total_amount'] : '');
        $searchModel->updated_at = (isset($_POST['updated_at']) ? $_POST['updated_at'] : '');
        $searchModel->woo_order_id = (isset($_POST['woo_order_id']) ? $_POST['woo_order_id'] : '');
        $searchModel->location_id = (isset($_POST['location_id']) ? $_POST['location_id'] : '');


        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);

        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            $arr = [];
            $arr['OrderNumber'] = $object->woo_order_id;
            $arr['Name'] = $object->getShippingAddress()->one()->first_name.' '.$object->getShippingAddress()->one()->last_name;
            $arr['OrderDate'] = date('d-M-y h:i a', $object->created_at);
            $arr['Status'] = $object->status->description;
            $arr['OrderNote'] = $object->order_note;
            $arr['DeliveryDate'] =  ($object->updated_at!=null ? date('d-M-y h:i a', $object->updated_at) : '' ) ;
            $arr['Address'] = $object->getShippingAddress()->one()->address_1.' '.$object->getShippingAddress()->one()->address_2;
            $arr['Suburb'] = $object->getShippingAddress()->one()->suburb;
            $arr['State'] = $object->getShippingAddress()->one()->state;
            $arr['Postcode'] = $object->getShippingAddress()->one()->postcode;
            $arr['Phone'] = $object->getContactNo();
            $arr['Boxes'] = $object->boxes;
            $arr['Channel'] = $object->trading_channel;
            $arr['Barcode'] = $object->barcode;
            $arr['OrderPrice'] = $object->total_amount;
            $arr['ShippingCost'] = $object->shipping_amount;
//            $customer = AppUser::find()->where(['id' => $object->customer_id])->one();
            if(isset($object->customer)){
                $arr['EmailAddress'] = ($object->customer->email_address == 'default' ? $object->getBillingAddress()->one()->email : $object->customer->email_address);
            }else{
                $arr['EmailAddress'] = '';
            }
            $arr['RequestedDeliveryDate'] = $object->subscription_delivery_date;


            array_push($toCSVArray, $arr);
        }
        $fh = fopen('csv/order.csv', 'w') or die('Cannot open the file');
        $strHeader = 'OrderNumber,Name,OrderDate,Status,OrderNote,DeliveryDate,Address,Suburb,State,Postcode,Phone,Boxes,Channel,Barcode,OrderPrice,ShippingCost,EmailAddress,RequestedDeliveryDate';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );


        $str = '';
        for ($i = 0; $i < count($toCSVArray); $i++) {
            $str .= csvToArray::str_putcsv($toCSVArray[$i]);
            $str .= "\n";
        }
        $writeSuccessfull = fwrite($fh, $str);
        fclose($fh);
        return $writeSuccessfull;
    }


    public function actionGenerateStockCsv()
    {
        $toCSVArray= [];

        $id = (isset($_POST['id']) ? $_POST['id'] : '');
        $location_id = ($_POST['location_id']!='' ? $_POST['location_id'] : '2');
        $product_name = ($_POST['product_name']!='' ? $_POST['product_name'] : '');
        $product_number = ($_POST['product_number']!='' ? $_POST['product_number'] : '');

//        return json_encode(['id' => $id, 'location_id' => $location_id, 'product_name' => $product_name, 'product_number' =>$product_number]);

        $location = Language::find()->where(['id' => $location_id])->one();

        $products = ProductLocationAttribute::find()
                        ->joinWith('product')
                        ->where(['language_id' => $location_id])
                        ->andFilterWhere(['like', 'product.product_name', $product_name])
                        ->andFilterWhere(['like', 'product.product_number', $product_number])
                        ->andFilterWhere(['product.id' => $id])
                        ->orderBy(['id' => SORT_ASC])
                        ->all();
        foreach($products as $product){

            $arr = [];
            $arr['ID'] = '"'.$product->product->id.'"';
            $arr['ProductNumber'] = '"'.$product->product->product_number.'"';
            $arr['ProductName'] = '"'.$product->product->product_name.'"';
            $arr['Location'] = '"'. isset($location) ? $location->name : '' .'"';
            $arr['NoOfBatches'] = '"'.(count($product->product->getBatches()->all()) - 1).'"';

            $arr['BatchLevelQuantity'] = '"'.$product->product->getProductBatchLevelQuantity().'"';
            $arr['ActualQuantity'] = '"'.$product->product->getProductQuantity().'"';
            $arr['TheoQuantity'] = '"'.$product->product->getTheoraticalQuantity().'"';
            $arr['SoldQuantity'] = '"'.$product->product->getProductSoldQuantity().'"';
            $arr['DiscardedQuantity'] = '"'.$product->product->getProductDiscardQuantity().'"';
            $arr['Variance'] = '"'.$product->product->getProductVariance().'"';
            array_push($toCSVArray, $arr);
        }


        $fh = fopen('csv/stock.csv', 'w') or die('Cannot open the file');
        $strHeader = 'ID,ProductNumber,ProductName,Location,NoOfBatches,BatchLevelQuantity,ActualQuantity,TheoQuantity,SoldQuantity,DiscardedQuantity,Variance';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);
        return true;
    }

    public function actionGenerateOrderItemCsv()
    {
//        ini_set('max_execution_time', 180);
        ini_set('memory_limit','512M');
        $searchModel = new OrderSearch();

        $searchModel->contact_no = (isset($_POST['contact_no']) ? $_POST['contact_no'] : '');
        $searchModel->created_at = (isset($_POST['created_at']) ? $_POST['created_at'] : '');
        $searchModel->customer_id = (isset($_POST['customer_id']) ? $_POST['customer_id'] : '');
        $searchModel->order_note = (isset($_POST['order_note']) ? $_POST['order_note'] : '');
        $searchModel->status_id = (isset($_POST['status_id']) ? $_POST['status_id'] : '');
        $searchModel->total_amount = (isset($_POST['total_amount']) ? $_POST['total_amount'] : '');
        $searchModel->updated_at = (isset($_POST['updated_at']) ? $_POST['updated_at'] : '');
        $searchModel->woo_order_id = (isset($_POST['woo_order_id']) ? $_POST['woo_order_id'] : '');
        $searchModel->location_id = (isset($_POST['location_id']) ? $_POST['location_id'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);
        $toCSVArray = [];

        foreach($dataProvider->getModels() as $object){
            foreach($object->getOrderItems() as $item){
                $arr = [];
                $arr['OrderID'] = '"'.$object->woo_order_id.'"';
                $arr['OrderDate'] = '"'.date('d-M-y h:i a', $object->created_at).'"';
                $arr['TradingChannel'] = '"'.$object->trading_channel.'"';
                $arr['ShippingDate'] = '"'. ($object->updated_at!=null ? date('d-M-y h:i a', $object->updated_at) : '' ) .'"';
                $arr['ProductBatchID'] = '"'.$item->getBatchID().'"';
                $arr['ProductOzirisID'] = '"'.$item->product->id.'"';
                $arr['ProductName'] = '"'.$item->product->product_name.'"';
                $arr['ProductNumber'] = '"'.$item->product->product_number.'"';
                $arr['ProductBarcode'] = '"'.$item->product->barcode.'"';
                $arr['ProductNature'] = '"'.$item->product->productNature->name.'"';
                $arr['ProductGroup'] = '"'.$item->product->productGroup->product_group_name.'"';
                $arr['ProductType'] = '"'.$item->product->productType->type_name.'"';
                $arr['ProductQuantity'] = '"'.$item->quantity.'"';
                $arr['UnitSize'] = '"'.$item->product->unit_size.'"';
                $arr['Brand'] = '"'.$item->product->brand->brand_name.'"';
                $arr['Manufacturer'] = '"'.$item->product->manufacturer->manufacturer_name.'"';

                $arr['ActualProductPrice'] = '"'.$item->actual_price.'"';
                $arr['ActualProductGST'] = '"'.$item->actual_tax.'"';
                $arr['PaidProductPrice'] = '"'.$item->price.'"';
                $arr['PaidProductGST'] = '"'.$item->tax.'"';
                $arr['DiscountAmount'] = '"'.(($item->actual_price+$item->actual_tax)-($item->price+$item->tax)).'"';
                $arr['Coupons'] = '"'.$object->getOrderCoupons().'"';

                array_push($toCSVArray, $arr);

                if($item->product->is_collection){
                    foreach($item->product->getCollectionItemsForReport() as $coll_item){
                        $arr['OrderID'] = '"' . $object->woo_order_id . '"';
                        $arr['OrderDate'] = '"' . date('d-M-y h:i a', $object->created_at) . '"';
                        $arr['TradingChannel'] = '"' . $object->trading_channel . '"';
                        $arr['ShippingDate'] = '"' . ($object->updated_at != null ? date('d-M-y h:i a', $object->updated_at) : '') . '"';
                        $arr['ProductBatchID'] = '"'.$coll_item->getBatchID($object->woo_order_id).'"';
                        $arr['ProductOzirisID'] = '"' . $coll_item->product->id . '"';
                        $arr['ProductName'] = '" >>>>>>>>>>>' . $coll_item->product->product_name . '"';
                        $arr['ProductNumber'] = '"' . $coll_item->product->product_number . '"';
                        $arr['ProductBarcode'] = '"' . $coll_item->product->barcode . '"';
                        $arr['ProductNature'] = '"' . $coll_item->product->productNature->name . '"';
                        $arr['ProductGroup'] = '"' . $coll_item->product->productGroup->product_group_name . '"';
                        $arr['ProductType'] = '"' . $coll_item->product->productType->type_name . '"';
                        $arr['ProductQuantity'] = '"' . $coll_item->quantity . '"';
                        $arr['UnitSize'] = '"'.$coll_item->product->unit_size.'"';
                        $arr['Brand'] = '"' . $coll_item->product->brand->brand_name . '"';
                        $arr['Manufacturer'] = '"' . $coll_item->product->manufacturer->manufacturer_name . '"';
                        $arr['ActualProductPrice'] = '" * "';
                        $arr['ActualProductGST'] = '" * "';
                        $arr['PaidProductPrice'] = '" * "';
                        $arr['PaidProductGST'] = '" * "';
                        $arr['DiscountAmount'] = '" * "';
                        $arr['Coupons'] = '"'.$object->getOrderCoupons().'"';
                        array_push($toCSVArray, $arr);
                    }
                }

            }
        }

        $fh = fopen('csv/order-item.csv', 'w') or die('Cannot open the file');
        $strHeader = 'OrderID,OrderDate,TradingChannel,ShippingDate,ProductBatchID,ProductOzirisID,ProductName,ProductNumber,ProductBarcode,ProductNature,ProductGroup,ProductType,ProductQuantity,UnitSize,Brand,Manufacture,ActualProductPrice,ActualProductGST,PaidProductPrice,PaidProductGST,DiscountAmount,Coupons';
        fwrite( $fh, $strHeader);
        fwrite( $fh, "\n" );
        $writeSuccessful = false;
        for( $i=0; $i<count($toCSVArray); $i++ ){
            $str = implode( ',', $toCSVArray[$i] );
            $writeSuccessful = fwrite( $fh, $str );
            $writeSuccessful = fwrite( $fh, "\n" );
        }
        fclose($fh);
        return $writeSuccessful;
    }

    public function actionPrintLabel() {

        $searchModel = new OrderSearch();

        $searchModel->contact_no = (isset($_POST['contact_no']) ? $_POST['contact_no'] : '');
        $searchModel->created_at = (isset($_POST['created_at']) ? $_POST['created_at'] : '');
        $searchModel->customer_id = (isset($_POST['customer_id']) ? $_POST['customer_id'] : '');
        $searchModel->order_note = (isset($_POST['order_note']) ? $_POST['order_note'] : '');
        $searchModel->status_id = (isset($_POST['status_id']) ? $_POST['status_id'] : '');
        $searchModel->total_amount = (isset($_POST['total_amount']) ? $_POST['total_amount'] : '');
        $searchModel->updated_at = (isset($_POST['updated_at']) ? $_POST['updated_at'] : '');
        $searchModel->woo_order_id = (isset($_POST['woo_order_id']) ? $_POST['woo_order_id'] : '');
        $searchModel->subscription_delivery_date = (isset($_POST['subscription_delivery_date']) ? $_POST['subscription_delivery_date'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);

        $content = $this->renderPartial('_labelView',['orders' => $dataProvider->getModels()]);
        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => [150,100],
            'marginLeft' => 2.00,
            'marginRight' => 2.00,
            'marginTop' => 2.00,
            'marginBottom' => 2.00,
            'orientation' => Pdf::ORIENT_LANDSCAPE,
            'destination' => Pdf::DEST_FILE,
            'filename' => 'label.pdf',
            'content' => $content,
        ]);

        $pdf->options = [
            'autoScriptToLang' => true,
            'ignore_invalid_utf8' => true,
            'tabSpaces' => 4,
            'autoLangToFont' => true,
        ];

        return $pdf->render();
    }

    public function actionPrintOrders() {

        $searchModel = new OrderSearch();

        $searchModel->contact_no = (isset($_POST['contact_no']) ? $_POST['contact_no'] : '');
        $searchModel->created_at = (isset($_POST['created_at']) ? $_POST['created_at'] : '');
        $searchModel->customer_id = (isset($_POST['customer_id']) ? $_POST['customer_id'] : '');
        $searchModel->order_note = (isset($_POST['order_note']) ? $_POST['order_note'] : '');
        $searchModel->status_id = (isset($_POST['status_id']) ? $_POST['status_id'] : '');
        $searchModel->total_amount = (isset($_POST['total_amount']) ? $_POST['total_amount'] : '');
        $searchModel->updated_at = (isset($_POST['updated_at']) ? $_POST['updated_at'] : '');
        $searchModel->woo_order_id = (isset($_POST['woo_order_id']) ? $_POST['woo_order_id'] : '');
        $searchModel->subscription_delivery_date = (isset($_POST['subscription_delivery_date']) ? $_POST['subscription_delivery_date'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);

        $content = $this->renderPartial('_printOrder',['orders' => $dataProvider->getModels()]);

//        echo $content;

        $pdf = new Pdf([
            'mode' => Pdf::MODE_UTF8,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_FILE,
            'filename' => 'orders.pdf',
            'content' => $content,
        ]);

        $pdf->options = [
            'autoScriptToLang' => true,
            'ignore_invalid_utf8' => true,
            'tabSpaces' => 4,
            'autoLangToFont' => true,
        ];

        return $pdf->render();
    }

    /**
     * Creates a new B2B Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        try {
            $model = new Order;
            $model->shippingAddress = new Address();
            $model->billingAddress = new Address();

            if (\Yii::$app->request->isPost){

                $model->status_id = 1;
                $model->shipping_id = 1;
                $model->currency_id = 1;
                $model->total_amount = 0.0;
                $model->subtotal = 0.0;
                $model->payment_id = null;
                $model->tax_amount = 0.0;

                $user = AppUser::find()->where(['email_address' => $model->billingAddress->email])->one();
                if (isset($user)) {
                    $model->customer_id = $user->id;
                } else {
                    $user = AppUser::find()->where(['email_address' => 'default'])->one();
                    $model->customer_id = $user->id;
                }
                if ($model->load($_POST)) {
                    $model->shippingAddress->address_1 = $_POST['Address']['address_1'];
                    $model->shippingAddress->type = 'shipping';
                    $model->shippingAddress->suburb = $_POST['Address']['suburb'];
                    $model->shippingAddress->state = $_POST['Address']['state'];
                    $model->shippingAddress->postcode = $_POST['Address']['postcode'];
                    $model->shippingAddress->country = 'AU';
                    $model->shippingAddress->continent = FetchContinentFromCountryCode::fetchContinentFromCountryCode($model->shippingAddress->country);
                    $model->shippingAddress->first_name = $_POST['first_name'];
                    $model->shippingAddress->last_name = $_POST['last_name'];
                    $model->shippingAddress->save(false);

                    $model->billingAddress = new Address();
                    $model->billingAddress->address_1 = $_POST['address_1'];
                    $model->billingAddress->type = 'billing';
                    $model->billingAddress->first_name = $_POST['first_name'];
                    $model->billingAddress->contact_no = $_POST['contact_no'];
                    $model->billingAddress->email = $_POST['email'];
                    $model->billingAddress->last_name = $_POST['last_name'];
                    $model->billingAddress->suburb = $_POST['suburb'];;
                    $model->billingAddress->state = $_POST['state'];;
                    $model->billingAddress->postcode = $_POST['postcode'];;
                    $model->billingAddress->country = 'CN';
                    $model->billingAddress->save(false);

                    $model->billing_address_id = $model->billingAddress->id;
                    $model->shipping_address_id = $model->shippingAddress->id;

                $model->tax_id = 1;
                $model->trading_channel = 'B2B';

                $model->save(false);
                $model->woo_order_id = $model->id;
                $model->generateQrCode();

                $allStatuses = OrderStatus::find()->all();
                foreach ($allStatuses as $status) {
                    $statusTime = new OrderStatusTime();
                    $statusTime->order_id = $model->id;
                    $statusTime->status_id = $status->id;
                    $statusTime->sort = $status->sort;
                    $statusTime->save();
                }
                $order_date = new DateTime('now');
                $model->created_at = ($order_date->getTimestamp());
                $orderStatusTime = OrderStatusTime::find()->where(['order_id' => $model->woo_order_id, 'status_id' => $model->status_id])->one();
                $orderStatusTime->time = $model->created_at;
                $orderStatusTime->save();
            }

               if($model->save()){
                   return $this->redirect(['update', 'id' => $model->id]);
               }
            } else{
                $model->load($_GET);
            }
        } catch (\Exception $e) {
            $msg = (isset($e->errorInfo[2]))?$e->errorInfo[2]:$e->getMessage();
            $model->addError('_exception', $msg);
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionAddStockTransferItem() {
        $item = new OrderItem();
        $item->product_id = $_POST['product_id'];
        $item->quantity = $_POST['quantity'];
        $item->order_id = $_POST['temp_order_id'];
        $item->save();

        return json_encode([
            'order_item_id' => $item->product_id,
            'brand' =>  $item->product->brand->brand_name,
            'manufacturer' =>  $item->product->manufacturer->manufacturer_name,
            'product_name' =>  $item->product->product_name,
            'quantity' =>  $item->quantity,
        ]);
    }
}