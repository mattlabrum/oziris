<?php

namespace app\modules\admin\controllers;
use app\models\base\Product;
use app\models\form\EcommerceForm;
use app\models\Order;
use app\models\search\ProductSearch;
use app\models\search\TranslationSummarySearch;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * This is the class for controller "app\modules\admin\controllers\CollectionItemController".
 */
class ReportsController extends Controller
{
    public $layout = '@app/views/layouts/main';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index','ecommerce'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }


    /**
     * Renders the start page
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    /**
     * Renders the start page
     * @return string
     */
    public function actionEcommerce()
    {
        $data = new EcommerceForm();

        $searchModel = new ProductSearch;

        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dataProvider->query->andWhere(['is_collection' => 0 ]);
        $dataProvider->setPagination([
            'defaultPageSize' => 50,
            'pageSizeLimit'=>50]
        );

//        $data->setSales($this->actionGetSalesData());

        $data->setProductDataProvider($dataProvider);
        $data->setProductSearchModel($searchModel);
        return $this->render('ecommerce/index', [
            'model' => $data,
        ]);
    }
}