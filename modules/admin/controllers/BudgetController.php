<?php

namespace app\modules\admin\controllers;

use app\models\Budget;
use app\utils\csvToArray;
use Exception;
use Yii;
use yii\filters\AccessControl;

/**
 * This is the class for controller "BudgetController".
 */
class BudgetController extends \app\modules\admin\controllers\base\BudgetController
{
    public $layout = '@app/views/layouts/main';


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'generate-sample-csv', 'import-budget'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function actionGenerateSampleCsv()
    {
            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'continent',
                'budget_amount' => '1000',
            ];

            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'country',
                'budget_amount' => '1000',
            ];
//            array_push($toCSVArray, $arr);

            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'state',
                'budget_amount' => '1000',
            ];
//            array_push($toCSVArray, $arr);

            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'trading-channel',
                'budget_amount' => '1000',
            ];
//            array_push($toCSVArray, $arr);

            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'product-type',
                'budget_amount' => '1000',
            ];
//            array_push($toCSVArray, $arr);

            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'product-group',
                'budget_amount' => '1000',
            ];
//            array_push($toCSVArray, $arr);

            $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'product-nature',
                'budget_amount' => '1000',
            ];

        $arr[] = [
                'month' => '03',
                'year' => '2017',
                'budget_type' => 'over-all-sales',
                'budget_amount' => '1000',
            ];

        $fh = fopen('csv/budget.csv', 'w') or die('Cannot open the file');
        $strHeader = 'Month,Year,BudgetType,BudgetAmount';
        fwrite($fh, $strHeader);
        fwrite($fh, "\n");
        $writeSuccessfull = false;
        for ($i = 0; $i < count($arr); $i++) {
            $str = implode(',', $arr[$i]);
            $writeSuccessfull = fwrite($fh, $str);
            $writeSuccessfull = fwrite($fh, "\n");
        }
        fclose($fh);

        return $writeSuccessfull;

    }


    public function actionImportBudget()
    {
        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($csvArray as $budget) {
                    $budget_item = Budget::find()->where(['month' => $budget['Month'], 'year' => $budget['Year'], 'budget_type' => $budget['BudgetType']])->one();

                        if (isset($budget_item)) {
                            $budget_item->budget_amount = $budget['BudgetAmount'];
                            if (!$budget_item->save(false)) {
                                throw new Exception('Unable to save record.');
                            }


                        } else {
                            $budget_item = new Budget();
                            $budget_item->budget_amount = $budget['BudgetAmount'];
                            $budget_item->budget_type = $budget['BudgetType'];
                            $budget_item->month = $budget['Month'];
                            $budget_item->year = $budget['Year'];
                            if (!$budget_item->save(false)) {
                                throw new Exception('Unable to save record.');
                            }
                        }

                    }

                        $transaction->commit();
                    } catch (Exception $e) {
                        Yii::$app->getSession()->setFlash('error', Yii::t('app', "All budget Entries we not saved please check csv again"));
                        $transaction->rollback();
                        return $this->redirect('index');
                    }

                    Yii::$app->getSession()->setFlash('success', Yii::t('app', "All budget Entries saved Successfully"));
                    return $this->redirect('index');
                }





}
