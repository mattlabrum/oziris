<?php

namespace app\modules\admin\controllers;
use app\models\form\NutritionForm;
use app\models\Nutrition;
use app\models\base\ProductNutrition;
use app\models\NutritionTranslation;
use cornernote\returnurl\ReturnUrl;
use Yii;
use yii\filters\AccessControl;

/**
 * This is the class for controller "app\controllers\NutritionController".
 */
class NutritionController extends \app\modules\admin\controllers\base\NutritionController
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'get-nutrition-for-product'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    public $nutritionTranslation;

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = NutritionTranslation::find()->where(['nutrition_id' => $id])->one();

        $nutritionModel = new NutritionForm();
        $nutritionModel->setNutrition($model);
        $nutritionModel->setNutritionTranslation($modelTranslation);
        return $this->render('view', ['model' => $nutritionModel]);
    }

    public function actionCreate()
    {
        $model = new Nutrition();
        $nutritionModel = new NutritionForm();
        $nutritionModel->scenario = 'create';
        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['Nutrition'];
            if($model->save()){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition has been '.$nutritionModel->scenario.'d .'));
                if(Yii::$app->request->post()['NutritionTranslation']['language_id']!=''){
                    $this->nutritionTranslation = new NutritionTranslation();
                    $this->nutritionTranslation->attributes = $_POST['NutritionTranslation'];
                    $this->nutritionTranslation->nutrition_id = $model->id;
                    if($this->nutritionTranslation->save(false)) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition has been '.$nutritionModel->scenario.'d and translated.'));
                    }
                }
                $nutritionModel->setNutrition($model);
                $nutritionModel->setNutritionTranslation($this->nutritionTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        return $this->render('create', ['model' => $nutritionModel]);
    }


    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nutritionModel = new NutritionForm();
        $nutritionModel->scenario = 'update';
        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['Nutrition'];
            if($model->save(true)){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition has been '.$nutritionModel->scenario.'d .'));

                if(Yii::$app->request->post()['NutritionTranslation']['language_id']!=''){
                    $this->nutritionTranslation = NutritionTranslation::find()->where(['nutrition_id' => $model->id, 'language_id' => $_POST['NutritionTranslation']['language_id'] ])->one();
                    if (isset($this->nutritionTranslation)) {
                        $this->nutritionTranslation->attributes = $_POST['NutritionTranslation'];
                        $this->nutritionTranslation->nutrition_id = $model->id;
                    } else {
                        $this->nutritionTranslation = new NutritionTranslation();
                        $this->nutritionTranslation->attributes = $_POST['NutritionTranslation'];
                        $this->nutritionTranslation->nutrition_id = $model->id;
                    }
                    if($this->nutritionTranslation->save(false)) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition  has been '.$nutritionModel->scenario.'d and translated.'));
                    }
                }
                $nutritionModel->setNutrition($model);
                $nutritionModel->setNutritionTranslation($this->nutritionTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        $nutritionModel->setNutrition($model);
        $nutritionModel->setNutritionTranslation($this->nutritionTranslation);
        return $this->render('update', ['model' => $nutritionModel]);
    }


    /**
     * Returns all existing ProductNutrition models.
     * @return ProductNutrition[]
     */
    public function actionGetNutritionForProduct()
    {
        $productNutritions = [];
        $nutritions  = Nutrition::find()->all();

//        print_r($nutritions);

        foreach ($nutritions as $nutrition){

            $productNutrition = ProductNutrition::find()->where(['product_id' => $_POST['product_id'],'nutrition_id' => $nutrition->id ])->one();

            if(isset($productNutrition)){
                array_push($productNutritions,$productNutrition);
            }else{
                $productNutrition = new ProductNutrition;
                $productNutrition->scenario = 'create';
                $productNutrition->nutrition_id = $nutrition->id;
                $productNutrition->product_id = $_POST['product_id'];
                array_push($productNutritions,$productNutrition);
            }
        }
        if(count($productNutritions)){
            return $this->renderAjax('product-nutrition', ['model' => $productNutritions]);
        }
    }


}
