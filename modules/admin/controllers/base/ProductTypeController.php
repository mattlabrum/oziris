<?php

namespace app\modules\admin\controllers\base;

use app\models\base\ProductTypeTranslation;
use app\models\form\ProductTypeForm;
use app\models\ProductType;
use app\models\search\ProductTypeSearch;
use app\models\upload\UploadFile;
use Exception;
use yii\web\Controller;
use Yii;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;
use yii\web\UploadedFile;

/**
 * ProductTypeController implements the CRUD actions for ProductType model.
 */
class ProductTypeController extends Controller
{

    public $layout = '@app/views/layouts/main';
    public $productTypeTranslation;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete' , 'change-status'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all ProductType models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductTypeSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        Tabs::clearLocalStorage();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single ProductType model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = ProductTypeTranslation::findOne(['id' => $id]);
        $productTypeModel = new ProductTypeForm();
        $productTypeModel->setProductType($model);
        $productTypeModel->setProductTypeTranslation($modelTranslation);
        return $this->render('view', ['model' => $productTypeModel]);




//        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new ProductType model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

//        $model = $this->findModel($id);
        $productTypeModel = new ProductTypeForm();
        $productTypeModel->scenario = 'create';
//        $type_icon = $model->icon;
        if (Yii::$app->request->post()) {
            $productTypeModel->producttype->attributes = $_POST['ProductType'];
            if ((isset($_FILES['ProductType']['name']['icon']) && ($_FILES['ProductType']['name']['icon'] != ''))) {
                $type_icon_name = tempnam('uploads/type', 'type-');
                $type_icon_name = substr($type_icon_name, (strrpos($type_icon_name, '/') + 1), strlen($type_icon_name));
                if ($type_icon_name != 'default.jpg') {
                    unlink('uploads/type/' . $type_icon_name);
                }
                $image_upload_status = $this->uploadFile($productTypeModel->getProductType(), $type_icon_name);
                if (isset($image_upload_status['type_icon_status'])) {
                    if (!$image_upload_status['type_icon_status']) {
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Product Type Upload unsuccessful!'));
                    } else {
                        $productTypeModel->getProductType()->icon = $type_icon_name . '.' . $image_upload_status['type_icon_extension'];
                        }
                    }
                }
            }

            if($productTypeModel->getProductType()->save(true)){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type has been '.$productTypeModel->scenario.'d .'));
                if(Yii::$app->request->post()['ProductTypeTranslation']['language_id']!=''){
                    $this->productTypeTranslation = ProductTypeTranslation::find()->where(['product_type_id' => $productTypeModel->getProductType()->id, 'language_id' => $_POST['ProductTypeTranslation']['language_id'] ])->one();
                    if (isset($this->productTypeTranslation)) {
                        $this->productTypeTranslation->attributes = $_POST['ProductTypeTranslation'];
                        $this->productTypeTranslation->product_type_id = $productTypeModel->getProductType()->id;
                    } else {
                        $this->productTypeTranslation = new ProductTypeTranslation();
                        $this->productTypeTranslation->attributes = $_POST['ProductTypeTranslation'];
                        $this->productTypeTranslation->product_type_id = $productTypeModel->getProductType()->id;
                    }
                    if($this->productTypeTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type  has been '.$productTypeModel->scenario.'d and translated.'));
                    }
                }
                $productTypeModel->setProductType($productTypeModel->getProductType());
                $productTypeModel->setProductTypeTranslation($this->productTypeTranslation);
                return $this->redirect(['view', 'id' => $productTypeModel->getProductType()->id, 'ru' => ReturnUrl::getRequestToken()]);
            }

        $productTypeModel->setProductType($productTypeModel->getProductType());
        $productTypeModel->setProductTypeTranslation($this->productTypeTranslation);
        return $this->render('update', ['model' => $productTypeModel]);

//        $model = new ProductType;
//        $model->scenario = 'create';
//        if (Yii::$app->request->isPost) {
//            $model->attributes = $_POST['ProductType'];
//            $type_icon_name = tempnam('uploads/type', 'type-');
//            $type_icon_name = substr($type_icon_name, (strrpos($type_icon_name, '/') + 1), strlen($type_icon_name));
//            if($type_icon_name != 'default.jpg'){
//                unlink('uploads/type/' . $type_icon_name);
//            }
//            $image_upload_status = $this->uploadFile($model, $type_icon_name);
//            if (isset($image_upload_status['type_icon_status'])) {
//                if (!$image_upload_status['type_icon_status']) {
//                    Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Product Type Upload unsuccessful!'));
//                } else {
//                    $model->icon = $type_icon_name . '.' . $image_upload_status['type_icon_extension'];
//                }
//            }
//            if($model->save()){
//                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type has been created.'));
//                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
//            }
//        } elseif (!\Yii::$app->request->isPost) {
//            $model->load(Yii::$app->request->get());
//        }
//        return $this->render('create', compact('model'));
    }

    private function uploadFile($mainModel, $type_icon_name)
    {
        $type_icon = new UploadFile();
        $type_icon->upload_file = UploadedFile::getInstance($mainModel, 'icon');
        try {
            $image_upload_status = [
                'type_icon_status' => $type_icon->upload('type',$type_icon_name),
                'type_icon_extension' => $type_icon->upload_file->extension,
            ];
            return $image_upload_status;
        } catch (Exception $e) {
        }
    }

    /**
     * Updates an existing ProductType model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $productTypeModel = new ProductTypeForm();
        $productTypeModel->scenario = 'update';
        $type_icon = $model->icon;

        $this->productTypeTranslation = ProductTypeTranslation::find()->where(['product_type_id' => $model->id, 'language_id' => 1 ])->one();


        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['ProductType'];
            if ((isset($_FILES['ProductType']['name']['icon']) && ($_FILES['ProductType']['name']['icon'] != ''))) {
                $type_icon_name = tempnam('uploads/type', 'type-');
                $type_icon_name = substr($type_icon_name, (strrpos($type_icon_name, '/') + 1), strlen($type_icon_name));
                if ($type_icon_name != 'default.jpg') {
                    unlink('uploads/type/' . $type_icon_name);
                }
                $image_upload_status = $this->uploadFile($model, $type_icon_name);
                if (isset($image_upload_status['type_icon_status'])) {
                    if (!$image_upload_status['type_icon_status']) {
                        $model->icon = $type_icon;
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Product Type Upload unsuccessful!'));
                    } else {
                        $model->icon = $type_icon_name . '.' . $image_upload_status['type_icon_extension'];
                        try {
                            if ($type_icon != 'default.jpg') {
                                unlink('uploads/type/' . $type_icon);
                            }
                        } catch (Exception $e) {
                        }
                    }
                }
            } else {
                $model->icon = $type_icon;
            }

            if($model->save(true)){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type has been '.$productTypeModel->scenario.'d .'));
                if(Yii::$app->request->post()['ProductTypeTranslation']['language_id']!=''){
                    if (isset($this->productTypeTranslation)) {
                        $this->productTypeTranslation->attributes = $_POST['ProductTypeTranslation'];
                        $this->productTypeTranslation->product_type_id = $model->id;
                    } else {
                        $this->productTypeTranslation = new ProductTypeTranslation();
                        $this->productTypeTranslation->attributes = $_POST['ProductTypeTranslation'];
                        $this->productTypeTranslation->product_type_id = $model->id;
                    }

                    if($this->productTypeTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type  has been '.$productTypeModel->scenario.'d and translated.'));
                    }
                }
                $productTypeModel->setProductType($model);
                $productTypeModel->setProductTypeTranslation($this->productTypeTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        $productTypeModel->setProductType($model);
        $productTypeModel->setProductTypeTranslation($this->productTypeTranslation);
        return $this->render('update', ['model' => $productTypeModel]);


//        $model = $this->findModel($id);
//        $model->scenario = 'update';

//        if (Yii::$app->request->isPost) {
//            $model->attributes = $_POST['ProductType'];
//            if($model->save()){
//                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type has been updated.'));
//                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
//            }
//        } elseif (!\Yii::$app->request->isPost) {
//            $model->load(Yii::$app->request->get());
//        }
//        return $this->render('update', compact('model'));
    }


    /**
     * Deletes an existing ProductType model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type has been deleted.'));

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Finds the ProductType model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductType the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductType::findOne($id)) !== null) {
            return $model;
        }
        throw new HttpException(404, 'The requested page does not exist.');
    }

    /**
     * Enables/Disables an existing ProductType model.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeStatus($id)
    {
        if($this->findModel($id)->changeStatus()){
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Type Status Changed Successfully.'));
        }else{
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Product Type Status Change was Unsuccessful.'));
        }

        return $this->redirect('index');
    }

}
