<?php

namespace app\modules\admin\controllers\base;

use app\models\form\NutritionTagForm;
use app\models\NutritionTag;
use app\models\NutritionTagTranslation;
use app\models\search\NutritionTagsSearch;
use yii\web\Controller;
use Yii;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * NutritionTagController implements the CRUD actions for NutritionTag model.
 */
class NutritionTagController extends Controller
{

    public $layout = '@app/views/layouts/main';
    public $nutritionTagTranslation;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all NutritionTag models.
     * @return mixed
     */



    public function actionIndex()
    {
        $searchModel = new NutritionTagsSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());

        Tabs::clearLocalStorage();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Displays a single NutritionTag model.
     * @param integer $id
     *
     * @return mixed
     */


    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelTranslation = NutritionTagTranslation::find()->where(['nutrition_tag_id' => $id])->one();
        $nutritionTagModel = new NutritionTagForm();
        $nutritionTagModel->setNutritionTag($model);
        $nutritionTagModel->setNutritionTagTranslation($modelTranslation);
        return $this->render('view', ['model' => $nutritionTagModel]);
    }

    /**
     * Creates a new NutritionTag model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new NutritionTag();
        $nutritionTagModel = new NutritionTagForm();
        $nutritionTagModel->scenario = 'create';
        if (Yii::$app->request->post()) {
            if($model->save()){
                $model->attributes = $_POST['NutritionTag'];
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition Tag has been '.$nutritionTagModel->scenario.'d .'));
                if(Yii::$app->request->post()['NutritionTagTranslation']['language_id']!=''){
                    $this->nutritionTagTranslation = new NutritionTagTranslation();
                    $this->nutritionTagTranslation->attributes = $_POST['NutritionTagTranslation'];
                    $this->nutritionTagTranslation->nutrition_tag_id = $model->id;
                    if($this->nutritionTagTranslation->save()) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition Tag has been '.$nutritionTagModel->scenario.'d and translated.'));
                    }
                }
                $nutritionTagModel->setNutritionTag($model);
                $nutritionTagModel->setNutritionTagTranslation($this->nutritionTagTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        return $this->render('create', ['model' => $nutritionTagModel]);
    }



    /**
     * Updates an existing NutritionTag model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $nutritionTagModel = new NutritionTagForm();
        $nutritionTagModel->scenario = 'update';
        if (Yii::$app->request->post()) {
            $model->attributes = $_POST['NutritionTag'];
            if($model->save(true)){
                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition Tag has been '.$nutritionTagModel->scenario.'d .'));
                if(Yii::$app->request->post()['NutritionTagTranslation']['language_id']!=''){
                    $this->nutritionTagTranslation = NutritionTagTranslation::find()->where(['nutrition_tag_id' => $model->id, 'language_id' => $_POST['NutritionTagTranslation']['language_id'] ])->one();
                    if (isset($this->nutritionTagTranslation)) {
                        $this->nutritionTagTranslation->attributes = $_POST['NutritionTagTranslation'];
                        $this->nutritionTagTranslation->nutrition_tag_id = $model->id;
                    } else {
                        $this->nutritionTagTranslation = new NutritionTagTranslation();
                        $this->nutritionTagTranslation->attributes = $_POST['NutritionTagTranslation'];
                        $this->nutritionTagTranslation->nutrition_tag_id = $model->id;
                    }
                    if($this->nutritionTagTranslation->save(false)) {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition Tag has been '.$nutritionTagModel->scenario.'d and translated.'));
                    }
                }
                $nutritionTagModel->setNutritionTag($model);
                $nutritionTagModel->setNutritionTagTranslation($this->nutritionTagTranslation);
                return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
            }
        }
        $nutritionTagModel->setNutritionTag($model);
        $nutritionTagModel->setNutritionTagTranslation($this->nutritionTagTranslation);
        return $this->render('update', ['model' => $nutritionTagModel]);
    }

    /**
     * Deletes an existing NutritionTag model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Nutrition Tag has been deleted.'));

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Finds the NutritionTag model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NutritionTag the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NutritionTag::findOne($id)) !== null) {
            return $model;
        }
        throw new HttpException(404, 'The requested page does not exist.');
    }
}
