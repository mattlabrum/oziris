<?php

namespace app\modules\admin\controllers\base;

use app\models\base\CollectionItem;
use app\models\base\NutritionTag;
use app\models\base\ProductNutrition;
use app\models\base\ProductRelatedImage;
use app\models\Batch;
use app\models\Brand;
use app\models\form\ProductForm;
use app\models\Language;
use app\models\Manufacturer;
use app\models\Nutrition;
use app\models\ProductIngredient;
use app\models\ProductLocationAttribute;
use app\models\ProductNutritionTag;
use app\models\ProductPrice;
use app\models\ProductTranslation;
use app\models\ProductType;
use app\models\Product;
use app\models\ProductGroup;
use app\models\search\AuditTrailSearch;
use app\models\search\ProductSearch;
use app\models\upload\UploadFile;
use app\utils\csvToArray;
use Exception;
use Tinify\Tinify;
use yii\helpers\Url;
use yii\httpclient\Client;
use yii\web\Controller;
use Yii;
use yii\web\HttpException;
use yii\filters\AccessControl;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;
use yii\web\Response;
use yii\web\UploadedFile;


/**
 * ProductController implements the CRUD actions for Product model.
 * @property \app\models\Product $object
 */
class ProductController extends Controller
{

    public $layout = '@app/views/layouts/main';

    public $errorBrand = false;
    public $errorManufacturer = false;
    public $errorType = false;
    public $productTranslation;
    public $productLocationAttributes;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['guest']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['update-all-bmp-products'],

                        'roles' => ['bulk-bmp-update']
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'duplicate', 'import', 'upload-images', 'delete-image', 'save-data', 'generate-csv',
                            'populate-dropdown-products', 'change-product-status', 'set-beston-pure-product', 'set-ecommerce-enabled', 'fetch-product-location-attributes',
                            'resort-images', 'update-bmp-quantities', 'update-bmp-product', 'upload-nutrition-image',
                            'update-ecommerce-product', 'export-all-data', 'generate-audit-csv'],

                        'roles' => ['admin']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {

        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        try {
            $searchModel = new ProductSearch;
            $dataProvider = $searchModel->search(Yii::$app->request->get());

            Tabs::clearLocalStorage();

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,
            ]);
        } catch (\yii\base\Exception $e) {
        }

    }

    /**
     * Searches all products according to search criteria and generates CSV.
     * @return CSV Download link
     */
    public function actionExportAllData()
    {

        $searchModel = new ProductSearch;

        $searchModel->id = (isset($_POST['id']) ? $_POST['id'] : '');
        $searchModel->product_number = (isset($_POST['product_number']) ? $_POST['product_number'] : '');
        $searchModel->barcode = (isset($_POST['barcode']) ? $_POST['barcode'] : '');
        $searchModel->product_group_id = (isset($_POST['product_group_id']) ? $_POST['product_group_id'] : '');
        $searchModel->product_name = (isset($_POST['product_name']) ? $_POST['product_name'] : '');
        $searchModel->product_description = (isset($_POST['product_description']) ? $_POST['product_description'] : '');
        $searchModel->product_type_id = (isset($_POST['product_type_id']) ? $_POST['product_type_id'] : '');
        $searchModel->brand_id = (isset($_POST['brand_id']) ? $_POST['brand_id'] : '');
        $searchModel->manufacturer_id = (isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '');
        $searchModel->unit_size = (isset($_POST['unit_size']) ? $_POST['unit_size'] : '');
        $searchModel->shelf_life = (isset($_POST['shelf_life']) ? $_POST['shelf_life'] : '');
        $searchModel->nutrition_text = (isset($_POST['nutrition_text']) ? $_POST['nutrition_text'] : '');

        $searchModel->shelf_life_unit = (isset($_POST['shelf_life_unit']) ? $_POST['shelf_life_unit'] : '');
        $searchModel->product_nature_id = (isset($_POST['product_nature_id']) ? $_POST['product_nature_id'] : '');
        $searchModel->service_type_id = (isset($_POST['service_type_id']) ? $_POST['service_type_id'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);


        $toCSVArray = [];

        $strHeader = '';
        foreach ($dataProvider->getModels() as $object) {
            $arr = [];


            $arr['id'] =    $object->id   ;
            $arr['product_number'] =    $object->product_number   ;
            $arr['barcode'] =    $object->barcode   ;
            $arr['product_group_id'] =    $object->productGroup->product_group_name   ;

            $arr['product_name'] =    $object->product_name   ;

            $arr['product_description'] =    strip_tags(html_entity_decode(html_entity_decode($object->product_description, ENT_QUOTES | ENT_XML1, 'UTF-8')))   ;

            $arr['product_type_id'] =    $object->productType->type_name   ;

            $arr['brand_id'] =    $object->brand->brand_name   ;

            $arr['manufacturer_id'] =    $object->manufacturer->manufacturer_name   ;

            $arr['unit_size'] =    $object->unit_size   ;

            $arr['unit_size_kg'] =    $object->unit_size_kg   ;

            $arr['unit_per_box'] =    $object->unit_per_box   ;

            $arr['shelf_life'] =    $object->shelf_life   ;

            $arr['shelf_life_unit'] =    $object->shelfLifeUnit->name   ;

            $arr['product_nature_id'] =    $object->productNature->name   ;

            $arr['service_type_id'] =    $object->serviceType->name   ;

            $arr['nutrition_text'] =    strip_tags(html_entity_decode(html_entity_decode($object->nutrition_text, ENT_QUOTES | ENT_XML1, 'UTF-8')))   ;

            $arr['sample_availability'] =    $object->sample_availability   ;

            $arr['annual_supply'] =    $object->annual_supply   ;

            $arr['minimum_order'] =    $object->minimum_order   ;

            $arr['available_since'] =    $object->available_since   ;

            $arr['is_collection'] =    $object->is_collection   ;

            $arr['serves_per_pack'] =    $object->serves_per_pack   ;

            $arr['serve_size'] =    $object->serve_size   ;

            $arr['width'] =    $object->width   ;

            $arr['height'] =    $object->height   ;

            $arr['length'] =    $object->length   ;

            $arr['preorder_enabled'] =    $object->preorder_enabled   ;


            $logistics = '';

            if (is_array($object->logistics)) {
                $logistics = implode(',', $object->logistics);
            }

            $arr['logistics'] =    $logistics   ;

            $arr['is_beston_pure_product'] =    $object->is_beston_pure_product   ;

            $arr['is_featured'] =    $object->is_featured   ;

            $arr['is_top_seller'] =    $object->is_top_seller   ;

            $arr['is_eatable'] =    $object->is_eatable   ;

            $arr['show_batch'] =    $object->show_batch   ;

            $langs = Language::find()->where(['<>', 'id', '2'])->all();


            foreach ($langs as $lang) {

                $translation = ProductTranslation::find()->where(['product_id' => $object->id, 'language_id' => $lang->id])->one();
                if (isset($translation)) {
                    $arr['ProductName-' . $translation->language->code] =    str_replace('"', '', strip_tags(html_entity_decode($translation->product_name)))   ;
                    $arr['ProductDescription-' . $translation->language->code] =    str_replace('"', '', strip_tags(html_entity_decode($translation->product_description)))   ;
                    $arr['ProductNutritionText-' . $translation->language->code] =    str_replace('"', '', strip_tags(html_entity_decode($translation->nutrition_text)))   ;
                } else {
                    $arr['ProductName-' . $lang->code] = '';
                    $arr['ProductDescription-' . $lang->code] = '';
                    $arr['ProductNutritionText-' . $lang->code] = '';
                }
            }

            $langs = Language::find()->all();


            foreach ($langs as $lang) {

                $locationAttribute = ProductLocationAttribute::find()->where(['product_id' => $object->id, 'language_id' => $lang->id])->one();
                if (isset($locationAttribute)) {
                    $arr['BestonQuantity-' . $lang->code] =    $locationAttribute->beston_quantity   ;
                    $arr['ReOrderThreshold-' . $lang->code] =    $locationAttribute->reorder_threshold   ;
                    $arr['EcommerceEnabled-' . $lang->code] =    $locationAttribute->ecommerce_enabled   ;
                    $arr['Taxable-' . $lang->code] =    $locationAttribute->taxable   ;
                    $arr['Price-' . $lang->code] =    $locationAttribute->price   ;
                    $arr['SalePrice-' . $lang->code] =    $locationAttribute->sale_price   ;
                } else {
                    $arr['BestonQuantity-' . $lang->code] = '';
                    $arr['ReOrderThreshold-' . $lang->code] = '';
                    $arr['EcommerceEnabled-' . $lang->code] = '';
                    $arr['Taxable-' . $lang->code] = '';
                    $arr['Price-' . $lang->code] = '';
                    $arr['SalePrice-' . $lang->code] = '';
                }


            }


            $nutritions = Nutrition::find()->all();


            foreach ($nutritions as $nutrition) {

                $product_nutrition = ProductNutrition::find()->where(['product_id' => $object->id, 'nutrition_id' => $nutrition->id])->one();
                if (isset($product_nutrition)) {
                    $arr[$nutrition->name . '(per serving )'] =    $product_nutrition->avg_qty_per_serving   ;
                    $arr[$nutrition->name . '(per 100g )'] =    $product_nutrition->avg_qty_per_100g   ;
                } else {
                    $arr[$nutrition->name . '(per serving )'] = '';
                    $arr[$nutrition->name . '(per 100g )'] = '';
                }


            }

            $ingredients = ProductIngredient::find()->where(['product_id' => $object->id])->all();
            $ing = '';
            foreach ($ingredients as $ingredient) {
                $ing .= '{ ' . $ingredient->ingredient->group->name . ' : ' . $ingredient->ingredient->name . ' : ' . $ingredient->percentage . ' } ,';
            }

            $arr['Ingredients'] =    $ing   ;


            $collection_items = CollectionItem::find()->where(['collection_id' => $object->id])->all();
            $hamper_item = '';
            foreach ($collection_items as $collection_item) {
                $hamper_item .= '{ Collection Item ID : ' . $collection_item->product_id . ' ; Quantity  : ' . $collection_item->quantity . ' } ,';
            }

            $arr['Hampers'] =    $hamper_item   ;


            array_push($toCSVArray, $arr);
        }

        $strHeader .= 'ID, ';
        $strHeader .= 'ProductNumber, ';
        $strHeader .= 'BarCode, ';
        $strHeader .= 'ProductGroup, ';
        $strHeader .= 'ProductName, ';
        $strHeader .= 'ProductDescription, ';
        $strHeader .= 'ProductType, ';
        $strHeader .= 'BrandName, ';
        $strHeader .= 'ManufacturerName, ';
        $strHeader .= 'UnitSize, ';
        $strHeader .= 'UnitSizeKg, ';
        $strHeader .= 'UnitPerBox, ';
        $strHeader .= 'ShelfLife, ';
        $strHeader .= 'ShelfLifeUnit, ';
        $strHeader .= 'ProductNatureID, ';
        $strHeader .= 'ServiceTypeID, ';
        $strHeader .= 'NutritionText, ';
        $strHeader .= 'SampleAvailability, ';
        $strHeader .= 'AnnualSupply, ';
        $strHeader .= 'MinimumOrder, ';
        $strHeader .= 'AvailableSince, ';
        $strHeader .= 'IsCollection, ';
        $strHeader .= 'ServesPerPack, ';
        $strHeader .= 'ServeSize, ';
        $strHeader .= 'Width, ';
        $strHeader .= 'Height, ';
        $strHeader .= 'Length, ';
        $strHeader .= 'PreOrderEnabled, ';
        $strHeader .= 'Logistics, ';
        $strHeader .= 'IsBestonPureProduct, ';
        $strHeader .= 'IsFeatured, ';
        $strHeader .= 'IsTopSeller, ';
        $strHeader .= 'IsEdible, ';
        $strHeader .= 'ShowBatch, ';

        foreach (Language::find()->where(['<>', 'id', '2'])->all() as $lang) {
            $strHeader .= 'ProductName-' . $lang->code . ', ';
            $strHeader .= 'ProductDescription-' . $lang->code . ', ';
            $strHeader .= 'ProductNutritionText-' . $lang->code . ', ';
        }

        foreach (Language::find()->all() as $lang) {
            $strHeader .= 'BestonQuantity-' . $lang->code . ', ';
            $strHeader .= 'ReorderThreshold-' . $lang->code . ', ';
            $strHeader .= 'EcommerceEnabled-' . $lang->code . ', ';
            $strHeader .= 'Taxable-' . $lang->code . ', ';
            $strHeader .= 'Price-' . $lang->code . ', ';
            $strHeader .= 'SalePrice-' . $lang->code . ', ';
        }

        foreach (Nutrition::find()->all() as $nutrition) {

            $strHeader .= str_replace(', ', '', $nutrition->name) . '(per serving ), ';
            $strHeader .= str_replace(', ', '', $nutrition->name) . '(per 100g), ';
        }

        $strHeader .= 'Ingredients, ';
        $strHeader .= 'Hampers, ';

        $fh = fopen('csv/products-all.csv', 'w') or die('Cannot open the file');
        fwrite($fh, $strHeader);
        fwrite($fh, "\n");
        $str = '';
        for ($i = 0; $i < count($toCSVArray); $i++) {
            $str .= csvToArray::str_putcsv($toCSVArray[$i]);
            $str .= "\n";
        }

        $writeSuccessfull = fwrite($fh, $str);

        fclose($fh);

        return $writeSuccessfull;

    }

    public function actionGenerateAuditCsv()
    {

        $data = json_decode($_GET['data'],true);

//        print_r($data);

        $auditTrailSearch = new AuditTrailSearch();

        $auditTrailSearch->
        $auditTrailDataProvider = $auditTrailSearch->search($data);
        $auditTrailDataProvider->pagination = ['pageSize' => 50, 'pageParam' => 'page-auditTrails'];
        $auditTrailDataProvider->sort = ['defaultOrder' => ['id' => SORT_DESC]];

//        print_r($auditTrailDataProvider);

    }


    /**
     * Searches all products according to search criteria and generates CSV.
     * @return CSV Download link
     */
    public function actionGenerateCsv()
    {

        $searchModel = new ProductSearch;

        $searchModel->id = (isset($_POST['id']) ? $_POST['id'] : '');
        $searchModel->product_number = (isset($_POST['product_number']) ? $_POST['product_number'] : '');
        $searchModel->barcode = (isset($_POST['barcode']) ? $_POST['barcode'] : '');
        $searchModel->product_group_id = (isset($_POST['product_group_id']) ? $_POST['product_group_id'] : '');
        $searchModel->product_name = (isset($_POST['product_name']) ? $_POST['product_name'] : '');
        $searchModel->product_description = (isset($_POST['product_description']) ? $_POST['product_description'] : '');
        $searchModel->product_type_id = (isset($_POST['product_type_id']) ? $_POST['product_type_id'] : '');
        $searchModel->brand_id = (isset($_POST['brand_id']) ? $_POST['brand_id'] : '');
        $searchModel->manufacturer_id = (isset($_POST['manufacturer_id']) ? $_POST['manufacturer_id'] : '');
        $searchModel->unit_size = (isset($_POST['unit_size']) ? $_POST['unit_size'] : '');
        $searchModel->shelf_life = (isset($_POST['shelf_life']) ? $_POST['shelf_life'] : '');
        $searchModel->nutrition_text = (isset($_POST['nutrition_text']) ? $_POST['nutrition_text'] : '');

        $searchModel->shelf_life_unit = (isset($_POST['shelf_life_unit']) ? $_POST['shelf_life_unit'] : '');
        $searchModel->product_nature_id = (isset($_POST['product_nature_id']) ? $_POST['product_nature_id'] : '');
        $searchModel->service_type_id = (isset($_POST['service_type_id']) ? $_POST['service_type_id'] : '');

        $dataProvider = $searchModel->search($searchModel);
        $dataProvider->setPagination(false);


        $toCSVArray = [];

                foreach ($dataProvider->getModels() as $object) {
            $arr = [];

            $arr['id'] =  $object->id  ;
            $arr['product_number'] =  $object->product_number  ;
            $arr['barcode'] =  $object->barcode  ;
            $arr['product_group_id'] =  $object->productGroup->product_group_name  ;
            $arr['product_name'] =  $object->product_name  ;

            $arr['product_description'] =  strip_tags(html_entity_decode(html_entity_decode($object->product_description, ENT_QUOTES | ENT_XML1, 'UTF-8')))  ;
            $arr['product_type_id'] =  $object->productType->type_name  ;
            $arr['brand_id'] =  $object->brand->brand_name  ;
            $arr['manufacturer_id'] =  $object->manufacturer->manufacturer_name  ;
            $arr['unit_size'] =  $object->unit_size  ;
            $arr['unit_size_kg'] =  $object->unit_size_kg  ;
            $arr['shelf_life'] =  $object->shelf_life  ;
            $arr['nutrition_image'] =  $object->nutrition_image  ;
            $arr['product_image'] =  $object->product_image  ;

            $arr['shelf_life_unit'] =  $object->shelf_life_unit  ;
            $arr['product_nature_id'] =  $object->product_nature_id  ;
            $arr['service_type_id'] =  $object->service_type_id  ;

            $arr['nutrition_text'] =  $object->nutrition_text  ;

            $allPrices = "";
            foreach ($object->getProductPrices()->all() as $price) {
                if ($price->language_id != "") {
                    $allPrices = $allPrices . " " . $price->language->name . " : " . $price->price;
                }
            }

            $arr['product_price'] =  $allPrices  ;
            $arr['created_at'] =  date('d/m/Y H:i:s', $object->created_at)  ;
            $arr['ecommerce_enabled'] =  ($object->getEcommerceEnabled(2) == 1 ? 'Yes' : 'No')  ;

            array_push($toCSVArray, $arr);
        }


        $fh = fopen('csv/products.csv', 'w') or die('Cannot open the file');
        $strHeader = 'ID,ProductNumber,Barcode,ProductGroup,ProductName,ProductDescription,ProductType,Brand,Manufacturer,UnitSize,UnitSizeKg,ShelfLife,NutritionImage, ProductImage, ShelfLifeUnit, ProductNature, ServiceType,NutritionText,ProductPrice,CreatedAt,ECommerceEnabled';
        fwrite($fh, $strHeader);
        fwrite($fh, "\n");
        $writeSuccessfull = false;
        $str = '';
        for ($i = 0; $i < count($toCSVArray); $i++) {
            $str .= csvToArray::str_putcsv($toCSVArray[$i]);
            $str .= "\n";
        }

        $writeSuccessfull = fwrite($fh, $str);

        fclose($fh);



        return $writeSuccessfull;

    }

//    public function actionUpdateAllBmpProducts()
//    {
//        $products = ProductLocationAttribute::find()->where(['language_id' => 2 , 'ecommerce_enabled' =>1])->all();
//        $product_list = [];
//
//        $msg = '';
//        foreach($products as $product){
//            echo 'done';
//            if( $product->product->deleted_at == null ){
//                    $client = new Client();
//                    $response = $client->createRequest()
//                        ->setMethod('post')
//                        ->setUrl(getenv('SERVER_TYPE') . '/update_product_details')
//                        ->setData(
//                            [
//                                'product' => $product->product->id,
//                            ])
//                        ->send();
//                    $msg = $msg .'  Done ';
//                echo 'done';
//            }
//            Yii::$app->getSession()->setFlash('success', $msg);
//        }
//    }


    public function actionUpdateAllBmpProducts()
    {

        \Yii::$app->response->format = Response::FORMAT_JSON;
        $products = ProductLocationAttribute::find()->where(['language_id' => 2, 'ecommerce_enabled' => 1])->all();
        $product_list = [];

        $counter = 0;
        $msg = '';
        foreach ($products as $product) {
            if ($product->product->deleted_at == null) {
                $product_list[] = [
                    'product_id' => $product->product->id,
                ];
            }
        }
        return $product_list;
    }

    public function actionUpdateEcommerceProduct()
    {

        header('Access-Control-Allow-Origin: *');

        \Yii::$app->response->format = Response::FORMAT_JSON;

        $id = $_POST['id'];
        $product = Product::find()->where(['id' => $id])->one();
        if (isset($product)) {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(getenv('SERVER_TYPE') . '/update_product_details')
                ->setData(
                    [
                        'product' => $product->id,
                    ])
                ->send();
        }

        return $response->content;
    }


    public function actionUpdateBmpProduct($id)
    {
        $product = Product::find()->where(['id' => $id, 'deleted_at' => null])->one();
        if (isset($product)) {
            $client = new Client();
            $response = $client->createRequest()
                ->setMethod('post')
                ->setUrl(getenv('SERVER_TYPE') . '/update_product_details')
                ->setData(
                    [
                        'product' => $product->id,
                    ])
//                ->setOptions([
//                    'ssl_verify_host' => false,
//                ])
                ->send();

            Yii::$app->getSession()->setFlash('success', $response->content);

//            if(json_decode($response->content)->response == 'Success'){
//                Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product details updated on Beston Market Place!'));
//            }else{
//                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Something went wrong, Please try again!.'));
//            }
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Something went wrong, Please try again!.'));
        }
        return $this->actionIndex();
    }

    public function actionUpdateBmpQuantities()
    {
        $client = new Client();


        $response = $client->createRequest()
            ->setMethod('post')
            ->setUrl(getenv('SERVER_TYPE') . '/update-products-quantity')
            ->send();
        if (json_decode($response->content)->response == 'Success') {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product details updated on Beston Market Place!'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Something went wrong, Please try again!.'));
        }
        return $this->actionIndex();
    }


    public function actionPopulateDropdownProducts()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $products = Product::find()->joinWith(['brand', 'manufacturer', 'product_type'])
            ->where(['product.brand_id' => $_POST['brand_id']])
            ->orWhere(['product.manufacturer_id' => $_POST['manufacturer_id']])
            ->andWhere(['product.is_collection' => false])
            ->andWhere(['product.deleted_at' => null])
            ->andWhere(['brand.deleted_at' => null])
            ->andWhere(['manufacturer.deleted_at' => null])
            ->andWhere(['product_type.deleted_at' => null])
            ->all();

        $results = [];
        $results[] = ['id' => '', 'text' => ''];
        foreach ($products as $product) {
            $results[] = ['id' => $product->id, 'text' => $product->product_name];
        }
        return ['results' => $results];
    }


    /**
     * Displays a single Product model.
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        Tabs::rememberActiveState();

        $model = $this->findModel($id);
        $modelTranslation = ProductTranslation::find()->where(['product_id' => $id]);
        $productLocationAttribute = ProductLocationAttribute::find()->where(['product_id' => $id]);
        $productModel = new ProductForm();//$this->findModel($id);
        $productModel->setProduct($model);
        $productModel->setProductTranslation($modelTranslation);
        $productModel->setProductLocationAttributes($productLocationAttribute);

        return $this->render('view', ['model' => $productModel]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Product;
        $model->scenario = 'create';
        return $this->addAutoCompleteItems($model, $model->scenario);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDuplicate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'duplicate';


        return $this->addAutoCompleteItems($model, $model->scenario);

    }


    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';
        return $this->addAutoCompleteItems($model, $model->scenario);
    }


    private function uploadTranslatedNutritionImage($mainModel, $nutrition_image_name)
    {
        $image_upload_status = [];

        if ($nutrition_image_name != '') {

            $nutrition_image = new UploadFile();
            $nutrition_image->upload_file = UploadedFile::getInstance($mainModel, 'nutrition_image');

            $nutrition_image_status = $nutrition_image->upload('product', $nutrition_image_name);
            $nutrition_image_extension = $nutrition_image->upload_file->extension;

            $image_upload_status = [
                'nutrition_image_status' => $nutrition_image_status,
                'nutrition_image_extension' => $nutrition_image_extension,
            ];

        }
        return $image_upload_status;
    }


    private function uploadFile($mainModel, $nutrition_image_name)
    {
        $nutrition_image_status = '';
        $nutrition_image_extension = '';
        $image_upload_status = [];

        if ($nutrition_image_name != '') {

            $nutrition_image = new UploadFile();
            $nutrition_image->upload_file = UploadedFile::getInstance($mainModel, 'nutrition_image');

            $nutrition_image_status = $nutrition_image->upload('product', $nutrition_image_name);
            $nutrition_image_extension = $nutrition_image->upload_file->extension;

            $image_upload_status = [
                'nutrition_image_status' => $nutrition_image_status,
                'nutrition_image_extension' => $nutrition_image_extension,
            ];

        }

        if ($nutrition_image_name != '') {

            $image_upload_status = [
                'nutrition_image_status' => $nutrition_image_status,
                'nutrition_image_extension' => $nutrition_image_extension,
            ];

        }


        return $image_upload_status;
    }

    public function addAutoCompleteItems($model, $scenario)
    {
        $groupCreated = true;
        $productModel = new ProductForm();
        $nutrition_image = $model->nutrition_image;
        $nutrition_image_name = '';
        $nutrition_translation_image_name = '';

        if (\Yii::$app->request->isPost) {
            if ($scenario == 'duplicate') {
                $model = new Product();
            }
            $model->load(Yii::$app->request->post());
            $model->is_eatable = $_POST['Product']['is_eatable'];
            if ($model->is_collection) {
                $items = CollectionItem::find()->where(['collection_id' => $model->id])->all();

                $total_volume = 0;
                $total_weight = 0;
                foreach ($items as $item) {
                    $volume = ($item->product->length * $item->product->width * $item->product->height) * $item->quantity;
                    $total_volume = $total_volume + $volume;

                    $weight = ($item->product->unit_size_kg) * $item->quantity;
                    $total_weight = $total_weight + $weight;
                }
                $dimension = pow($total_volume, 1 / 3);

                $model->unit_size_kg = '' . $total_weight;

                $model->length = $dimension;
                $model->width = $dimension;
                $model->height = $dimension;
//                $model->logistics = json_encode($_POST['Product']['length']);
            } else {
                $model->length = $_POST['Product']['length'];
                $model->width = $_POST['Product']['width'];
                $model->height = $_POST['Product']['height'];
            }
            $model->serves_per_pack = $_POST['Product']['serves_per_pack'];
            $model->serve_size = $_POST['Product']['serve_size'];

            if ((isset($_FILES['Product']['name']['nutrition_image']) && ($_FILES['Product']['name']['nutrition_image'] != ''))) {
                $nutrition_image_name = tempnam('uploads/product', 'nutrition-');
                $nutrition_image_name = substr($nutrition_image_name, (strrpos($nutrition_image_name, '/') + 1), strlen($nutrition_image_name));
                unlink('uploads/product/' . $nutrition_image_name);
            }

            $image_upload_status = $this->uploadFile($model, $nutrition_image_name);

            if (isset($image_upload_status['nutrition_image_status'])) {
                if (!$image_upload_status['nutrition_image_status']) {
                    $model->nutrition_image = $nutrition_image;
                    if ((isset($_FILES['Product']['name']['nutrition_image']) && ($_FILES['Product']['name']['nutrition_image'] != ''))) {
                        Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Nutrition Image Upload unsuccessful!'));
                    }
                } else {
                    try {
                        unlink('uploads/product/' . $nutrition_image);
                    } catch (Exception $e) {

                    }
                    $model->nutrition_image = $nutrition_image_name . '.' . $image_upload_status['nutrition_image_extension'];
                }
            } else {
                $model->nutrition_image = $nutrition_image;
            }

///////////////////////Product Group Autocomplete Start/////////////////////////////////////////////////////
            $queryGroup = ProductGroup::find();
            $queryGroup->andFilterWhere(['id' => $model->product_group_id]);
            $queryGroup->orFilterWhere(['product_group_name' => $model->product_group_id]);

            if ($queryGroup->count() == 0) {
                $group = New ProductGroup();
                $group->product_group_name = $model->product_group_id;
                if ($groupCreated = $group->save()) {
                    $model->product_group_id = $group->id;
                }
            }
///////////////////////Product Group Autocomplete End/////////////////////////////////////////////////////

            if ($groupCreated) {
                if (($_FILES['Product']['name']['nutrition_image'] == '') && $scenario == 'create') {
                    $model->nutrition_image = 'default.jpg';
                }
                if ($scenario == 'create') {
                    $model->product_image = 'default.jpg';
                }
                if ($model->save()) {
                    $productModel->setProduct($model);
                    $productModel->setProductTranslation($this->productTranslation);
                    if ($scenario != 'update') {
                        $default_batch = new Batch();
                        $default_batch->batch_number = '' . $model->id;
                        $default_batch->product_id = $model->id;
                        $default_batch->save(false);
                        $default_batch->qr_code_image = $default_batch->generateQrCode();
                        $default_batch->save(false);
                    }

                    if (Yii::$app->request->post()['ProductTranslation']['language_id'] != '') {
                        $this->productTranslation = ProductTranslation::find()->where(['product_id' => $model->id])->andWhere(['language_id' => $_POST['ProductTranslation']['language_id']])->one();

                        if (isset($this->productTranslation)) {
                            $translated_nutrition_image = $this->productTranslation->nutrition_image;
                            $this->productTranslation->attributes = $_POST['ProductTranslation'];
                            $this->productTranslation->product_id = $model->id;
                        } else {
                            $translated_nutrition_image = '';
                            $this->productTranslation = new ProductTranslation();
                            $this->productTranslation->attributes = $_POST['ProductTranslation'];
                            $this->productTranslation->product_id = $model->id;
                        }
                        if ((isset($_FILES['ProductTranslation']['name']['nutrition_image']) && ($_FILES['ProductTranslation']['name']['nutrition_image'] != ''))) {
                            $nutrition_translation_image_name = tempnam('uploads/product', 'nutrition-');
                            $nutrition_translation_image_name = substr($nutrition_translation_image_name, (strrpos($nutrition_translation_image_name, '/') + 1), strlen($nutrition_translation_image_name));
                            print_r($nutrition_translation_image_name);
                            unlink('uploads/product/' . $nutrition_translation_image_name);
                        }
                        $nutrition_image_upload_status = $this->uploadTranslatedNutritionImage($this->productTranslation, $nutrition_translation_image_name);

                        if (isset($nutrition_image_upload_status['nutrition_image_status'])) {
                            if (!$nutrition_image_upload_status['nutrition_image_status']) {
                                $this->productTranslation->nutrition_image = $translated_nutrition_image;
                                if ((isset($_FILES['Product']['name']['nutrition_image']) && ($_FILES['Product']['name']['nutrition_image'] != ''))) {
                                    Yii::$app->getSession()->addFlash('error', Yii::t('app', 'Nutrition Image Upload unsuccessful!'));
                                }
                            } else {
                                try {
                                    unlink('uploads/product/' . $translated_nutrition_image);
                                } catch (Exception $e) {

                                }
                                $this->productTranslation->nutrition_image = $nutrition_translation_image_name . '.' . $nutrition_image_upload_status['nutrition_image_extension'];
                            }
                        } else {
                            $this->productTranslation->nutrition_image = $translated_nutrition_image;
                        }

                        if ($this->productTranslation->save()) {
                            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product  has been ' . $scenario . 'd and translated.'));
                        }
                    }

                    $this->productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $model->id])->andWhere(['language_id' => $_POST['ProductLocationAttribute']['language_id']])->one();
                    $productModel->setProductLocationAttributes($this->productLocationAttributes);

                    if (isset($productModel->productLocationAttributes)) {
                        $productModel->productLocationAttributes->attributes = $_POST['ProductLocationAttribute'];
                        if ($productModel->productLocationAttributes->language_id == '') {
                            $productModel->productLocationAttributes->language_id = 2;
                        }
                        if ($productModel->productLocationAttributes->beston_quantity == '') {
                            $productModel->productLocationAttributes->beston_quantity = 0;
                        }
                        if ($productModel->productLocationAttributes->reorder_threshold == '') {
                            $productModel->productLocationAttributes->reorder_threshold = 0;
                        }
                        if ($productModel->productLocationAttributes->sale_price == '') {
                            $productModel->productLocationAttributes->sale_price = 0;
                        }
                        $productModel->productLocationAttributes->product_id = $model->id;
                    } else {
                        $productModel->productLocationAttributes = new ProductLocationAttribute();
                        $productModel->productLocationAttributes->attributes = $_POST['ProductLocationAttributes'];
                        if ($productModel->productLocationAttributes->language_id == '') {
                            $productModel->productLocationAttributes->language_id = 2;
                        }
                        if ($productModel->productLocationAttributes->beston_quantity == '') {
                            $productModel->productLocationAttributes->beston_quantity = 0;
                        }
                        if ($productModel->productLocationAttributes->reorder_threshold == '') {
                            $productModel->productLocationAttributes->reorder_threshold = 0;
                        }
                        if ($productModel->productLocationAttributes->sale_price == '') {
                            $productModel->productLocationAttributes->sale_price = 0;
                        }

                        $productModel->productLocationAttributes->product_id = $model->id;
                    }
                    $productModel->productLocationAttributes->save(false);
                    $this->saveNutritionData($_POST, $model->id);

                    if ($_POST['Product']['product_nutrition_tags'] != '') {
                        $this->saveNutritionTags($_POST['Product']['product_nutrition_tags'], $model->id);
                    }

                    Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product has been ' . $scenario . 'd .'));
                    if ($model->is_collection && $scenario == 'create') {
                        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Collection Product has been ' . $scenario . 'd .'));
                        return $this->redirect(['update', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
                    } else {
                        return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
                    }
                }
            } else {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Something went wrong, Please try again!.'));
            }
        }

        $this->productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $model->id])->one();
        $productModel->setProductLocationAttributes($this->productLocationAttributes);
//        $model->logistics = json_encode($model->logistics);
        $productModel->setProduct($model);
        $productModel->setProductTranslation($this->productTranslation);

        return $this->render($scenario, ['model' => $productModel]);
    }

    public function saveNutritionTags($product_tags, $product_id)
    {
        $nutritionTags = NutritionTag::find()->asArray(true)->all();
        $tags = [];

        ProductNutritionTag::deleteAll(['product_id' => $product_id]);
        foreach ($nutritionTags as $nutritionTag) {
            array_push($tags, $nutritionTag['name']);
            array_push($tags, $nutritionTag['id']);
        }

        foreach ($product_tags as $product_tag) {
            if (!in_array($product_tag, $tags, true)) {
                $tag = new NutritionTag();
                $tag->name = $product_tag;
                $tag->save();

                $pnt = new ProductNutritionTag();
                $pnt->product_id = $product_id;
                $pnt->nutrition_tag_id = $tag->id;
                $pnt->save(false);

            } else {
                $pnt = new ProductNutritionTag();
                $pnt->product_id = $product_id;
                $pnt->nutrition_tag_id = $product_tag;
                $pnt->save(false);
            }
        }
    }

    public function saveNutritionData($data, $product_id)
    {

        for ($i = 0; $i < $data['no_of_nutrition']; $i++) {
            $productNutrition = ProductNutrition::find()->where(['product_id' => $product_id, 'nutrition_id' => $data['nutrition_id-' . $i]])->one();
            if (isset($productNutrition)) {
                $productNutrition->avg_qty_per_100g = $data['nutrition_per_100g-' . $i];
                $productNutrition->avg_qty_per_serving = $data['nutrition_per_serve-' . $i];
                $productNutrition->save(false);
            } else {
                $productNutrition = new ProductNutrition;
                $productNutrition->scenario = 'create';
                $productNutrition->nutrition_id = $data['nutrition_id-' . $i];
                $productNutrition->product_id = $product_id;

                $productNutrition->avg_qty_per_100g = $data['nutrition_per_100g-' . $i];
                $productNutrition->avg_qty_per_serving = $data['nutrition_per_serve-' . $i];
                $productNutrition->save();
            }
        }
    }


    public function actionSaveData()
    {


        $productsToSave = [];
        $errorString = 'Products Were not Imported! Please check the CSV for incorrect data' . '</br></br>';


        $csvArray = csvToArray::csvToArray('dataimport/' . $_POST['fileName']);
//        $csvArray = csvToArray::csvToArray('dataimport/products.csv');
        foreach ($csvArray as $product) {
            if (isset($product['ID']) && $product['ID'] > 0 && isset(Product::findOne($product['ID'])->id)) {
                $productToSave = Product::findOne($product['ID']);
                $productToSave->nutrition_image = $product['NutritionImage'];
                $productToSave->product_image = $product['ProductImage'];

                $this->populateProductData($product, $productToSave);
            } else {
                $productToSave = new Product();
                $productToSave->nutrition_image = 'default.jpg';
                $productToSave->product_image = 'default.jpg';
                $this->populateProductData($product, $productToSave);
            }

            if (!$this->errorBrand && !$this->errorType && !$this->errorManufacturer) {
                array_push($productsToSave, $productToSave);
            } else if (!$this->errorBrand || !$this->errorType || !$this->errorManufacturer) {
                $errorString .= $this->handleError($this->errorBrand, $this->errorType, $this->errorManufacturer, $product['ProductNumber']);
            }
        }

        if (count($productsToSave) == count($csvArray)) {
            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();
            try {
                foreach ($productsToSave as $productToSave) {
                    if (!$productToSave->save()) {
                        throw new Exception('Unable to save record.');
                    }

                    $default_batch = new Batch();
                    $default_batch->batch_number = '' . $productToSave->id;
                    $default_batch->product_id = $productToSave->id;
                    $default_batch->qr_code = 'default';
                    $default_batch->save();
                }
                $transaction->commit();
                $errorString = count($productsToSave) . " Products Imported Successfully!";
                return $errorString;
            } catch (Exception $e) {
                Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
                $transaction->rollback();
                return $this->redirect('index');
            }
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', $errorString));
            return $this->redirect('index');
        }


    }

    function populateProductData($product, $productToSave)
    {
        $productToSave->product_number = $product['ProductNumber'];
        $productToSave->barcode = $product['Barcode'];
        $productToSave->brand_id = (isset(Brand::findOne(['brand_name' => $product['Brand']])->id) ? Brand::findOne(['brand_name' => $product['Brand']])->id : $this->errorBrand = true);
        $productToSave->product_group_id = (isset(ProductGroup::findOne(['product_group_name' => $product['ProductGroup']])->id) ? ProductGroup::findOne(['product_group_name' => $product['ProductGroup']])->id : $this->addGroup($product['ProductGroup']));
        $productToSave->product_name = $product['ProductName'];
        $productToSave->product_description = $product['ProductDescription'];
        $productToSave->product_type_id = (isset(ProductType::findOne(['type_name' => $product['ProductType']])->id) ? ProductType::findOne(['type_name' => $product['ProductType']])->id : $this->errorType = true);
        $productToSave->unit_size = $product['UnitSize'];
        $productToSave->unit_size_kg = $product['UnitSizeKg'];
        $productToSave->shelf_life = $product['ShelfLife'];
        $productToSave->nutrition_text = $product['NutritionText'];

        $productToSave->shelf_life_unit = $product['ShelfLifeUnit'];
        $productToSave->service_type_id = $product['ServiceType'];
        $productToSave->product_nature_id = $product['ProductNature'];


        $productToSave->manufacturer_id = (isset(Manufacturer::findOne(['manufacturer_name' => $product['Manufacturer']])->id) ? Manufacturer::findOne(['manufacturer_name' => $product['Manufacturer']])->id : $this->errorManufacturer = true);
        return $productToSave;

    }

    function addGroup($productGroup)
    {
        $group = new ProductGroup();
        $group->product_group_name = $productGroup;
        $group->save(true);
        return $group->id;
    }

    function handleError($errorBrand, $errorType, $errorManufacturer, $productNumber)
    {
        $errorString = "";
        if ($errorBrand) {
            $errorString .= 'Incorrect Brand Entry for :' . $productNumber . '</br>';
        } else if ($errorType) {
            $errorString .= 'Incorrect Type Entry for :' . $productNumber . '</br>';
        } else if ($errorManufacturer) {
            $errorString .= 'Incorrect Manufacturer Entry for :' . $productNumber . '</br>';
        }

        $this->errorBrand = false;
        $this->errorManufacturer = false;
        $this->errorType = false;

        return $errorString;
    }

    /**
     * Upload image controller, 1 file at a time.
     *
     */
    public function actionUploadImages()
    {

        $product_image_name = 'product-' . substr($_FILES['Product1']['tmp_name'], 8) . '.png';
        $sourcePath = $_FILES['Product1']['tmp_name'];       // Storing source path of the file in a variable
        $targetPath = "uploads/product/" . $product_image_name; // Target path where file is to be stored
        $thumbnailPath = "uploads/product/thumbnail/" . $product_image_name;
//        move_uploaded_file($sourcePath, $targetPath);


        Tinify::setKey(getenv("TinyPNG"));
        $tinify = new Tinify();
//        $tinify->fromFile($sourcePath)->toFile($targetPath);

        $source = $tinify->fromFile($sourcePath);

        $resized = $source->resize(array(
            "method" => "scale",
            "height" => 200
        ));
        $resized->toFile($thumbnailPath);
        $source->toFile($targetPath);
        $image = new ProductRelatedImage();
        $image->image_url = $product_image_name;
        $image->product_id = $_GET['id'];
        return $image->save();

    }

    /**
     * Upload image controller, 1 file at a time.
     *
     */
    public function actionResortImages()
    {
        $product = Product::find()->where(['id' => $_GET['id']])->one();

        $relatedImageDelete = ProductRelatedImage::find()->where(['product_id' => $_GET['id']])->all();
        foreach ($relatedImageDelete as $delImage) {
            $delImage->delete();
        }

        $product->product_image = $_POST['stack'][0]['key'];
        $product->save();

        for ($j = 1; $j < count($_POST['stack']); $j++) {
            $relatedImageAdd = new ProductRelatedImage();
            $relatedImageAdd->image_url = $_POST['stack'][$j]['key'];
            $relatedImageAdd->product_id = $_GET['id'];
            $relatedImageAdd->save();
        }
    }


    /**
     * Upload image controller, 1 file at a time.
     *
     */
    public function actionDeleteImage()
    {
        $product = Product::find()->where(['id' => $_GET['id']])->one();
        if ($_POST['key'] == $product->product_image) {
            try {
                if ($_POST['key'] != 'default.jpg') {
                    unlink('uploads/product/' . $product->product_image);
                    unlink('uploads/product/thumbnail/' . $_POST['key']);
                }
            } catch (Exception $e) {
            }
            $related = ProductRelatedImage::find()->where(['product_id' => $_GET['id']])->all();
            if (count($related) > 0) {
                $product->product_image = $related[0]->image_url;
                $product->save();
                $related[0]->delete();
            } else {
                $product->product_image = 'default.jpg';
                $product->save();
            }
        } else {
            $relatedImg = ProductRelatedImage::find()->where(['image_url' => $_POST['key']])->one();
            if (isset($relatedImg)) {
                try {
                    if ($_POST['key'] != 'default.jpg') {
                        unlink('uploads/product/' . $_POST['key']);
                        unlink('uploads/product/thumbnail/' . $_POST['key']);
                    }
                } catch (Exception $e) {
                }
                $relatedImg->delete();
            }
        }
        return true;
    }


    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product has been deleted.'));

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        }
        throw new HttpException(404, 'The requested page does not exist.');
    }

//    public function actionLog($id)
//    {
//        $model = $this->findModel($id);
//        return $this->render('log', ['model' => $model]);
//    }

    /**
     * Enables/Disables an existing Product model.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */
    public function actionChangeProductStatus($id)
    {
        if ($this->findModel($id)->changeStatus()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Product Status Changed Successfully.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Product Status Change was Unsuccessful.'));
        }

        return $this->redirect('index');
    }

    /**
     * Set is_beston_pure_product.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */

    public function actionSetBestonPureProduct($id)
    {
        if ($this->findModel($id)->setBestonPureProduct()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Set as Beston Pure Product Successfully.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Set as Beston Pure Product was Un-Successfully.'));
        }

        return $this->redirect('index');
    }

    /**
     * Set is_beston_pure_product.
     * If update is successful, the browser will refresh 'Index' page to reflect changes.
     * @param integer $id
     * @return mixed
     */

    public function actionSetEcommerceEnabled($id)
    {
        if ($this->findModel($id)->ecommerceEnabled()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Ecommerce Enabled Successfully.'));
        } else {
            Yii::$app->getSession()->setFlash('error', Yii::t('app', 'Ecommerce Enabled Un-Successfully.'));
        }

        return $this->redirect('index');
    }


    /**
     * Fetch Product Price based on location.
     * @return string
     */


    public function actionFetchProductLocationAttributes()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $productLocationAttributes = ProductLocationAttribute::find()->where(['product_id' => $_POST['product_id'], 'language_id' => $_POST['location']])->one();
        if (isset($productLocationAttributes)) {
            $attributes = [
                'ecommerce_enabled' => $productLocationAttributes->ecommerce_enabled,
                'beston_qty' => $productLocationAttributes->beston_quantity,
                'reorder_threshold' => $productLocationAttributes->reorder_threshold,
                'price' => $productLocationAttributes->price,
            ];
        } else {
            $attributes = [
                'ecommerce_enabled' => '',
                'beston_qty' => '',
                'reorder_threshold' => '',
                'price' => '',
            ];
        }
        return $attributes;
    }

    public function actionUploadNutritionImage()
    {
        $filteredData = substr($_POST['img'], strpos($_POST['img'], ",") + 1);
        $unencodedData = base64_decode($filteredData);

        $nutrition_image_name = tempnam('uploads/product', 'nutrition-');
        $nutrition_image_name = substr($nutrition_image_name, (strrpos($nutrition_image_name, '/') + 1), strlen($nutrition_image_name));
        unlink('uploads/product/' . $nutrition_image_name);

        file_put_contents(Url::to('uploads/product/') . $nutrition_image_name . '.png', $unencodedData);

        $product = Product::find()->where(['id' => $_POST['product_id']])->one();

        if (isset($product)) {
            $product->nutrition_image = $nutrition_image_name . '.png';
            return $product->save(false);
        }

    }


}
