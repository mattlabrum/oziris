<?php

namespace app\modules\admin\controllers\base;

use app\models\search\AuditTrailSearch;
use bedezign\yii2\audit\models\AuditTrail;
use yii\web\Controller;
use Yii;
use yii\web\HttpException;
use yii\filters\AccessControl;
use dmstr\bootstrap\Tabs;
use cornernote\returnurl\ReturnUrl;

/**
 * AuditTrailController implements the CRUD actions for AuditTrail model.
 */
class AuditTrailController extends Controller
{

    public $layout = '@app/views/layouts/main';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'generate-csv'],
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Lists all AuditTrail models.
     * @return mixed
     */
    public function actionIndex()
    {
        Tabs::clearLocalStorage();

        return $this->render('index');
    }

    /**
     * Displays a single AuditTrail model.
     * @param integer $data
     *
     * @return mixed
     */
    public function actionView($data)
    {
        Tabs::rememberActiveState();
        $model = $this->findModel($data);

        return $this->render('view', compact('model'));
    }

    /**
     * Creates a new AuditTrail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuditTrail;
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Audit Trail has been created.'));
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());
        }

        return $this->render('create', compact('model'));
    }

    /**
     * Updates an existing AuditTrail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Audit Trail has been updated.'));
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());
        }

        return $this->render('update', compact('model'));
    }


    /**
     * Deletes an existing AuditTrail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Audit Trail has been deleted.'));

        return $this->redirect(ReturnUrl::getUrl(['index']));
    }

    /**
     * Finds the AuditTrail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AuditTrail the loaded model
     * @throws HttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuditTrail::findOne($id)) !== null) {
            return $model;
        }
        throw new HttpException(404, 'The requested page does not exist.');
    }

    public function actionGenerateCsv($data)
    {
        $arr[] = json_decode($_GET['data']);
        $search_string = [
        'user_id' => (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->user_id : '' ),
        'action'=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->action : '' ),
        'model'=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->model : '' ),
        'model_id'=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->model_id : '' ),
        ''=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->field : '' ),
        'old_value'=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->old_value : '' ),
        'new_value'=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->new_value : '' ),
        'created'=> (isset($arr[0]->search_string->AuditTrailSearch) ? $arr[0]->search_string->AuditTrailSearch->created : '' ),
        ];
        $params= [
            'search_string' => $search_string,
            'type' => $arr[0]->type,
            'filter' => (array)$arr[0]->filter,
        ];
        $params['query'] = AuditTrail::find();
            if(isset($params['type']) && $params['type']!=''){
                $params['query']->where(['model' => $params['type'] ]);
                $file_name = explode ( "\\" , $params['type']);
                $file_name = strtolower($file_name[count($file_name)-1].'.csv');
            }else{
                $file_name = 'all.csv';
            }

        $searchModel = new AuditTrailSearch;
        $dataProvider = $searchModel->search($params);
        $dataProvider->setPagination(false);
        if(count($dataProvider->getModels())>0){
            $toCSVArray = [];

            foreach ($dataProvider->getModels() as $object) {

                $dataArray = [];

                $dataArray['user_id'] = '"'.$object->user_id.'"';
                $dataArray['action'] = '"'.$object->action.'"';
                $dataArray['model'] = '"'.$object->model.'"';
                $dataArray['model_id'] = '"'.$object->model_id.'"';
                $dataArray['field'] = '"'.$object->field.'"';
                $dataArray['old_value'] = '"'.$object->old_value.'"';
                $dataArray['new_value'] = '"'.$object->new_value.'"';
                $dataArray['created'] = '"'.$object->created.'"';

                array_push($toCSVArray, $dataArray);
            }


            $fh = fopen('csv/audit/'.$file_name, 'w') or die('Cannot open the file');
            $strHeader = 'UserID,Action,Type,ID,Field,OldValue,NewValue,Created';
            fwrite($fh, $strHeader);
            fwrite($fh, "\n");
            $writeSuccessfull = false;
            for ($i = 0; $i < count($toCSVArray); $i++) {
                $str = implode(' , ', $toCSVArray[$i]);
                $writeSuccessfull = fwrite($fh, $str);
                $writeSuccessfull = fwrite($fh, "\n");
            }
            fclose($fh);
            if($writeSuccessfull){
                return $this->redirect(['../../web/csv/audit/'.$file_name]);
            }

        }else{
            return $this->redirect(['index']);
        }
    }
    public function logPostObject(){

        $req = [
            'date' => date("Y-m-d H:i:s"),
            'request' => $_REQUEST
        ];

        $req_dump =  print_r($req,true);
        $fp = fopen('request.log', 'a');
        fwrite($fp, $req_dump);
        fclose($fp);
    }
}
