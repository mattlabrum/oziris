<?php

namespace app\modules\admin\controllers;
use app\models\base\IngredientGroup;
use Yii;
use cornernote\returnurl\ReturnUrl;

/**
 * This is the class for controller "app\controllers\IngredientGroupController".
 */
class IngredientGroupController extends \app\modules\admin\controllers\base\IngredientGroupController
{

    /**
     * Creates a new IngredientGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new IngredientGroup;
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Ingredient Group has been created.'));
            return $this->goBack();
//            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());
        }

        return $this->render('create', compact('model'));
    }


}
