<?php

namespace app\modules\admin\controllers;
use app\models\search\OrderSearch;
use Yii;
use yii\web\Controller;

class ECommerceController extends Controller
{

    public $layout = '@app/views/layouts/main';

    /**
     * Displays ECommerce Management Options.
     * @return mixed
     */

    public function actionIndex()
    {
        $searchModel = new OrderSearch;
        $dataProvider = $searchModel->search(Yii::$app->request->get());



        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }


}
