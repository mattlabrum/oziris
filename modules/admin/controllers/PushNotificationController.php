<?php

namespace app\modules\admin\controllers;
use app\models\AppUserDeviceInfo;
use app\models\PushNotification;
use Yii;
use cornernote\returnurl\ReturnUrl;

/**
 * This is the class for controller "app\controllers\PushNotificationController".
 */
class PushNotificationController extends \app\modules\admin\controllers\base\PushNotificationController
{

    /**
     * Creates a new PushNotification model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PushNotification();
        $model->scenario = 'create';
        $model->load(Yii::$app->request->post());
        return $this->sendNotification($model);
    }


    function send_feedback_request() {
        $passphrase = 'Oz1r1s';

        $stream_context = stream_context_create();
        stream_context_set_option($stream_context, 'ssl', 'local_cert', 'apple_push_notification_production.pem');
        stream_context_set_option($stream_context, 'ssl', 'passphrase', $passphrase);

        $apns = stream_socket_client('ssl://feedback.push.apple.com:2196', $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $stream_context);
        if(!$apns) {
            echo "ERROR $errcode: $errstr\n";
            return;
        }


        $feedback_tokens = array();
        while(!feof($apns)) {
            $data = fread($apns, 38);
            if(strlen($data)) {
                $feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
            }
        }

        if(count($feedback_tokens)>0){
            foreach($feedback_tokens as $device){
                $device_to_delete = AppUserDeviceInfo::find()->where(['device_token' => $device['devtoken']])->one();
                if (isset($device_to_delete)) {
                    print_r($device_to_delete->delete());
                }
            }
        }

        fclose($apns);

    }


    public function sendNotification($model){

        $android_devices = AppUserDeviceInfo::find()->addSelect(['device_token'])->where(['device_type'=>'android'])->all();
        $ios_devices = AppUserDeviceInfo::find()->addSelect(['device_token'])->where(['device_type'=>'ios'])->all();

        $notification = $model->message;

        $status = false;
        if ($model->for_ios) {
            $status = $this->sendNotificationToIOSDevice($ios_devices, $notification);
        }

        if ($model->for_android) {
            $status = $this->sendNotificationToAndroidDevice($android_devices, $notification);
        }


        if ($status && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Push Notification has been created.'));
            return $this->redirect(['index', 'ru' => ReturnUrl::getRequestToken()]);
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());
        }

        return $this->render('create', compact('model'));
    }

    public function sendNotificationToAndroidDevice($devices, $notification)
    {
        $registration_ids = [];
        foreach($devices as $device)
            array_push($registration_ids,$device->device_token);

        // API access key from Google API's Console
        define('API_ACCESS_KEY', 'AIzaSyDZznaCTsjEkoWKP37KaNED7AbZQx8kQug');

        $msg = array
        (
            'message' => $notification,
            'vibrate' => 1,
            'sound' => 1,

        );
        $fields = array
        (
            'registration_ids' => $registration_ids,
            'data' => $msg
        );

        $headers = array
        (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);

        $result =  json_decode($result);
        if ($result->success > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function sendNotificationToIOSDevice($devices, $notification)
    {

        $this->send_feedback_request();
        $passphrase = 'Oz1r1s';
        $counter = 0;

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', 'apple_push_notification_production.pem');
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
//        stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');

        $message = $notification;

        foreach ($devices as $device) {
            $fp = stream_socket_client(
                'ssl://gateway.push.apple.com:2195', $err,
                $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

            if (!$fp) {
                return false;
            }

            $body['aps'] = array(
                'alert' => $message,
                'sound' => 'default'
            );

            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $device->device_token) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            if ($result) {
                $counter++;
                echo $counter;
            }
            fclose($fp);
        }


        if ($counter > 0) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * Resend push notification an existing PushNotification model.
     * If resend is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionResend($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'resend';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->getSession()->setFlash('success', Yii::t('app', 'Push Notification has been resent.'));
            return $this->redirect(['view', 'id' => $model->id, 'ru' => ReturnUrl::getRequestToken()]);
        } elseif (!\Yii::$app->request->isPost) {
            $model->load(Yii::$app->request->get());
        }

        return $this->render('resend', compact('model'));
    }


}
